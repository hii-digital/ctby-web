import React, { useState, useEffect } from "react";
import cx from "classnames";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { useRouteMatch, useHistory, Redirect } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";

// components
import Message from "./Message";
import Loading from "../modules/Loading";

// hooks
import { useConversations } from "../../hooks/conversations";

// actions
import {
  setMessages,
  setLoading,
} from "../../store/actions/conversationsActions";

// helpers
import { parseUniqueName } from "../../helpers/conversations";

import "./styles.scss";

const messagesAmountToUpload = 10;

const Conversation = () => {
  const [inputValue, setInputValue] = useState("");
  const { params } = useRouteMatch();
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    conversationsArr: conversations,
    messages,
    initLoaded,
    isLoading,
    api,
  } = useConversations();
  const [lastMsgUploaded, setLastMsgUploaded] = useState(null);

  const conversation =
    params.id !== "new"
      ? conversations.find(({ sid }) => sid === params.id)
      : null;

  useEffect(() => {
    if (messages.length) {
      const lastMsg = messages[messages.length - 1];
      if (lastMsg.conversation.sid === conversation?.sid) {
        conversation.advanceLastReadMessageIndex(lastMsg.index);
      }
    }
  }, [messages, conversation]);

  useEffect(() => {
    if (conversation) {
      dispatch(setLoading(true));
      conversation.getMessagesCount().then((amount) => {
        conversation
          .getMessages(messagesAmountToUpload)
          .then((messagePaginator) => {
            dispatch(setMessages(messagePaginator.items));
            setLastMsgUploaded(amount - messagesAmountToUpload);
            dispatch(setLoading(false));
          })
          .catch((err) => {
            console.log("getMessagesCount", err);
            dispatch(setLoading(false));
          });
      });
    }
  }, [conversation, dispatch]);

  useEffect(() => {
    return () => {
      dispatch(setMessages([]));
    };
  }, [dispatch]);

  const uploadNextMessages = () => {
    if (lastMsgUploaded > 0) {
      conversation
        .getMessages(messagesAmountToUpload, lastMsgUploaded - 1)
        .then((messagePaginator) => {
          setLastMsgUploaded((prevState) => prevState - messagesAmountToUpload);
          dispatch(setMessages([...messagePaginator.items, ...messages]));
        });
    }
  };

  const sendMessage = async (conv) => {
    try {
      conv.sendMessage(inputValue);
    } catch (err) {
      console.log("send message err", err);
    } finally {
      setInputValue("");
    }
  };

  const createConversation = async () => {
    dispatch(setLoading(true));
    try {
      const newConversation = await api.createConversation({
        uniqueName: params.uniqueName,
      });

      const convHandler = async (conv) => {
        if (conv.sid === newConversation.sid) {
          await sendMessage(newConversation);
          history.replace(`/conversations/${newConversation.sid}`);
          dispatch(setLoading(false));
          api.off("conversationJoined", convHandler);
        }
      };
      api.on("conversationJoined", convHandler);
    } catch (err) {
      console.log("createConversation err", err);
      dispatch(setLoading(false));
    }
  };

  const handleSendMessage = async (e) => {
    e.preventDefault();
    if (!conversation) {
      await createConversation();
    } else {
      sendMessage(conversation);
    }
  };

  const onInputChange = (e) => {
    setInputValue(e.target.value);
  };

  if (initLoaded !== "success") {
    return <Loading />;
  }

  if (initLoaded === "success") {
    const isParticipantOfConv = parseUniqueName(params.uniqueName).includes(
      auth.uid
    );
    if (
      conversation === undefined ||
      (conversation === null && !isParticipantOfConv)
    ) {
      return <Redirect to="/404" />;
    }
  }

  const renderMessages = () => {
    return messages.length ? (
      messages.map(({ state, conversation: conv, sid }) => (
        <Message
          key={sid}
          time={state.timestamp}
          content={state.body}
          sender={state.author}
          participants={parseUniqueName(conv.uniqueName)}
        />
      ))
    ) : (
      <h4>no messages</h4>
    );
  };

  return (
    <div className="conversation">
      <div className="message">
        <div className="message__history" id="scrollableDiv">
          <InfiniteScroll
            className="infinite-scroll"
            loader={<h4>Loading...</h4>}
            dataLength={messages.length}
            next={uploadNextMessages}
            hasMore={lastMsgUploaded > 0}
            inverse
            scrollableTarget="scrollableDiv"
          >
            {!isLoading ? renderMessages() : <h4>Loading...</h4>}
          </InfiniteScroll>
        </div>
        <div className="message__create">
          <form onSubmit={handleSendMessage}>
            <textarea
              onChange={onInputChange}
              value={inputValue}
              placeholder="Message"
            />
            <button
              className={cx("button button--secondary text--uppercase", {
                "button--inactive": isLoading || !inputValue,
              })}
              type="submit"
            >
              Send
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Conversation;
