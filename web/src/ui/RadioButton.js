import React from "react";
import MuiRadio from "@material-ui/core/Radio";
import { makeStyles } from "@material-ui/core/styles";

import colors from "./colors";


const useStyles = makeStyles(() => ({
  root: {
    "&, &.Mui-checked": {
      color: colors.ORANGE_FFA,
      width: 18,
      height: 18,
    },

    "& .MuiIconButton-label": {
      height: "100%",
    },

    "&:hover, &.Mui-checked:hover": {
      background: "none",
    }
  }
}))

const Radio = (props) => {
  const classes = useStyles();
  
  return (
    <MuiRadio disableRipple className={classes.root} { ...props } />
  )
}

export default Radio;