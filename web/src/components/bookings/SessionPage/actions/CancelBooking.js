import React from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Link, useRouteMatch } from "react-router-dom";
import { Button } from "semantic-ui-react";

// components
import Modal from "../../../modal/Modal";

// actions;
import { updateBooking } from "../../../../store/actions/bookingsActions";

// hooks
import { useBookingHook } from "../../../../hooks/bookings/booking";

// data;
import { STATUSES } from "../../../../data/bookings";

const { cancelled } = STATUSES;

const CancelBooking = () => {
  const { params } = useRouteMatch();
  const {
    auth: { uid },
  } = useSelector(({ firebase }) => firebase, shallowEqual);
  const dispatch = useDispatch();
  const { clientId } = useBookingHook(params.id);

  const cancelSession = () => {
    document.body.style.overflow = "unset"; // old code style
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    const payloads = {
      status: cancelled.value,
      refundTimestamp: tomorrow,
      cancellationInitiator: uid,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  return (
    <div className="interaction-details__buttons text--center">
      <Modal
        buttonText="Cancel Booking"
        buttonStyle="button button--primary button--full"
        content={
          <>
            <h2>Confirm Cancellation</h2>
            <p style={{ marginBottom: "10px" }}>
              If you cancel 24 hours or less after the confirmed date and
              time,&nbsp;
              {clientId === uid ? "you" : "client"} will recieve a full refund.
              For more information about our cancellation policy,&nbsp;
              <Link to="/cancellation-policy">click here</Link>.
            </p>
            <Button
              className="button--secondary button--full"
              onClick={cancelSession}
            >
              Confirm Cancel Booking
            </Button>
          </>
        }
      />
    </div>
  );
};

export default CancelBooking;
