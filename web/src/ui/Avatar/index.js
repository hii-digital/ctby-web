import React from "react";

import "./styles.scss";

const Avatar = ({ alt, src, ...rest }) => {
  return src ? (
    <img className="avatar-root" alt={alt} src={src} {...rest} />
  ) : null;
};

export default Avatar;
