const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");

const twilio = require("./twilio");
const mailgun = require("./mailgun");

const { notificationsPro } = require("./notificationsTexts");

const getUncompletedBookings = async () => {
  const now = new Date();
  const bookingsRef = admin.firestore().collection("bookings");
  const isPending = bookingsRef
    .where("status", "==", "pending")
    .where("to", "<=", now)
    .get();
  const isActive = bookingsRef
    .where("status", "==", "active")
    .where("to", "<=", now)
    .get();

  const [pendingQuerySnapshot, activeQuerySnapshot] = await Promise.all([
    isPending,
    isActive,
  ]);

  const pendingArray = pendingQuerySnapshot.docs;
  const activeArray = activeQuerySnapshot.docs;

  const bookingsArray = pendingArray.concat(activeArray);
  return bookingsArray;
};

const updateStatus = (snap) => {
  const batch = admin.firestore().batch();
  const day = 24;
  const now = new Date();
  const tomorrow = new Date(now);
  tomorrow.setDate(tomorrow.getDate() + 1);
  return snap.docs.forEach((doc) => {
    const { createdAt, from } = doc.data();
    const diffWithCreation = moment(from.toDate()).diff(
      createdAt.toDate(),
      "hours"
    );
    if (diffWithCreation >= day) {
      const docRef = admin.firestore().collection("bookings").doc(doc.id);
      batch.update(docRef, {
        status: "declined",
        refundTimestamp: tomorrow,
      });
    }
    return batch.commit().then(() => {
      console.log(
        `updateSessionsToCompleted: updated documents inside bookings`
      );
    });
  });
};

exports.updateSessionsToCompleted = functions.pubsub
  .schedule("every 1 mins")
  .onRun(() => {
    return getUncompletedBookings().then((docs) => {
      const batch = admin.firestore().batch();
      docs.forEach((doc) => {
        const docRef = admin.firestore().collection("bookings").doc(doc.id);
        batch.update(docRef, { status: "completed" });
      });
      return batch.commit().then(() => {
        console.log(
          `updateSessionsToCompleted: updated documents inside bookings`
        );
      });
    });
  });

exports.updateSessionsToDeclined = functions.pubsub
  .schedule("every 1 mins")
  .onRun(() => {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return admin
      .firestore()
      .collection("bookings")
      .where("from", "<=", tomorrow)
      .where("status", "==", "awaitingApproval")
      .get()
      .then((snap) => updateStatus(snap));
  });

exports.createBooking = async (booking, paymentIntent) => {
 // console.log("createBooking", booking, paymentIntent);
  const { id } = admin.firestore().collection("bookings").doc();
  const { from, to, proPhoneNumber, url, ...rest } = booking;

  const f = new Date(from);
  const t = new Date(to);
  const nextDayAfterSessionCompleted = new Date(t);
  nextDayAfterSessionCompleted.setDate(
    nextDayAfterSessionCompleted.getDate() + 1
  );

  try {
    await admin
      .firestore()
      .collection("bookings")
      .doc(id)
      .set({
        id,
        from: f,
        to: t,
        refundTimestamp: nextDayAfterSessionCompleted,
        createdAt: new Date(),
        transactionId: paymentIntent,
        ...rest,
      });
    const text = notificationsPro.clientRequestToBook.getFinal({
      url,
      id,
    });
    const { proId } = booking;
    try {
      mailgun.sendEmail({
        uid: proId,
        message: text,
        subject: "Client request to book session",
      });
mailgun.sendEmailToAdmin({

  message: text,
  subject: "Client request to book session",
})


    } catch (err) {
      console.log("send email error", err);
    }
    try {
      twilio.sendSMS({ to: proPhoneNumber, body: text });
    } catch (err) {
      console.log("send sms error", err);
    }
  } catch (err) {
    console.log("err creating booking", err);
  }
};
