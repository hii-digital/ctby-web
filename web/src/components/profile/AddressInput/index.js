import React, { useState } from "react";
import cx from "classnames";
import PlacesAutocomplete from "react-places-autocomplete";
import { Autocomplete } from "@material-ui/lab";
import { makeStyles } from "@material-ui/core/styles";

// components
import Input from "../../../ui/Input";
import colors from "../../../ui/colors";

const useStyles = makeStyles(() => ({
  listbox: {
    maxHeight: 200,
    padding: 0,
  },
  paper: {
    boxShadow: "0px 8px 16px rgb(0 0 0 / 5%)",
    margin: 0,
  },
  option: {
    boxSizing: "border-box",
    minHeight: 40,
    position: "relative",
    "&, &[data-focus='true'], &[aria-selected='true']": {
      backgroundColor: colors.WHITE_FFF,
      paddingRight: 33,
    },
    "&[aria-selected='true']::after": {
      display: "block",
      position: "absolute",
      content: "''",
      backgroundImage: `url("data:image/svg+xml,%3Csvg width='22' height='15' viewBox='0 0 22 15' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1 7.67568L7.54545 14L21 1' stroke='%23ED9954' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E%0A")`,
      height: 15,
      width: 33,
      right: 0,
      zIndex: 4,
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
    },
    "&:not(:last-child)": {
      borderBottom: `1px solid ${colors.GREY_E0D}`,
    },
    color: colors.BLACK_272,
    fontSize: 14,
    lineHeight: "14px",
    fontFamily: "QuestrialRegular",
    "&:hover": {
      backgroundColor: "rgba(237, 153, 84, 0.1)",
      color: colors.ORANGE_ED9,
    },
  },
}));

const AddressInput = ({ isError, state, onSelect }) => {
  const classes = useStyles();
  const [ inputValue, setInputValue ] = useState(state);

  const handleChange = (value) => {
    setInputValue(value);
  };

  const handleSelect = (value) => {
    onSelect(value);
    setInputValue(value);
  };

  const onSelectedValueChange = (e, value) => {
    if (value) {
      handleSelect(value.description);
    } else {
      handleSelect("");
    }
  };

  const inputStyles = {};
  if (isError) {
    inputStyles.border = `2px solid ${colors.RED_ED5}`;
  }
  return (
    <PlacesAutocomplete
      value={inputValue}
      searchOptions={{ componentRestrictions: { country: ["us"] } }}
      onChange={handleChange}
    >
      {({ getInputProps, suggestions }) => {
        return (
          <Autocomplete
            options={suggestions}
            getOptionLabel={({ description }) => description}
            getOptionSelected={(option, value) =>
              option.description === value.description
            }
            autoSelect
            classes={{
              listbox: classes.listbox,
              paper: classes.paper,
              option: classes.option,
              noOptions: cx(classes.paper, classes.option),
            }}
            onChange={onSelectedValueChange}
            noOptionsText="No addresses"
            renderInput={(params) => (
              <div ref={params.InputProps.ref}>
                <Input
                  styles={inputStyles}
                  {...params}
                  {...getInputProps({
                    placeholder: "Search Places...",
                  })}
                />
              </div>
            )}
          />
        );
      }}
    </PlacesAutocomplete>
  );
};

export default AddressInput;
