import React, { useState, useEffect } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { Form } from "semantic-ui-react";

// actions
import { clientToPro } from "../../store/actions/authActions";

const ClientToPro = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);

  useEffect(() => {
    const { isPro, onboardingCompleted } = profile;
    if (isPro && !onboardingCompleted) {
      history.push("/onboarding");
    }
  }, [history, profile]);

  const { authError } = useSelector((store) => store.auth, shallowEqual);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(clientToPro());
  };

  return (
    <div className="signup">
      <div className="container container--small container--top-bottom-padding">
        <h1 className="text--center text--bold">Upgrade to Pro</h1>
        <Form onSubmit={handleSubmit}>
          <div className="form__inner">
            <Form.Field>
              <p className="text--center">
                By clicking <strong>Upgrade to Pro</strong>, you agree to the&nbsp;
                <Link to="/terms-of-use">Terms of Use</Link> and&nbsp;
                <Link to="/privacy-policy">Privacy Policy</Link>.
              </p>
            </Form.Field>
            <Form.Field>
              <button
                type="submit"
                className={`button button--secondary text--uppercase text--bold text--font-secondary  `}
              >
                Upgrade to Pro
              </button>
              <div className="warning">
                {authError ? <p>{authError}</p> : null}
              </div>
            </Form.Field>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default ClientToPro;
