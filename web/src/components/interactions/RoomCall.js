import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// components
import Participant from "../twilio/Participant";
import Video from "../twilio/Video";

const Room = ({
  room,
  getOtherUserName,
  profile,
  mute,
  muteToggle,
  videoToggle,
  statusContent,
  stop,
}) => {
  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    const participantConnected = (participant) => {
      setParticipants((prevParticipants) => [...prevParticipants, participant]);
    };

    const participantDisconnected = (participant) => {
      setParticipants((prevParticipants) =>
        prevParticipants.filter((p) => p !== participant)
      );
    };

    room.on("participantConnected", participantConnected);
    room.on("participantDisconnected", participantDisconnected);
    room.participants.forEach(participantConnected);
    return () => {
      room.off("participantConnected", participantConnected);
      room.off("participantDisconnected", participantDisconnected);
    };
  }, [room]);

  const remoteParticipant = participants[0];

  if (!room) {
    return <p>loading</p>;
  }

  return (
    <div className="row" style={{ padding: "15px" }}>
      <div className="col col--6">
        <Participant
          key={room.localParticipant.sid}
          participant={room.localParticipant}
        />
        <p className="text--capitalize">{getOtherUserName()}</p>
      </div>
      <div className="col col--6">
        {remoteParticipant ? (
          <Participant
            key={remoteParticipant?.sid}
            participant={remoteParticipant}
          />
        ) : (
          <Video />
        )}

        <p className="text--capitalize">
          {`${profile.firstName} ${profile.lastName}`}
        </p>
      </div>

      <div className="col col--12 call-status">
        <div className="call-status__container">
          <div className="call-status__col">
            <button
              type="button"
              className="button button--primary"
              onClick={muteToggle}
            >
              {mute ? (
                <>
                  <FontAwesomeIcon
                    icon="volume-up"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />{" "}
                  Unmute Audio
                </>
              ) : (
                <>
                  <FontAwesomeIcon
                    icon="volume-mute"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />{" "}
                  Mute Audio
                </>
              )}
            </button>
          </div>
          <div className="call-status__col call-status__col--center">
            {/* {statusContent()} */}
          </div>
          <div className="call-status__col">
            <button
              type="button"
              className="button button--primary"
              onClick={videoToggle}
            >
              {stop ? (
                <>
                  <FontAwesomeIcon
                    icon="video"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />{" "}
                  Play Video
                </>
              ) : (
                <>
                  <FontAwesomeIcon
                    icon="video-slash"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />{" "}
                  Pause Video
                </>
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Room;
