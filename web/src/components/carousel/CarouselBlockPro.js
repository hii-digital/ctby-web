import React from "react";
import Slider from "react-slick";
import "./Carousel.scss";

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <div className="slick-slider__next-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow"
        onClick={onClick}
      />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <div className="slick-slider__prev-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow prev-arrow"
        onClick={onClick}
      />
    </div>
  );
}

export default function CarouselComponent({ items }) {
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <div className="slider-wrapper-block">
      <Slider {...settings}>
        {items.map(({ question, answear }) => (
          <div className="slider-wrapper__item-block">
            <div className="slider-wrapper-block-h1">{question}</div>

            <div className="slider-wrapper__text-block">{answear}</div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
