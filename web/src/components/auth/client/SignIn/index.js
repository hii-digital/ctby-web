import React, { useState, useEffect, useContext, useCallback } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Divider } from "@material-ui/core";

// components
import Input from "../../../../ui/Input";
import PasswordInput from "../../../../ui/PasswordInput";
import { FilledButton } from "../../../../ui/Button";
import LinkButton from "../../../../ui/LinkButton";

// context
import { SidebarContext } from "../../../../context/SidebarProvider";

// actions
import * as authActions from "../../../../store/actions/authActions";

// icons
import { ReactComponent as GoogleIcon } from "../../../../assets/icons/google.svg";
import { ReactComponent as FacebookIcon } from "../../../../assets/icons/facebook.svg";

import "../../styles.scss";

const SignIn = ({ children }) => {
  const [state, setState] = useState({
    email: "",
    password: "",
  });
  const { openSidebar, closeSidebar } = useContext(SidebarContext);
  const [passwordsVisibleIds, setPassVisibleIds] = useState([]);

  const {
    profile: { isOnboardingClientCompleted },
    auth,
  } = useSelector(({ firebase }) => firebase, shallowEqual);

  const { email, password } = state;

  const dispatch = useDispatch();
  const history = useHistory();

  const { authError: authErrorMsg, authValid } = useSelector(
    (store) => store.auth,
    shallowEqual
  );

  const redirectToOnboarding = useCallback(() => {
    history.push("/onboarding-client");
  }, [history]);

  useEffect(() => {
    if (auth.uid && !authErrorMsg && authValid) {
      closeSidebar();
      if (!isOnboardingClientCompleted) {
        redirectToOnboarding();
      }
    }
  }, [
    auth.uid,
    authErrorMsg,
    authValid,
    closeSidebar,
    isOnboardingClientCompleted,
    redirectToOnboarding,
  ]);

  useEffect(() => {
    dispatch(authActions.clearErrorMessage());
  }, [dispatch]);

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(authActions.clearErrorMessage());
    const creds = {
      email,
      password,
    };
    dispatch(authActions.signIn(creds, "client"));
  };

  const signUpOpen = () => {
    openSidebar("signUpClient");
  };

  const resetPasswordOpen = () => {
    openSidebar("resetPassword", { prevOpenedId: "signInClient" });
  };

  const togglePasswordVisible = (id) => {
    setPassVisibleIds((prevState) => {
      return prevState.includes(id)
        ? prevState.filter((el) => el !== id)
        : [...prevState, id];
    });
  };

  const onChange = (e) => {
    const { id, value } = e.target;
    setState((prevState) => ({ ...prevState, [id]: value }));
  };

  const isSignUpBtnActive = Boolean(email && password);

  const signInWithGoogle = () => {
    dispatch(authActions.signInWithGoogle());
  };

  const signInWithFacebook = () => {
    dispatch(authActions.signInWithFacebook());
  };

  return (
    <div className="auth-wrapper">
      <div className="auth-form-root">
        <h1>sign in</h1>
        <div>
          <div>{children}</div>
          <div className="social-buttons-group">
            <button type="button" onClick={signInWithGoogle}>
              <GoogleIcon />
            </button>
            <button type="button" onClick={signInWithFacebook}>
              <FacebookIcon />
            </button>
          </div>
          <div className="or-divider">
            <Divider className="divider" />
            <span>or</span>
            <Divider className="divider" />
          </div>
          <form onSubmit={onSubmit}>
            <label htmlFor="email" className="input-label">
              your email
            </label>
            <Input
              type="email"
              className="text-input"
              value={email}
              id="email"
              onChange={onChange}
              placeholder="abc@mail.com"
            />
            <label htmlFor="password" className="input-label">
              password
            </label>
            <PasswordInput
              isVisible={passwordsVisibleIds.includes("password")}
              className="text-input"
              value={password}
              id="password"
              onChange={onChange}
              toggleVisible={togglePasswordVisible}
            />
            <LinkButton
              className="password-reset-trigger"
              onClick={resetPasswordOpen}
            >
              forgot password
            </LinkButton>
            {authErrorMsg && <p className="error">{authErrorMsg}</p>}
            <FilledButton
              className="form-submit"
              fullWidth
              type="submit"
              disabled={!isSignUpBtnActive}
            >
              sign in
            </FilledButton>
            <span>
              Don&apos;t have an account yet?
              <LinkButton onClick={signUpOpen}>sign up</LinkButton>
            </span>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
