import React, { useState } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { useHistory } from "react-router-dom";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";

// components
import BookingsList from "../bookings/BookingsList";
import Toggler from "../../ui/Toggler";
import TopNotificationMessage from "../../ui/TopNotificationMessage";

// data
import { STATUSES } from "../../data/bookings";

// hooks
import { useBookingsHook } from "../../hooks/bookings";

import "./styles/bookings.scss";

const {
  awaitingApproval,
  pending,
  active,
  completed,
  cancelled,
  declined,
} = STATUSES;

const [pro, client] = ["pro", "client"];

const Bookings = () => {
  const history = useHistory();
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const [openedId, setOpenedId] = useState(pro);

  const getUrlTabIndex = (hash) => {
    const initHash = hash;
    if (initHash.startsWith("#")) {
      const removeHash = initHash.replace("#", "");
      return parseInt(removeHash);
    }
    return parseInt(initHash);
  };

  const {
    bookingsAsClientOrdered: clientBookings,
    bookingsAsProOrdered: proBookings,
  } = useBookingsHook(auth.uid);

  const { isApproved } = profile;

  const toggleActive = (value) => {
    setOpenedId(value);
  };

  const items = [
    { title: "As a Pro", action: toggleActive, value: pro },
    { title: "As a Client", action: toggleActive, value: client },
  ];

  const bookings =
    proBookings && openedId === pro ? proBookings : clientBookings;

  return (
    <div className="bookings">
      <TopNotificationMessage />
      <div className="container container--top-bottom-padding container--small">
        <div className="row">
          <div className="col">
            <div className="bookings__head">
              <h1 className="text--lg text--uppercase">Bookings</h1>
            </div>
          </div>
        </div>
        {isApproved && <Toggler items={items} active={openedId} />}
        <div className="row">
          <div className="col">
            <Tabs defaultIndex={getUrlTabIndex(history.location.hash)}>
              <TabList>
                <Tab>Awaiting Approval</Tab>
                <Tab>Upcoming</Tab>
                <Tab>Active</Tab>
                <Tab>Completed</Tab>
                <Tab>Cancelled</Tab>
                <Tab>Declined</Tab>
              </TabList>
              {[
                awaitingApproval,
                pending,
                active,
                completed,
                cancelled,
                declined,
              ].map(({ value }) => (
                <TabPanel key={value}>
                  <BookingsList status={value} bookings={bookings} />
                </TabPanel>
              ))}
            </Tabs>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Bookings;
