import React, { useState, useEffect, useRef } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Form, Input } from "semantic-ui-react";

// data
import {
  trainingTypeValues,
  DEFAULT_VALUE,
  STEP,
} from "../../data/professions";

// hooks
import { useProfessionsHook } from "../../hooks/professions";

const BookingType = (props) => {
  const {
    onlineRate,
    inpersonRate,
    options,
    handleRateChange,
    id,
    professionName,
  } = props;
  const [checkedType, setCheckedType] = useState(null);

  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const { online, inperson } = trainingTypeValues;

  const { professionsData: professionsFS } = useProfessionsHook(auth.uid);
  const onlineRateFS = professionsFS?.[id]?.onlineRate;
  const inpersonRateFS = professionsFS?.[id]?.inpersonRate;

  const firstRender = useRef(true);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      if (onlineRate && inpersonRate) {
        setCheckedType("both");
      } else if (onlineRate) {
        setCheckedType("online");
      } else if (inpersonRate) {
        setCheckedType("inperson");
      }
    }
  }, [firstRender, onlineRate, inpersonRate]);

  const toggleCheckedRate = (e) => {
    const onlineR = onlineRateFS || DEFAULT_VALUE;
    const inpersonR = inpersonRateFS || DEFAULT_VALUE;

    const ratesData = {};

    if (e.target.value === "both") {
      ratesData[online.db] = onlineR;
      ratesData[inperson.db] = inpersonR;
    }
    if (e.target.value === "inperson") {
      ratesData[online.db] = null;
      ratesData[inperson.db] = inpersonR;
    }
    if (e.target.value === "online") {
      ratesData[online.db] = onlineR;
      ratesData[inperson.db] = null;
    }

    setCheckedType(e.target.value);

    handleRateChange({
      type: "SET_RATES",
      profession: id,
      payloads: { ratesData },
    });
  };

  const onRateChange = (e, { name, value }) => {
    const ratesData = { [name]: +value };
    handleRateChange({
      type: "SET_RATES",
      profession: id,
      payloads: { ratesData },
    });
  };

  return (
    <>
      <h2>Select Booking Type(s) For {professionName}</h2>
      <Form.Field className="field" style={{ padding: "0" }}>
        {options.map(({ value, text }) => (
          <div key={value} className="ui checkbox">
            <input
              id={`${id}-${value}`}
              value={value}
              tabIndex="0"
              type="radio"
              checked={checkedType === value}
              onChange={toggleCheckedRate}
            />
            <label htmlFor={`${id}-${value}`}>{text}</label>
          </div>
        ))}
      </Form.Field>
      {(checkedType === "online" || checkedType === "both") && (
        <Form.Field className="field--half">
          <Input
            id="ratesOnlineFitnessTrainer"
            type="number"
            placeholder="e.g. 45"
            label="Your Online Rates"
            min={0}
            step={STEP}
            value={Number(onlineRate).toString()}
            name={online.db}
            onChange={onRateChange}
          />
        </Form.Field>
      )}
      {(checkedType === "inperson" || checkedType === "both") && (
        <Form.Field className="field--half">
          <Input
            id="ratesInPersonFitnessTrainer"
            type="number"
            placeholder="e.g. 75"
            label="Your In-Person Rates"
            min={0}
            step={STEP}
            value={Number(inpersonRate).toString()}
            name={inperson.db}
            onChange={onRateChange}
          />
        </Form.Field>
      )}
    </>
  );
};

export default BookingType;
