import React from "react";
import moment from "moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// icons
import { ReactComponent as ArrowIcon } from "../../assets/icons/arrow.svg";

import "./styles.scss";

const Header = (props) => {
  const { date, increaseMonth, decreaseMonth, prevMonthButtonDisabled } = props;

  return (
    <div className="calendar-header">
      {moment(date).format("MMMM YYYY")}

      <div className="icons">
        <button
          type="button"
          onClick={decreaseMonth}
          disabled={prevMonthButtonDisabled}
        >
          <ArrowIcon />
        </button>

        <button type="button" onClick={increaseMonth}>
          <ArrowIcon />
        </button>
      </div>
    </div>
  );
};

const StyledDatePicker = ({ onFocus, ...rest }) => {
  const handleFocus = (e) => {
    e.target.blur();
    onFocus();
  }
  return (
    <DatePicker
      renderCustomHeader={Header}
      calendarClassName="calendar-popup"
      popperClassName="calendar-popup-popper"
      onFocus={handleFocus}
      {...rest}
    />
  );
};

export default StyledDatePicker;
