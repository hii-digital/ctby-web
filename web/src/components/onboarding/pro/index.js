import React, { Component } from "react";
import { compose } from "redux";
import { firestoreConnect } from "react-redux-firebase";
import { connect } from "react-redux";
import Fade from "react-reveal/Fade";
import { Redirect } from "react-router-dom";

// components
import OnboardingHeader from "../Header";
import Welcome from "../Welcome";
import AboutMe from "./AboutMe";
import Specialties from "./Specialties";
import TrainingTypeRates from "./TrainingTypeRates";
import LicenseProfileImage from "./LicenseProfileImage";
import Loading from "../../modules/Loading";
import Snackbar from "../../../ui/Snackbar";
import BackgroundStrip from "../../../ui/BackgroundStrip";

// actions
import * as actions from "../../../store/actions/onboardingActions";

// styles
import "../styles.scss";

const headers = {
  1: {
    title: "license & profile image",
    subtitle: "This information is necessary for clients to view your profile",
  },
  2: {
    title: "specialties",
    subtitle: "What are your specialties?",
  },
  3: {
    title: "training type & rates",
    subtitle: "Please, choose your training type & rates",
  },
  4: {
    title: "about me",
    subtitle:
      "We’d like to know more about you! The information you provide here will be displayed in your profile",
  },
};

class Onboarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      right: true,
      left: false,
      warningMessage: "",
    };
  }

  componentDidUpdate(prevProps){
    if (prevProps.profile.proStep !== this.props.profile.proStep) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  }

  showWarning = (message) => {
    this.setState({ warningMessage: message });
  };

  closeWarning = () => {
    this.setState({ warningMessage: "" });
  };

  goNext = () => {
    this.setState({
      left: false,
      right: true,
    });
  };

  goBack = () => {
    const { profile, auth: authState, updateUser } = this.props;
    const proStep = profile.proStep || 1;
    updateUser({
      uid: authState.uid,
      payloads: { proStep: proStep - 1 },
    });
    this.setState({
      left: true,
      right: false,
    });
  };

  startOnboarding = async () => {
    const { profile, auth: authState, updateUser } = this.props;

    const proStep = profile.proStep || 0;
    updateUser({
      uid: authState.uid,
      payloads: { proStep: proStep + 1 },
    });
    this.goNext();
  };

  render() {
    const { profile, isLoading } = this.props;
    const { right, left, warningMessage } = this.state;
    if (profile.onboardingCompleted && !isLoading)
      return <Redirect to="/dashboard" />;
    let { proStep: step } = profile;

    if (!step) {
      step = 0;
    }

    if (profile.isEmpty !== true) {
      return (
        <div className="onboarding-root">
          <BackgroundStrip />
          {step > 0 && (
            <OnboardingHeader
              title={headers[step].title}
              subtitle={headers[step].subtitle}
              step={step}
              stepsAmount={Object.keys(headers).length}
            />
          )}
          <div>
            {step === 0 && (
              <Fade right={right} left={left}>
                <Welcome
                  goNext={this.startOnboarding}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 1 && (
              <Fade right={right} left={left}>
                <LicenseProfileImage
                  goNext={this.goNext}
                  goBack={this.goBack}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 2 && (
              <Fade right={right} left={left}>
                <Specialties
                  goNext={this.goNext}
                  goBack={this.goBack}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 3 && (
              <Fade right={right} left={left}>
                <TrainingTypeRates goNext={this.goNext} goBack={this.goBack} />
              </Fade>
            )}

            {step === 4 && (
              <Fade right={right} left={left}>
                <AboutMe
                  goBack={this.goBack}
                  goNext={this.goNext}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            <Snackbar message={warningMessage} onClose={this.closeWarning} />
          </div>
        </div>
      );
    }
    return <Loading />;
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    isLoading: state.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: ({ uid, payloads }) =>
      dispatch(actions.updateUser({ uid, payloads })),
    reloadAuth: (authState) =>
      dispatch({
        type: "@@reactReduxFirebase/AUTH_RELOAD_SUCCESS",
        auth: authState,
      }),
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect(({ auth: authState }) => {
    const { uid } = authState;
    const subcollections = uid ? [{ collection: "professions" }] : [];
    return [
      {
        collection: "users",
        doc: uid,
        subcollections,
        storeAs: "professions",
      },
    ];
  })
)(Onboarding);
