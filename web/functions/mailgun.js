const functions = require("firebase-functions");
const admin = require("firebase-admin");

const ADMIN_EMAIL = "lifestyle@choosetobeyou.com";

const { mailgun: mailgunSecretKeys } = functions.config();

const mailgun = require("mailgun-js")({
  apiKey: mailgunSecretKeys.apikey,
  domain: mailgunSecretKeys.domain,
});

exports.sendEmail = async ({ uid, message: text, subject }) => {
  try {
    const userRecord = await admin.auth().getUser(uid);
    const { email } = userRecord.providerData[0];
    const from = ADMIN_EMAIL;

    const data = {
      from,
      to: email,
      subject,
      text,
    };

    mailgun.messages().send(data, (error, body) => {
      console.log("body", body);
      console.log("error", error);
    });
  } catch (error) {
    console.log("Error sendEmail:", error);
  }
};

exports.sendEmailToAdmin = async ({  message: text, subject }) => {
  try {
 
    const from = ADMIN_EMAIL;

    const data = {
      from,
      to: from,
      subject,
      text,
    };

    mailgun.messages().send(data, (error, body) => {
      console.log("body", body);
      console.log("error", error);
    });
  } catch (error) {
    console.log("Error sendEmail:", error);
  }
};



const contactAdminFunc = async ({ message: text, subject, email }) => {
  try {
    const to = ADMIN_EMAIL;

    const data = {
      from: email,
      to,
      subject,
      text,
    };

    mailgun.messages().send(data, (error, body) => {
      console.log("body", body);
      console.log("error", error);
    });

  } catch (error) {
    console.log("Error sendEmail:", error);
  }
};

exports.contactAdminFunc = contactAdminFunc;

exports.contactAdmin = functions.https.onCall(
  async ({ message, subject, email }) => {
    contactAdminFunc({ message, subject, email });
  }
);
