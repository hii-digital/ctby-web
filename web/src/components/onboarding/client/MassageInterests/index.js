import React, { useState } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";

// components
import Checkbox from "../../../../ui/Checkbox";
import CheckboxGroup from "../../../../ui/CheckboxGroup";
import NavButtons from "../../NavButtons";

// actions
import { updateUser } from "../../../../store/actions/onboardingActions";

import { massageOptions, warningMessages } from "../../../../data/professions";

const MassageInterests = ({ goNext, goBack, showMessage }) => {
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { isLoading } = useSelector((store) => store, shallowEqual);

  const [selectedInterests, setSelectedInterests] = useState(
    profile.massageInterests || []
  );

  const dispatch = useDispatch();

  const handleSelect = (e) => {
    const { checked, value } = e.target;
    setSelectedInterests((prevState) => {
      return checked
        ? [...prevState, value]
        : prevState.filter((interest) => interest !== value);
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValidToMoveNext = Boolean(selectedInterests.length);
    if (!isValidToMoveNext) {
      showMessage(warningMessages.atLeastOneInterest);
      return;
    }
    dispatch(
      updateUser({
        uid: auth.uid,
        payloads: {
          massageInterests: selectedInterests,
          clientStep: profile.clientStep + 1,
        },
      })
    );
    goNext(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <CheckboxGroup>
          {massageOptions.map(({ value, text }) => (
            <Checkbox
              key={value}
              id={value}
              value={value}
              checked={selectedInterests.includes(value)}
              onChange={handleSelect}
              label={text}
            />
          ))}
        </CheckboxGroup>
      </div>
      <NavButtons goBack={goBack} isLoading={isLoading} />
    </form>
  );
};

export default MassageInterests;
