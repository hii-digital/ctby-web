import moment from "moment";
import { v4 as uuid } from "uuid";

// data
import { weekdays, FORMAT } from "../data/hours";

const addId = (items) => {
  return items.map((t) => ({ ...t, id: uuid() }));
};

const removeId = (items) => {
  return items.map(({ from, to }) => ({ from, to }));
};

const momentFormat = (val, format) => {
  return moment(val, format);
};

/* WORKING HOURS */

const unparseWeekdaysFromDB = (hours) => {
  return weekdays.reduce((acc, day) => {
    const { state = false, workingTimes = [] } = hours[day] || {};

    const newWorkingTimes = workingTimes.length
      ? workingTimes
      : [{ from: "", to: "" }];

    acc[day] = {
      state,
      edit: false,
      workingTimes: addId(newWorkingTimes),
    };
    return acc;
  }, {});
};

const parseWeekdaysToDB = (state) => {
  return weekdays.reduce((acc, day) => {
    const { workingTimes, ...rest } = state[day];
    acc[day] = {
      ...rest,
      workingTimes: removeId(workingTimes),
    };
    return acc;
  }, {});
};

/* SPECIAL HOURS */

const prepareDataForDB = (dates) => {
  return dates.map((d) => {
    if (!d.state) {
      return {
        ...d,
        workingTimes: null,
      };
    }

    const workingTimes = d.workingTimes.map(({ from, to }) => {
      const [momentFrom, momentTo] = [from, to].map((val) =>
        momentFormat(`${d.date} ${val}`, `${FORMAT} HH:mm A`).toDate()
      );
      return {
        from: momentFrom,
        to: momentTo,
      };
    });
    return {
      ...d,
      workingTimes,
    };
  });
};

const prepareDataForLocal = (dates) => {
  return dates?.map((d) => {
    const workingTimes = d.workingTimes
      ? d.workingTimes.map(({ from, to }) => {
          const [momentFrom, momentTo] = [from, to].map((val) =>
            moment(val.toDate()).format("h:mm A")
          );
          return {
            from: momentFrom,
            to: momentTo,
            id: uuid(),
          };
        })
      : [];

    return {
      ...d,
      workingTimes,
    };
  });
};

export const getTomorrow = () => {
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  return tomorrow;
};
export const getNow = () => {
  const now = new Date();
  return now;
};

export {
  parseWeekdaysToDB,
  unparseWeekdaysFromDB,
  prepareDataForLocal,
  prepareDataForDB,
};
