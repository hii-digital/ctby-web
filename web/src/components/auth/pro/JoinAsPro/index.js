import React, { useState, useEffect, useContext } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// components
import Input from "../../../../ui/Input";
import PasswordInput from "../../../../ui/PasswordInput";
import { FilledButton } from "../../../../ui/Button";
import LinkButton from "../../../../ui/LinkButton";

// actions
import * as authActions from "../../../../store/actions/authActions";

// context
import { SidebarContext } from "../../../../context/SidebarProvider";

// styles
import "../../styles.scss";
import "./styles.scss";

const JoinAsPro = () => {
  const [state, setState] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    repeatedPassword: "",
  });

  const [passwordsVisibleIds, setPassVisibleIds] = useState([]);
  const { openSidebar, closeSidebar } = useContext(SidebarContext);

  const {
    firstName,
    lastName,
    email,
    password,
    repeatedPassword,
  } = state;

  const dispatch = useDispatch();
  const history = useHistory();

  const { authError: authErrorMsg } = useSelector(
    (store) => store.auth,
    shallowEqual
  );

  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  useEffect(() => {
    if (auth.uid && !authErrorMsg && firstName) {
      closeSidebar();
      history.push("/onboarding");
    }
  }, [auth.uid, authErrorMsg, closeSidebar, dispatch, history, firstName]);

  useEffect(() => {
    dispatch(authActions.clearErrorMessage());
  }, [dispatch]);

  const togglePasswordVisible = (id) => {
    setPassVisibleIds((prevState) => {
      return prevState.includes(id)
        ? prevState.filter((el) => el !== id)
        : [...prevState, id];
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    dispatch(authActions.clearErrorMessage());

    const isBlank = (value) => {
      return !value.trim().length;
    };

    if (isBlank(firstName) || isBlank(lastName)) {
      dispatch(
        authActions.setErrorMessage(
          "All fields are required, please fill them."
        )
      );
      return;
    }

    if (password !== repeatedPassword) {
      dispatch(authActions.setErrorMessage("Passwords don't match."));
      return;
    }

    dispatch(
      authActions.signUpPro({
        email,
        password,
        firstName,
        lastName,
      })
    );
  };

  const onChange = (e) => {
    const { id, value } = e.target;
    setState((prevState) => ({ ...prevState, [id]: value }));
  };

  const signInProOpen = () => {
    openSidebar("signInPro");
  };

  const isSignUpBtnActive = Boolean(firstName && lastName && email && password);

  return (
    <div className="auth-wrapper">
      <div className="auth-form-root">
        <h1>join as pro</h1>
        <div>
          <form onSubmit={onSubmit}>
            <div>
              <div>
                <label htmlFor="first-name" className="input-label">
                  first name
                </label>
                <Input
                  id="firstName"
                  className="text-input"
                  value={firstName}
                  onChange={onChange}
                  placeholder="e.g. John"
                />
              </div>
              <div>
                <label htmlFor="last-name" className="input-label">
                  last name
                </label>
                <Input
                  id="lastName"
                  className="text-input"
                  value={lastName}
                  onChange={onChange}
                  placeholder="e.g. Doe"
                />
              </div>
            </div>
            <label htmlFor="email" className="input-label">
              email
            </label>
            <Input
              type="email"
              id="email"
              className="text-input"
              value={email}
              onChange={onChange}
              placeholder="abc@mail.com"
            />
            <label htmlFor="password" className="input-label">
              create password
            </label>
            <PasswordInput
              isVisible={passwordsVisibleIds.includes("password")}
              className="text-input"
              value={password}
              id="password"
              onChange={onChange}
              toggleVisible={togglePasswordVisible}
            />
            <label htmlFor="repeatedPassword" className="input-label">
              repeat password
            </label>
            <PasswordInput
              isVisible={passwordsVisibleIds.includes("repeatedPassword")}
              className="text-input"
              value={repeatedPassword}
              id="repeatedPassword"
              onChange={onChange}
              toggleVisible={togglePasswordVisible}
            />
            {authErrorMsg && <p className="error">{authErrorMsg}</p>}
            <FilledButton
              className="form-submit"
              fullWidth
              type="submit"
              disabled={!isSignUpBtnActive}
            >
              join
            </FilledButton>
            <span>
              Already have an account?
              <LinkButton onClick={signInProOpen}>sign in</LinkButton>
            </span>
          </form>
        </div>
      </div>
    </div>
  );
};

export default JoinAsPro;
