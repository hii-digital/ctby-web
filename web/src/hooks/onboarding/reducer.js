export const professionsReducer = (state, { type, profession, payloads }) => {
  switch (type) {
    case "SET_INITIAL":
      return {
        ...state,
        [profession]: {
          ...state[profession],
          ...payloads,
        },
      };
    case "SET_PROFESSION":
      return {
        ...state,
        [profession]: {
          ...state[profession],
          value: payloads.value,
          zipCode: "",
          licenseImage: "",
          licensePreview: "",
        },
      };
    case "SET_ZIP_CODE": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          zipCode: payloads.zipCode,
        },
      };
    }
    case "SET_ADDRESS_LINE_FIRST": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          addressLineFirst: payloads.address,
        },
      };
    }
    case "SET_ADDRESS_LINE_SECOND": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          addressLineSecond: payloads.address,
        },
      };
    }
    case "SET_LICENSE_IMAGE":
      return {
        ...state,
        [profession]: {
          ...state[profession],
          licenseImage: payloads.licenseImage,
          licensePreview: payloads.licensePreview,
        },
      };
    case "RESET":
      return {
        ...state,
        [profession]: null,
      };
    default:
      return state;
  }
};
