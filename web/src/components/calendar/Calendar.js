import React from "react";

// components
import WorkingHours from "./WorkingHours";
import SpecialHours from "./SpecialHours";

import "./styles.scss";

const Calendar = () => {
  return (
    <div className="calendar-root">
      <WorkingHours />
      <SpecialHours />
    </div>
  );
};

export default Calendar;
