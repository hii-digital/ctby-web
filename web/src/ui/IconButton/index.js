import React from "react";
import MuiIconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
}));

const IconButton = ({ children, ...props }) => {
  const classes = useStyles();
  return (
    <MuiIconButton disableRipple classes={{ root: classes.root }} {...props}>
      {children}
    </MuiIconButton>
  );
};

export default IconButton;
