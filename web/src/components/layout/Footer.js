import React from "react";
import { Link } from "react-router-dom";

// components
import ContactSupport from "./ContactSupport";
import CopyRight from "./CopyRight";

const Footer = () => (
  <footer className="footer">
    <div className="footer__top">
      <div className="container">
        <div className="footer__row">
          <div className="footer__item">
            <div className="footer__col">
              <h2 className="footer__title">Hot links</h2>
              <ul className="list--unstyled">
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/how-it-works">How It Works</Link>
                </li>
                <li>
                  <Link to="/find-a-pro">Find a Pro</Link>
                </li>
                <li>
                  <Link to="/join-as-pro">Join as a Pro</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="footer__item">
            <div className="footer__col">
              <h2 className="footer__title">More info</h2>
              <ul className="list--unstyled">
                <li>
                  <Link to="/about">About Us</Link>
                </li>
                <li>
                  <Link to="/contact">Contact Us</Link>
                </li>
              </ul>
            </div>
          </div>

          <div className="footer__item">
            <div className="footer__col">
              <h2 className="footer__title">Support</h2>
              <ul className="list--unstyled">
                <li>
                  <Link to="/faq">FAQ</Link>
                </li>
                <li>
                  <Link to="/terms-of-use">Terms of use</Link>
                </li>

                <li>
                  <Link to="/privacy-policy">Privacy Policy</Link>
                </li>
              </ul>
            </div>
          </div>

          <div className="footer__item">
            <div className="footer__col footer__col--alt">
              <h2 className="footer__title">Contact</h2>
              <p className="footer__col__support">
                We’re here to support you. Email us if you have any questions or
                concerns.
              </p>
              <ContactSupport />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="footer__bottom">
      <CopyRight />
    </div>
  </footer>
);

export default Footer;
