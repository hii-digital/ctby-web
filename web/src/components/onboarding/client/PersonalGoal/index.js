import React, { useState } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { isPossiblePhoneNumber } from "react-phone-number-input";

// components
import NavButtons from "../../NavButtons";
import TextArea from "../../../../ui/TextArea";
import PhoneNumberInput from "../../../../ui/PhoneInput";

// actions
import { updateUser } from "../../../../store/actions/onboardingActions";

// styles
import "./styles.scss";

const PersonalGoal = ({ goBack, showMessage }) => {
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { isLoading } = useSelector((store) => store, shallowEqual);

  const dispatch = useDispatch();

  const [personalGoal, setPersonalGoal] = useState(profile.personalGoal || "");
  const [phoneNumber, setPhoneNumber] = useState(profile.phoneNumber || "");

  const onChange = (e) => {
    setPersonalGoal(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const isValidToMoveNext = personalGoal.trim().length && phoneNumber.trim().length;
    if (!isValidToMoveNext) {
      showMessage("This field is required, please fill it.");
      return;
    }

    if (!isPossiblePhoneNumber(phoneNumber)) {
      showMessage("Please fill valid phone number.");
      return;
    }

    dispatch(
      updateUser({
        uid: auth.uid,
        payloads: { personalGoal, phoneNumber, isOnboardingClientCompleted: true },
      })
    );
  };

  const handleNumberChange = (v = "") => {
    setPhoneNumber(v);
  };

  return (
    <form onSubmit={handleSubmit} className="root-personal">
      <div>
        <TextArea
          id="personal-goal"
          label="Personal Goal"
          placeholder="My personal goal is..."
          value={personalGoal}
          onChange={onChange}
        />
      </div>
      <div>
        <label htmlFor="phoneNumber" className="phoneNumber">
          Phone Number
        </label>
        <PhoneNumberInput
          id="phoneNumber"
          value={phoneNumber}
          onChange={handleNumberChange}
        />
      </div>
      <NavButtons isLoading={isLoading} goBack={goBack} isLastStep />
    </form>
  );
};

export default PersonalGoal;
