import React, { useState, useEffect, useRef } from "react";
import { useRouteMatch } from "react-router-dom";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { connect as twilioConnect } from "twilio-video";

// config
import firebaseApi from "../../../../../config/fbConfig";

// components
import Modal from "../../../../modal/Modalreact";
import Room from "./Room";

// actions
import { updateBooking } from "../../../../../store/actions/bookingsActions";

// hooks
import { useBookingHook } from "../../../../../hooks/bookings/booking";

// data
import { STATUSES } from "../../../../../data/bookings";

const { active } = STATUSES;

const ModalView = ({ isStartActive = true, children }) => {
  const dispatch = useDispatch();
  const { params } = useRouteMatch();
  const [isModalOpened, setModalOpen] = useState(false);
  const [room, setRoom] = useState(null);
  const [isMute, setMute] = useState(false);
  const [isVideoHidden, setVideoHidden] = useState(false);
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { proId, status, clientId } = useBookingHook(params.id);
  const isStatusActive = status === active.value;
  const prevVideo = useRef(isVideoHidden);
  const prevAudio = useRef(isMute);
  useEffect(() => {
    return () => {
      if (room) {
        room.localParticipant.tracks.forEach((trackPub) => {
          trackPub.track.stop();
        });
        room.disconnect();
        setMute(false);
        setVideoHidden(false);
        prevVideo.current = false;
        prevAudio.current = false;
      }
    };
  }, [room]);

  useEffect(() => {
    if (prevVideo.current !== isVideoHidden) {
      const { videoTracks } = room?.localParticipant || {};
      if (videoTracks?.size) {
        videoTracks.forEach((publication) => {
          return isVideoHidden
            ? publication.track.disable()
            : publication.track.enable();
        });
        prevVideo.current = isVideoHidden;
      }
    }
  }, [isVideoHidden, room]);

  useEffect(() => {
    if (prevAudio.current !== isMute) {
      const { audioTracks } = room?.localParticipant || {};
      if (audioTracks?.size) {
        room.localParticipant.audioTracks.forEach((publication) => {
          return isMute
            ? publication.track.disable()
            : publication.track.enable();
        });
        prevVideo.current = isMute;
      }
    }
  }, [isMute, room]);

  const videoToggle = () => {
    setVideoHidden((prevState) => !prevState);
  };

  const muteToggle = () => {
    setMute((prevState) => !prevState);
  };

  const createRoom = async () => {
    const getToken = firebaseApi.functions().httpsCallable("getVideoToken");
    const { data: tokenVal } = await getToken({
      identity: auth.uid,
      room: params.id,
    });
    const payloads = {
      roomId: tokenVal,
    };

    if (!isStatusActive) {
      payloads.status = active.value;
    }

    dispatch(updateBooking({ id: params.id, payloads }));

    const roomVal = await twilioConnect(tokenVal, {
      audio: true,
      video: true,
    });

    setRoom(roomVal);
  };

  const startSession = async () => {
    document.body.style.overflow = 'hidden'; // old code style
    setModalOpen(true);
    await createRoom();
  };

  const dropSession = () => {
    setRoom((prevRoom) => {
      if (prevRoom) {
        prevRoom.localParticipant.tracks.forEach((trackPub) => {
          trackPub.track.stop();
        });
        prevRoom.disconnect();
      }
      return null;
    });
    setModalOpen(false);
  };

  return (
    <>
      <div className="interaction-call--component">
        <div className="text--center">
          <Modal
            buttonStyle="button button--secondary button--full"
            buttonText={
              isStatusActive ? "Join the session" : "Start Video Call"
            }
            isBtnActive={isStartActive}
            isOpen={isModalOpened}
            ariaHideApp={false}
            content={
              <Room
                proId={proId}
                clientId={clientId}
                room={room}
                mute={isMute}
                muteToggle={muteToggle}
                videoToggle={videoToggle}
                stop={isVideoHidden}
              />
            }
            openEvent={startSession}
            closeEvent={dropSession}
          />
        </div>
      </div>
      {children}
    </>
  );
};

export default ModalView;
