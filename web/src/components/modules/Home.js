import React, { useContext } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Link, useHistory } from "react-router-dom";

// context
import { FilterContext } from "../../context/FilterProvider";

// components
import CarouselComponent from "../carousel/CarouselComponent";
import ScrollDown from "../scrollDown/ScrollDown";
import OrangeCube from "../cubes/OrangeCube";
import Cube from "../cubes/Cube";
import ProCard from "../search/ProCard";
import Filter from "../filter/index";
import { FilledButton } from "../../ui/Button";

// pictures, video
import brandStoryVideo from "../../assets/videos/brand_story.mp4";
import checkpoint from "../../assets/images/icons/checpoint.png";
import dollar from "../../assets/images/icons/dollar.png";
import done from "../../assets/images/icons/done.png";
import papper from "../../assets/images/icons/papper.png";
import people from "../../assets/images/icons/people.png";
import shiled from "../../assets/images/icons/shiled.png";

const Home = () => {
  const history = useHistory();
  const { filteredPros } = useContext(FilterContext);
  return (
    <div className="home">
      <div className="home__hero">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="home__hero-inner">
                <div className="home__hero-content">
                  <div className="home__text  home__backgroundText">
                    Be the best version of you
                  </div>
                  <h2 className="home__text home__text-main text--uppercase text--lg">
                    Be the best version of you
                  </h2>
                  <p className="home__text home__text-subtext text--uppercase text--sm">
                    Connect with local fitness trainers &amp; massage therapists
                  </p>
                  <div>
                    <div className="home__hero-search stick">
                      <Filter />
                    </div>
                  </div>
                  <ScrollDown />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="home__works">
        <div className="home__background-wrapper">
          <div className="background-strip" />
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="container home__our-pros">
                  <div className="home__our-pros__text-wrapper">
                    <div className="home__text  home__backgroundText home__pro-text">
                      Our pros
                    </div>
                    <h2 className="home__text home__text-main text--uppercase text--lg">
                      Our Pros
                    </h2>
                  </div>
                  <div className="row pro-list-wrapper">
                    {filteredPros?.slice(0, 4).map((uid) => (
                      <Link
                        className="pro-list__card"
                        to={`/pro/${uid}`}
                        key={uid}
                      >
                        <ProCard pro={uid} />
                      </Link>
                    ))}
                  </div>
                  <div className="home__button-wrapper">
                    <FilledButton
                      onClick={(e) => {
                        e.preventDefault();
                        history.push("/find-a-pro");
                      }}
                    >
                      See all
                    </FilledButton>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row home__howItWorks">
          <div className="howItWorks__block">
            <div className="howItWorks__wrapper">
              <div>
                <div className="howItWorks__text text--center">
                  <div className="home__text  home__backgroundText">
                    How it works
                  </div>
                  <h2 className="home__text home__text-main text--uppercase text--lg">
                    How it works
                  </h2>
                  <p className="text--sm mb--0 home__howItWorks__text">
                    Browse profiles. Find your match. Then get ready to achieve
                    your health and wellness goals.
                  </p>
                </div>
                <div className="home__howItWorks-benefitWrapper">
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={checkpoint} alt="checkpoint" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Find a pro in person or online
                    </div>
                  </div>
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={dollar} alt="dollar" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Affordable prices
                    </div>
                  </div>
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={people} alt="people" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Our Customer Care Team is here to support you and your
                      goals.
                    </div>
                  </div>
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={done} alt="done" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Certified licensed pros have a badge of certification.
                    </div>
                  </div>
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={papper} alt="papper" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Resources to support your fitness journey
                    </div>
                  </div>
                  <div className="home__howItWorks-benefit">
                    <div className="home__howItWorks-icon">
                      <img src={shiled} alt="shield" />
                    </div>
                    <div className="home__howItWorks-iconText">
                      Your safety is important to us. Our pros go through an
                      application process and background check
                    </div>
                  </div>
                </div>
              </div>

              <div className="howItWorks__right-block">
                <Cube />
                <OrangeCube />
              </div>
            </div>
          </div>
        </div>

        <div className="home__about">
          <div className="home__background-wrapper">
            <div className="background-strip" />
            <div className="container">
              <div className="row">
                <div className="col col--10 text--center home__text_center">
                  <div className="home__about__text-block home__about-text-wrapper">
                    <div className="home__text  home__backgroundText">
                      Who we are
                    </div>
                    <h2 className="home__text home__text-main text--uppercase text--lg">
                      Who we are
                    </h2>
                  </div>

                  <p className="text--sm home__about__main-text">
                    We created Choose To Be You (C2Byou) to help people lead a
                    healthier lifestyle. C2Byou is a platform where you can book
                    fitness trainers &amp; massage therapists instantly online,
                    based on your convenience, and get the resources you need to
                    live a healthy &amp; happy life.
                  </p>
                  <div className="col col--12">
                    <div className="video video--responsive video--small">
                      <video
                        preload="metadata"
                        className="video__source"
                        controls
                      >
                        <source type="video/mp4" src={brandStoryVideo} />
                        Your browser does not support the video.
                      </video>
                    </div>
                  </div>
                  <div className="home__about__button">
                    <FilledButton
                      onClick={(e) => {
                        e.preventDefault();
                        history.push("/about");
                      }}
                    >
                      View More
                    </FilledButton>
                  </div>
                </div>
                <div className="col col--12 text--center" />
              </div>
            </div>
          </div>
        </div>

        {/* WILL BE USED AFTER REAL TESTIMONIALS */}
        <div className="home__testimonials">
          <div className="home__text  home__backgroundText home__pro-text">
            Testimonials
          </div>
          <h2 className="home__text home__text-main text--uppercase text--lg">
            Testimonials
          </h2>
          <div className="container">
            <div className="row">
              <div className="col" style={{ width: "100%", padding: "0" }}>
                <CarouselComponent className="home__carouse" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
