import React from "react";
import { Checkbox as MuiCheckbox, FormControlLabel } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import colors from "../assets/theme/colors";

const useStyles = makeStyles(() => ({
  checkbox: {
    padding: 0,
    color: colors.GREY_D8D,
    "&:hover, &.Mui-checked:hover": {
      background: "transparent",
    },
    "&.Mui-checked": {
      color: colors.ORANGE_FFA,
    },
  },
  formControlLabel: {
    marginLeft: 0,
    marginRight: 0,
    "& .MuiFormControlLabel-label": {
      marginLeft: "12px",
      textTransform: "capitalize",
      lineHeight: "24px",
      cursor: "pointer",
    },
  },
}));

const Checkbox = ({
  id,
  label,
  checked,
  value,
  onChange,
  className,
  ...props
}) => {
  const classes = useStyles();
  return (
    <div className={className}>
      <FormControlLabel
        className={classes.formControlLabel}
        control={
          <MuiCheckbox
            id={id}
            checked={checked}
            value={value}
            disableRipple
            disableTouchRipple
            className={classes.checkbox}
            onChange={onChange}
            {...props}
          />
        }
        label={label}
      />
    </div>
  );
};

export default Checkbox;
