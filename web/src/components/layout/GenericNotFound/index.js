import React from "react";

// icons
import { ReactComponent as LoupeIcon } from "../../../assets/icons/loupe.svg";

import "./styles.scss";

const NotFound = () => {
  return (
    <div className="not-found">
      <div className="icon-wrapper">
        <LoupeIcon />
      </div>
      <h2 className="title">Page Not Found</h2>
      <div className="subtitle">Looks like this page doesn't exist...</div>
    </div>
  );
};

export default NotFound;
