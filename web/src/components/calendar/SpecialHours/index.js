import React, { useState, useReducer, useEffect } from "react";
import cx from "classnames";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import moment from "moment";
import { v4 as uuid } from "uuid";

// components
import { FilledButton, Button } from "../../../ui/Button";
import Date from "./Date";

// actions
import { updateCalendar } from "../../../store/actions/profileActions";

// reducer
import { datesReducer } from "./reducer";

// helpers
import { prepareDataForLocal, prepareDataForDB } from "../../../helpers/time";

// icons
import { ReactComponent as CalendarIcon } from "../../../assets/icons/calendar.svg";

// data
import { FORMAT } from "../../../data/hours";

const SpecialHours = () => {
  const dispatch = useDispatch();
  const [isSaved, setSaved] = useState(true);
  const [dates, datesDispatch] = useReducer(datesReducer, []);
  const [isValid, setValid] = useState(true);

  const { specialDates = [] } = useSelector(
    ({ firebase }) => firebase.profile,
    shallowEqual
  );

  const blockedDates = dates.map(({ date }) => date);

  useEffect(() => {
    if (specialDates.length) {
      const preparedDates = prepareDataForLocal(specialDates);
      datesDispatch({
        type: "SET_INITIAL",
        payloads: { dates: preparedDates },
      });
    }
  }, [specialDates]);

  const clearValid = () => {
    if (!isValid) {
      setValid(true);
    }
  };

  const save = () => {
    clearValid();
    const empties = dates.reduce((acc, { workingTimes, state }) => {
      return acc.concat(
        workingTimes.filter(({ from, to }) => state && (!from || !to))
      );
    }, []);

    if (empties.length) {
      setValid(false);
      return;
    }

    const preparedDates = prepareDataForDB(dates);
    dispatch(updateCalendar(preparedDates));
    setSaved(true);
  };

  const edit = () => {
    setSaved(false);
  };

  const cancel = () => {
    const preparedDates = prepareDataForLocal(specialDates);
    datesDispatch({
      type: "SET_INITIAL",
      payloads: { dates: preparedDates },
    });
    setSaved(true);
  };

  const handleTimeChange = (timeId, { name, value, id }) => {
    datesDispatch({
      type: "SET_TIME",
      payloads: { timeId, date: name, value, id },
    });
  };

  const handleDateChange = (oldId, newId) => {
    clearValid();
    datesDispatch({
      type: "DATE_CHANGE",
      payloads: { id: oldId, newDate: newId },
    });
  };

  const findFirstFreeDay = (d) => {
    const isDayBlocked = dates.findIndex(
      ({ date }) => date === d.format(FORMAT)
    );
    if (isDayBlocked >= 0) {
      const nextDate = moment(d).add(1, "day");
      return findFirstFreeDay(nextDate);
    }
    return d.format(FORMAT);
  };

  const addDate = () => {
    clearValid();
    const tomorrow = moment().add(1, "day");
    const firstFreeDay = findFirstFreeDay(tomorrow);
    const date = {
      state: true,
      date: firstFreeDay,
      workingTimes: [{ from: "", to: "", id: uuid() }],
    };

    datesDispatch({
      type: "ADD_DATE",
      payloads: { date },
    });
  };

  const removeDate = (day) => {
    datesDispatch({
      type: "DELETE_DATE",
      payloads: { date: day },
    });
  };

  const deleteSpecialTime = ({ day, timeId }, isLast) => {
    clearValid();
    if (isLast) {
      removeDate(day);
    } else {
      datesDispatch({ type: "REMOVE_HOURS", payloads: { day, timeId } });
    }
  };

  const toggleAvailable = (date) => {
    clearValid();
    datesDispatch({
      type: "TOGGLE_STATE",
      payloads: { item: date },
    });
  };

  const addHours = (day) => {
    clearValid();
    datesDispatch({ type: "ADD_HOURS", payloads: { day } });
  };

  const addDateButton = (
    <button type="button" className="add" onClick={addDate}>
      Add Date
    </button>
  );

  return (
    <div>
      <div className={cx({ "saved-mode": isSaved, "edit-mode": !isSaved })}>
        <div className="title">
          <span className="icon">
            <CalendarIcon />
          </span>

          <span>special hours</span>
        </div>

        <div className="btn-group">
          {isSaved ? (
            <Button className="edit" onClick={edit}>
              Edit
            </Button>
          ) : (
            <>
              <FilledButton onClick={save}>Save</FilledButton>
              <Button onClick={cancel}>Cancel</Button>
            </>
          )}
        </div>
      </div>

      <div>
        <ul>
          {dates.map((d, i) => (
            <Date
              key={d.date}
              initialState={d}
              isSaved={isSaved}
              timeChange={handleTimeChange}
              dateChange={handleDateChange}
              removeHours={deleteSpecialTime}
              toggle={toggleAvailable}
              addHours={addHours}
              removeDate={removeDate}
              blockedDates={blockedDates}
              addDateButton={addDateButton}
              isLast={i === dates.length - 1}
              isValid={isValid}
              clearValid={clearValid}
            />
          ))}
          {!isSaved && !dates.length && addDateButton}
        </ul>
      </div>
    </div>
  );
};

export default SpecialHours;
