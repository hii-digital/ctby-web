import React, { useContext, useEffect } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Divider } from "@material-ui/core";

// component
import { FilledButton, LoadingButton } from "../../../ui/Button";
import colors from "../../../ui/colors";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// icons
import { ReactComponent as StripeLogo } from "../../../assets/icons/stripeLogo.svg";

import "./styles.scss";

const btnStyles = {
  background: colors.BLACK_2C2,
  boxShadow: "0px 8px 16px rgba(44, 46, 47, 0.3)",
  color: colors.GREY_FBF,
  marginBottom: 16,
  height: 44,
  hover: {
    background: colors.BLACK_2C2,
    boxShadow: "0px 8px 16px rgba(44, 46, 47, 0.6)",
  },
};

const PayButton = (props) => {
  console.log("pay button props: ",props)
  return (
    <FilledButton styles={btnStyles} {...props}>
     {props.totalPrice?" Debit or Credit Card":"Complete Booking"}
    </FilledButton>
  );
};

const FinishBooking = () => {
  const stripe = window.Stripe('pk_live_51IN6miCwVJ6rvBZtVrUIMAjgdJsZuBHyJtTfEkLg9cOd9UevzrK34swdAqrv6zBqTkUqoIL2STl3SznTzUk5tEyR00zzcA5K19');
  const { payloads } = useContext(SidebarContext);
  const { proId, duration, date, from, to, totalPrice, onSubmit } = payloads;
  const { firstName, lastName } = useSelector(
    ({ firestore }) => firestore.data.approvedUsers[proId],
    shallowEqual
  );
  const { paymentLoading } = useSelector(({ stripe }) => stripe, shallowEqual);
  const { sessionId } = useSelector((store) => store.stripe, shallowEqual);

  useEffect(() => {
    if (sessionId) {
      stripe.redirectToCheckout({ sessionId });
    }
  }, [sessionId, stripe]);

  return (
    <div className="finishBooking">
      <h1>
        finish
        <br />
        booking
      </h1>
      <div className="data">
        <Divider className="divider" />
        <div>
          <h5>Your Trainer</h5>
          <span>
            {firstName} {lastName}
          </span>
        </div>
        <div>
          <h5>Hours</h5>
          <span>{duration / 60}</span>
        </div>
        <div>
          <h5>Date &amp; Time</h5>
          <span>
            {date}
            <br />
            {from} - {to}
          </span>
        </div>
        <div>
          <h5>Total Price</h5>
          <span>${totalPrice}</span>
        </div>
        <Divider className="divider" />
      </div>
      <div className="payment">
        {!paymentLoading ? (
          <PayButton onClick={onSubmit}  totalPrice={totalPrice}/>
        ) : (
          <LoadingButton component={<PayButton totalPrice={totalPrice}/>} />
        )}
      </div>
      <div className="poweredBy">
        <span>Powered by </span>
        <StripeLogo />
      </div>
    </div>
  );
};

export default FinishBooking;
