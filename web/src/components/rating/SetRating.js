import React, { useState } from "react";
import StarRatingComponent from "react-star-rating-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "semantic-ui-react";

const SetRating = ({ handleLeaveReview }) => {
  const [score, setScore] = useState(1);
  const [ratingMessage, setRatingMessage] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    const payloads = {
      rating: {
        score,
        text: ratingMessage,
      },
    };
		handleLeaveReview(payloads);
  };

  const handleStarClick = (nextValue) => {
    setScore(nextValue);
  };

  const handleRatingMessageChange = (e) => {
    setRatingMessage(e.target.value);
  };

  return (
    <div className="rating__stars">
      <form onSubmit={onSubmit}>
        <StarRatingComponent
          name="rate1"
          starCount={5}
          starColor="#e5c30a"
          renderStarIcon={() => <FontAwesomeIcon icon={["fa", "star"]} />}
          value={score}
          onStarClick={handleStarClick}
        />
        <div className="rating__message">
          <textarea
            id="ratingMessage"
            onChange={handleRatingMessageChange}
            placeholder="Write about your experience..."
          />
        </div>
        <div className="rating__button">
          <Button className="button button--accent">Send Review</Button>
        </div>
      </form>
    </div>
  );
};

export default SetRating;
