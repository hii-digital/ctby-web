import React, { Component, useState, useEffect } from "react";
import { connect, useSelector, shallowEqual, useDispatch } from "react-redux";
import Fade from "react-reveal/Fade";
import { Redirect, withRouter } from "react-router-dom";
import Loading from "../modules/Loading";
import { db } from "../../config/fbConfig";
import * as moment from 'moment';
// import Popup from 'reactjs-popup';

// styles
import "./Rstyles.scss";


const SessionProgression = () => {
    // let flag = false;
    // const clickButton = () => {
    //     flag = true;
    //     console.log("🚀 ~ file: index.js ~ line 16 ~ clickButton ~ flag", flag)
    //     Fetchdata();
    // }
    const [info, setInfo] = useState([]);
    // const [info2, setInfo2] = useState([]);
    // var [Obj, setObj] = useState({});

    useEffect(() => {
        Fetchdata();
        // Fetchdata2();
    }, []);
    // window.addEventListener('load', () => {
    //     Fetchdata();
    //   });


    const {
        profile: { firstName, isPro, lastName, clientStep, initials },
        auth: { uid },
    } = useSelector(({ firebase }) => firebase, shallowEqual);
    // db.collection("bookings").get();

    const dispatch = useDispatch();
    const firstN = firstName;
    const id = uid;
    const lastN = lastName;
    const hel = clientStep;
    const init = initials;
    // const zone = timezone;
    // const sta = status;
    // const time = rate;

    const Fetchdata = () => {
        var DateCheck = Date.now();
        // console.log("🚀 ~ file: Report.js ~ line 56 ~ db.collection ~ DateCheck", new Date(moment(DateCheck).format('MM/DD/YYYY')))
        db.collection("bookings").get().then((result) => {

            result.forEach(element => {
                var Bid = element.data();
                // console.log("this is the from time :", new Date(moment(Bid.from.toDate()).format('MM/DD/YYYY')));
                // setInfo(arr => [...arr, Bid]);
                // setObj(con => ({
                //     ...con, ...Bid
                // }));
                // console.log(Bid);
                //take the name for the user
                if (new Date(moment(Bid.from.toDate()).format('MM/DD/YYYY')) >= new Date(moment(DateCheck).format('MM/DD/YYYY'))) {
                    db.collection("users").where('uid', '==', Bid.clientId).get().then((res) => {
                        res.forEach(element => {
                            var Uid = element.data();
                            Uid.CFname = Uid.firstName;
                            Uid.CLname = Uid.lastName;
                            // setObj(con => ({
                            //     ...con, ...Uid
                            // }));
                            // var Fid = {...Uid, ...Bid};
                            // setInfo(arr => [...arr, Fid]);
                            // console.log(`is for ${Uid.firstName}`);
                            db.collection("users").where('uid', '==', Bid.proId).get().then((res) => {
                                res.forEach(element => {
                                    var Pid = element.data();
                                    Pid.PFname = Pid.firstName;
                                    Pid.PLname = Pid.lastName;
                                    // setObj(con => ({
                                    //     ...con, ...Pid
                                    // }));
                                    var Fid = { ...Uid, ...Bid, ...Pid };
                                    setInfo(arr => [...arr, Fid]);
                                    // console.log(`is for ${Uid.firstName}`);
                                });
                            });
                        });
                    });
                }
                // setInfo(arr => [...arr, Obj]);
            });
        })
    }
    // console.log(Obj);

    // const Fetchdata2 = () => {
    //     //take the name for the user
    //     db.collection("users").where('uid', '==', Bid.clientId).get().then((res) => {
    //         res.forEach(element => {
    //             var Uid = element.data();
    //             setInfo2(arr => [...arr, Uid]);
    //             console.log(`is for ${Uid.firstName}`);
    //             // console.log(Bid);
    //         });
    //     });
    // }

    return (
        <div className="SessionProgression-root">
            <h2>Session Progression</h2>
            <table>
                <thead>
                    <tr>
                        {/* <th>Time Zone</th> */}
                        <th>Pro Name</th>
                        <th>Service Name</th>
                        <th>Status</th>
                        <th>Client Name</th>
                        <th>Book Time</th>
                    </tr>
                </thead>
                <tbody>
                    {/* {console.log("this is the info ", info)} */}
                    {
                        info.map((Obj) => (
                            // info2.map((Obj) => (
                            // Obj.forEach(ele => {
                            <Frame
                                // timeZ={Obj.timezone}
                                state={Obj.status}
                                type={Obj.serviceName}
                                fName={Obj.CFname}
                                lName={Obj.CLname}
                                Cfrom={moment(Obj.from.toDate()).format('MM/DD/YYYY H:mma')}
                                fNameP={Obj.PFname}
                                lNameP={Obj.PLname}
                                key={Obj.createdAt}
                            />
                            // })
                            // ))
                        ))
                    }
                </tbody>
            </table>
            {/* <Popup trigger={<button> Trigger</button>} position="right center">
                <div>Popup content here !!</div>
            </Popup> */}
        </div>
    );
}

const Frame = ({ state, type, fName, lName, Cfrom, fNameP, lNameP }) => {
    // console.log(timeZ + " " + state + " " + type);
    return (
        <tr>
            {/* <td>{timeZ}</td> */}
            <td>{fNameP} {lNameP}</td>
            <td>{type}</td>
            <td>{state}</td>
            <td>{fName} {lName}</td>
            <td>{Cfrom}</td>
        </tr>
    );
}

export default SessionProgression;