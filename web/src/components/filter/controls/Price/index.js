import React, { useContext } from "react";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../../../ui/Select";

// context
import { FilterContext } from "../../../../context/FilterProvider";

const step = 10;
const min = 50;
const max = 130;
const length = (max - min) / step + 1;


const options = Array.from({ length }, (_, index) => step * index + min);

const Price = ({ styles }) => {
  const { updateState, price } = useContext(FilterContext);

  const handleChange = ({ id, value }) => {
    updateState({ key: id, value });
  };

  return (
    <div className="priceContainer">
      <label htmlFor="price">Price Per Hour</label>
      <Select
        styles={styles}
        id="price"
        value={price || ""}
        displayEmpty
        renderValue={() => (price ? `Up to $${price}` : "All")}
        onChange={handleChange}
      >
        <MenuItem value="">All</MenuItem>
        {options.map((option) => (
          <MenuItem key={option} value={option}>
            Up to ${option}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
export default Price;
