import React, { useContext, useEffect, useRef } from "react";
import { useLocation } from "react-router-dom";
import cx from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import MuiDrawer from "@material-ui/core/SwipeableDrawer";

// context
import { SidebarContext } from "../../context/SidebarProvider";

// hooks
import { useBreakpoints } from "../../hooks/window/breakpoints";

// components
import SignIn from "../../components/auth/SignIn";
import ResetPassword from "../../components/auth/ResetPassword";
import SignUpClient from "../../components/auth/client/SignUp";
import JoinAsPro from "../../components/auth/pro/JoinAsPro";
import ShareProfile from "../../components/profile/ShareProfile";
import RequestBooking from "../../components/profile/RequestForm";
import FinishBooking from "../../components/bookings/FinishBooking";
import NavSidebar from "../../components/layout/NavSidebar";
import IconButton from "../IconButton";

// icons
import { ReactComponent as CrossIcon } from "../../assets/icons/cross.svg";

import colors from "../colors";

import "./styles.scss";

const useStyles = makeStyles((theme) => ({
  paper: {
    maxWidth: "100%",
    padding: 0,
    boxSizing: "border-box",
    display: "flex",
    justifyContent: "center",
    backgroundColor: colors.GREY_FBF,
    overflowY: "visible",
    [theme.breakpoints.up("l")]: {
      maxWidth: 470,
    },
  },
  paperAnchorBottom: {
    maxHeight: "calc(100vh - 80px)",
    overflowY: "visible",
  },
}));

const Drawer = (props) => {
  const classes = useStyles();
  const { pathname } = useLocation();
  const { openedId, closeSidebar, openSidebar, payloads, anchor } = useContext(
    SidebarContext
  );

  const { isBreakpointL } = useBreakpoints();

  const drawerRef = useRef();

  useEffect(() => {
    if (drawerRef.current) {
      drawerRef.current.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    }
  }, [openedId]);

  useEffect(() => {
    closeSidebar();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  useEffect(() => {
    if (isBreakpointL) {
      const onlyForMobile = ["mediaRightMenu", "requestBooking"];
      if (onlyForMobile.includes(openedId)) {
        closeSidebar();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isBreakpointL, openedId]);

  return (
    <MuiDrawer
      anchor={anchor}
      disableSwipeToOpen
      open={Boolean(openedId)}
      onClose={closeSidebar}
      onOpen={openSidebar}
      classes={{
        paper: classes.paper,
        paperAnchorBottom: classes.paperAnchorBottom,
      }}
      className="drawer"
      {...props}
    >
      <IconButton
        className={cx("close-icon", { invisible: !openedId })}
        onClick={closeSidebar}
      >
        <CrossIcon />
      </IconButton>
      <div className={cx(`drawer-content`, anchor)} ref={drawerRef}>
        {openedId === "signInClient" && <SignIn />}
        {openedId === "signInPro" && <SignIn />}
        {openedId === "signUpClient" && <SignUpClient />}
        {openedId === "joinAsPro" && <JoinAsPro />}
        {openedId === "resetPassword" && <ResetPassword />}
        {openedId === "shareProfile" && <ShareProfile />}
        {openedId === "finishBooking" && <FinishBooking />}
        {openedId === "requestBooking" && <RequestBooking {...payloads} />}
        {openedId === "mediaRightMenu" && <NavSidebar />}
      </div>
    </MuiDrawer>
  );
};

export default Drawer;
