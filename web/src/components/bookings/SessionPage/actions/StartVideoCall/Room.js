import React, { useState, useEffect, useRef } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// components
import Participant from "../../../../twilio/Participant";
import Video from "../../../../twilio/Video";

// hooks
import { useBookingData } from "../../../../../hooks/bookings/bookingData";
import { useBookingHook } from "../../../../../hooks/bookings/booking";

// helpers
import { getNow } from "../../../../../helpers/time";

const Room = ({
  room,
  mute,
  muteToggle,
  videoToggle,
  stop,
  proId,
  clientId,
}) => {
  const { params } = useRouteMatch();
  const { clientData, proData } = useBookingData({ proId, clientId });
  const { from, to } = useBookingHook(params.id);
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const [participants, setParticipants] = useState([]);
  const [timeLeft, setTimeLeft] = useState(null);

  const getOtherUserName = () => {
    const { firstName, lastName } = auth.uid === proId ? clientData : proData;
    return `${firstName} ${lastName}`;
  };

  const timerToEnd = useRef(null);
  const now = getNow();

  useEffect(() => {
    timerToEnd.current = setTimeout(() => {
      const left = moment(to.toDate()).diff(now, "seconds");
      setTimeLeft(+left);
    }, 1000);

    return () => {
      if (timerToEnd.current) {
        clearTimeout(timerToEnd.current);
      }
    };
  }, [now, to, from, timeLeft]);

  const getTimeLeft = () => {
    if (from.toDate() <= now) {
      return moment.utc(timeLeft * 1000).format("HH:mm:ss");
    }
    return "00:00:00";
  };

  useEffect(() => {
    if (room) {
      const participantConnected = (participant) => {
        setParticipants((prevParticipants) => [
          ...prevParticipants,
          participant,
        ]);
      };

      const participantDisconnected = (participant) => {
        setParticipants((prevParticipants) =>
          prevParticipants.filter((p) => p !== participant)
        );
      };

      room.on("participantConnected", participantConnected);
      room.on("participantDisconnected", participantDisconnected);
      room.participants.forEach(participantConnected);
      return () => {
        room.off("participantConnected", participantConnected);
        room.off("participantDisconnected", participantDisconnected);
      };
    }
  }, [room]);

  const remoteParticipant = participants[0];
  return (
    <div className="row" style={{ padding: "15px" }}>
      <div className="col col--6">
        {room ? (
          <Participant
            key={room.localParticipant.sid}
            participant={room.localParticipant}
          />
        ) : (
          <Video />
        )}
        <p className="text--capitalize">
          {`${profile.firstName} ${profile.lastName}`}
        </p>
      </div>
      <div className="col col--6">
        {remoteParticipant ? (
          <Participant
            key={remoteParticipant?.sid}
            participant={remoteParticipant}
          />
        ) : (
          <Video />
        )}

        <p className="text--capitalize">{getOtherUserName()}</p>
      </div>

      <div className="col col--12 call-status">
        <div className="call-status__container">
          <div className="call-status__col">
            <button
              type="button"
              className="button button--primary"
              onClick={muteToggle}
            >
              {mute ? (
                <>
                  <FontAwesomeIcon
                    icon="volume-up"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />
                  Unmute Audio
                </>
              ) : (
                <>
                  <FontAwesomeIcon
                    icon="volume-mute"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />
                  Mute Audio
                </>
              )}
            </button>
          </div>
          <div className="call-status__col call-status__col--center">
            {getTimeLeft()}
          </div>
          <div className="call-status__col">
            <button
              type="button"
              className="button button--primary"
              onClick={videoToggle}
            >
              {stop ? (
                <>
                  <FontAwesomeIcon
                    icon="video"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />
                  Play Video
                </>
              ) : (
                <>
                  <FontAwesomeIcon
                    icon="video-slash"
                    style={{ marginRight: "10px", marginTop: "-2px" }}
                  />
                  Pause Video
                </>
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Room;
