import React from "react";
import MuiCircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

import colors from "../assets/theme/colors";

const useStyles = makeStyles(() => ({
  root: {
    color: colors.ORANGE_ED9,
  },
}));

const CircularProgress = (props) => {
  const classes = useStyles();
  return <MuiCircularProgress {...props} classes={{ root: classes.root }} />;
};

export default CircularProgress;
