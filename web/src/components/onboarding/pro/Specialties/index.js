import React, { useEffect, useState } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Divider } from "@material-ui/core";

// components
import Checkbox from "../../../../ui/Checkbox";
import CheckboxGroup from "../../../../ui/CheckboxGroup";
import NavButtons from "../../NavButtons";

// actions
import {
  updateProfessionNode,
  updateUser,
} from "../../../../store/actions/onboardingActions";

import {
  fitnessOptions,
  massageOptions,
  FITNESS_TRAINER,
  MASSAGE_THERAPIST,
  warningMessages,
} from "../../../../data/professions";

// styles
import "./styles.scss";

const Specialties = ({ goNext, goBack, showMessage }) => {
  const {
    auth: { uid },
    profile: { proStep },
  } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { professions } = useSelector(
    ({ firestore }) => firestore.data,
    shallowEqual
  );

  const { isLoading } = useSelector((store) => store, shallowEqual);

  const dispatch = useDispatch();

  const [fitness, setFitness] = useState(null);
  const [massage, setMassage] = useState(null);

  const fitnessTrainer = professions?.[FITNESS_TRAINER];
  const massageTherapist = professions?.[MASSAGE_THERAPIST];

  const fitnessSpecialties = fitnessTrainer?.specialties;
  const massageSpecialties = massageTherapist?.specialties;

  useEffect(() => {
    if (fitnessTrainer) {
      if (fitnessSpecialties?.length) {
        setFitness(fitnessSpecialties);
      } else {
        setFitness([]);
      }
    }
    if (massageTherapist) {
      if (massageSpecialties?.length) {
        setMassage(massageSpecialties);
      } else {
        setMassage([]);
      }
    }
  }, [
    fitnessTrainer,
    massageTherapist,
    fitnessSpecialties,
    massageSpecialties,
  ]);

  const checkValidToMoveNext = () => {
    const isFitnessValid = fitness ? fitness.length >= 1 : true;
    const isMassageValid = massage ? massage.length >= 1 : true;
    return Boolean(isFitnessValid && isMassageValid);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValidToMoveNext = checkValidToMoveNext();
    if (!isValidToMoveNext) {
      showMessage(warningMessages.atLeastOneSpecialty);
      return;
    }

    const updates = {};
    if (fitness) {
      updates[FITNESS_TRAINER] = [{ value: fitness, node: "specialties" }];
    }
    if (massage) {
      updates[MASSAGE_THERAPIST] = [{ value: massage, node: "specialties" }];
    }
    dispatch(updateUser({ uid, payloads: { proStep: proStep + 1 } }));
    dispatch(updateProfessionNode({ uid, updates }));
    goNext(e);
  };

  const onFitnessSpecialtiesChange = (e) => {
    const { value, checked } = e.target;
    setFitness((prevState) => {
      return checked
        ? [...prevState, value]
        : prevState.filter((specialty) => specialty !== value);
    });
  };

  const onMassageSpecialtiesChange = (e) => {
    const { value, checked } = e.target;
    setMassage((prevState) => {
      return checked
        ? [...prevState, value]
        : prevState.filter((specialty) => specialty !== value);
    });
  };

  return (
    <form onSubmit={handleSubmit} className="specialties-root">
      <div>
        {fitnessTrainer && Boolean(fitness) && (
          <>
            <div className="profession-header">
              <span>As a Fitness Trainer</span>
              <Divider />
            </div>
            <CheckboxGroup>
              {fitnessOptions.map(({ value, text }) => (
                <Checkbox
                  id={value}
                  key={value}
                  value={value}
                  checked={fitness.includes(value)}
                  onChange={onFitnessSpecialtiesChange}
                  label={text}
                />
              ))}
            </CheckboxGroup>
          </>
        )}
        {massage && Boolean(massage) && (
          <>
            <div className="profession-header">
              <span>As a Massage Therapist</span>
              <Divider />
            </div>
            <CheckboxGroup>
              { professions.massageTherapist.state !== 'FL' ? (
                <>
                  <p style={{marginBottom: '20px'}}>Massage therapy is only available for Florida. Please visit the previous page and remove from profession.</p>
                </>
              ) : (
                <>
                  {massageOptions.map(({ value, text }) => (
                    <Checkbox
                      key={value}
                      id={value}
                      value={value}
                      checked={massage.includes(value)}
                      onChange={onMassageSpecialtiesChange}
                      label={text}
                    />
                  ))}
                </>
              )}
            </CheckboxGroup>
          </>
        )}
      </div>
      
      <NavButtons goBack={goBack} isLoading={isLoading} hideNext={professions && professions.massageTherapist && professions.massageTherapist.state !== 'FL' ? true : false} />
    </form>
  );
};

export default Specialties;
