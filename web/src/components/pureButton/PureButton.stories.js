import React from "react";
import { withKnobs } from "@storybook/addon-knobs";

import PureButton from "./PureButton";

export const pureButton = () => {
  return (
    <div style={{ padding: "20px" }}>
      <PureButton title="Title" />
    </div>
  );
};

export default {
  title: "PureButton",
  component: PureButton,
  decorators: [withKnobs],
};
