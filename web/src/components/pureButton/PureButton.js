import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import "./pureButton.scss";

export default function Buttons(props) {
  console.log(props);
  return (
    <div className="pureButtonWrapper">
      <button
        {...(props.disabled && { disabled: true })}
        className="pureButton"
      >
        {props.title}
      </button>
    </div>
  );
}
