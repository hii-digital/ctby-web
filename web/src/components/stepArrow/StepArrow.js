import React from 'react';
import "./stepArrow.scss";

export default function StepArrow () {
    return (
      <div className='stepArrow'>
        <div className="scrollDown-wrapper">
          <div className="scrollDown">
          <section id="section07">

            <a href="#section08">
              <div className="scrollDown__arrowBlock">
                <span></span>
                <span></span>
              </div>
            </a>
          </section>
          </div>
        </div>
      </div>
    )
}