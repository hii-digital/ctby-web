import React, { useState } from "react";
import Slider from "react-slick";

// components
import LinkButton from "../../ui/LinkButton";

// images

import userOne from "../../assets/images/icons/cosmin-serban.png";
import userSecond from "../../assets/images/icons/cosmin-serban1.png";
import userThird from "../../assets/images/icons/cosmin-serban2.png";
import userFourth from "../../assets/images/icons/cosmin-serban3.png";
import { ReactComponent as StarIcn } from "../../assets/images/icons/star_icn.svg";

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <div className="slick-slider__next-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow"
        onClick={onClick}
      />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <div className="slick-slider__prev-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow prev-arrow"
        onClick={onClick}
      />
    </div>
  );
}

const CarouselComponent = () => {
  const settings = {
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3,
          nextArrow: <SampleNextArrow />,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          nextArrow: <SampleNextArrow />,
        },
      },
      {
        breakpoint: 512,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow: <SampleNextArrow />,
        },
      },
    ],
  };

  const SmartText = ({ text, length = 20 }) => {
    const [showLess, setShowLess] = useState(true);

    if (text.length < length) {
      return <p>{text}</p>;
    }

    return (
      <div>
        <p
          dangerouslySetInnerHTML={{
            __html: showLess ? `${text.slice(0, length)}...` : text,
          }}
        />
        <LinkButton
          onClick={() => setShowLess(!showLess)}
          onKeyDown={() => setShowLess(!showLess)}
        >
          Read {showLess ? "More" : "Less"}
        </LinkButton>
      </div>
    );
  };

  const htmlText =
    "Because of C2B I got 10 kilograms of. Thank you guys for developing such great platform. I'll advice it to every";

  return (
    <div className="slider-wrapper">
      <Slider {...settings}>
        <div className="slider-wrapper__item-block">
          <div className="slider-wrapper__icon-wrapper">
            <div className="slider-wrapper__icon">
              <img src={userSecond} alt="userSecond" />
            </div>
            <div className="slider-wrapper__name">
              Pole <br />
              Anderson
            </div>
          </div>
          <div className="slider-wrapper__ranting">
            <div className="slider-wrapper__ranting-score">
              <StarIcn />
              <div>4/5</div>
            </div>
            <div className="slider-wrapper__ranting-text">Feb 2020</div>
          </div>
          <div className="slider-wrapper__text">
            <SmartText text={htmlText} length={100} />
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="slider-wrapper__icon-wrapper">
            <div className="slider-wrapper__icon">
              <img src={userThird} alt="userThird" />
            </div>
            <div className="slider-wrapper__name">
              Cavin <br />
              Radkovsky
            </div>
          </div>
          <div className="slider-wrapper__ranting">
            <div className="slider-wrapper__ranting-score">
              <StarIcn />
              <div>4/5</div>
            </div>
            <div className="slider-wrapper__ranting-text">Feb 2020</div>
          </div>
          <div className="slider-wrapper__text">
            <SmartText text={htmlText} length={100} />
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="slider-wrapper__icon-wrapper">
            <div className="slider-wrapper__icon">
              <img src={userFourth} alt="userFourth" />
            </div>
            <div className="slider-wrapper__name">
              Liana <br />
              Vislova
            </div>
          </div>
          <div className="slider-wrapper__ranting">
            <div className="slider-wrapper__ranting-score">
              <StarIcn />
              <div>4/5</div>
            </div>
            <div className="slider-wrapper__ranting-text">Feb 2020</div>
          </div>
          <div className="slider-wrapper__text">
            <SmartText text={htmlText} length={100} />
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="slider-wrapper__icon-wrapper">
            <div className="slider-wrapper__icon">
              <img src={userOne} alt="userOne" />
            </div>
            <div className="slider-wrapper__name">
              James <br />
              Wilsons
            </div>
          </div>
          <div className="slider-wrapper__ranting">
            <div className="slider-wrapper__ranting-score">
              <StarIcn />
              <div>4/5</div>
            </div>
            <div className="slider-wrapper__ranting-text">Feb 2020</div>
          </div>
          <div className="slider-wrapper__text">
            <SmartText text={htmlText} length={100} />
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="slider-wrapper__icon-wrapper">
            <div className="slider-wrapper__icon">
              <img src={userOne} alt="userOne" />
            </div>
            <div className="slider-wrapper__name">
              James <br />
              Wilson
            </div>
          </div>
          <div className="slider-wrapper__ranting">
            <div className="slider-wrapper__ranting-score">
              <StarIcn />
              <div>4/5</div>
            </div>
            <div className="slider-wrapper__ranting-text">Feb 2020</div>
          </div>
          <div className="slider-wrapper__text">
            Because of C2B I got 10 kilograms of. Thank you guys for developing
            such great platform. I`&apos;`ll advice it to every
          </div>
        </div>
      </Slider>
    </div>
  );
};

export default CarouselComponent;
