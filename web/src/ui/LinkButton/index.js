import React from "react";
import { Link } from "react-router-dom";
import cx from "classnames";

import "./styles.scss";

const LinkButton = ({ children, className, to, ...rest }) => {
  return (
    <>
      {to ? (
        <Link className={cx("linkbtn", className)} {...rest}>
          {children}
        </Link>
      ) : (
        <button type="button" className={cx("linkbtn", className)} {...rest}>
          {children}
        </button>
      )}
    </>
  );
};

export default LinkButton;
