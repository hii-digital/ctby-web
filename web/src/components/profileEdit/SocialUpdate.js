import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Form, Input } from "semantic-ui-react";
import ellipses from "../../assets/images/ellipsis.gif";
import { updateProfile } from "../../store/actions/profileActions";

class SocialUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitting: false,
    };
  }

  onChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const $this = this;
    $this.setState({
      submitting: true,
    });
    setTimeout(() => {
      $this.setState({
        submitting: false,
      });
      $this.props.updateProfile($this.state);
    }, 3000);
  };

  render() {
    const { profile } = this.props;
    const { submitting } = this.state;

    return (
      <div className="profile-edit__profile">
        <div style={{ marginBottom: "20px" }}>
          <h2 style={{ marginBottom: 0 }}>Update Social Accounts</h2>
          <small>Pro Premium Feature</small>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div
            className={submitting ? `form__loading active` : `form__loading`}
          >
            <div style={{ textAlign: "center" }}>
              <img src={ellipses} />
              <p>Updating</p>
            </div>
          </div>
          <Form.Field className="field--half">
            <Input
              disabled={!profile.isProPremium}
              id="socialFacebook"
              type="url"
              label="Facebook"
              placeholder="Enter your Facebook profile url"
              defaultValue={profile.isProPremium ? profile.socialFacebook : ""}
              onChange={this.onChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              disabled={!profile.isProPremium}
              id="socialTwitter"
              type="url"
              placeholder="Enter your Twitter profile url"
              label="Twitter"
              defaultValue={profile.isProPremium ? profile.socialTwitter : ""}
              onChange={this.onChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              disabled={!profile.isProPremium}
              id="socialInstagram"
              type="url"
              placeholder="Enter your Instagram profile url"
              label="Instagram"
              defaultValue={profile.isProPremium ? profile.socialInstagram : ""}
              onChange={this.onChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              disabled={!profile.isProPremium}
              id="socialPinterest"
              type="url"
              placeholder="Enter your Pinterest profile url"
              label="Pinterest"
              defaultValue={profile.isProPremium ? profile.socialPinterest : ""}
              onChange={this.onChange}
            />
          </Form.Field>

          <Form.Field>
            <Button
              disabled={!profile.isProPremium}
              className={`button  text--uppercase text--font-secondary text--sm ${
                !profile.isProPremium ? "inactive" : "button--secondary"
              }`}
            >
              Update Social Accounts
            </Button>
          </Form.Field>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    isInitializing: state.firebase.isInitializing,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateProfile: (profileDetails) => dispatch(updateProfile(profileDetails)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SocialUpdate);
