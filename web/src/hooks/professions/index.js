import { useSelector, shallowEqual } from "react-redux";

export const useProfessionsHook = (proId) => {
  const orderedProfessions = useSelector(
    ({ firestore }) => firestore.ordered?.[`${proId}/professions`],
    shallowEqual
  );

  const professions = useSelector(
    ({ firestore }) => firestore.data?.[`${proId}/professions`],
    shallowEqual
  );

  return {
    professionsOrderedData: orderedProfessions,
    professionsData: professions,
  };
};
