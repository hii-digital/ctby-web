import React, { useState, useEffect, useCallback, useRef } from "react";
import { useSelector, shallowEqual } from "react-redux";

// components
import Radio from "../../../../ui/RadioButton";
import Slider from "../../../../ui/Slider";

// helpers
import {
  DEFAULT_VALUE,
  MIN_RATE,
  MAX_RATE,
  STEP,
} from "../../../../data/professions";

const Form = ({
  profession,
  options,
  onlineRate,
  inpersonRate,
  onRateChange,
  initialChecked,
}) => {
  const { professions } = useSelector(
    ({ firestore }) => firestore.data,
    shallowEqual
  );

  const [checkedType, setCheckedType] = useState(initialChecked);

  const handleRateChange = useCallback(
    (type, value) => {
      onRateChange({
        profession,
        type,
        value,
      });
    },
    [onRateChange, profession]
  );

  const { onlineRate: onlineFS, inpersonRate: inpersonFS } = professions[
    profession
  ];

  const firstRender = useRef(true);
  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      if (onlineFS && inpersonFS) {
        handleRateChange("online", onlineFS);
        handleRateChange("inperson", inpersonFS);
        setCheckedType("both");
      } else if (onlineFS) {
        handleRateChange("online", onlineFS);
        handleRateChange("inperson", null);
        setCheckedType("online");
      } else if (inpersonFS) {
        handleRateChange("inperson", inpersonFS);
        handleRateChange("online", null);
        setCheckedType("inperson");
      }
    }
  }, [handleRateChange, inpersonFS, onlineFS, firstRender]);

  const toggleCheckedRate = (e) => {
    const online = onlineRate || onlineFS || DEFAULT_VALUE;
    const inperson = inpersonRate || inpersonFS || DEFAULT_VALUE;

    if (e.target.name === "online") {
      handleRateChange("online", online);
      handleRateChange("inperson", null);
    }
    if (e.target.name === "inperson") {
      handleRateChange("inperson", inperson);
      handleRateChange("online", null);
    }
    if (e.target.name === "both") {
      handleRateChange("online", online);
      handleRateChange("inperson", inperson);
    }
    setCheckedType(e.target.value);
  };

  return (
    <div className="professions">
      <div>
        <span className="title">Your Training Type</span>
        <div className="options">
          {options.map(({ value, text }) => (
            <div key={value}>
              
              { profession === 'fitnessTrainer' && professions.fitnessTrainer.state !== 'FL'  ? (
                value === 'online' ? 
                  <>
                    <Radio
                      id={`${value}${profession}`}
                      name={value}
                      value={value}
                      checked={value === checkedType}
                      onChange={toggleCheckedRate}
                    />
                    <label htmlFor={`${value}${profession}`}>{text}</label>
                  </> : ''
              ) : 
                <>
                  <Radio
                    id={`${value}${profession}`}
                    name={value}
                    value={value}
                    checked={value === checkedType}
                    onChange={toggleCheckedRate}
                  />
                  <label htmlFor={`${value}${profession}`}>{text}</label>
                </>
              }

              { profession === 'massageTherapist' ? (
                <>
                  <Radio
                    id={`${value}${profession}`}
                    name={value}
                    value={value}
                    disabled={professions.fitnessTrainer.state !== 'FL' && value === 'inperson' ? true : false}
                    checked={professions.fitnessTrainer.state === 'FL' ? value === checkedType : ''}
                    onChange={toggleCheckedRate}
                  />
                  <label htmlFor={`${value}${profession}`}>{text}</label>
                </>
              ) : ''}
              
            </div>
          ))}
        </div>
      </div>
      <div>
        <span className="title">Rates</span>
        {(checkedType === "online" || checkedType === "both") && (
          <div className="rates">
            <span>online</span>
            <div className="paper">
              <span className="rate">${onlineRate}/h</span>
              <Slider
                id="online"
                track="inverted"
                value={onlineRate}
                marks
                step={STEP}
                min={MIN_RATE}
                max={MAX_RATE}
                onChange={handleRateChange}
              />
            </div>
          </div>
        )}
        {(checkedType === "inperson" || checkedType === "both") && (
          <div className="rates">
            <span>in person</span>
            <div className="paper">
              <span className="rate">${inpersonRate}/h</span>
              <Slider
                id="inperson"
                track="inverted"
                value={inpersonRate}
                disabled={professions.fitnessTrainer.state !== 'FL' ? true : false}
                marks
                step={STEP}
                min={MIN_RATE}
                max={MAX_RATE}
                onChange={handleRateChange}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Form;
