import React from "react";
import cx from "classnames";
import MuiInput from "@material-ui/core/Input";
import { makeStyles } from "@material-ui/core/styles";

import colors from "./colors";

const useStyles = makeStyles((theme) => ({
  root: (props) => ({
    width: "100%",
    borderRadius: 8,
    border: `1px solid ${colors.GREY_E0D}`,
    boxSizing: "border-box",
    height: 44,
    fontFamily: "QuestrialRegular",
    fontSize: 16,
    lineHeight: "16px",
    color: colors.BLACK_484,
    opacity: 1,
    [theme.breakpoints.up("l")]: {
      fontSize: 14,
      lineHeight: "14px",
    },
    "&.Mui-focused": {
      border: `2px solid ${colors.ORANGE_ED9}`,
      outline: "none",
    },
    "&.Mui-disabled": {
      opacity: 0.6,
    },
    ...props,
  }),
  input: {
    boxSizing: "border-box",
    backgroundColor: "transparent",
    boxShadow: "0 0 0px 1000px transparent inset",
    padding: 15,
    "&::placeholder": {
      fontFamily: "QuestrialRegular",
      fontSize: 16,
      lineHeight: "16px",
      color: colors.BLACK_484,
      opacity: 1,
      [theme.breakpoints.up("l")]: {
        fontSize: 14,
        lineHeight: "14px",
      },
    },
  },
}));

const Input = ({ className, autoComplete = "on", styles, ...props }) => {
  const classes = useStyles(styles);
  return (
    <MuiInput
      className={cx(classes.root, className)}
      classes={{
        root: classes.root,
        input: classes.input,
        focused: classes.focused,
      }}
      {...props}
      disableUnderline
      autoComplete={autoComplete}
    />
  );
};

export default Input;
