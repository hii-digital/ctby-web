import React, { useReducer } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Divider } from "@material-ui/core";

// components
import NavButtons from "../../NavButtons";
import Form from "./Form";

// helpers
import {
  DEFAULT_VALUE,
  FITNESS_TRAINER,
  MASSAGE_THERAPIST,
} from "../../../../data/professions";

// actions
import {
  updateProfessionNode,
  updateUser,
} from "../../../../store/actions/onboardingActions";

// styles
import "./styles.scss";

const fitnessTrainerOptions = [
  { value: "both", text: "Both" },
  { value: "online", text: "Online" },
  { value: "inperson", text: "In Person" },
];

const massageTherapistOptions = [{ value: "inperson", text: "In Person" }];

const reducer = (state, { type, profession, value }) => {
  switch (type) {
    case "online":
      return {
        ...state,
        [profession]: {
          ...state[profession],
          online: value,
        },
      };
    case "inperson":
      return {
        ...state,
        [profession]: {
          ...state[profession],
          inperson: value,
        },
      };
    default:
      return state;
  }
};

const TrainingTypeRates = ({ goBack, goNext }) => {
  const {
    auth: { uid },
    profile: { proStep },
  } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { isLoading } = useSelector((store) => store, shallowEqual);

  const { professions } = useSelector(
    ({ firestore }) => firestore.data,
    shallowEqual
  );

  const dispatch = useDispatch();

  const [state, rateDispatch] = useReducer(reducer, {
    [FITNESS_TRAINER]: {
      online: DEFAULT_VALUE,
      inperson: DEFAULT_VALUE,
    },
    [MASSAGE_THERAPIST]: {
      inperson: DEFAULT_VALUE,
    },
  });

  const {
    [FITNESS_TRAINER]: fitnessState,
    [MASSAGE_THERAPIST]: massageState,
  } = state;

  const fitnessTrainer = professions?.[FITNESS_TRAINER];
  const massageTherapist = professions?.[MASSAGE_THERAPIST];

  const handleSubmit = (e) => {
    e.preventDefault();
    const updates = {};
    if (fitnessTrainer) {
      updates[FITNESS_TRAINER] = [];
      if (fitnessTrainer.onlineRate !== fitnessState.online) {
        updates[FITNESS_TRAINER].push({
          node: "onlineRate",
          value: fitnessState.online,
        });
      }
      if (fitnessTrainer.inpersonRate !== fitnessState.inperson) {
        updates[FITNESS_TRAINER].push({
          node: "inpersonRate",
          value: fitnessState.inperson,
        });
      }
    }

    if (massageTherapist) {
      if (massageTherapist.inpersonRate !== massageState.inperson) {
        updates[MASSAGE_THERAPIST] = [
          {
            value: massageState.inperson,
            node: "inpersonRate",
          },
        ];
      }
    }

    const isAnyUpdates =
      updates[FITNESS_TRAINER]?.length || updates[MASSAGE_THERAPIST]?.length;

    if (isAnyUpdates) {
      dispatch(updateProfessionNode({ uid, updates }));
    }
    dispatch(updateUser({ uid, payloads: { proStep: proStep + 1 } }));
    goNext(e);
  };

  const handleRateChange = ({ profession, type, value }) => {
    if (profession === FITNESS_TRAINER) {
      rateDispatch({ type, profession: FITNESS_TRAINER, value });
    }
    if (profession === MASSAGE_THERAPIST) {
      rateDispatch({ type, profession: MASSAGE_THERAPIST, value });
    }
  };

  return (
    <form onSubmit={handleSubmit} className="root_trainingTypeRate">
      <div>
        {fitnessTrainer && (
          <>
            <div className="profession-header">
              <span>As a Fitness Trainer</span>
              <Divider />
            </div>
            <Form
              profession={FITNESS_TRAINER}
              options={fitnessTrainerOptions}
              onlineRate={fitnessState.online}
              inpersonRate={fitnessState.inperson}
              onRateChange={handleRateChange}
              initialChecked="both"
            />
          </>
        )}
        {massageTherapist && (
          <>
            <div className="profession-header">
              <span>As a Massage Therapists</span>
              <Divider />
            </div>
            {professions.massageTherapist.state !== 'FL' ? (
              <>
                  <p style={{marginBottom: '20px'}}>Massage therapy is only available for Florida. Please visit the previous page and remove from profession.</p>
              </>
            ) : (
              <Form
                profession={MASSAGE_THERAPIST}
                options={massageTherapistOptions}
                inpersonRate={massageState.inperson}
                onRateChange={handleRateChange}
                initialChecked="inperson"
              />
            )}
          </>
        )}
      </div>
      <NavButtons goBack={goBack} isLoading={isLoading} hideNext={professions && professions.massageTherapist && professions.massageTherapist.state !== 'FL' ? true : false} />
    </form>
  );
};

export default TrainingTypeRates;
