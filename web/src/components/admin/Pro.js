import React, { Fragment } from "react";
import { useDispatch } from "react-redux";
import { Input } from "semantic-ui-react";
import { useFirestoreConnect } from "react-redux-firebase";

// actions
import { approveProfile } from "../../store/actions/authActions";

// hooks
import { useProfessionsHook } from "../../hooks/professions";

const Pro = ({ message, setMessage, user }) => {
  const dispatch = useDispatch();

  useFirestoreConnect([
    {
      collection: `users/${user.id}/professions`,
      storeAs: `${user.id}/professions`,
    },
  ]);

  const { professionsOrderedData: professions } = useProfessionsHook(user.id);

  const handleApprove = () => {
    document.body.style.overflow = "unset"; // old code style
    dispatch(approveProfile(user.id));
  };

  const proData = professions?.reduce((acc, profession, i) => {
    if (i === 0) {
      acc.proNames = [];
      acc.licenseURLs = [];
    }
    const { name, licenseURL } = profession;
    acc.proNames = [...acc.proNames, name];
    if (licenseURL) {
      acc.licenseURLs = [...acc.licenseURLs, licenseURL];
    }
    return acc;
  }, {});

  const { proNames, licenseURLs } = proData || {};

  return (
    <div className="review">
      <div className="review__name">
        <h2>Review Profile</h2>
        <p>
          <strong>ID:</strong> {user.id}
        </p>
        <p>
          <strong>Name:</strong> {user.firstName} {user.lastName}
        </p>
        <p>
          <strong>Profession:&nbsp;</strong>
          {proNames?.length
            ? proNames.map((name, i) => (
                <Fragment key={i}>
                  {i > 0 && ", "}
                  {name}
                </Fragment>
              ))
            : "No Profession Added"}
        </p>
        <p>
          <strong>Profile Image:&nbsp;</strong>
          {user.photoURL ? (
            <a href={user.photoURL} target="_blank" rel="noopener noreferrer">
              Open Image
            </a>
          ) : (
            "No Profile Image Added"
          )}
        </p>
        <p>
          <strong>License Image:&nbsp;</strong>
          {licenseURLs?.length
            ? licenseURLs.map((url, i) => (
                <Fragment key={url}>
                  <a href={url} target="_blank" rel="noopener noreferrer">
                    {i > 0 && ", "}Open Image
                  </a>
                  &nbsp;
                </Fragment>
              ))
            : "No License Image Added"}
        </p>
        <p>
          <strong>About/Experience:</strong> {user.about}
        </p>
        <p>
          <strong>Favorite Quote:</strong> {user.favQuote}
        </p>
        <p>
          <strong>Fun Fact:</strong> {user.funFact}
        </p>
        <p>
          <strong>Social:</strong>{" "}
          {user.socialInstagram ? (
            <a
              href={user.socialInstagram}
              target="_blank"
              rel="noopener noreferrer"
            >
              Instagram
            </a>
          ) : <span>No Instagram Link Added</span>}
        </p>

        <div className="review__buttons">
          <button
            type="button"
            onClick={handleApprove}
            className="button button--secondary"
            style={{ marginBottom: "10px" }}
          >
            Approve
          </button>
          <p>
            <strong>-- OR --</strong>
          </p>
          <Input
            value={message}
            placeholder="Decline Message"
            onChange={(e) => setMessage(e.target.value)}
          />
        </div>
      </div>
    </div>
  );
};

export default Pro;
