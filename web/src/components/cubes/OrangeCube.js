import React from "react";
import { useHistory } from "react-router-dom";

import { ReactComponent as Weight } from "../../assets/icons/body.svg";

import "./cube.scss";

export default function OrangeCube() {
  const history = useHistory();
  function redirectFunction() {
    history.push("/how-it-works");
  }
  return (
    <div
      role="button"
      type="button"
      tabIndex={0}
      className="orange-cube-wrapper"
      onClick={redirectFunction}
      onKeyDown={redirectFunction}
    >
      <div className="cube">
        <div className="cube__text">How it works for Client</div>
        <div className="cube__img">
          <Weight />
        </div>
      </div>
      <div className="orange-cube-wrapper__little-box-wrapper">
        <div className="orange-cube-wrapper__little-box" />
        <div className="orange-cube-wrapper__middle-box" />
      </div>
    </div>
  );
}
