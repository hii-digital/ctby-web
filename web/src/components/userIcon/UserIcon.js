import React from "react";
import { useSelector, shallowEqual } from "react-redux";

import { ReactComponent as AvatarIcon } from "../../assets/images/iconUser.svg";
import "./userIcon.scss";

const UserIcon = (props) => {
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { photoURL } = profile;
  return (
    <div className="userIcon" {...props}>
      {photoURL ? <img src={photoURL} alt="userAvatar" /> : <AvatarIcon />}
    </div>
  );
};

export default UserIcon;
