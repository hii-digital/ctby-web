export const getRating = (arr) => {
  const bookingsWithRating = arr.filter(({ rating: proRating }) => !!proRating);
  const sum = bookingsWithRating.reduce(
    (acc, { rating: proRating }) => proRating.score + acc,
    0
  );
  const average = sum / bookingsWithRating.length;
  const float = average.toFixed(1);
  return {
    rating: bookingsWithRating.length ? float : 0,
    length: bookingsWithRating.length,
  };
};
