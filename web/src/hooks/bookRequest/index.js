import { useEffect, useState, useCallback ,useContext} from "react";
import moment from "moment";
import { useSelector, shallowEqual, useDispatch } from "react-redux";

import { geocodeByAddress } from "react-places-autocomplete";
import momentTimezone from "moment-timezone";
// data
import { durations, FORMAT } from "../../data/hours";
import {
  trainingTypeValues,
  inpersonTypeValues,
  MASSAGE_THERAPIST,
} from "../../data/professions";
import { STATUSES } from "../../data/bookings";

// hooks
import { useProfessionsHook } from "../professions";
import { useDateTimeHook } from "./dateTimeHook";

// helpers
import { prepareDataForLocal } from "../../helpers/time";
import { getNearbyZipcodes } from "../../helpers/general";

//context
import { SidebarContext } from "../../context/SidebarProvider";

// data
import { RADIUS } from "../../data/address";

// actions
import { getSessionId ,createBookingWithCoupon } from "../../store/actions/stripeActions";
import { User } from "@twilio/conversations";


const initialState = {
  service: "",
  date: null,
  startTime: "",
  duration: "",
  inpersonType: "",
  address: "",
  addressLineSecond: "",
};

export const useBookRequestHook = (userId) => {
  const dispatch = useDispatch();
  const { online, inperson } = trainingTypeValues;
  const { comeToMe } = inpersonTypeValues;
  const { closeSidebar } = useContext(SidebarContext);
  const [state, setState] = useState(initialState);
  const [activeType, setActive] = useState(null);
  const [errorMsg, setErrorMsg] = useState("");
  const [isCloseToProData, setIsCloseToPro] = useState({
    isClose: null,
    message: "",
  });

  const { professionsData, professionsOrderedData } = useProfessionsHook(
    userId
  );
  const checkInPersonState=()=>{
    return state&&(state==="FL"||state==="FLORIDA")
  }

  const checkIsTypeAvailable = useCallback(
    (type) => {
      switch (type) {
        case online.value:
          return !!professionsOrderedData.find(
            ({ id, onlineRate }) => id !== MASSAGE_THERAPIST && !!onlineRate
          );
        case inperson.value:
          return checkInPersonState()&&!!professionsOrderedData.find(
            ({ inpersonRate }) => inpersonRate
          );
        default:
          return false;
      }
    },
    [inperson.value, online.value, professionsOrderedData]
  );

  const clearState = () => {
    setState(initialState);
  };

  const clearError = () => {
    setErrorMsg("");
  };

  const toggleActive = (value) => {
    clearError();
    setActive(value);
    if (activeType !== value) {
      clearState();
    }
  };
 
  const items = [
    {
      title: online.text,
      action: toggleActive,
      value: online.value,
      disabled: !checkIsTypeAvailable(online.value),
    },
    {
      title: inperson.text,
      action: toggleActive,
      value: inperson.value,
      disabled: !checkIsTypeAvailable(inperson.value),
    },
  ];

  useEffect(() => {
    return checkIsTypeAvailable(online.value)
      ? setActive(online.value)
      : setActive(inperson.value);
  }, [checkIsTypeAvailable, inperson.value, online.value]);

  const {
    Hours: hours,
    specialDates,
    firstName,
    lastName,
    stripeId,
    phoneNumber,
    city
  } = useSelector(
    ({ firestore }) => firestore.data.approvedUsers[userId],
    shallowEqual
  );

  const {
    auth: { uid },
  } = useSelector(({ firebase }) => firebase, shallowEqual);

  const {
    service,
    date,
    startTime,
    duration,
    inpersonType,
    address,
    addressLineSecond,
  } = state;
  const specialDatesFormatted = prepareDataForLocal(specialDates);

  const getProfessionsList = () => {
    return professionsOrderedData.filter(({ onlineRate, inpersonRate }) => {
      return activeType === online.value ? !!onlineRate : !!inpersonRate;
    });
  };

  const checkIsCloseToPro = async () => {
    const result = await geocodeByAddress(address);
    // eslint-disable-next-line camelcase
    const { address_components } = result[0];
    const data = address_components.find(
      ({ types }) => types[0] === "postal_code"
    );
    if (data) {
      const { long_name: zipcode } = data;
      const { zipCode ,state} = professionsData[service];
      const nearbyZipcodes = getNearbyZipcodes({ zipcode, radius: RADIUS });
      const isClose = nearbyZipcodes.includes(zipCode);
      return {
        isClose,
        message: isClose ? "" : "Pro is too far from the selected address",
      };
    }
    return {
      message: "Choose more specific address",
    };
  };

  const {
    checkIsStartTimeAvailable,
    filterDates,
    checkIsDurationAvailable,
  } = useDateTimeHook(userId);





  const checkIsSelectorDisabled = (id) => {
   
    const checkAddress = () => {
      const isComeToMe = inpersonType === comeToMe.value;
      return isComeToMe && !address;
    };

    if (uid === userId) {
      return true;
    }
    if (id === "service") {
      if (activeType === inperson.value) {
        return checkAddress() || !inpersonType;
      }
    }
    if (id === "date") {
      return checkAddress() || !service;
    }
    if (id === "startTime") {
      return checkAddress() || !(service && date);
    }
    if (id === "duration") {
      return checkAddress() || !(service && date && startTime);
    }
    return false;
  };

  const renderInpersonTypeValue = (value) => {
    if (!value) {
      return "Choose In Person Type";
    }
    return inpersonTypeValues[value].text;
  };

  const renderServiceValue = (value) => {
    return professionsData?.[value]?.name || "Choose Service";
  };

  const renderDurationValue = (val) => {
    if (!val) {
      return "Duration";
    }
    return durations.find(({ value }) => value === val).text;
  };

  const renderPrice = (discount) => {
    if(discount){
      return 0
    }
    if (!service) {
      return 0;
    }
    const { onlineRate, inpersonRate } = professionsData[service];
    return activeType === online.value ? onlineRate : inpersonRate;
  };

  const renderTotal = (discount) => {


let init_discount=0
if(discount){
  
    return 0;
}else{
  const rate = renderPrice();
  return (rate * (duration / 60))-init_discount;

}

   
  };

  const onDateChange = (d) => {
    clearError();
    setState((prevState) => ({
      ...prevState,
      date: d,
      startTime: "",
      duration: "",
    }));
  };

  const onSelectChange = ({ id, value }) => {
    clearError();
    if (id === "startTime") {
      setState((prevState) => ({ ...prevState, [id]: value, duration: "" }));
    } else if (id === "inpersonType") {
      setState({ ...initialState, [id]: value });
    } else {
      setState((prevState) => ({ ...prevState, [id]: value }));
    }
  };

  const onInputChange = ({ id, value }) => {
    clearError();
    setState((prevState) => ({ ...prevState, [id]: value }));
  };

  const hasErrorBorder = (element) => {
    const isDisabled = checkIsSelectorDisabled(element);
    if (!errorMsg || isDisabled) {
      return false;
    }
    const { isClose, message } = isCloseToProData;
    if (element === "address" && !isClose && message === errorMsg) {
      return true;
    }
    return !state[element];
  };

  const stringDate = moment(date).format(FORMAT);
  const from = moment(
    `${stringDate} ${startTime}`,
    `${FORMAT} HH:mm A`
  ).toDate();
  const to = moment(from).add(duration, "m").toDate();

  const onSubmit = async (coupon,history) => {


  const url = window.location.origin;

  const payloads = {
    proId: userId,
    clientId: uid,
    rate: renderPrice(),
    serviceName: professionsData[service].name,
    status: STATUSES.awaitingApproval.value,
    type: activeType,
    from: from.toISOString(),
    to: to.toISOString(),
    proPhoneNumber: phoneNumber,
    url,
    timezone: momentTimezone.tz.guess(),
  };

  if (activeType === inperson.value) {
    payloads.inpersonType = inpersonType;
    if (inpersonType === comeToMe.value) {
      const result = await geocodeByAddress(address);
      const { formatted_address: formattedAddress } = result[0];
      payloads.address = formattedAddress;
      if (addressLineSecond) {
        payloads.addressLineSecond = addressLineSecond;
      }
    } else {
      const {
        zipCode: proZipcode,
        addressLineSecond: proAddressLineSecond,
        addressLineFirst: proAddressLineFirst,
      } = professionsData[service];
      payloads.address = proAddressLineFirst;
      if (proAddressLineSecond) {
        payloads.addressLineSecond = proAddressLineSecond;
      }
      payloads.zipcode = proZipcode;
    }
  }

  const totalAmount = renderTotal();
  const orderData = {
    currency: "USD",
    quantity: 1,
    amount: totalAmount * 100,
    name: `Payment for session with ${firstName} ${lastName}`,
    description: `Date: ${moment(from).format(FORMAT)} Time: ${moment(
      from
    ).format("hh:mm A")} - ${moment(to).format("hh:mm A")}`,
    clientId: userId,
    url,
    destination: stripeId,
    metaData: payloads,
  };

 
 
  //create booking on firebase
//create stripe account for payout
if(!coupon){


  dispatch(getSessionId(orderData));
}else{
  
try {
 
  dispatch(createBookingWithCoupon(orderData));
  closeSidebar()
  history.push("/bookings")
} catch (error) {
  console.log("error")
}
  

}

   
  };

 
  return {
    professions: getProfessionsList(),
    items,
    hours,
    specialDatesFormatted,
    service,
    date,
    startTime,
    duration,
    inpersonType,
    activeType,
    from,
    to,
    address,
    addressLineSecond,
    isCloseToProData,
    errorMsg,
    setIsCloseToPro,
    setErrorMsg,
    setState,
    clearState,
    checkIsCloseToPro,
    checkIsSelectorDisabled,
    checkIsDurationAvailable,
    checkIsStartTimeAvailable,
    hasErrorBorder,
    filterDates,
    renderTotal,
    renderPrice,
    renderDurationValue,
    renderServiceValue,
    renderInpersonTypeValue,
    onDateChange,
    onSelectChange,
    onInputChange,
    clearError,
    onSubmit,
  };
};
