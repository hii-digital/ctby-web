import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { Button } from "semantic-ui-react";
import DeleteAccount from "../auth/DeleteAccount";
import { toggleHideProfile } from "../../store/actions/profileActions";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { auth, profile ,hideProfileToggle} = this.props;

    const hideProfile=()=>{
      hideProfileToggle("hidden")
    }

    const showProfile=()=>{
      hideProfileToggle("")
    }
    return (
      <div className="settings">
        <div className="container container--top-bottom-padding">
          <div className="row">
            <div className="col">
              <div className="dashboard__head">
                <h1 className="text--lg text--uppercase">Settings</h1>
              </div>
            </div>
          </div>

          {!profile.isPro && (
            <div className="row">
              <div className="col" style={{ marginBottom: "50px" }}>
                <div style={{ width: "100%" }}>
                  <h2>Become a Pro</h2>
                </div>
                <Button
                  className="button button--md button--secondary"
                  as={Link}
                  to="/upgrade-pro"
                >
                  Upgrade Account to Pro
                </Button>
              </div>
            </div>
          )}
          {profile.isPro && (
            <div className="row">
              <div className="col" style={{ marginBottom: "50px" }}>
               {!profile.isHidden==="hidden"?
               <Button
               className="button button--md button--secondary"
               onClick={hideProfile}
             >
              Hide Profile
             </Button>:
              <Button
              className="button button--md button--secondary"
              onClick={showProfile}
            >
            Show Profile
            </Button> 
              }
               
              </div>
            </div>
          )}


          {/* <div className="row">
            <div className="col" style={{ marginBottom: "50px" }}>
              <DeleteAccount auth={auth} />
            </div>
          </div> */}

          <div className="row">
            <div className="col" style={{ marginBottom: "50px" }}>
              <div>
                <h2>File a Claim</h2>
                <Link to="/file-claim" className="button button--primary">
                  Start Process
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hideProfileToggle: (isHidden) => dispatch(toggleHideProfile(isHidden)),
  };
};


export default compose(connect(mapStateToProps,mapDispatchToProps))(Settings);
