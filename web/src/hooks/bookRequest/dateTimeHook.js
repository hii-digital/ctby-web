import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";

// data
import { HOURS, FORMAT } from "../../data/hours";
import { STATUSES } from "../../data/bookings";

// hooks
import { useBookingsHook } from "../bookings";

// helpers
import { prepareDataForLocal, getNow } from "../../helpers/time";

const { declined, cancelled } = STATUSES;

export const useDateTimeHook = (proId) => {
  const { Hours: hours, specialDates } = useSelector(
    ({ firestore }) => firestore.data.approvedUsers[proId],
    shallowEqual
  );
  const {
    bookingsAsClientOrdered = [],
    bookingsAsProOrdered = [],
  } = useBookingsHook(proId);
  const specialDatesFormatted = prepareDataForLocal(specialDates);

  const checkIsStartTimeAvailable = (
    time,
    checkedDate,
    ignoredBookingId = null
  ) => {
    const mCheckedDate = moment(checkedDate);
    const stringDate = mCheckedDate.format(FORMAT);
    const checkedDateTime = moment(
      `${stringDate} ${time}`,
      `${FORMAT} HH:mm A`
    ).toDate();

    if (moment(checkedDateTime).diff(getNow(), "hours") < 24) {
      return false;
    }

    const specDate = specialDatesFormatted?.find((d) => stringDate === d.date);
    const selectedWeekday = mCheckedDate.format("dddd").toLowerCase();

    const dateData = specDate || hours?.[selectedWeekday];
    if (!dateData) {
      return false;
    }
    const { workingTimes: timesToMap } = dateData;

    const list = timesToMap.map(({ from, to }) => {
      const fromIndex = HOURS.findIndex(({ value }) => value === from);
      const toIndex = HOURS.findIndex(({ value }) => value === to);
      return HOURS.slice(fromIndex, toIndex);
    });

    const bookings = [...bookingsAsProOrdered, ...bookingsAsClientOrdered];

    const foundBookingSlot = bookings.find(({ from, to, status, id }) => {
      if (id === ignoredBookingId) {
        return false;
      }
      const isActive = status !== cancelled.value && status !== declined.value;
      return (
        checkedDateTime >= from.toDate() &&
        checkedDateTime < to.toDate() &&
        isActive
      );
    });

    if (foundBookingSlot) {
      return false;
    }
    return list.flat().find(({ value }) => value === time);
  };

  const filterDates = (d) => {
    const hasAvialableSlot = HOURS.find(({ value }) =>
      checkIsStartTimeAvailable(value, d)
    );

    if (!hasAvialableSlot) {
      return false;
    }

    const momentDate = moment(d);
    const specialDate = specialDatesFormatted?.find((spec) => {
      return momentDate.format(FORMAT) === spec.date;
    });

    if (specialDate) {
      return specialDate.state;
    }

    const isWeekdayBlocked = hours
      ? Object.keys(hours).find((weekday) => {
          return (
            momentDate.format("dddd").toLowerCase() === weekday &&
            !hours[weekday].state
          );
        })
      : true;

    return !isWeekdayBlocked;
  };

  const checkIsDurationAvailable = ({
    duration,
    startTime,
    date,
    ignoredBookingId = null,
  }) => {
    const startTimeIndex = HOURS.findIndex(({ value }) => value === startTime);
    let lastIndex;
    switch (duration) {
      case "120":
        lastIndex = startTimeIndex + 4;
        break;
      case "90":
        lastIndex = startTimeIndex + 3;
        break;
      case "60":
        lastIndex = startTimeIndex + 2;
        break;
      case "30":
        lastIndex = startTimeIndex + 1;
        break;
      default:
        lastIndex = startTimeIndex;
    }

    const hasUnavialableSlot = HOURS.slice(startTimeIndex + 1, lastIndex).find(
      ({ value }) => !checkIsStartTimeAvailable(value, date, ignoredBookingId)
    );
    return !hasUnavialableSlot;
  };

  return {
    checkIsStartTimeAvailable,
    filterDates,
    checkIsDurationAvailable,
  };
};
