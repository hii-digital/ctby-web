import React, { useEffect, useRef, useState } from "react";

const ImageUpload = (props) => {
  const [error, setError] = useState("");
  const { handleChange, progress, required, image } = props;
  const inputRef = useRef(null);

  useEffect(() => {
    if (!image) {
      inputRef.current.value = "";
    }
  }, [image]);

  const hanldeImageChange = (e) => {
    setError("");
    if (e.target.files[0]) {
      const image = e.target.files[0];
      const fileSize = image.size / 1024 / 1024; // in MB
      if (fileSize > 7) {
        inputRef.current.value = "";
        setError("Please upload an image under 7mb");
      } else {
        handleChange(image);
        console.log("image changed")
      }
    }
  };

  return (
    <div className="profile-image__upload">
      <progress value={progress} max="100" />
      <input
        ref={inputRef}
        type="file"
        accept=".png, .jpg, .jpeg"
        onChange={hanldeImageChange}
        required={required}
      />
      <span className="message">
        {error || (!image && "Maximum file upload size: 7 MB")}
      </span>
    </div>
  );
};

export default ImageUpload;
