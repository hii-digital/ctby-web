import React, { useState, useEffect, useContext } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Divider } from "@material-ui/core";

// components
import Input from "../../../../ui/Input";
import PasswordInput from "../../../../ui/PasswordInput";
import { FilledButton } from "../../../../ui/Button";
import LinkButton from "../../../../ui/LinkButton";

// context
import { SidebarContext } from "../../../../context/SidebarProvider";

// actions
import * as authActions from "../../../../store/actions/authActions";

// icons
import { ReactComponent as GoogleIcon } from "../../../../assets/icons/google.svg";
import { ReactComponent as FacebookIcon } from "../../../../assets/icons/facebook.svg";

// styles
import "../../styles.scss";
import "./styles.scss";

const SignUp = () => {
  const [state, setState] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    repeatedPassword: "",
  });

  const { openSidebar, closeSidebar } = useContext(SidebarContext);
  const [passwordsVisibleIds, setPassVisibleIds] = useState([]);

  const {
    firstName,
    lastName,
    email,
    password,
    repeatedPassword,
  } = state;

  const dispatch = useDispatch();
  const history = useHistory();

  const { authError: authErrorMsg } = useSelector(
    (store) => store.auth,
    shallowEqual
  );

  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  useEffect(() => {
    if (auth.uid && !authErrorMsg && firstName) {
      closeSidebar();
      history.push("/onboarding-client");
    }
  }, [auth.uid, authErrorMsg, closeSidebar, dispatch, firstName, history]);

  useEffect(() => {
    dispatch(authActions.clearErrorMessage());
  }, [dispatch]);

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(authActions.clearErrorMessage());

    const isBlank = (value) => {
      return !value.trim().length;
    };

    if (isBlank(firstName) || isBlank(lastName)) {
      dispatch(
        authActions.setErrorMessage(
          "All fields are required, please fill them."
        )
      );
      return;
    }

    if (password !== repeatedPassword) {
      dispatch(authActions.setErrorMessage("Passwords don't match."));
      return;
    }

    dispatch(
      authActions.signUp({
        email,
        password,
        firstName,
        lastName,
      })
    );
  };

  const togglePasswordVisible = (id) => {
    setPassVisibleIds((prevState) => {
      return prevState.includes(id)
        ? prevState.filter((el) => el !== id)
        : [...prevState, id];
    });
  };

  const onChange = (e) => {
    const { id, value } = e.target;
    setState((prevState) => ({ ...prevState, [id]: value }));
  };

  const isSignUpBtnActive = Boolean(firstName && lastName && email && password);

  const signInClientOpen = () => {
    openSidebar("signInClient");
  };

  const signUpWithGoogle = () => {
    dispatch(authActions.signInWithGoogle());
  };

  const signUpWithFacebook = () => {
    dispatch(authActions.signInWithFacebook());
  };

  return (
    <div className="auth-form-root">
      <h1>sign up</h1>
      <div>
        <div className="social-buttons-group">
          <button type="button" onClick={signUpWithGoogle}>
            <GoogleIcon />
          </button>
          <button type="button" onClick={signUpWithFacebook}>
            <FacebookIcon />
          </button>
        </div>
        <div className="or-divider">
          <Divider className="divider" />
          <span>or</span>
          <Divider className="divider" />
        </div>
        <form onSubmit={onSubmit}>
          <div>
            <div>
              <span className="input-label">first name</span>
              <Input
                className="text-input"
                value={firstName}
                id="firstName"
                onChange={onChange}
                placeholder="e.g. John"
              />
            </div>
            <div>
              <span className="input-label">last name</span>
              <Input
                className="text-input"
                value={lastName}
                id="lastName"
                onChange={onChange}
                placeholder="e.g. Doe"
              />
            </div>
          </div>
          <span className="input-label">email</span>
          <Input
            type="email"
            className="text-input"
            value={email}
            id="email"
            onChange={onChange}
            placeholder="abc@mail.com"
          />
          <label htmlFor="password" className="input-label">
            create password
          </label>
          <PasswordInput
            isVisible={passwordsVisibleIds.includes("password")}
            className="text-input"
            value={password}
            id="password"
            onChange={onChange}
            toggleVisible={togglePasswordVisible}
          />
          <label htmlFor="repeated-password" className="input-label">
            repeat password
          </label>
          <PasswordInput
            isVisible={passwordsVisibleIds.includes("repeatedPassword")}
            className="text-input"
            value={repeatedPassword}
            id="repeatedPassword"
            onChange={onChange}
            toggleVisible={togglePasswordVisible}
          />
          {authErrorMsg && <p className="error">{authErrorMsg}</p>}
          <FilledButton
            className="form-submit"
            fullWidth
            type="submit"
            disabled={!isSignUpBtnActive}
          >
            sign up
          </FilledButton>
          <span>
            Already have an account?
            <LinkButton onClick={signInClientOpen}>sign in</LinkButton>
          </span>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
