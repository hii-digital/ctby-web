import React, { useState, useRef } from "react";
import classnames from "classnames";
import { useIntersection } from "./intersectionObserver";
import "./ImageRenderer.css";

const ImageRenderer = ({ url, thumb, width, height, alt }) => {
	const [isLoaded, setIsLoaded] = useState(false);
	const [isInView, setIsInView] = useState(false);
	const imgRef = useRef();
	useIntersection(imgRef, () => {
		setIsInView(true);
	});

	const handleOnLoad = () => {
		setIsLoaded(true);
	};
	// console.log("isloaded",isLoaded)
	
	return (
		<div
			className="image-container-lazy"
			ref={imgRef}
			style={{
				paddingBottom: `${(height / width) * 100}%`,
				width: "100%",
			}}
		>
			{isInView && (
				<>
				 
						<img src={thumb} style={{
							opacity:( !isLoaded ? "1" : "0" ),
							objectFit: 'scale-down',
							background: 'white',
						}}/>
				 
						<img
						 style={{opacity:( isLoaded ? "1": "0")}}
							src={url}
							onLoad={handleOnLoad}
						/>
				 
				</>
			)}
		</div>
	);
};

export default ImageRenderer;
