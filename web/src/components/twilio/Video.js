import React from "react";

const Video = ({ videoRef }) => {
  return (
    <div>
      <video
        ref={videoRef || null}
        className="chat__video"
        id="call-user-video"
        playsInline="true"
        autoPlay
      >
        <track kind="captions" />
        <p>Video is not supported, please update your browser.</p>
      </video>
    </div>
  );
};

export default Video;
