import React, { Component } from "react";

class Terms extends Component {
  render() {
    return (
      <div className="about page">
        <div className="container container--top-bottom-padding">
          <div className="row">
            <div className="col">
              <h1 className="text--lg">Terms of Use</h1>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <p>
                <strong>Choose To Be You Terms of Use Agreement</strong>
              </p>
              <p>
                The chooosetobeyou.com web site (the “Site”) is open to both
                clients and professionals. The purpose of the Site is to provide
                an efficient process for scheduling training sessions between
                clients and professionals. By joining our community, clients
                will be able to learn about professionals in their area,
                schedule sessions, and gain insight on valuable promotions and
                offerings. professionals will be able to advertise their
                services, acquire client bookings, as well as promote their own
                expertise in the industry. For the purpose of this Terms of Use
                Agreement, both clients and professionals shall be referred to
                as “you” or “your.”
              </p>
              <p>
                Please understand that, by joining our community and accessing
                information on the Site, you are acknowledging that you have
                read, understood, and agree to comply with the terms and
                conditions stated within this Terms of Use Agreement (this
                “Agreement”), which shall constitute a binding agreement between
                you, as a user of the Site. This Agreement is an electronic
                contract that sets out the legally binding terms of your use and
                membership of the Site. If there is any conflict between this
                Agreement and the terms of any other agreement that you have
                entered with Servicient, those terms will supercede the terms of
                this Agreement.
              </p>
              <p>
                <strong>1. How to Use our Service</strong>
              </p>
              <p>
                The service is available to both professionals and clients, so
                that they may advertise and schedule sessions. To enjoy the
                service, professionals must develop a profile and input their
                locations and availability into the scheduling software. This
                will allow clients to select times that meet the professional’s
                availability. The clients may then register on the Site, and
                begin to research professionals in their area. Then, the client
                may schedule appointments with the professional of his or her
                choice.
              </p>
              <p>
                <strong>2. Registering on the Site</strong>
              </p>
              <p>
                Both clients and professionals must register with the Site, in
                order to enjoy the service that Choose To Be You is offering. To
                register, clients and professionals must provide their full
                name, state of residence, zip code, phone number, date of birth,
                email address, and create a password. professionals may also
                upload photos and create bios that provide information about
                their experience and location.
              </p>
              <p>
                You agree that any information that you supply during the
                registration process will be accurate, truthful, and complete.
                You also agree not to (i) select, register, or attempt to
                register, or use a name of another person with the intention of
                impersonating that person; (ii) use a name of anyone else
                without authorization; or (iii) use any content, names, data, or
                other information that is in violation of the intellectual
                property rights of any person or that Choose To Be You considers
                to be offensive or adverse to the spirit and purpose of the
                Site. Choose To Be You reserves the right to reject or terminate
                any registration that, in its judgment, it deems offensive or
                otherwise in violation of this Agreement.
              </p>
              <p>
                You will be responsible for preserving the confidentiality of
                your password and will notify Choose To Be You of any known or
                suspected unauthorized use of your account. Further, you agree
                that you are responsible for all statements made and acts or
                omissions that occur on your account while your password is
                being used. If you believe someone has used your password or
                account without your authorization, you must notify Choose To Be
                You immediately. Choose To Be You reserves the right to access
                and disclose any information including, without limitation, user
                names of accounts and other information to comply with
                applicable laws and lawful government requests. You must be at
                least 13 years old to register on the Site.
              </p>
              <p>
                Please inform Choose To Be You if there is a change in the
                information you provided at the time of your initial
                registration, including any change of address or name, by
                contacting our representatives at support@choosetobefitness.com
              </p>
              <p>
                <strong>3. Proprietary Rights to Content</strong>
              </p>
              <p>
                All materials contained on the Site are protected by copyright
                law except where explicitly noted otherwise. Choose To Be You
                reserves all rights in this content.
              </p>
              <p>
                You acknowledge and agree that content, including but not
                limited to text, software, photographs, video, design,
                recordings, music, graphics, or other material contained on the
                Site (the “Content”) is protected by copyrights, trademarks,
                service marks, patents, or other proprietary rights and
                applicable law. You agree not to copy, reproduce, distribute, or
                create derivative works from the Content or otherwise use,
                transmit, rebroadcast, publish, or distribute the Content in any
                form other than as expressly authorized by this Agreement
                without Choose To Be Yous’ prior written consent. You understand
                and agree that you are permitted to make one copy of the Content
                for personal use only, provided you:
              </p>
              <ul>
                <li>
                  Keep all copyright and other proprietary notices on each copy
                  you make
                </li>
                <li>
                  Use the material in a manner consistent with this Agreement
                </li>
                <li>
                  Understand that we are neither transferring ownership of the
                  materials directly or by implication, nor granting any license
                  or right to the trademarks, tradenames, or copyrights of any
                  party
                </li>
                <li>
                  Using any of our materials for a commercial purpose without
                  our express written consent-violates our copyrights and other
                  proprietary rights.
                </li>
              </ul>
              <p>
                You may not use any data mining, robots, scraping, or similar
                data gathering and extraction tools on the Content, frame or
                scrape data from any portion of the Site or Content, or
                reproduce, reprint, copy, store, publicly display, broadcast,
                transmit, modify, translate, publish, sublicense, assign,
                transfer, sell, loan, or otherwise distribute the Content
                without our prior written consent. You may not circumvent any
                mechanisms included in the Content for preventing the
                unauthorized reproduction or distribution of the Content. You
                also are prohibited from taking any action that imposes or may
                impose (in our sole discretion) an unreasonable or
                disproportionately large load on our infrastructure. Finally,
                you may not utilize any device, software, or routine that will
                interfere or attempt to interfere with the functionality of the
                Site.
              </p>
              <p>
                <strong>4. User Conduct</strong>
              </p>
              <p>
                You agree that all the information that you access on the Site
                will be used only for your own personal purposes. You will not
                make any other unauthorized use of the Site or any interactive
                features available on the Site or otherwise act in a manner that
                is disruptive to the purpose and intent of the Site. You may not
                engage in any conduct or action that is prohibited by any
                applicable law.
              </p>
              <p>
                <strong>5. Privacy</strong>
              </p>
              <p>
                Choose To Be You collects information about the users of the
                Site. Collection of this information is governed by our Privacy
                Policy, which may be accessed at
                https://choosetobefit.net/privacy-policy/.
              </p>
              <p>
                <strong>6. No Show Policy</strong>
              </p>
              <p>
                Choose To Be You is committed to providing effective scheduling
                services to both clients and professionals. We understand,
                however, that a situation may arise where either a client or
                professional is forced to cancel an appointment. If such a
                situation arises, we ask that you cancel your reservation online
                and/or call the client/professional directly.
              </p>
              <p>
                If a client is unable to make a session and fails to cancel at
                least twenty-four hours prior to the scheduled session, the
                client will be charged for the session. Choose To Be You will
                send that client an email letting him or her know that we have
                listed him or her as a no-show for the relevant session. A
                client’s account will be terminated if he or she is a no-show
                for three sessions within the same six-month period.
              </p>
              <p>
                If a professional is unable to make a session and fails to
                cancel at least twenty-four hours prior to the scheduled
                session, the professional will be required to reschedule the
                appointment with the client. Choose To Be You will send that
                professional an email letting him or her know that we have
                listed him or her as a no-show for the relevant session
                reservation. A professional’s account will be terminated if he
                or she is a no-show for three sessions within the same six-month
                period.
              </p>
              <p>
                You agree that all final no-show determination will be made by
                Choose To Be You in its sole discretion.
              </p>
              <p>
                <strong>7. Waiver</strong>
              </p>
              <p>
                You acknowledge that training can be dangerous. The facilities
                at which a sessions takes place may contain hazards that could
                result in your injury or death or damage to your property. The
                equipment used at a session could cause your injury or death or
                damage to your property. Your client or professional could cause
                your injury or death or damage to your property. You assume all
                risk of injury, death, and property damage related to all
                sessions scheduled through the Site. Choose To Be You does not
                inspect or maintain any facilities at which any lesson takes
                place, does not provide any equipment used at any session, and
                is not responsible for the behavior of any client or
                professional. By scheduling a session, you are agreeing that
                Choose To Be You has no liability whatsoever for any injury,
                death, or property damage suffered by you in connection with
                such session.
              </p>
              <p>
                <strong>8. Corporate Identification and Trademarks</strong>
              </p>
              <p>
                All registered and unregistered trademarks and service marks
                (collectively, “Marks”) used or referred to on the Site are the
                property of Choose To Be You or its affiliate, unless otherwise
                noted. You may not use, copy, reproduce, republish, upload,
                post, transmit, distribute, or modify these Marks in any way
                without Choose To Be Yous’ prior written permission. The use of
                Choose To Be Yous’ Marks on any other web site, without
                authorization, is prohibited.
              </p>
              <p>
                <strong>
                  9. Public Areas of the Site; IP Warranty and Grant
                </strong>
              </p>
              <p>
                Certain areas of the Site may allow both professionals and
                clients to post their own Content (as defined in Paragraph 3),
                which can be accessed and viewed by others, including the public
                in general. For example, the professionals may upload photos and
                biographical information that is created by the professional for
                the purpose of publication on the Site. Please know that you may
                only post such Content to public areas on the Site that you
                created or that you have permission to post. You may not post
                any Content that violates these Terms of Use.
              </p>
              <p>
                We do not claim ownership in any Content (as defined above) that
                you may post. However, by submitting such Content to public
                areas of the Site, you hereby warrant that you own the
                appropriate rights in the Content, so as not to violate any
                third party’s intellectual property or proprietary rights.
                Further, you grant to Choose To Be You a worldwide,
                royalty-free, perpetual, irrevocable, non-exclusive right and
                license to use, reproduce, modify, adapt, publish, translate,
                create derivative works from, distribute, perform, and display
                such Content, including any message or any e-mail sent by you to
                us or posted on the Site (in whole or in part), and/or to
                incorporate it in other works in any form, media, or technology
                now known or later developed without the need to attributed
                authorship.
              </p>
              <p>
                To be clear, you understand and agree that you may not upload,
                post, or otherwise make available on the Site any Content
                protected by copyright, trademark, or other proprietary right
                without the express permission of the owner of the copyright,
                trademark, or other proprietary right. You are responsible for
                determining that such material is not protected by copyright,
                trademark, or other proprietary right. You shall be solely
                liable for any damages resulting from any infringement of
                copyright, trademark, or other proprietary right, or any other
                harm resulting from any uploading, posting or submission.
              </p>
              <p>
                <strong>10. Prohibited Conduct</strong>
              </p>
              <p>
                By using the Site you agree not to submit, post, or transmit any
                content or otherwise to engage in any conduct, that violates any
                of the following rules:
              </p>
              <ul>
                <li>
                  You may not attempt to harm, disrupt, or otherwise engage in
                  activity that impairs, the Site.
                </li>
                <li>
                  You may not post any content on the Site that (a) violates or
                  encourages others to violate any applicable law or regulation,
                  or which would give rise to civil liability; (b) is
                  fraudulent, false, misleading, or deceptive; (c) is
                  defamatory, pornographic, obscene, vulgar, or offensive; (d)
                  promotes bigotry, racism, hatred, harassment, or harm against
                  any individual or group; or (e) is abusive or threatening.
                </li>
                <li>
                  You may not attempt to interfere with any other person’s use
                  of the Site or the services offered on the Site.
                </li>
                <li>
                  You may not misrepresent your identity or impersonate any
                  person.
                </li>
                <li>
                  You may not access, descramble, deactivate, remove, impair
                  bypass, or circumvent any technological measure that we, our
                  partners, or a third party have implemented to protect the
                  Site.
                </li>
                <li>
                  You may not attempt to gain access to any account, computers
                  or networks related to or used in connection with the Site,
                  without authorization, or otherwise tamper with any aspect of
                  the Site, or use any robot, spider, data mining tool, web
                  tool, engine, software, device, or mechanism to access data on
                  the Site, unless we provided you with such tool and authorized
                  you to use it for the specific purpose(s) in which you have
                  used it
                </li>
                <li>
                  You may not attempt to obtain any data through any means from
                  the Site, except if we intend to provide or make it available
                  to you.
                </li>
                <li>
                  You may not use the Site to participate in pyramid schemes or
                  chain letters.
                </li>
                <li>
                  You may not use the Site to send, either directly or
                  indirectly, any unsolicited bulk email or communications or
                  unsolicited commercial email or communications.
                </li>
                <li>
                  You may not use the Site to post, display, send or otherwise
                  make available or use, any material protected by intellectual
                  property laws unless you own or control all necessary rights
                  to such material or have received all necessary authorization.
                </li>
                <li>
                  You may not use the Site to send or otherwise make available
                  any material that contains viruses, Trojan horses, worms,
                  corrupted files, or any other similar software that may damage
                  the operation of any computer or property.
                </li>
                <li>
                  You may not use the Site to download any material sent by
                  another user of the Site that you know, or reasonably should
                  know, cannot be legally distributed in such manner.
                </li>
                <li>
                  You may not use the Site in a manner that violates this
                  Agreement, or any code of conduct or other guidelines which
                  may be applicable to the Site.
                </li>
                <li>
                  You may not use the Site to harvest or otherwise collect
                  information about others, including without limitation email
                  addresses.
                </li>
                <li>
                  You may not attempt to modify, translate, adapt, edit, copy,
                  decompile, disassemble, or reverse engineer any software used
                  or provided by us in connection with the Site.
                </li>
                <li>
                  You may not use the Site in a manner that results in excessive
                  bandwidth usage, as determined solely by us.
                </li>
                <li>
                  We have the right to make all judgments concerning any of the
                  above prohibitions in our sole, exclusive, and complete
                  discretion. We reserve the right, also in our sole discretion,
                  to determine whether and what action to take in response to
                  any violation or potential violation of this Agreement, and
                  any action or inaction in a particular instance shall not
                  dictate or limit our response to a future complaint or
                  situation.
                </li>
              </ul>
              <p>
                <strong>11. Right to Terminate</strong>
              </p>
              <p>
                We may, in our sole discretion, terminate the registration of
                any user or professional who violates the terms of this
                Agreement.
              </p>
              <p>
                <strong>12. Breach</strong>
              </p>
              <p>
                In the event of a breach of this Agreement, Choose To Be You may
                terminate your ability to use the Site and prohibit you from any
                future access of the Site. Choose To Be You reserves the right
                to take any further action, as allowed by applicable law.
              </p>
              <p>
                <strong>13. Links to Third Party Sites</strong>
              </p>
              <p>
                At times, the Site may contain links to third-party web sites,
                which are not under the control of Choose To Be You. Choose To
                Be You makes no representations whatsoever about any other web
                site to which you may have access through the Site. When you
                access another web site, you do so at your own risk and
                acknowledge that Choose To Be You is not responsible or liable
                for any content, advertising, services, products, or other
                materials available from such third party websites. You also
                agree that Choose To Be You shall not be liable for any loss or
                damage of any sort incurred as the result of using any third
                party’s website. Mention of third-party companies and web sites
                on the Site is for informational purposes only and does not
                constitute an endorsement or recommendation.
              </p>
              <p>
                <strong>14. Email Policy</strong>
              </p>
              <p>
                You may receive periodic emails from Choose To Be You. If you
                would rather not receive email from Choose To Be You, please
                send an email to support@choosetobefitness.com and you will be
                unsubscribed from receiving further mailings. You acknowledge
                and agree, however, that you will still receive session
                confirmation emails, no-show confirmation emails, session change
                confirmation emails, session cancellation confirmation emails,
                and other emails relating to any sessions that are booked
                through the Site, even if you have opted not to receive periodic
                email from Choose To Be You.
              </p>
              <p>
                <strong>15. Limitation of Liability</strong>
              </p>
              <p>
                IN NO EVENT SHALL Choose To Be You BE LIABLE FOR ANY INJURY,
                LOSS, CLAIM, OR DAMAGES (INCLUDING INDIRECT, SPECIAL, EXEMPLARY,
                PUNITIVE, INCIDENTAL, OR CONSEQUENTIAL DAMAGES) OF ANY KIND,
                WHETHER BASED IN CONTRACT, TORT, OR OTHERWISE, WHICH ARISES OUT
                OF OR IS ANY WAY CONNECTED WITH (I) ANY USE OF THE SERVICES, THE
                SITE, OR ITS CONTENT, (II) ANY FAILURE OR DELAY (INCLUDING, BUT
                NOT LIMITED TO, THE USE OR INABILITY TO USE ANY COMPONENT OF THE
                SERVICES OR THE SITE FOR SCHEDULING PURPOSES), OR (III) THE
                PERFORMANCE OR NON-PERFORMANCE OF ANY professional OR CLIENT IN
                CONNECTION WITH THE SERVICES, EVEN IF Choose To Be You HAS BEEN
                ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
              </p>
              <p>
                <strong>16. Disclaimer of Warranties</strong>
              </p>
              <p>
                Choose To Be You makes no warranty that the Site will be
                uninterrupted, timely, secure, or error free. Choose To Be You
                makes no warranty as to the accuracy of the information on the
                Site.
              </p>
              <p>
                UNLESS EXPRESSLY STATED OTHERWISE, Choose To Be You PROVIDES THE
                CONTENT ON THE SITE “AS IS” AND WITHOUT WARRANTIES OF ANY KIND,
                EITHER EXPRESS OR IMPLIED, TO THE FULLEST EXTENT ALLOWABLE BY
                APPLICABLE LAW, INCLUDING THE IMPLIED WARRANTIES OF
                MERCHANTABILITY, NONINFRINGEMENT OF INTELLECTUAL PROPERTY, AND
                FITNESS FOR A PARTICULAR PURPOSE.
              </p>
              <p>
                <strong>17. Indemnification</strong>
              </p>
              <p>
                You agree to indemnify, defend, and hold harmless Choose To Be
                You, its officers, directors, employees, agents, affiliates,
                distributors, and licensees from and against any judgment,
                losses, deficiencies, damages, liabilities, costs, and expenses
                (including reasonable attorneys’ fees and expenses) incurred in
                connection with or arising from any claim, demand, suit, action,
                or proceeding arising out of any breach of this Agreement by you
                or in connection with your use of the Site or any service (or
                product) related thereto.
              </p>
              <p>
                <strong>18. Right to Modify or Change</strong>
              </p>
              <p>
                Choose To Be You reserves the right to modify this Agreement
                periodically at its sole discretion. When it makes any
                modification to this Agreement, it will update the date
                referenced at the end of this page. Your continued use of the
                Site constitutes acceptance of the terms and conditions stated
                at the time of use.
              </p>
              <p>
                <strong>19. Governing Law</strong>
              </p>
              <p>
                This Agreement and the relationship between Choose To Be You and
                you shall be governed by and construed in accordance with the
                laws of the State of Florida. Any controversy or claim arising
                out of or relating to this Agreement or relating to use of this
                web site and the material contained in this web site shall be
                resolved in a court in the State of Texas.
              </p>
              <p>
                <strong>20. Electronic Signature</strong>
              </p>
              <p>
                The Terms of Use Agreement is an electronic contract that
                governs your use of and access to the Site and Services. Upon
                agreeing to the Terms of Use when enrolling for the Instructor
                Application and Customer Checkout, you acknowledge that you
                understand and agree to be legally bound by the Terms of Use
                Agreement and the terms, conditions and notices contained or
                referenced herein. Checking the box creates an electronic
                signature that has the same legal force and effect as a
                handwritten signature.
              </p>
              <p>
                <strong>21. Waiver / Severability</strong>
              </p>
              <p>
                The waiver by either party of a breach or right under this
                Agreement will not constitute a waiver of any subsequent breach
                or right.If any provision of this Agreement is found to be
                invalid or unenforceable by a court of competent jurisdiction,
                such provision shall be severed from the remainder of this
                Agreement, which will otherwise remain in full force and effect.
              </p>
              <p>
                <strong>22. Payments / Disputes</strong>
              </p>
              <p>
                The Choose To Be You platform may facilitate payments between
                clients and pros through our payment processing partner Stripe,
                but Choose To Be You is not a party to any such payments and
                does not handle funds on behalf of its pros and clients. Pros
                who receive Marketplace Payments from clients on Choose To Be
                You must agree to the{" "}
                <a href="https://stripe.com/us/connect-account/legal">
                  Stripe Connected Account Agreement
                </a>
                , which includes the{" "}
                <a href="https://stripe.com/us/legal">
                  Stripe Services Agreement
                </a>
                . As a pro, by agreeing to these terms or continuing to operate
                as a Service Member on Choose To Be You, you agree to be bound
                by the Stripe Connected Account Agreement and Stripe Services
                Agreement, as those agreements may be modified by Stripe from
                time to time. As a condition of receiving payment processing
                services through Stripe, you agree to provide Choose To Be You
                with accurate and complete information about you and your
                business, and you authorize Choose To Be You to share this
                information and transaction information related to your use of
                the payment processing services provided by Stripe.
              </p>
              <p>
                As a client making a payment through Stripe, you agree to pay
                all amounts you owe when due using your preferred payment
                method. You further authorize Stripe to charge your preferred
                payment method for amounts you owe when they are due, whether
                they are recurring or one-time payments. You must keep all
                payment information you provide us up-to-date, accurate and
                complete. Do not share your payment card, bank account or other
                financial information with any other individuals. We take steps
                to secure all payment methods and other personal financial
                information, but we expressly disclaim any liability to you, and
                you agree to hold us harmless for any damages you may suffer as
                a result of the disclosure of your personal financial
                information to any unintended recipients.
              </p>
              <p>
                Choose To Be You may enable you to make payments using credit,
                debit, or prepaid cards, by linking your bank account, or by any
                other payment method we support. We reserve the right to cancel
                your ability to make payments with one or more of the payment
                methods you have authorized in our sole and absolute discretion.
              </p>
              <p>
                If you choose your bank account as your payment method, you
                authorize Stripe to make Automated Clearing House (“ACH”)
                withdrawals from your bank account, and to make any inquiries we
                consider necessary to validate any dispute involving payments
                you make to us or to a Member, which may include ordering a
                credit report and performing other credit checks or verifying
                the information you provide us against third-party databases.
                You authorize Choose To Be You or its provider to initiate one
                or more ACH debit entries (withdrawals) or the creation of an
                equivalent bank draft for the specified amount(s) from your bank
                account, and you authorize the financial institutions that holds
                your bank account to deduct such payments. You also authorize
                the bank that holds your bank account to deduct any such
                payments in the amounts and frequency designated in your
                Account.
              </p>
              <p>
                If your full order is not processed by us at the same time, you
                hereby authorize partial debits from your bank account, not to
                exceed the total amount of your order You agree to pay any ACH
                fees or fines you or we incur associated with transactions you
                authorize. This return fee will vary based on which state you
                are located in. The return fee may be added to your payment
                amount and debited from your bank account if we resubmit an ACH
                debit due to insufficient funds. We may initiate a collection
                process or legal action to collect any money owed to us. You
                agree to pay all our costs for such action, including any
                reasonable attorneys' fees. Federal law limits your liability
                for any fraudulent, erroneous, or unauthorized transactions from
                your bank account based on how quickly you report it to your
                financial institution. You must report any fraudulent, erroneous
                or unauthorized transactions to your bank no more than 60 days
                after the disputed transaction appeared on your bank account
                statement. Please contact your bank for more information about
                the policies and procedures that apply unauthorized transactions
                and the limits on your liability.
              </p>
              <p>
                Payment Disputes: All requests for chargebacks, errors, claims,
                refunds and disputes (“Payment Disputes”) will be subject to
                review by Choose To Be You in accordance with the rules
                applicable to the payment method you used to make the payment
                and will be in Choose To Be You’s absolute discretion. Choose To
                Be You is not liable to you under any circumstances for Payment
                Disputes we are unable to resolve in your favor. We will
                normally process your valid written Payment Dispute request
                within thirty (30) days after we receive it, unless a shorter
                period is required by law. You may file a Payment Dispute by
                emailing it to Choose To Be You support at
                lifestyle@choosetobeyou.com. If you close or deactivate your
                Account before we adjudicate your Payment Dispute, we will not
                be able to issue you any amounts you are owed. We will attempt
                to pay you any Payment Disputed amounts you are owed using the
                method with which you made the disputed payment, but we cannot
                guarantee that we will be able to do so if your payment method
                information is inaccurate, incomplete, or has been cancelled.
              </p>
              <p>
                If your actions result, or are likely to result in a Payment
                Dispute, a violation of these Terms of Use or create other risks
                to Choose To Be You or our payment processing partners, or if we
                determine that your Account has been used to engage in
                deceptive, fraudulent, or illegal activity, then we may
                permanently withhold any amounts owed to you in our sole
                discretion. If you have a past due balance due on any Account,
                or for any additional amounts that we determine you owe us, we
                may, without limiting any other rights or remedies: (a) charge
                one or more of your payment methods; (b) offset any amounts you
                owe us against amounts you may be owed; (c) invoice you for
                amounts due to us, which such amounts will be due upon receipt;
                (d) reverse or block any credits to your bank account; or (e)
                collect payment from you by any other lawful means.
              </p>
              <p>
                If you fail to make Marketplace Payments you owe when due, or if
                Choose To Be You is unable to charge one of your payment methods
                for any reason, Choose To Be You reserves all rights permissible
                under law to recover payment and all costs and expenses
                incurred, including reasonable attorneys' fees, in our pursuit
                of payment. You explicitly agree that all communication in
                relation to delinquent accounts or overdue payments will be made
                by electronic mail or by phone. Such communication may be made
                by Choose To Be You or by anyone on its behalf, including but
                not limited to a third-party collections agent.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Terms;
