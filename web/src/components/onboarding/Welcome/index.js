import React from "react";
import { Link } from "react-router-dom";
import { useSelector, shallowEqual, useDispatch } from "react-redux";

// components
import { FilledButton } from "../../../ui/Button";
import LinkButton from "../../../ui/LinkButton";

// actions
import { resendEmail } from "../../../store/actions/authActions";

// helpers
import { checkIsVerified } from "../../../helpers/general";

// icons
import { ReactComponent as MessageIcon } from "../../../assets/icons/message.svg";

import { auth } from "../../../config/fbConfig";

// styles
import "./styles.scss";

const Welcome = ({ goNext, showMessage }) => {
  const {
    profile: { firstName, isPro },
  } = useSelector(({ firebase }) => firebase, shallowEqual);
  const dispatch = useDispatch();
  const title = `hi ${firstName}, welcome to c2b`;

  const start = async () => {
    const isVerified = checkIsVerified();
    if (!isVerified) {
      await auth().currentUser.reload();
      const isVerifiedAfterReload = checkIsVerified();
      if (isVerifiedAfterReload) {
        goNext();
      } else {
        showMessage("Please, verify your email");
      }
    } else {
      goNext();
    }
  };

  const handleResendEmail = () => {
    dispatch(resendEmail());
  };

  return (
    <div className="welcome-root">
      <h2>{title}</h2>
      <h2 className="title-background">{title}</h2>
      <p>
        <span>Welcome to the onboarding process!</span>
        {!isPro && (
          <span>We need to get to know you to suggest the right pros.</span>
        )}
      </p>
      {!checkIsVerified() && (
        <div className="verification-email">
          <div className="msg">
            <MessageIcon />
            <span>A verification email has been sent to your inbox</span>
          </div>
          <div className="resend">
            <span>No email in your inbox folder?</span>
            <LinkButton onClick={handleResendEmail}>
              Let&apos;s Resend Email
            </LinkButton>
          </div>
        </div>
      )}
      <div>
        <span>
          Have any questions?
          <LinkButton>
            <Link to="/contact">Contact Us</Link>
          </LinkButton>
        </span>
        <FilledButton className="startbtn" onClick={start}>
          Let&apos;s Start
        </FilledButton>
      </div>
    </div>
  );
};

export default Welcome;
