import React, { useEffect, useState, useContext } from "react";
import { Table } from "semantic-ui-react";
import { useHistory } from "react-router-dom";

// images and videos
import howItWorksVideo from "../../assets/videos/how_it_works.mov";
import photoGroup from "../../assets/images/photoGroup1.jpg";
import photoGroup2 from "../../assets/images/photoGroup2.jpg";
import photoGroup3 from "../../assets/images/photoGroup3.jpg";
import photoGroup4 from "../../assets/images/photoGroup4.jpg";
import checkbox from "../../assets/images/icons/checkbox.png";

// icons
import { ReactComponent as Dumbbell } from "../../assets/icons/dumbbell.svg";
import { ReactComponent as Wave } from "../../assets/icons/wave.svg";

// context
import { SidebarContext } from "../../context/SidebarProvider";

// hooks
import { useBreakpoints } from "../../hooks/window/breakpoints";

// components
import StepArrow from "../stepArrow/StepArrow";
import CarouselBlock from "../carousel/CarouselBlock";
import CarouselBlockPro from "../carousel/CarouselBlockPro";
import MainSlider from "../carousel/MainSlider";
import TabsComponent from "../tabs/tabs.component";
import Tab from "../tabs/tab.component";
import { FilledButton } from "../../ui/Button";
import Carousel from "../../ui/Carousel";

// data
import { faqlistClient, faqlistPro } from "./Faq";
import "./styles/howItWorks.scss";

const Client = () => {
  const { openSidebar } = useContext(SidebarContext);

  const handleSignUpOpen = () => {
    openSidebar("signUpClient");
  };
  const history = useHistory();

  return (
    <div className="how-it-works page">
      <div className="how-it-works__background-wrapper">
        <div className="background-strip" />
        <div className="how-it-works__video-wrapper">
          <div>
            <div className="home__backgroundText">How it works</div>
            <h2 className="how-it-works__h2 home__text-main text--uppercase text--lg">
              How it works
            </h2>
          </div>
          <div>
            <p className="text--sm how-it-works__videoText">
              Experience health &amp; wellness in all aspects of your life; make
              it a lifestyle. Choose your category. Browse profiles. Book your
              pro. Reach your goal. It’s that convenient and easy. Choose to be
              you; work on being the best version of you.
            </p>
            <div className="video video--responsive video--small">
              <video preload="metadata" className="video__source" controls>
                <source type="video/mp4" src={howItWorksVideo} />
                Your browser does not support the video.
              </video>
            </div>
          </div>
        </div>
      </div>
      <div className="how-it-works__line">
        <div className="how-it-works__block">
          <div className="how-it-works__line-wrapper">
            <div className="how-it-works__line-block">
              <div className="how-it-works__img-wrapper">
                <Wave />
              </div>
              <div>
                <div className="how-it-works__line-title">Massage</div>
                <div className="how-it-works__line-text">
                  Say goodbye to stress, aches and pains. Say hello to a better
                  you with our customized massages
                </div>
              </div>
            </div>
            <div className="how-it-works__line-block">
              <div className="how-it-works__img-wrapper">
                <Dumbbell />
              </div>
              <div>
                <div className="how-it-works__line-title">Train</div>
                <div className="how-it-works__line-text">
                  Reach your fitness goals online or in-person with the support
                  of our expert personal trainers
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="how-it-works__background-wrapper-slider">
        <div className="background-strip" />
        <MainSlider />
      </div>
      <div className="how-it-works__table-wrapper">
        <div>
          <div>
            <div className="how-it-works__table__text-wrapper">
              <div className="home__text  home__backgroundText">
                С2b VS Competitors
              </div>
              <h2 className="home__text home__text-main text--uppercase text--lg">
                С2b VS Competitors
              </h2>
            </div>

            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Value &amp; Benefits</Table.HeaderCell>
                  <Table.HeaderCell>Competitors</Table.HeaderCell>
                  <Table.HeaderCell>C2Byou</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>Pricing</Table.Cell>
                  <Table.Cell>Strict</Table.Cell>
                  <Table.Cell>Flexible, with variety of pros</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Service Model</Table.Cell>
                  <Table.Cell>
                    Receive session through their business location
                  </Table.Cell>
                  <Table.Cell>
                    Receive session anywhere you choose; your home, pros’
                    business, or online
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Availability</Table.Cell>
                  <Table.Cell>Dependant on their schedule</Table.Cell>
                  <Table.Cell>When you want to schedule</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Pro Review Rating System</Table.Cell>
                  <Table.Cell>Nonexistent</Table.Cell>
                  <Table.Cell>Exists</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>100% Satisfaction</Table.Cell>
                  <Table.Cell>Maybe</Table.Cell>
                  <Table.Cell>Customer Care team is here for you</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>Hours of Operation</Table.Cell>
                  <Table.Cell>Typically 6 am - 10 pm</Table.Cell>
                  <Table.Cell>Till the internet stops working</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </div>
        </div>
      </div>
      <div className="how-it-works__frequent-block">
        <div className="home__text  home__backgroundText">
          Frequent questions
        </div>
        <h2 className="home__text home__text-main text--uppercase text--lg">
          Frequent questions
        </h2>
        <div className="how-it-works__carousel">
          <CarouselBlock items={faqlistClient.slice(0, 3)} />
        </div>
        <div className="how-it-works-pro__frequent-button">
          <FilledButton
            onClick={(e) => {
              e.preventDefault();
              history.push("/faq");
            }}
          >
            View More
          </FilledButton>
        </div>
      </div>
      <div className="how-it-works__sign-up-block">
        <div className="how-it-works__sign-up-text">
          Want to join our platform?
        </div>
        <div className="how-it-works__sign-up-button">
          <FilledButton onClick={handleSignUpOpen}>Sign Up</FilledButton>
        </div>
      </div>
    </div>
  );
};

const settings = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
};

const descSettings = {
  ...settings,
  dots: true,
  centerMode: true,
  centerPadding: "20%",
};

const Pro = () => {
  const { openSidebar } = useContext(SidebarContext);
  const { isBreakpointL } = useBreakpoints();

  const handleJoinAsProOpen = () => {
    openSidebar("joinAsPro");
  };
  const history = useHistory();

  return (
    <div className="how-it-works how-it-works-pro page">
      <div className="how-it-works-pro__section">
        <div>
          <div className="home__backgroundText">How it works</div>
          <h2 className="how-it-works__h2 home__text-main text--uppercase text--lg">
            How it works
          </h2>
        </div>
        <div className="how-it-works-pro__text-wrapper">
          <div className="how-it-works-pro__text">
            <h2>Pro Standards</h2>
            <div className="how-it-works-pro__description">
              Being a C2Byou Professional (pro) is more than just a way to get
              clients. A C2Byou pro is here to serve, grow in the industry, and
              educate their clients. Our pros are given the opportunity to help
              someone live a happier and healthier life, which is not only
              significant to them, but also to their loved ones. Always keep in
              mind that as a C2Byou Pro, you will be responsible for providing a
              5-star experience to every client.
            </div>
            <h2>Match Generation</h2>
            <div className="how-it-works-pro__description">
              Complete and submit your application and profile. Once accepted,
              your profile will be visible to prospective clients who use our
              search engine.
            </div>
            <h2>Pricing</h2>
            <div className="how-it-works-pro__description">
              Pros have the flexibility to set their own rates. There is a
              minimum of $50 per hour.
            </div>
            <h2>Insurance</h2>
            <div className="how-it-works-pro__description">
              We encourage all professionals to obtain insurance to be protected
              against any client claims. Here are a few suggested insurers:
              IdeaFit, NASM, Hiscox
            </div>
          </div>
          {isBreakpointL ? (
            <div className="how-it-works-pro__images-wrapper">
              <div className="how-it-works-pro__images">
                <img
                  className="how-it-works-pro__image-first"
                  src={photoGroup}
                  alt="-group"
                />
                <img src={photoGroup3} alt="group-3" />
              </div>
              <div className="how-it-works-pro__images">
                <img src={photoGroup2} alt="group-2" />
                <img src={photoGroup4} alt="group-4" />
              </div>
            </div>
          ) : (
            <Carousel settings={descSettings}>
              <div className="slider-wrapper__item">
                <img src={photoGroup} />
              </div>
              <div className="slider-wrapper__item">
                <img src={photoGroup2} alt="group-2" />
              </div>
              <div className="slider-wrapper__item">
                <img src={photoGroup3} alt="group-3" />
              </div>
              <div className="slider-wrapper__item">
                <img src={photoGroup4} alt="group-4" />
              </div>
            </Carousel>
          )}
        </div>
      </div>
      <div className="how-it-works-pro__steps-wrapper">
        <div className="how-it-works-pro__h1-text">
          <div className="home__text  home__backgroundText">
            Our process for pro
          </div>
          <h2 className="home__text home__text-main text--uppercase text--lg">
            Our process for pro
          </h2>
        </div>

        {isBreakpointL ? (
          <>
            <div className="how-it-works-pro__steps">
              <div className="how-it-works-pro__step">1</div>
              <div className="how-it-works-pro__dots-line">
                <StepArrow />
              </div>
              <div className="how-it-works-pro__step">2</div>
              <div className="how-it-works-pro__dots-line">
                <StepArrow />
              </div>
              <div className="how-it-works-pro__step">3</div>
            </div>
            <div className="how-it-works-pro__steps-description">
              <div>
                <div className="how-it-works-pro__steps-description-item">
                  Create a free profile
                </div>
                <div className="how-it-works-pro__steps-description-text">
                  There's no fee to join. build a winning profile by adding a
                  detailed description, pricing, and specialties
                </div>
              </div>
              <div>
                <div className="how-it-works-pro__steps-description-item">
                  Wait to be approved
                </div>
                <div className="how-it-works-pro__steps-description-text">
                  Once you're finished, our team will review it and respond
                  within 1-2 business days. Make sure your application is
                  complete before submitting!
                </div>
              </div>
              <div>
                <div className="how-it-works-pro__steps-description-item">
                  See customers come
                </div>
                <div className="how-it-works-pro__steps-description-text">
                  If the customer thinks you're a great fit, they'll request to
                  book you through our platform
                </div>
              </div>
            </div>
          </>
        ) : (
          <div id="carousel-in-pro">
          <Carousel settings={settings} >
            <div className="slider-wrapper__item">
              <div>
                <div className="how-it-works-pro__dots-line">
                  <StepArrow />
                </div>
                <div className="how-it-works-pro__step">1</div>
                <div className="how-it-works-pro__dots-line" />
              </div>
              <div className="how-it-works-pro__steps-description">
                <div>
                  <div className="how-it-works-pro__steps-description-item">
                    Create a free profile
                  </div>
                  <div className="how-it-works-pro__steps-description-text">
                    There's no fee to join. build a winning profile by adding a
                    detailed description, pricing, and specialties
                  </div>
                </div>
              </div>
            </div>
            <div className="slider-wrapper__item">
              <div>
                <div className="how-it-works-pro__dots-line">
                  <StepArrow />
                </div>
                <div className="how-it-works-pro__step">2</div>
                <div className="how-it-works-pro__dots-line" />
              </div>
              <div className="how-it-works-pro__steps-description">
                <div>
                  <div className="how-it-works-pro__steps-description-item">
                    Wait to be approved
                  </div>
                  <div className="how-it-works-pro__steps-description-text">
                    Once you're finished, our team will review it and respond
                    within 1-2 business days. Make sure your application is
                    complete before submitting!
                  </div>
                </div>
              </div>
            </div>
            <div className="slider-wrapper__item">
              <div>
                <div className="how-it-works-pro__dots-line">
                  <StepArrow />
                </div>
                <div className="how-it-works-pro__step">3</div>
                <div className="how-it-works-pro__dots-line" />
              </div>
              <div className="how-it-works-pro__steps-description">
                <div>
                  <div className="how-it-works-pro__steps-description-item">
                    See customers come
                  </div>
                  <div className="how-it-works-pro__steps-description-text">
                    If the customer thinks you're a great fit, they'll request
                    to book you through our platform
                  </div>
                </div>
              </div>
            </div>
          </Carousel>
          </div>
        )}
      </div>
      <div className="how-it-works-pro__frequent-questions">
        <div className="how-it-works-pro__questions-wrapper">
          <div className="home__text  home__backgroundText">
            Frequent questions
          </div>
          <h2 className="home__text home__text-main text--uppercase text--lg">
            Frequent questions
          </h2>
          <div className="how-it-works-pro__slide-wrapper">
            <CarouselBlockPro items={faqlistPro.slice(0, 3)} />
          </div>
          <div>
            <div className="how-it-works-pro__frequent-button">
              <FilledButton
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/faq");
                }}
              >
                View More
              </FilledButton>
            </div>
          </div>
        </div>

        <div className="how-it-works-pro__benefits home__benefits-block">
          <div className="container">
            <div className="row">
              <div className="col how-it-works-pro__benefits-block-wrapper">
                <div className="how-it-works-pro__benefits-text-wrapper">
                  <div className="home__text  home__backgroundText how-it-works-pro__background-text">
                    Why Choose To Be You?
                  </div>
                  <h2 className="home__text home__text-main text--uppercase text--lg how-it-works-pro__main-text">
                    Why Choose To Be You?
                  </h2>
                </div>

                <div className="how-it-works-pro__benefits-block">
                  <div className="home__pro__benefits-wrapper">
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Availability calendar</div>
                    </div>
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Free digital advertising</div>
                    </div>
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Earn additional income</div>
                    </div>
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Secure online booking and payments</div>
                    </div>
                  </div>

                  <div className="home__pro__benefits-wrapper-second">
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Ratings reviews</div>
                    </div>
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Recognition for your efforts</div>
                    </div>
                    <div className="home__pro__benefits-item">
                      <img src={checkbox} alt="checkbox" />
                      <div>Decide where you want to work</div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="how-it-works-pro__button">
                <FilledButton onClick={handleJoinAsProOpen}>
                  Join as Pro
                </FilledButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

function HowItWorks() {
  const [key, setKey] = useState(0);

  const location = window.location.href;
  const locationSplit = location.split("?");
  const lastItem = locationSplit[locationSplit.length - 1];

  useEffect(() => {
    if (lastItem === "pro") {
      setKey(1);
    }
  }, [lastItem]);

  return (
    <>
      <div className="how-it-works page">
        <div className="container--top-bottom-padding how-it-works__picture" />
        <div>
          <TabsComponent
            activeKey={key}
            onSelect={(k) => {
              setKey(k);
            }}
          >
            <Tab eventKey={0} title="Client">
              <Client />
            </Tab>
            <Tab eventKey={1} title="Pro">
              <Pro />
            </Tab>
          </TabsComponent>
        </div>
      </div>
    </>
  );
}

export default HowItWorks;
