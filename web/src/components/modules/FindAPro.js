import React, { useContext, useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";

import queryString from "query-string";

// context
import { FilterContext } from "../../context/FilterProvider";

// hooks
import { useBreakpoints } from "../../hooks/window/breakpoints";

// icons
import { ReactComponent as LoupeIcon } from "../../assets/icons/loupe.svg";

// components
import Filter from "../filter";
import ProCard from "../search/ProCard";
import Loading from "./Loading";
import CircularProgress from "../../ui/CircularProgress";

import "./findaPro.scss";

const Pros = () => {
  const { filteredPros, checkIsBookingsLoaded, byNearbyZipcodes } = useContext(
    FilterContext
  );
  const [selectedFilters, setSelectedFilters] = useState(false);
  const [hiddenPros, setHiddenPros] = useState([]);
  const { isBreakpointL } = useBreakpoints();
  const location = useLocation();
  const currentSearch = location.search;
  const { query: appliedFilteres } = queryString.parseUrl(currentSearch);

  useEffect(() => {
    const handleScrollBlockVisibible = () => {
      if (!isBreakpointL && window.scrollY >= 1000) {
        const filledControls = Object.keys(appliedFilteres);
        setSelectedFilters(filledControls.length);
      } else {
        setSelectedFilters(null);
      }
    };
    window.addEventListener("scroll", handleScrollBlockVisibible);

    return () => {
      window.removeEventListener("scroll", handleScrollBlockVisibible);
    };
  }, [isBreakpointL, appliedFilteres]);

  const scrollToFilter = () => {
    window.scrollTo({ top: 150, left: 0, behavior: "smooth" });
  };

  const setHiddenfunct = (id) => {
   
   console.log(id)
  };



  if (filteredPros) {
    return (
      <div className="container find-pro__container">
        {!checkIsBookingsLoaded() ? (
          <div className="progress">
            <CircularProgress size={20} />
          </div>
        ) : (
          <>
            {typeof selectedFilters === "number" && (
              <button
                type="button"
                className="filter-scroll-block"
                onClick={scrollToFilter}
              >
                ({selectedFilters}) selected filters
              </button>
            )}

            {byNearbyZipcodes && !!filteredPros.length && (
              <span className="find-pro__no-pros-message">
                No pros found in specified location. Nearby pros are listed
                below:
              </span>
            )}

            <div className="row">
              {!!filteredPros.length &&
                filteredPros.map((uid, idx) => {
                
                    return (
                      
                        <ProCard key={idx} pro={uid}  id={uid} />
                    
                    );
                })}
              {!filteredPros.length && (
                <div className="find-pro__not-found">
                  <div className="find-pro__icon-wrapper">
                    <LoupeIcon />
                  </div>
                  <h2 className="find-pro__title">No Pros Found</h2>
                  <div className="find-pro__subtitle">
                    Please, try another search criterias...
                  </div>
                </div>
              )}
            </div>
          </>
        )}
      </div>
    );
  }
  return <Loading />;
};

const FindAPro = () => {
  return (
    <div className="find-pro">
      <div className="container--top-bottom-padding how-it-works__picture" />
      <Filter />
      <Pros />
    </div>
  );
};

export default FindAPro;
