import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

// actions
import { onboardingAgain } from "../../store/actions/authActions";

const Resubmit = () => {
  const dispatch = useDispatch();
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);

  useEffect(() => {
    if (profile.isDeclined) {
      dispatch(onboardingAgain());
    }
  }, [dispatch, profile]);

  return <Redirect to="/onboarding" />;
};

export default Resubmit;
