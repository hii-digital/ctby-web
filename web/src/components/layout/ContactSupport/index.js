import React from "react";

// icons
import { ReactComponent as PhoneIcon } from "../../../assets/icons/phone.svg";
import { ReactComponent as MailIcon } from "../../../assets/icons/mail.svg";

// styles
import "./styles.scss";

const ContactSupport = () => {
  return (
    <div className="contact-items">
      <p className="contact-item">
        <MailIcon />
        <a href="mailto:lifestyle@choosetobeyou.com">Contact Support</a>
      </p>
      <p className="contact-item">
        <PhoneIcon />
        <a href="tel:+14078900587">407-890-0587</a>
      </p>
    </div>
  );
};

export default ContactSupport;
