import React from "react";
import MuiSlider from "@material-ui/core/Slider";
import { makeStyles } from "@material-ui/core/styles";

import colors from "./colors";

const useStyles = makeStyles(() => ({
  root: {
    padding: "8px 0px",
    color: colors.ORANGE_ED9,
    "& .MuiSlider-track": {
      backgroundColor: colors.GREY_E0D,
    },
    "& .MuiSlider-thumb": {
      color: colors.WHITE_FFF,
      border: `1px solid ${colors.GREY_E0D}`,
    },
    "& .MuiSlider-thumb:hover, .MuiSlider-active": {
      boxShadow: "none",
    },
  },
}));

const Slider = ({ id, onChange, ...props }) => {
  const classes = useStyles();

  const handleChange = (e, value) => {
    onChange(id, value);
  };

  return (
    <MuiSlider className={classes.root} {...props} onChange={handleChange} />
  );
};

export default Slider;
