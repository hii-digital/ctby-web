import React from "react";

// icons
import { ReactComponent as CrossIcon } from "../../../assets/icons/cross.svg";

const RenderImage = ({ urlPath, children = null }) => {
  return (
    <div className="profile-image__current">
      {urlPath ? <img alt="avatar" src={urlPath} /> : null}
      {!!urlPath && children}
    </div>
  );
};

const RemoveButton = ({ profValue, deleteImage }) => {
  const handleDelete = () => {
    deleteImage({
      type: "SET_LICENSE_IMAGE",
      profession: profValue,
      payloads: {
        preview: null,
        image: null,
        progress: 0,
        licenseURL: "",
      },
    });
  };
  return (
    <button type="button" onClick={handleDelete}>
      <CrossIcon />
    </button>
  );
};

RenderImage.RemoveButton = RemoveButton;

export default RenderImage;
