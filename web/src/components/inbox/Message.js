import React from "react";
import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";

// hooks
import { useUserHook } from "../../hooks/users/useUserHook";

const Message = ({ time, content, sender, participants }) => {
  const { auth, profile } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );

  const otherUser = participants.find((id) => id !== auth.uid);
  const userData = useUserHook(otherUser);

  const renderUser = (user) => {
    if (!user) {
      return null;
    }
    return (
      <>
        {user.photoURL ? (
          <img src={user.photoURL} alt="user" />
        ) : (
          <div className="initials">{user.initials}</div>
        )}
        <p>
          {user.firstName} {user.lastName[0]}.
        </p>
      </>
    );
  };

  return (
    <div
      key={time}
      className={`message__item ${
        sender !== auth.uid ? `message__item--receiver` : ""
      }`}
    >
      <div className="message__content">
        <div className="message__content-sent">
          <p>{content}</p>
        </div>
        <div className="message__content-time">
          <p>{moment(time).format("dddd, MMMM Do YYYY h:mm A")}</p>
        </div>
      </div>
      <div className="message__sender">
        {auth.uid === sender ? renderUser(profile) : renderUser(userData)}
      </div>
    </div>
  );
};

export default Message;
