import React from "react";
import cx from "classnames";

import "./styles.scss";

const Divider = ({ className, ...rest }) => {
  return <hr className={cx("divider-root", className)} {...rest} />;
};

export default Divider;
