import { db } from "../../config/fbConfig";
import { startLoading, stopLoading } from "./loadingActions";

export const clearErrorMessage = () => {
  return (dispatch) => {
    dispatch({ type: "CLEAR_ERROR_MESSAGE" });
  };
};

export const setErrorMessage = (message) => {
  return (dispatch) => {
    dispatch({ type: "SIGNUP_ERROR", err: { message } });
  };
};

export const signOut = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch({ type: "SIGNOUT_SUCCESS" });
      });
  };
};

export const signIn = (credentials, formType) => {
  return (dispatch, getState, { getFirebase }) => {
    dispatch({ type: "RESET_AUTH_VALID" });
    const firebase = getFirebase();
    firebase
      .auth()
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(({ user }) => {
        const docRef = db.collection("users").doc(user.uid);
        docRef.get().then((doc) => {
          if (doc.exists) {
            const {
              isPro,
              isOnboardingClientCompleted,
              isApproved,
            } = doc.data();
            const proFromScratch = isPro && !isOnboardingClientCompleted;
            const approvedClientToPro =
              !!isOnboardingClientCompleted && isPro && isApproved;

            if (
              formType === "pro" &&
              ((!proFromScratch && !approvedClientToPro) || !isPro)
            ) {
              dispatch(setErrorMessage("No pro with such credentials."));
              dispatch(signOut());
            } else if (
              formType === "client" &&
              (proFromScratch || approvedClientToPro)
            ) {
              dispatch(setErrorMessage("No client with such credentials."));
              dispatch(signOut());
            } else {
              dispatch({ type: "LOGIN_SUCCESS" });
            }
          }
        });
      })
      .catch((err) => {
        dispatch({ type: "LOGIN_ERROR", err });
      });
  };
};

export const signUp = (newUser) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase
      .auth()
      .createUserWithEmailAndPassword(newUser.email, newUser.password)
      .then((response) => {
        const currentUser = response.user;
        currentUser
          .sendEmailVerification()
          .then(() => {
            dispatch({ type: "SIGNUP_SUCCESS" });
            // Email sent.
            return firestore
              .collection("users")
              .doc(response.user.uid)
              .set({
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                initials: newUser.firstName[0] + newUser.lastName[0],
                isPro: false,
                isProPremium: false,
                clientStep: 0,
                uid: response.user.uid,
              });
          })
          .catch((error) => {
            // An error happened.
            console.log("error", error);
          });
      })
      .catch((err) => {
        dispatch({ type: "SIGNUP_ERROR", err });
      });
  };
};

export const resendEmail = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const { currentUser } = firebase.auth();
    currentUser
      .sendEmailVerification()
      .then(() => {
        dispatch({ type: "SIGNUP_SUCCESS" });
        // Email sent.
      })
      .catch((error) => {
        // An error happened.
        console.log("error", error);
      });
  };
};

export const signInWithFacebook = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    const fb = new firebase.auth.FacebookAuthProvider();
    firebase
      .auth()
      .signInWithPopup(fb)
      .then(async ({ user }) => {
        const userExist = await db.collection("users").doc(user.uid).get();

        if (userExist.exists) {
          dispatch({ type: "LOGIN_SUCCESS" });
        } else {
          user
            .sendEmailVerification()
            .then(() => {
              dispatch({ type: "SIGNUP_SUCCESS" });
              return firestore
                .collection("users")
                .doc(user.uid)
                .set({
                  firstName: user.displayName,
                  initials: user.displayName[0] + user.displayName[1],
                  isPro: false,
                  photoURL: user.photoURL,
                  isProPremium: false,
                  googleOrFacebook: true,
                  clientStep: 0,
                });
            })
            .catch((err) => {
              dispatch({ type: "SIGNUP_ERROR", err });
            });
        }
      })
      .catch((err) => {
        dispatch({ type: "LOGIN_ERROR", err });
      });
  };
};

export const signInWithGoogle = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    const google = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(google)
      .then(async (result) => {
        const userExist = await db
          .collection("users")
          .doc(result.user.uid)
          .get();
        if (userExist.exists) {
          dispatch({ type: "LOGIN_SUCCESS" });
        } else {
          const { user } = result;
          const userId = result.user.uid;
          const userFirstName = result.additionalUserInfo.profile.given_name;
          const userLastName = result.additionalUserInfo.profile.family_name;
          const userImageUrl = result.additionalUserInfo.profile.picture;
          user
            .sendEmailVerification()
            .then(() => {
              dispatch({ type: "SIGNUP_SUCCESS" });
              return firestore
                .collection("users")
                .doc(userId)
                .set({
                  firstName: userFirstName,
                  lastName: userLastName,
                  initials: userFirstName[0] + userLastName[0],
                  isPro: false,
                  photoURL: userImageUrl,
                  isProPremium: false,
                  googleOrFacebook: true,
                  clientStep: 0,
                });
            })
            .catch((err) => {
              dispatch({ type: "SIGNUP_ERROR", err });
            });
        }
      })
      .catch((err) => {
        dispatch({ type: "LOGIN_ERROR", err });
      });
  };
};

export const clientToPro = () => {
  return async (dispatch, getState, { getFirestore, getFirebase }) => {
    const firestore = getFirestore();
    const firebase = getFirebase();
    const userID = getState().firebase.auth.uid;
    const func = firebase.functions().httpsCallable("createStripeAccount");
    const { data } = await func({
      userId: userID,
    });
    firestore.collection("users").doc(userID).set(
      {
        isPro: true,
        isApproved: false,
        stripeId: data.account.id,
        isStripeOnboardingCompleted: false,
      },
      { merge: true }
    );
  };
};

export const signUpPro = (newUser) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase
      .auth()
      .createUserWithEmailAndPassword(newUser.email, newUser.password)
      .then((response) => {
        const currentUser = response.user;
        const newUserID = response.user.uid;

        currentUser
          .sendEmailVerification()
          .then(async () => {
            const func = firebase
              .functions()
              .httpsCallable("createStripeAccount");
            const { data } = await func({
              userId: newUserID,
            });
            return firestore
              .collection("users")
              .doc(newUserID)
              .set({
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                initials: newUser.firstName[0] + newUser.lastName[0],
                isPro: true,
                isProPremium: false,
                isApproved: false,
                uid: response.user.uid,
                onboardingCompleted: false,
                proStep: 0,
                stripeId: data.account.id,
                isStripeOnboardingCompleted: false,
              })
              .then(() => dispatch({ type: "SIGNUP_SUCCESS" }));
          })
          .catch((error) => {
            // An error happened.
            console.log("error", error);
          });
      })
      .catch((err) => {
        dispatch({ type: "SIGNUP_ERROR", err });
      });
  };
};

export const passwordReset = (email) => {
  return async (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const currentUser = await firebase.auth();
    try {
      await currentUser.sendPasswordResetEmail(email);
      return Promise.resolve({
        isSent: true,
        msg:
          "The letter with the instructions was sent to your email. Please check it.",
      });
    } catch (error) {
      return Promise.resolve({ isSent: false, msg: error.message });
    }
  };
};

export const deleteAccount = (password) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const user = firebase.auth().currentUser;
    const credential = firebase.auth.EmailAuthProvider.credential(
      user.email,
      password
    );
    user
      .reauthenticateWithCredential(credential)
      .then(() => {
        // User re-authenticated.
        user
          .delete()
          .then((response) => {
            // User deleted
            console.log(response);
          })
          .catch((err) => {
            // user error
            dispatch({ type: "SIGNUP_ERROR", err });
          });
      })
      .catch((error) => {
        // An error happened.
      });
  };
};

export const onboardingAgain = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(startLoading());
    const firebase = getFirebase();
    const user = firebase.auth().currentUser;
    const firestore = getFirestore();
    firestore
      .collection("users")
      .doc(user.uid)
      .update({
        onboardingCompleted: false,
        proStep: 1,
      })
      .then(() => {
        dispatch(stopLoading());
      })
      .catch((error) => {
        dispatch(stopLoading());
        console.log("onboardingAgain", error);
      });
  };
};

export const upgrade = (upgradeParams) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const userID = getState().firebase.auth.uid;

    console.log("upgrade called", upgradeParams);

    firestore
      .collection("users")
      .doc(userID)
      .update({
        ...upgradeParams,
      })
      .then(() => {
        console.log("success");
        dispatch({ type: "CREATE_INTERACTION", upgradeParams });
      })
      .catch((error) => {
        console.log("nah");
        dispatch({ type: "CREATE_INTERACTION_ERROR", error });
      });
  };
};

export const downgrade = (downgradeParams) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const userID = getState().firebase.auth.uid;

    console.log("downgrade called", downgradeParams);

    firestore
      .collection("users")
      .doc(userID)
      .update({
        ...downgradeParams,
      })
      .then(() => {
        console.log("success");
        dispatch({ type: "CREATE_INTERACTION", downgradeParams });
      })
      .catch((error) => {
        console.log("nah");
        dispatch({ type: "CREATE_INTERACTION_ERROR", error });
      });
  };
};

export const approveProfile = (userID) => {
  return (dispatch, getState, { getFirestore }) => {
    const firestore = getFirestore();
    firestore
      .collection("users")
      .doc(userID)
      .update({
        isApproved: true,
      })
      .catch((error) => {
        console.log("approveProfile error", error);
      });
  };
};

export const declineProfile = (proUID, message) => {
  return (dispatch, getState, { getFirestore }) => {
    const firestore = getFirestore();
    firestore
      .collection("users")
      .doc(proUID)
      .update({
        isApproved: false,
        declineMessage: message,
        isDeclined: true,
        reSubmit: false,
        proStep: 1,
      })
      .then(async () => {
        dispatch({ type: "DECLINE_INTERACTION", proUID });
      })
      .catch((error) => {
        dispatch({ type: "DECLINE_INTERACTION_ERROR", error });
      });
  };
};
