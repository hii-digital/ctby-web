import { useReducer, useEffect, useState, useMemo } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";

// actions
import {
  updateUser,
  updateProfessionNode,
  removeLicenseImage,
} from "../../store/actions/onboardingActions";

import { professionsReducer } from "./reducer";

// helpers
import { fileStorage, db } from "../../config/fbConfig";

const PROFESSIONS = [
  { value: "fitnessTrainer", text: "Fitness Trainer" },
  { value: "massageTherapist", text: "Massage Therapist" },
];

export const useLicenseProfileHook = () => {
  const {
    profile,
    auth: { uid },
  } = useSelector(({ firebase }) => firebase, shallowEqual);

  const dispatch = useDispatch();

  const [profileImageFile, setProfileImageFile] = useState("");
  const [profileImagePreview, setProfileImagePreview] = useState("");

  const { professions: professionsFS } = useSelector(
    ({ firestore }) => firestore.data,
    shallowEqual
  );

  const [professions, dispatchProfessions] = useReducer(professionsReducer, {
    firstProfession: null,
    secondProfession: null,
  });

  const { firstProfession, secondProfession } = professions;

  const firstFSProfession = useMemo(() => {
    if (professionsFS) {
      const professionsKeys = Object.keys(professionsFS);
      return professionsKeys.find(
        (key) => professionsFS[key].type === "primary"
      );
    }
    return null;
  }, [professionsFS]);

  const secondFSProfession = useMemo(() => {
    if (professionsFS) {
      const professionsKeys = Object.keys(professionsFS);
      return professionsKeys.find(
        (key) => professionsFS[key].type === "secondary"
      );
    }
    return null;
  }, [professionsFS]);

  const setInitial = (profession, data) => {
    const payloads = {
      value: data?.value || "",
      zipCode: data?.zipCode || "",
      addressLineFirst: data?.addressLineFirst || "",
      addressLineSecond: data?.addressLineSecond || "",
      licenseImage: data?.licenseImage,
      licensePreview: data?.licensePreview,
    };
    dispatchProfessions({
      type: "SET_INITIAL",
      profession,
      payloads,
    });
  };

  useEffect(() => {
    if (profile.photoURL) {
      setProfileImageFile(profile.photoURL);
      setProfileImagePreview(profile.photoURL);
    }
    if (firstFSProfession) {
      const firstProData = {
        value: firstFSProfession,
        zipCode: professionsFS[firstFSProfession].zipCode,
        addressLineFirst: professionsFS[firstFSProfession].addressLineFirst,
        addressLineSecond: professionsFS[firstFSProfession].addressLineSecond,
        licenseImage: professionsFS[firstFSProfession].licenseURL,
        licensePreview: professionsFS[firstFSProfession].licenseURL,
      };
      setInitial("firstProfession", firstProData);

      if (secondFSProfession) {
        const secondProData = {
          value: secondFSProfession,
          zipCode: professionsFS[secondFSProfession].zipCode,
          addressLineFirst: professionsFS[secondFSProfession].addressLineFirst,
          addressLineSecond:
            professionsFS[secondFSProfession].addressLineSecond,
          licenseImage: professionsFS[secondFSProfession].licenseURL,
          licensePreview: professionsFS[secondFSProfession].licenseURL,
        };
        setInitial("secondProfession", secondProData);
      }
    } else {
      setInitial("firstProfession", { value: PROFESSIONS[0].value });
    }
  }, [firstFSProfession, professionsFS, profile.photoURL, secondFSProfession]);

  const setProfileImage = ({ file, preview }) => {
    setProfileImageFile(file);
    setProfileImagePreview(preview);
  };

  const removeProfileImage = () => {
    setProfileImageFile("");
    setProfileImagePreview("");
  };

  const setProfession = (profession, { value }) => {
    dispatchProfessions({
      type: "SET_PROFESSION",
      profession,
      payloads: { value },
    });
  };

  const setZipcode = (profession, zipCode) => {
    dispatchProfessions({
      type: "SET_ZIP_CODE",
      profession,
      payloads: { zipCode },
    });
  };

  const setAddressLineFirst = (profession, address) => {
    dispatchProfessions({
      type: "SET_ADDRESS_LINE_FIRST",
      profession,
      payloads: { address },
    });
  };

  const setAddressLineSecond = (profession, address) => {
    dispatchProfessions({
      type: "SET_ADDRESS_LINE_SECOND",
      profession,
      payloads: { address },
    });
  };

  const setLicenseImg = (profession, data) => {
    const { file: licenseImage, preview: licensePreview } = data;
    dispatchProfessions({
      type: "SET_LICENSE_IMAGE",
      profession,
      payloads: {
        licenseImage,
        licensePreview,
      },
    });
  };

  const deleteLicenseImg = (profession) => {
    dispatchProfessions({
      type: "SET_LICENSE_IMAGE",
      profession,
      payloads: {
        licenseImage: "",
        licensePreview: "",
      },
    });
  };

  const resetProfession = (profession) => {
    dispatchProfessions({
      type: "RESET",
      profession,
    });
  };

  const uploadLicenseImgToStorage = async (profession) => {
    const { value, licenseImage } = professions[profession];
    await fileStorage
      .ref(`users/${uid}/license/`)
      .child(value)
      .put(licenseImage);

    const url = await fileStorage
      .ref(`users/${uid}/license`)
      .child(value)
      .getDownloadURL();

    return url;
  };

  const submit = async ({
    step,
    firstProfessionZipCodeData,
    secondProfessionZipCodeData,
  }) => {
    const profileUpdates = {};
    profileUpdates.proStep = step + 1;
    if (profileImageFile !== profile.photoURL) {
      await fileStorage.ref(`users/${uid}/profileImage`).put(profileImageFile);
      profileUpdates.photoURL = await fileStorage
        .ref(`users/${uid}/profileImage`)
        .getDownloadURL();
    }

    const professionUpdates = {};
    professionUpdates[firstProfession.value] = [];

    const { text: firstProfessionName } = PROFESSIONS.filter(
      ({ value }) => firstProfession.value === value
    )[0];
    professionUpdates[firstProfession.value].push({
      node: "name",
      value: firstProfessionName,
    });

    if (
      firstProfession.licensePreview !==
      professionsFS?.[firstFSProfession]?.licenseURL
    ) {
      const isLicenseWasRemoved = !firstProfession.licensePreview;
      if (isLicenseWasRemoved) {
        dispatch(removeLicenseImage(uid, firstProfession.value));
      } else {
        const licenseURL = await uploadLicenseImgToStorage("firstProfession");

        professionUpdates[firstProfession.value].push({
          node: "licenseURL",
          value: licenseURL,
        });
      }
    }

    if (
      firstProfession.zipCode !== professionsFS?.[firstFSProfession]?.zipCode
    ) {
      const { city, state } = firstProfessionZipCodeData;
      professionUpdates[firstProfession.value].push({
        node: "zipCode",
        value: firstProfession.zipCode,
      });
      professionUpdates[firstProfession.value].push({
        node: "city",
        value: city,
      });
      professionUpdates[firstProfession.value].push({
        node: "state",
        value: state,
      });
    }

    if (
      firstProfession.addressLineFirst !==
      professionsFS?.[firstFSProfession]?.addressLineFirst
    ) {
      professionUpdates[firstProfession.value].push({
        node: "addressLineFirst",
        value: firstProfession.addressLineFirst,
      });
    }

    if (
      firstProfession.addressLineSecond !==
      professionsFS?.[firstFSProfession]?.addressLineSecond
    ) {
      professionUpdates[firstProfession.value].push({
        node: "addressLineSecond",
        value: firstProfession.addressLineSecond,
      });
    }

    if (firstProfession.value !== firstFSProfession) {
      const primaryProf = await db
        .collection("users")
        .doc(uid)
        .collection("professions")
        .where("type", "==", "primary");

      await primaryProf.get().then((snap) => {
        snap.forEach((doc) => {
          doc.ref.delete();
        });
      });

      professionUpdates[firstProfession.value].push({
        node: "type",
        value: "primary",
      });
    }

    if (secondProfession) {
      professionUpdates[secondProfession.value] = [];

      const { text: secondProfessionName } = PROFESSIONS.filter(
        ({ value }) => secondProfession.value === value
      )[0];
      professionUpdates[secondProfession.value].push({
        node: "name",
        value: secondProfessionName,
      });

      if (
        secondProfession.zipCode !==
        professionsFS?.[secondFSProfession]?.zipCode
      ) {
        const { city, state } = secondProfessionZipCodeData;
        professionUpdates[secondProfession.value].push({
          node: "zipCode",
          value: secondProfession.zipCode,
        });
        professionUpdates[secondProfession.value].push({
          node: "city",
          value: city,
        });
        professionUpdates[secondProfession.value].push({
          node: "state",
          value: state,
        });
      }

      if (
        secondProfession.addressLineFirst !==
        professionsFS?.[secondProfession]?.addressLineFirst
      ) {
        professionUpdates[secondProfession.value].push({
          node: "addressLineFirst",
          value: secondProfession.addressLineFirst,
        });
      }

      if (
        secondProfession.addressLineSecond !==
        professionsFS?.[secondProfession]?.addressLineSecond
      ) {
        professionUpdates[secondProfession.value].push({
          node: "addressLineSecond",
          value: secondProfession.addressLineSecond,
        });
      }

      if (secondProfession.value !== secondFSProfession) {
        professionUpdates[secondProfession.value].push({
          node: "type",
          value: "secondary",
        });
      }

      if (
        secondProfession.licensePreview !==
        professionsFS?.[secondFSProfession]?.licenseURL
      ) {
        const isLicenseWasRemoved = !secondProfession.licensePreview;
        if (isLicenseWasRemoved) {
          dispatch(removeLicenseImage(uid, secondProfession.value));
        } else {
          const licenseURL = await uploadLicenseImgToStorage(
            "secondProfession"
          );

          professionUpdates[secondProfession.value].push({
            node: "licenseURL",
            value: licenseURL,
          });
        }
      }
    } else if (secondFSProfession) {
      dispatch(removeLicenseImage(uid, secondFSProfession));
      const secondaryProfession = await db
        .collection("users")
        .doc(uid)
        .collection("professions")
        .where("type", "==", "secondary");

      await secondaryProfession.get().then((snap) => {
        snap.forEach((doc) => {
          doc.ref.delete();
        });
      });
    }

    dispatch(updateProfessionNode({ uid, updates: professionUpdates }));
    dispatch(updateUser({ uid, payloads: profileUpdates }));
  };

  return {
    professions,
    firstProfession,
    secondProfession,
    setProfession,
    setLicenseImg,
    setZipcode,
    setAddressLineFirst,
    setAddressLineSecond,
    deleteLicenseImg,
    setInitial,
    options: PROFESSIONS,
    profileImagePreview,
    setProfileImage,
    removeProfileImage,
    submit,
    resetProfession,
  };
};
