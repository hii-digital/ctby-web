const functions = require("firebase-functions");
const twilio = require("twilio");
const express = require("express");
const bodyParser = require("body-parser");

const notifications = require("./notifications");

const { AccessToken } = twilio.jwt;
const { VideoGrant, ChatGrant } = AccessToken;

// firebase functions:config:set someservice.key="THE API KEY"
const { twilio: twilioSecretKeys } = functions.config();

const client = twilio(twilioSecretKeys.accountsid, twilioSecretKeys.authtoken);

const getToken = (identity) => {
  const fifmins = 60 * 15;
  return new AccessToken(
    twilioSecretKeys.accountsid,
    twilioSecretKeys.apikey,
    twilioSecretKeys.apisecret,
    {
      ttl: fifmins, // TO_DO: check it
      identity,
    }
  );
};

const getChatTokenFunc = (identity) => {
  const chatGrant = new ChatGrant({
    serviceSid: twilioSecretKeys.servicesid,
  });
  const token = getToken(identity);
  token.addGrant(chatGrant);
  return token.toJwt();
};

const getVideoTokenFunc = (identity, room) => {
  let videoGrant;
  if (typeof room !== "undefined") {
    videoGrant = new VideoGrant({ room });
  } else {
    videoGrant = new VideoGrant();
  }
  const token = getToken(identity);
  token.addGrant(videoGrant);
  return token.toJwt();
};

const addParticipant = (participantUid, sid) => {
  return new Promise((resolve, reject) => {
    client.conversations.v1
      .conversations(sid)
      .participants.create({ identity: participantUid })
      .then((participant) => {
        resolve(participant);
      })
      .catch((err) => reject(err));
  });
};

const handleConversationAdded = async (body, res) => {
  const { UniqueName, ConversationSid } = body;
  const participants = UniqueName.split("-");
  const promises = participants.map((uid) =>
    addParticipant(uid, ConversationSid)
  );
  try {
    await Promise.all(promises);
    res.sendStatus(200);
  } catch (err) {
    console.log("err", err);
    res.sendStatus(400);
  }
};

const webhookTwilioConversationUpdateApp = express();
webhookTwilioConversationUpdateApp.use(bodyParser.json());
webhookTwilioConversationUpdateApp.use(
  bodyParser.urlencoded({ extended: true })
); // for parsing application/x-www-form-urlencoded

webhookTwilioConversationUpdateApp.post("/", async (req, res) => {
  console.log("Received a webhook:", req.body);
  try {
    if (req.body.EventType === "onMessageAdded") {
      return notifications.handleMessageAdded(req.body, res);
    }
    if (req.body.EventType === "onConversationAdded") {
      return handleConversationAdded(req.body, res);
    }
    return res.sendStatus(200);
  } catch (err) {
    console.log("onMessageAdded webhook err", err);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }
});

exports.webhookTwilioConversationUpdate = functions.https.onRequest(
  webhookTwilioConversationUpdateApp
);

exports.getVideoToken = functions.https.onCall((data) => {
  const { identity, room } = data;
  const token = getVideoTokenFunc(identity, room);
  return token;
});

exports.getChatToken = functions.https.onCall((data) => {
  const { identity } = data;
  const token = getChatTokenFunc(identity);
  return token;
});

exports.sendSMS = async ({ to, body }) => {
  console.log("sendSMS", to, body);
  const from = "+18134917398"; // TO_DO TEST
  client.messages
    .create({ body, from, to })
    .then((message) => console.log(message.sid))
    .catch((err) => console.log("err", err));
};

exports.client = client;
