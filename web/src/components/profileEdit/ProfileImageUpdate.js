import React, { useState } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { fileStorage, db } from "../../config/fbConfig";

// components
import ImageUpload from "../profileEdit/imageUpload/ImageUpload";
import RenderImage from "../profileEdit/imageUpload/RenderImage";

const ProfileImageUpdate = () => {
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const [state, setState] = useState({ image: null, progress: 0 });

  const handleImageChange = (image) => {
    setState((prevState) => ({
      ...prevState,
      image,
    }));
  };

  function resizedName(fileName) {
    const dimensions = '300x300'
    const extIndex = fileName.lastIndexOf('.');
    const ext = fileName.substring(extIndex);
    console.log(`${fileName.substring(0, extIndex)}_${dimensions}${ext}`)
    return `${fileName.substring(0, extIndex)}_${dimensions}${ext}`;
  }
  const handleImageUpload =async (e) => {
    try {
      e.preventDefault();
    
    const { image } = state;
    const imageOwner = auth.uid;
    const uploadTask =fileStorage
    
      .ref(`users/${imageOwner}/${image.name}`)
      .put(image)




    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // Progress
        const progress = Math.ceil(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setState((prevState) => ({
          ...prevState,
          progress,
        }));
       
      },
      (error) => {
        // Errors
      
        console.log("Error:", error.message);
      },
      () => {
        // Complete
       
        fileStorage
          .ref(`users/${imageOwner}/`)
          .child(resizedName(image.name))
          .getDownloadURL()
          .then((url) => {
            console.log(url)
            db.collection(`users`).doc(imageOwner).update({
              photoURL: url,
            });
          })
          .catch((error) => {
            console.log("inside error", error);
            return null;
          });
      }
    );
    } catch (error) {
      console.log("error",error)
    }
  };
  const { image, progress } = state;
  return (
    <div className="profile-edit__image">
      <RenderImage urlPath={profile.photoURL} required />
      <form onSubmit={handleImageUpload}>
        <ImageUpload
          image={image}
          handleChange={handleImageChange}
          progress={progress}
          required
        />
        <button
          type="submit"
          className={`button button--primary text--uppercase `}
        >
          {progress === 100 ? "Upload new Image" : "Upload Image"}
        </button>
      </form>
    </div>
  );
};

export default ProfileImageUpdate;
