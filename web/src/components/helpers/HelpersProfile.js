import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import imageDefaultUser from "../../assets/images/default-user.jpg";
import LazyLoad from 'react-lazyload';
import placeHolder from "../../assets/images/image-loading.gif"
import ImageRenderer from "../../components/ImageRenderer/ImageRenderer"
export function renderProfileImage(image, alt, size) {
  const altTag = alt !== "" ? alt : "";
  if (typeof image !== "string") {
    return null;
  }
  const pattern = /^((http|https|ftp):\/\/)/;
  if (!pattern.test(image)) {
    return (

      <ImageRenderer
     
      url={image}
      thumb={placeHolder}
      width={300}
      height={300}
    />
     
     
    
    );
  }
  return   <ImageRenderer
     
  url={image}
  thumb={placeHolder}
  width={300}
  height={300}
/>
 
}

export function renderBlueCheck(profile) {
  if (
    (profile.about === "",
    profile.background === "",
    profile.businessCity === "",
    profile.firstName === "",
    profile.isApproved !== true,
    profile.isPro !== true,
    profile.isProPremium !== true)
  )
    return null;

  return (
    <span className="blue-check">
      <FontAwesomeIcon size="xs" icon={["fa", "user-check"]} />
    </span>
  );
}

export function convertToCapitalizeCase(word) {
  const regex = /([A-Z])(?=[A-Z][a-z])|([a-z])(?=[A-Z])/g;
  return word.replace(regex, "$& ");
}
