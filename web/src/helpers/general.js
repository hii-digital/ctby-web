import { postcodeValidator } from "postcode-validator";
import zipcodes from "zipcodes";

// config
import { auth } from "../config/fbConfig";

export const convertToSenteneCase = (word) => {
  const result = word.replace(/([A-Z])/g, " $1");
  const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
  return finalResult;
};

export const checkValidNumbers = (value) => {
  const regex = /^[0-9\b]+$/;
  return regex.test(value);
};

export const getZipCodeData = (zipCode) => {
  if (postcodeValidator(zipCode, "US") === false) {
    return null;
  }
  return fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${zipCode}&sensor=true&key=AIzaSyBffpI8hvEPuICCsaEkV6dIl-gOW-4E49w`
  )
    .then((response) => {
      if (response.status !== 200) {
        console.log(
          `Looks like there was a problem. Status Code: ${response.status}`
        );
        return null;
      }

      return response.json().then((data) => data.results[0]);
    })
    .catch((err) => {
      console.log("Please contact webmaster with this error:", err);
    });
};

export const checkIsVerified = () => {
  if (auth().currentUser) {
    const { emailVerified } = auth().currentUser;
    return emailVerified;
  }
  return false;
};

export const generateUniqueRandom = (max, haveItArr) => {
  const usedNumbers = [...haveItArr];
  const random = Math.floor(Math.random() * max);
  if (!haveItArr.includes(random)) {
    usedNumbers.push(random);
    return random;
  }
  if (usedNumbers.length < max) {
    return generateUniqueRandom(max, usedNumbers);
  }
  console.log("No more numbers available");
  return null;
};

export const getNearbyZipcodes = ({ zipcode, radius }) =>
  zipcodes.radius(zipcode, radius);
