import React from "react";
import "./cube.scss";
import { useHistory } from "react-router-dom";
import { ReactComponent as Dumbbell } from "../../assets/icons/dumbbell.svg";

export default function Cube() {
  const history = useHistory();
  function redirectFunction() {
    history.push("/how-it-works?pro");
  }
  return (
    <div className="cube-wrapper">
      <div className="cube__little-box-wrapper">
        <div className="cube__little-box" />
        <div className="cube__middle-box" />
      </div>
      <div
        tabIndex={0}
        role="button"
        type="button"
        className="cube"
        onClick={redirectFunction}
        onKeyDown={redirectFunction}
      >
        <div className="cube__text">How it works for Pro</div>
        <div className="cube__img">
          <Dumbbell />
        </div>
      </div>
    </div>
  );
}
