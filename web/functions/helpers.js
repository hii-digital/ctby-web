const admin = require("firebase-admin");
const momentTimezone = require("moment-timezone");

exports.getUserData = async (uid) => {
  try {
    const userRef = await admin.firestore().collection("users").doc(uid);
    const userDoc = await userRef.get();
    return userDoc.data();
  } catch (err) {
    console.log("get user data err", err);
  }
};

exports.getBookingDetails = async ({
  from,
  to,
  type,
  address,
  rate,
  serviceName: professionName,
  timezone,
}) => {
  try {
    const fromRegardingTimezone = momentTimezone.tz(from.toDate(), timezone);
    const toRegardingTimezone = momentTimezone.tz(to.toDate(), timezone);

    const date = fromRegardingTimezone.format("dddd, MMMM Do YYYY");
    const time = fromRegardingTimezone.format("h:mm a");

    const duration = toRegardingTimezone.diff(fromRegardingTimezone, "minutes");
    const details = {
      location: type === "online" ? "Online" : address,
      date,
      time,
      service: professionName,
      hours: duration,
      rate,
    };
    return details;
  } catch (err) {
    console.log("get pro data err", err);
  }
};
