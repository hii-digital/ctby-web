import React from "react";

// styles
import "./styles.scss";

const OnboardingHeader = ({ title, subtitle, step, stepsAmount }) => {
  const progressLineWidth = (100 / stepsAmount) * step;

  return (
    <div className="header-root">
      <h2 className="title">{title}</h2>
      <h2 className="title-background">{title}</h2>
      <p className="subtitle">{subtitle}</p>
      <p className="step">
        Step {step}/{stepsAmount}
      </p>
      <div
        className="progress-line"
        style={{ width: `${progressLineWidth}%` }}
      />
    </div>
  );
};

export default OnboardingHeader;
