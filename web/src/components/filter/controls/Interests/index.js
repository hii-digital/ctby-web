import React, { useContext } from "react";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../../../ui/Select";

// context
import { FilterContext } from "../../../../context/FilterProvider";

const Interests = ({ styles }) => {
  const { interestsOptions, updateState, interest } = useContext(FilterContext);

  const handleChange = ({ id, value }) => {
    updateState({ key: id, value });
  };

  return (
    <div className="interestsContainer">
      <label htmlFor="interest">Interests</label>
      <Select
        displayEmpty
        value={interest || ""}
        styles={styles}
        id="interest"
        onChange={handleChange}
      >
        <MenuItem value="">All</MenuItem>
        {interestsOptions.map(({ value, text }) => (
          <MenuItem key={value} value={value}>
            {text}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
export default Interests;
