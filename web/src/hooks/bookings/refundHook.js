import { useState, useRef, useEffect } from "react";
import moment from "moment";

// hooks
import { useBookingHook } from "./booking";

export const useRefundHook = (id) => {
  const { refundTimestamp: leaveReviewTimestamp } = useBookingHook(id);

  const [isAvailableToChange, setAvailableToChange] = useState(true);

  const timer = useRef(null);

  useEffect(() => {
    const timeDifference = moment(leaveReviewTimestamp.toDate()).diff(
      moment(),
      "milliseconds"
    );

    const maxDelay = 21600000; // 6 hours
    if (timeDifference < maxDelay) {
      if (timeDifference) {
        timer.current = setTimeout(() => {
          setAvailableToChange(false);
        }, timeDifference);
      } else {
        setAvailableToChange(false);
      }
    }

    return () => {
      if (timer.current) {
        clearTimeout(timer.current);
      }
    };
  }, [leaveReviewTimestamp]);

  return isAvailableToChange;
};
