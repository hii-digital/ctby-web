export const clearSession = () => {
  return async (dispatch) => {
    dispatch({
      type: "CLEAR_SESSION",
    });
  };
};

export const setInitLoaded = (status) => {
  return async (dispatch) => {
    dispatch({
      type: "SET_INIT_LOADED",
      status,
    });
  };
};
const setChangeId = (changesMap) => {
  return async (dispatch) => {
    dispatch({
      type: "SET_CHANGE_ID",
      changesMap,
    });
  };
};

export const setLoading = (bool) => {
  return async (dispatch) => {
    dispatch({
      type: "SET_LOADING",
      isLoading: bool,
    });
  };
};

export const updateChangeId = ({
  sid,
  newDateCreated,
  author,
  body,
  index,
}) => {
  return async (dispatch) => {
    dispatch({
      type: "UPDATE_CHANGE_ID",
      sid,
      newDateCreated,
      author,
      body,
      index,
    });
  };
};

export const setInit = ({ api, chats }) => {
  return async (dispatch) => {
    // TO_DO: rewrite in to for in
    try {
      const changesMap = await chats.reduce(async (chat, data) => {
        const messagePaginator = await data.getMessages(1);
        const {
          body,
          author,
          timestamp,
          index,
        } = messagePaginator.items[0].state;
        return {
          ...(await chat),
          [data.sid]: {
            body,
            author,
            dateCreated: timestamp.getTime(),
            index,
          },
        };
      }, {});
      dispatch(setChangeId(changesMap));
    } catch (err) {
      console.log("err", err);
    }

    const conversationsMap = Object.values(chats).reduce(
      (acc, { uniqueName, sid }) => {
        acc[uniqueName] = sid;
        return acc;
      },
      {}
    );

    dispatch({
      type: "SET_INIT",
      api,
      conversationsMap,
    });

    dispatch(setInitLoaded("success")); // TO_DO: handle other cases
  };
};

export const addConversation = ({ uniqueName, sid }) => {
  return async (dispatch) => {
    dispatch({
      type: "ADD_CONVERSATION",
      conversation: { [uniqueName]: sid },
    });
  };
};

export const addMessage = (message) => {
  return async (dispatch) => {
    dispatch({
      type: "ADD_MESSAGE",
      message,
    });
  };
};

export const setMessages = (messages) => {
  return async (dispatch) => {
    dispatch({
      type: "SET_MESSAGES",
      messages,
    });
  };
};
