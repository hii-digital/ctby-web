import React, { useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

// components
import CircularProgress from "../../ui/CircularProgress";

// actions
import { getAccountLink } from "../../store/actions/stripeActions";

import "./styles/payments.scss";

const Payments = () => {
  const dispatch = useDispatch();
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { getLinkLoading, stripeAccountLink } = useSelector(
    ({ stripe }) => stripe,
    shallowEqual
  );
  useEffect(() => {
    if (profile.isPro && profile.stripeId) {
      dispatch(getAccountLink(profile.stripeId));
    }
  }, [dispatch, profile.isPro, profile.stripeId]);
  if (!auth.uid) return <Redirect to="/signin" />;
  return (
    <>
      <div className="payment page">
        <div className="container container--top-bottom-padding">
          <div className="row" style={{ marginBottom: "25px" }}>
            <div className="col">
              <h1 className="text--lg">Payments</h1>
            </div>
          </div>
          {getLinkLoading ? (
            <CircularProgress size={20} />
          ) : (
            <>
              {stripeAccountLink && (
                <div className="row">
                  <div className="col col--12" style={{ marginBottom: "25px" }}>
                    <a href={stripeAccountLink} target="_blank">
                      {stripeAccountLink}
                    </a>
                  </div>
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Payments;
