import React from "react";

// styles
import "./styles.scss";

const CheckboxGroup = ({ children }) => {
  return <div className="checkbox-box-root">{children}</div>;
};

export default CheckboxGroup;
