import { v4 as uuid } from "uuid";

export const weekdaysReducer = (state, { type, payloads }) => {
  switch (type) {
    case "SET": {
      return {
        ...state,
        ...payloads.data,
      };
    }
    case "ADD_HOURS": {
      const { day } = payloads;
      const workingTimes = [
        ...state[day].workingTimes,
        { from: "", to: "", id: uuid() },
      ];

      return {
        ...state,
        [day]: {
          ...state[day],
          workingTimes,
        },
      };
    }
    case "REMOVE_HOURS": {
      const { day, timeId } = payloads;
      const workingTimes = state[day].workingTimes.filter(
        ({ id }) => timeId !== id
      );

      return {
        ...state,
        [day]: {
          ...state[day],
          workingTimes,
        },
      };
    }
    case "SET_TIME": {
      const { name, timeId, value, id } = payloads;
      const { workingTimes } = state[name];
      const foundIdx = workingTimes.findIndex((time) => time.id === timeId);
      workingTimes[foundIdx] = { ...workingTimes[foundIdx], [id]: value };

      return {
        ...state,
        [name]: {
          ...state[name],
          workingTimes,
        },
      };
    }
    case "TOGGLE_STATE": {
      const { item } = payloads;
      return {
        ...state,
        [item]: {
          ...state[item],
          state: !state[item].state,
          workingTimes: [{ from: "", to: "", id: uuid() }],
        },
      };
    }

    default:
      return state;
  }
};
