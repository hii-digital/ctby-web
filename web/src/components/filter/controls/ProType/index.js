import React, { useContext } from "react";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../../../ui/Select";

// context
import { FilterContext } from "../../../../context/FilterProvider";

const ProType = ({ styles }) => {
  const { updateState, proTypeOptions, proType } = useContext(FilterContext);

  const handleChange = ({ id, value }) => {
    updateState({ key: id, value });
  };

  return (
    <div className="proTypeContainer">
      <label htmlFor="proType">Pro Type</label>
      <Select
        id="proType"
        displayEmpty
        value={proType}
        styles={styles}
        onChange={handleChange}
      >
        <MenuItem value="">All</MenuItem>
        {proTypeOptions.map(({ value, text }) => (
          <MenuItem key={value} value={value}>
            {text}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
export default ProType;
