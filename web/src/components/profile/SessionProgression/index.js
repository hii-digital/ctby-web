import React, { Component, useState, useEffect } from "react";
import { connect, useSelector, shallowEqual, useDispatch } from "react-redux";
import Fade from "react-reveal/Fade";
import { Redirect, withRouter } from "react-router-dom";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useProfileHook } from "../../../hooks/profile";
import Loading from "../../modules/Loading";
import { db } from "../../../config/fbConfig";
import * as moment from "moment";
// import Popup from 'reactjs-popup';

// styles
import "./styles.scss";


const SessionProgression = () => {
    const { params } = useRouteMatch();
    const {
        firstName,
        lastName,
        photoURL,
        about,
        funFact,
        favQuote,
        specialties,
        professions,
        activeProfession,
        fromRate,
        onlineRate,
        inpersonRate,
        licenseURL,
        city,
        state,
    } = useProfileHook(params.uid);
    // let flag = false;
    // const clickButton = () => {
    //     flag = true;
    //     console.log("🚀 ~ file: index.js ~ line 16 ~ clickButton ~ flag", flag)
    //     Fetchdata();
    // }
    const [info, setInfo] = useState([]);

    useEffect(() => {
        Fetchdata();
    }, []);
    // window.addEventListener('load', () => {
    //     Fetchdata();
    //   });


    const {
        // profile: { firstName, isPro, lastName, clientStep, initials },
        auth: { uid },
    } = useSelector(({ firebase }) => firebase, shallowEqual);
    // db.collection("bookings").get();

    const dispatch = useDispatch();
    // const firstN = firstName;
    const id = uid;
    // const lastN = lastName;
    // const hel = clientStep;
    // const init = initials;
    // const zone = timezone;
    // const sta = status;
    // const time = rate;

    const Fetchdata = () => {
        db.collection("users").where('firstName', '==', firstName).get().then((result) => {

            result.docs.forEach(element => {
                var Bid = element.data();
                // setInfo(arr => [...arr, Bid]);
                db.collection("bookings").where("proId", "==", Bid.uid).get().then((res) => {
                    res.forEach(element => {
                        var Pid = element.data();
                        db.collection("users").where('uid', '==', Pid.clientId).get().then((res) => {
                            res.forEach(element => {
                                var Uid = element.data();
                                var Fid = { ...Bid, ...Pid, ...Uid };
                                setInfo(arr => [...arr, Fid]);
                            });
                        });
                        // Pid.PFname = Pid.firstName;
                        // Pid.PLname = Pid.lastName;
                        // setObj(con => ({
                        //     ...con, ...Pid
                        // }));
                        // console.log(`is for ${Uid.firstName}`);
                    });
                });
            });
        })
        // db.collection("bookings").doc("PWuZx8sSLkwIKdTOHsWF").get().then((querySnapshot) => {
        //     console.log("gggggggggg");
        //     var x=querySnapshot.data()
        //         console.log(x.proId);
        //     // Loop through the data and store
        //     // it in array to display
        //     // querySnapshot.forEach(element => {
        //     //     var data = element.data();
        //     //     setInfo(arr => [...arr , data]);

        //     // });
        // })
    }

    return (
        <div className="SessionProgression-root">
            <h2>Session Progression</h2>
            <table>
                <thead>
                    <tr>
                        <th>Clients Name</th>
                        <th>Service Name</th>
                        <th>Status</th>
                        <th>Time</th>
                    </tr>
                </thead>
                {
                    info.map((Bid) => (
                        <Frame
                            from={moment(Bid.from.toDate()).format('DD/MM/YYYY H:mma')}
                            state={Bid.status}
                            type={Bid.serviceName}
                            CFName={Bid.firstName}
                            CLName={Bid.lastName}
                            key={Bid.createdAt}
                        />
                    ))
                }
            </table>
            {/* <Popup trigger={<button> Trigger</button>} position="right center">
                <div>Popup content here !!</div>
            </Popup> */}
        </div>
    );
}

const Frame = ({ from, state, type, CFName, CLName }) => {
    // console.log(from + " " + state + " " + type);
    return (
        <tbody>
            <tr>
                <td>{CFName} {CLName}</td>
                <td>{type}</td>
                <td>{state}</td>
                <td>{from}</td>
            </tr>
        </tbody>
    );
}

export default SessionProgression;