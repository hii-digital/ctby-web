import React from "react";
import { useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import { isLoaded } from "react-redux-firebase";

// components
import SetRating from "../../../rating/SetRating";
import GetSingleReview from "../../../rating/GetSingleReview";

// actions
import { updateBooking } from "../../../../store/actions/bookingsActions";

// hooks
import { useBookingData } from "../../../../hooks/bookings/bookingData";
import { useBookingHook } from "../../../../hooks/bookings/booking";
import { useRefundHook } from "../../../../hooks/bookings/refundHook";

const LeaveReview = () => {
  const dispatch = useDispatch();
  const { params } = useRouteMatch();
  const { clientId, proId, rating } = useBookingHook(params.id);
  const { clientData, proData } = useBookingData({ clientId, proId });
  const isAvailableToLeaveReview = useRefundHook(params.id);

  const handleLeaveReview = (payloads) => {
    dispatch(updateBooking({ id: params.id, payloads }));
  };
  if (!isLoaded(clientData)) {
    return null;
  }
  const { firstName, lastName } = proData;

  return (
    <>
      {rating && (
        <div className="rating">
          <div className="rating__inner">
            <h2 className="text--uppercase text--bold">Rating Completed</h2>
            <GetSingleReview rating={rating} />
          </div>
        </div>
      )}
      {isAvailableToLeaveReview && !rating && (
        <div className="rating">
          <div className="rating__inner">
            <h2 className="text--uppercase text--bold">
              Leave a review for {firstName} {lastName}.
            </h2>
            <SetRating handleLeaveReview={handleLeaveReview} />
          </div>
        </div>
      )}
    </>
  );
};

export default LeaveReview;
