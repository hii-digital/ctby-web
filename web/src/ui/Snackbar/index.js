import React from "react";
import MuiSnackbar from "@material-ui/core/Snackbar";

const Snackbar = ({ message, onClose }) => {

  return (
    <MuiSnackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      open={Boolean(message)}
      autoHideDuration={2000}
      message={message}
      onClose={onClose}
    />
  );
};

export default Snackbar;