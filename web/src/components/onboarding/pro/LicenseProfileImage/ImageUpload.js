import React, { useState } from "react";
import { useDropzone } from "react-dropzone";

// icons
import { ReactComponent as PlusIcon } from "../../../../assets/icons/plus.svg";
import { ReactComponent as CrossIcon } from "../../../../assets/icons/cross.svg";

const ImageUpload = ({ label, handleDrop, imageSrc, handleDelete }) => {
  const [errorMessage, setErrorMessage] = useState("");
  const onDrop = (acceptedFiles, fileRejections) => {
    setErrorMessage("");
    if (fileRejections.length) {
      fileRejections.forEach((file) => {
        file.errors.forEach((err) => {
          if (err.code === "file-too-large") {
            setErrorMessage("Selected file is larger than 7 MB");
          }
        });
      });
      return;
    }
    const file = acceptedFiles[0];
    const preview = URL.createObjectURL(file);
    handleDrop({ file, preview });
  };

  const { getRootProps, getInputProps } = useDropzone({
    accept: ".png, .jpg, .jpeg",
    onDrop,
    multiple: false,
    maxSize: 7000000, // 1mb
  });

  return (
    <div className="image-upload">
      <span>{label}</span>
      {imageSrc ? (
        <>
          <div className="image-area">
            <img src={imageSrc} alt="Uploaded file to represent how you look" />
            <button type="button" onClick={handleDelete}>
              <CrossIcon />
            </button>
          </div>
          <span className="message">{errorMessage}</span>
        </>
      ) : (
        <>
          <div className="drop-area" {...getRootProps()}>
            <PlusIcon />
            <input {...getInputProps()} />
            <div />
          </div>
          <span className="message">
            {errorMessage || (!imageSrc && "Maximum file upload size: 7 MB")}
          </span>
        </>
      )}
    </div>
  );
};

export default ImageUpload;
