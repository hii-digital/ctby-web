import { startLoading, stopLoading } from "./loadingActions";

export const updateBooking = ({ id, payloads }) => {
  return async (dispatch, getState, { getFirestore }) => {
    dispatch(startLoading());
    const firestore = getFirestore();
    await firestore
      .collection("bookings")
      .doc(id)
      .set(payloads, { merge: true });
    dispatch(stopLoading());
  };
};
