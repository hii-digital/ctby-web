import React, { createContext, useState } from "react";

export const SidebarContext = createContext({});

const initAnchor = "right";

const SidebarProvider = ({ children }) => {
  const [state, setState] = useState({
    openedId: null,
    payloads: {},
    anchor: initAnchor,
  });

  const { openedId, payloads, anchor } = state;

  const openSidebar = (id, data = {}) => {
    const { anchor: anch, ...rest } = data;

    /* quick update to synch state values */
    const newState = {
      openedId: id,
    };

    if (Object.keys(data).length) {
      newState.payloads = rest;
      if (anch) {
        newState.anchor = anch;
      } else {
        newState.anchor = initAnchor;
      }
    }
    setState((prevState) => ({ ...prevState, ...newState }));
  };

  const closeSidebar = () => {
    setState((prevState) => ({ ...prevState, openedId: null, payloads: {} }));
  };

  const ctx = {
    openedId,
    payloads,
    openSidebar,
    closeSidebar,
    anchor,
  };

  return (
    <SidebarContext.Provider value={ctx}>{children}</SidebarContext.Provider>
  );
};

export default SidebarProvider;
