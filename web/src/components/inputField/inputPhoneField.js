import React, { useState } from "react";
import PropTypes from "prop-types";
import "./InputField.scss";
import icon from "../../assets/images/icons/ratingStar.png";

const options = [
  { value: "England", label: "+1", icon: { icon } },
  { value: "England", label: "+2", icon: { icon } },
];

const Value = ({
  cx,
  getStyles,
  selectProps,
  data,
  isDisabled,
  className,
  ...props
}) => {
  return (
    <div
      className={cx(
        "single-value",
        {
          "single-value--is-disabled": isDisabled,
        },
        className
      )}
    >
      <div>
        <img src={icon} alt="icon" />
      </div>
      <div>{selectProps.getOptionLabel(data)}</div>
    </div>
  );
};

const InputField = ({
  value,
  label,
  placeholder,
  validators,
  type,
  onChange,
}) => {
  const { Option } = components;
  const IconOption = (props) => (
    <Option {...props}>
      <div className="select__icon">
        <img src={props.data.icon.icon} alt={props.data.label} />
        {props.data.label}
      </div>
    </Option>
  );

  const [count, setCount] = useState("igor");
  const [error, setError] = useState(false);

  const handleChange = (event) => {
    const { value } = event.target;
    onChange(value);
  };

  return (
    <div className="form-phone-field inputField">
      {label && <label htmlFor="app-input-field">{label}</label>}

      {type === "textarea" ? (
        <textarea
          className="form-control"
          placeholder={placeholder}
          value={value}
          defaultValue={value}
          onChange={handleChange}
        />
      ) : (
        <div>
          <Select
            defaultValue={options[0]}
            className="c-country-select"
            options={options}
            components={{
              Option: IconOption,
              SingleValue: Value,
            }}
          />
        </div>
      )}
      {error && <span className="text-danger">{error.message}</span>}
    </div>
  );
};

InputField.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  validators: PropTypes.array,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

InputField.defaultProps = {
  value: "",
  label: "",
  placeholder: "",
  type: "text",
  validators: [],
};

export default InputField;
