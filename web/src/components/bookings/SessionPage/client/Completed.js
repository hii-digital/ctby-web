import React from "react";

// components
import { AskForRefund, LeaveReview } from "../actions";


const Completed = () => {
  return (
    <>
      <LeaveReview />
      <AskForRefund />
    </>
  );
};

export default Completed;
