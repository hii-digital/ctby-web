import React, { useContext } from "react";

// components
import Input from "../../../../ui/Input";

// context
import { FilterContext } from "../../../../context/FilterProvider";

const Zipcode = () => {
  const { updateState, zipcode } = useContext(FilterContext);

  const handleChange = (e) => {
    updateState({ key: "zipcode", value: e.target.value }, e);
  };

  return (
    <div className="zipcodeContainer">
      <label htmlFor="zipcode">Zip Code/City</label>
      <Input
        value={zipcode}
        placeholder="e.g. 11111"
        id="zipcode"
        onChange={handleChange}
      />
    </div>
  );
};
export default Zipcode;
