import React from "react";
import { useDispatch } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import { Button } from "semantic-ui-react";

// actions
import { updateBooking } from "../../../../store/actions/bookingsActions";

// data
import { STATUSES } from "../../../../data/bookings";

const { pending, declined } = STATUSES;

const AwaitingApproval = () => {
  const { params } = useRouteMatch();
  const dispatch = useDispatch();

  const handleConfirmClick = () => {
    const payloads = {
      status: pending.value,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };
  
  const handleDecline = () => {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    const payloads = {
      status: declined.value,
      refundTimestamp: tomorrow,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  return (
    <>
      <Button
        className="button--secondary button--full"
        onClick={handleConfirmClick}
      >
        Accept
      </Button>
      <Button className="button--primary button--full" onClick={handleDecline}>
        Decline
      </Button>
    </>
  );
};

export default AwaitingApproval;
