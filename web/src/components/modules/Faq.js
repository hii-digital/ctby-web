import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

// components
import TabsComponent from "../tabs/tabs.component";
import Tab from "../tabs/tab.component";

import "./faq.scss";

export const faqlistClient = [
  {
    question: "What happens if I need to cancel or reschedule a session?",
    answear:
      "Simply edit your session to cancel or reschedule. To give our coaches enough time to reschedule, we request this to be done 24 hours in advance. If done within 24 hours, a refund is not guaranteed.  If you need additional assistance, you can also email or call us directly at 407-890-0587 and we will be more than happy to help.",
  },
  {
    question: "How much do sessions cost?",
    answear:
      "The price of sessions varies depending on the pro you select. You can view each pro’s profile for pricing and information on experience & expertise.",
  },
  {
    question: "Can I be a client and a pro?",
    answear:
      "Definitely! Clients are able to upgrade their profiles to become a pro on the site.  Also, you initially join as a pro, you are automatically a client and can book sessions with other pros.",
  },
  {
    question: "How long are sessions?",
    answear: "You are able to set the length when you book your session.",
  },
  {
    question: "How often should I take sessions?",
    answear:
      "You can take sessions at your own pace, but ultimately it depends on your health & wellness goals. There is no limit.",
  },
  {
    question: "How do I let you know I have completed a session?",
    answear:
      "Once the time for the session has lapsed, our system is notified of completion.",
  },
];

export const faqlistPro = [
  {
    question: "How does C2Byou help me acquire clients?",
    answear:
      "Using the information you provide in your application, C2Byou creates a unique profile and features it to clients in your area. We take care of all of the marketing, advertising, and client bookings on your behalf.  By creating the most comprehensive marketplace, we represent the premier source of prospective clients.",
  },
  {
    question: "Do I need to be certified?",
    answear:
      "While it is not a requirement to be listed as a certified professional, we strongly recommend going through a certification process.  Not only will you learn new techniques but you’ll also appeal to a wider client base searching for pros.",
  },
  {
    question: "How do I get paid as a pro?",
    answear:
      "You get paid via Stripe.  During your onboarding process, you will sign up for Stripe, which will connect to your bank account.  After you complete each booked session, funds will be transferred to your bank account.  See the Payment section of your Dashboard.  Choose To Be You will deduct a revenue share of 30% for service fees and the value & benefits pros receive from the platform.",
  },
  {
    question: "Are payments secured? ",
    answear:
      "Yes, via Stripe.  You do not have to worry about not being paid for services provided. Clients make payments prior to the session starting, and the funds are released to you after the session is completed.",
  },
  {
    question: "Can I be a pro and a client?",
    answear:
      "Definitely! We encourage our pros to be clients! When you join as a pro, you are automatically signed up as a client and can book sessions with other pros.",
  },
  {
    question: "How do I let you know I have completed a session?",
    answear:
      "Once your application is approved, you’ll have access to your Dashboard, where you can manage bookings and sessions. It is your responsibility as a pro to start all sessions. Once the time for the session has lapsed, our system is notified of completion.",
  },
  {
    question: "What are important contract details I should know?",
    answear:
      "Working with C2Byou is non-exclusive, so you are free to work with outside clients or facilities. Clients acquired through our platform however, cannot be taken on the side. Soliciting clients from C2Byou will result in the termination of your profile.",
  },
  {
    question: "Where do I conduct my sessions?",
    answear:
      "The client will make this selection when they book a session.  They can either meet you at your own facility, or request that you come to them. For fitness trainers, there is also an online option.",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    borderBottom: 8,
    padding: "30px 16px",
    boxSizing: "border-box",
    [theme.breakpoints.up("l")]: {
      padding: "54px 90px 58px",
    },
  },
  heading: {
    fontSize: 16,
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: "RubikBold",
    color: "#565659",
    [theme.breakpoints.up("l")]: {
      fontSize: 24,
    },
  },
  item: {
    marginBottom: 20,
    boxShadow: "0px 4px 16px rgba(0, 0, 0, 0.0443073)",
    backgroundColor: "none",
    borderRadius: "8px",
    "&:before": {
      backgroundColor: "white",
    },
  },
  expanded: {
    borderBottom: "1px solid #EEEEEE",
    margin: "0px 0px",
  },
  accordionContent: {
    border: "none",
    margin: "16px 0px",
  },
  edgeEnd: {
    margin: "unset",
    border: "none",
  },
  details: {
    fontFamily: "QuestrialRegular",
    fontSize: 14,
    [theme.breakpoints.up("l")]: {
      fontSize: 16,
    },
  },
}));

const FAQClient = () => {
  const classes = useStyles();
  return (
    <>
      <div className="faq__text">
        <div className="home__text  home__backgroundText">Faq for client</div>
        <h2 className="home__text home__text-main text--uppercase text--lg">
          Faq for client
        </h2>
      </div>

      {faqlistClient.map((faq, i) => {
        return (
          <Accordion
            classes={{
              root: classes.item,
              expanded: classes.expanded,
            }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              classes={{
                expanded: classes.expanded,
                content: classes.accordionContent,
                expandIcon: classes.edgeEnd,
              }}
            >
              <Typography className={classes.heading}>
                {i + 1}. {faq.question}
              </Typography>
            </AccordionSummary>
            <AccordionDetails classes={{ root: classes.details }}>
              {faq.answear}
            </AccordionDetails>
          </Accordion>
        );
      })}
    </>
  );
};
const FAQPro = () => {
  const classes = useStyles();
  return (
    <>
      <div className="faq__text">
        <div className="home__text  home__backgroundText">Faq for pro</div>
        <h2 className="home__text home__text-main text--uppercase text--lg">
          Faq for pro
        </h2>
      </div>
      {faqlistPro.map((faqPro, i) => {
        return (
          <Accordion
            classes={{ root: classes.item, expanded: classes.expanded }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              classes={{
                expanded: classes.expanded,
                content: classes.accordionContent,
                expandIcon: classes.edgeEnd,
              }}
            >
              <Typography className={classes.heading}>
                {i + 1}. {faqPro.question}
              </Typography>
            </AccordionSummary>
            <AccordionDetails classes={{ root: classes.details }}>
              {faqPro.answear}
            </AccordionDetails>
          </Accordion>
        );
      })}
    </>
  );
};

export default function Faq() {
  const classes = useStyles();
  const [key, setKey] = useState(0);

  const location = window.location.href;
  const locationSplit = location.split("?");
  const lastItem = locationSplit[locationSplit.length - 1];

  useEffect(() => {
    if (lastItem === "pro") {
      setKey(1);
    }
  }, [lastItem]);

  return (
    <div className="faq">
      <div className="container--top-bottom-padding how-it-works__picture" />
      <TabsComponent
        activeKey={key}
        onSelect={(k) => {
          setKey(k);
        }}
      >
        <Tab eventKey={0} title="Client">
          <div className={classes.root}>
            <FAQClient />
          </div>
        </Tab>
        <Tab eventKey={1} title="Pro">
          <div className={classes.root}>
            <FAQPro />
          </div>
        </Tab>
      </TabsComponent>
    </div>
  );
}
