import React, { useEffect } from "react";
import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";
import DatePicker from "react-datepicker";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../ui/Select";

// helpers
import { renderProfileImage } from "../helpers/HelpersProfile";
import { getTomorrow } from "../../helpers/time";

// data
import { HOURS } from "../../data/hours";

// hooks
import { useBookingData } from "../../hooks/bookings/bookingData";
import { useDateTimeHook } from "../../hooks/bookRequest/dateTimeHook";

const selectStyles = {
  height: 200,
};

const EditDate = ({
  from,
  bookingId,
  duration,
  proId,
  date,
  startTime,
  handleChangeDate,
  handleChangeTime,
  setCheckValid,
  checkValid,
}) => {
  const {
    checkIsStartTimeAvailable,
    filterDates,
    checkIsDurationAvailable,
  } = useDateTimeHook(proId);

  const timeFormatted = moment(from).format("h:mm A");
  const dateFormatted = from.toDate();
  useEffect(() => {
    if (!date && !startTime) {
      handleChangeDate(dateFormatted);
      handleChangeTime(timeFormatted);
    }
  }, [
    handleChangeDate,
    handleChangeTime,
    date,
    startTime,
    dateFormatted,
    timeFormatted,
  ]);

  const onDateChange = (d) => {
    handleChangeDate(d);
    handleChangeTime("");
  };

  const onTimeChange = ({ value }) => {
    handleChangeTime(value);
  };

  const clearValid = () => {
    setCheckValid(false);
  };

  const checkIsSlotAvailable = (value) => {
    const isStartTimeAvailable = checkIsStartTimeAvailable(
      value,
      date,
      bookingId
    );
    if (!isStartTimeAvailable) {
      return isStartTimeAvailable;
    }
    const isDurationAvailable = checkIsDurationAvailable({
      duration: `${duration}`,
      startTime: value,
      date,
      ignoredBookingId: bookingId,
    });
    return isDurationAvailable;
  };

  return (
    <>
      <div>
        <label htmlFor="date" className="title">
          Date
        </label>
        <DatePicker
          selected={date}
          onChange={onDateChange}
          formatWeekDay={(dayName) => dayName.substr(0, 1)}
          dateFormat="d MMM"
          minDate={getTomorrow()}
          popperPlacement="bottom-center"
          popperModifiers={{
            flip: { behavior: ["bottom"] },
            preventOverflow: { enabled: false },
            hide: {
              enabled: false,
            },
          }}
          filterDate={filterDates}
          placeholderText="Date"
          onFocus={clearValid}
        />
      </div>
      <div>
        <label htmlFor="time" className="title">
          Start Time
        </label>
        <Select
          id="startTime"
          renderValue={(value) => value || "Start Time"}
          value={startTime}
          displayEmpty
          onChange={onTimeChange}
          styles={{
            ...selectStyles,
            isError: !startTime && checkValid,
          }}
          onOpen={clearValid}
        >
          {date &&
            HOURS.map(({ value, text }) => (
              <MenuItem
                key={value}
                value={value}
                disabled={!checkIsSlotAvailable(value)}
              >
                {text}
              </MenuItem>
            ))}
        </Select>
      </div>
    </>
  );
};

const BookingDetails = ({
  proId,
  clientId,
  id,
  serviceName,
  rate,
  type,
  address,
  addressLineSecond,
  zipcode,
  from,
  to,
  isEdited,
  ...rest
}) => {
  const fromMoment = moment(from.toDate());
  const toMoment = moment(to.toDate());
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const { clientData, proData } = useBookingData({ proId, clientId });

  const { photoURL, initials, firstName, lastName } =
    (proId === auth.uid ? clientData : proData) || {};

  const duration = toMoment.diff(fromMoment, "minutes");
  const total = (rate * duration) / 60;

  const renderAddress = () => (
    <>
      <p>Address Line 1: {address}</p>
      <br />
      {addressLineSecond && (
        <>
          <p>Address Line 2: {addressLineSecond}</p>
          {zipcode && <br />}
        </>
      )}
      {zipcode && <p>Zip code: {zipcode}</p>}
    </>
  );

  return (
    <div className="interaction-details__summary">
      <h2 className="text--uppercase mn--double">Booking Details</h2>
      <div className="interaction-details__summary-meta">
        <div className="interaction-details__summary-image">
          {photoURL ? (
            renderProfileImage(photoURL)
          ) : (
            <div className="initials">{initials}</div>
          )}
        </div>
        <div className="interaction-details__summary-name">
          <p className="text--capitalize">
            {firstName} {lastName}
          </p>
        </div>
      </div>
      <div className="interaction-details__bookingId">
        <h3>Booking ID</h3>
        {id}
      </div>
      <div className="interaction-details__location">
        <h3>Location</h3>
        {type === "online" ? "Online" : renderAddress()}
      </div>
      {!isEdited && (
        <div className="interaction-details__summary-date">
          <h3>Date &amp; Time</h3>
          <p>{fromMoment.format("dddd, MMMM Do YYYY")}</p>
          <p>{fromMoment.format("h:mm a")}</p>
        </div>
      )}
      {isEdited && (
        <EditDate
          from={fromMoment}
          to={toMoment}
          duration={duration}
          proId={proId}
          bookingId={id}
          {...rest}
        />
      )}
      <div className="interaction-details__summary-service">
        <h3>Service</h3>
        <p className="text--capitalize">{serviceName}</p>
      </div>
      <div className="interaction-details__summary-pricing">
        <h3 className="text--uppercase">Price</h3>
        <p className="mb--double pb--double">
          <span className="text--lowercase">
            ${rate} x {duration / 60} hours
          </span>{" "}
          <span>${total}</span>
        </p>
        <p className="field--review-total text--uppercase text--bold">
          <span>Total</span> <span>${total}</span>
        </p>
      </div>
    </div>
  );
};

export default BookingDetails;
