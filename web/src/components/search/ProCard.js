import React from "react";
import { isLoaded } from "react-redux-firebase";
import { Link, useLocation } from "react-router-dom";
// helpers
import { renderProfileImage } from "../helpers/HelpersProfile";
import { getRating } from "../../helpers/rating";

// components
import Toggler from "../../ui/Toggler";

// hooks
import { useProfessionsHook } from "../../hooks/professions";
import { useBookingsHook } from "../../hooks/bookings";
import { useProfileHook } from "../../hooks/profile";

// icons
import { ReactComponent as Location } from "../../assets/icons/location.svg";
import { ReactComponent as Star } from "../../assets/icons/stars.svg";
import { ReactComponent as Label } from "../../assets/icons/label.svg";

const ProCard = ({ pro: uid }) => {
  const { bookingsAsProOrdered } = useBookingsHook(uid);

  const { professionsOrderedData: orderedProfessions } = useProfessionsHook(
    uid
  );

  const {
    firstName,
    lastName,
    photoURL,
    about,
    specialties,
    professions,
    activeProfession,
    fromRate,
    inpersonRate,
    onlineRate,
    licenseURL,
    isHidden,
    zipCode,
    city,
    state,
  } = useProfileHook(uid);

  if (!uid || !isLoaded(bookingsAsProOrdered)) return null;
  if (isHidden === "hidden") {
    return null;
  }

  const renderCosts = (label, rate) => {
    return (
      <div className="pro-card__price">
        <Label />
        <p className="mb--0 text--bold" style={{ fontSize: "14px" }}>
          {label}: ${rate}
        </p>
      </div>
    );
  };

  const { rating, length } = getRating(bookingsAsProOrdered);

  return (
    <Link className="pro-list__card col--3" to={`/pro/${uid}`} key={uid}>
      <div className="pro-card">
        <div className="pro-card__inner">
          <div className="pro-card__image">
            {renderProfileImage(
              photoURL,
              `Image of user ${firstName} + ${lastName}`,
              { width: "300", height: "300" }
            )}
          </div>

          <div className="pro-card__content">
            <h2 className="pro-card__content-name mb--0 text--capitalize">
              {firstName} {lastName}
            </h2>
            <div className="pro-card__title-name">
              <Toggler items={professions} active={activeProfession} />
            </div>
            <p>
              {about ? (
                `${about.substring(0, 48)}...`
              ) : (
                <div className="pro-card__empty-pharagraph" />
              )}
            </p>

            {onlineRate && renderCosts("Online", onlineRate)}
            {inpersonRate && renderCosts("In Person", inpersonRate)}
            <div className="pro-card__location">
              <div>
                <Location />
              </div>
              <p>
                {city}, {state}
              </p>
            </div>
            <div className="pro-card__location">
              <Location />
              <p>{zipCode}</p>
            </div>
            <div className="pro-card__rating-wrapper">
              <Star />
              {rating ? (
                <div className="stars__rating">
                  {rating} ({length})
                </div>
              ) : (
                <div className="stars__none">No Reviews Yet</div>
              )}
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default ProCard;
