const functions = require("firebase-functions");
const admin = require("firebase-admin");
const bodyParser = require("body-parser");
const cors = require("cors")({ origin: true });
const express = require("express");

const { stripe: stripeSecretKeys } = functions.config();
const stripe = require("stripe")(stripeSecretKeys.key);

const booking = require("./booking");

const endpointSecret = stripeSecretKeys.endpoint;
const endpointConnectedAccountsSecret = stripeSecretKeys.endpoint2;

const send = (res, code, body) => {
  res.send({
    statusCode: code,
    headers: { "Access-Control-Allow-Origin": "*" },
    data: body,
  });
};

// -------------------- Process the order ---------------------------------------------

const webhooksHandleAccountEventsApp = express();

webhooksHandleAccountEventsApp.post(
  "/",
  bodyParser.raw({ type: "application/json" }),
  (request, response) => {
    const sig = request.headers["stripe-signature"];
    let event;

    try {
      event = stripe.webhooks.constructEvent(
        request.rawBody,
        sig,
        endpointSecret
      );
    } catch (err) {
      console.log("Webhook Error", err);
      return response.status(400).send(`Webhook Error: ${err.message}`);
    }
    if (event.type === "checkout.session.completed") {
      const session = event.data.object;
      booking.createBooking(session.metadata, session.payment_intent);
    }
    return response.json({ received: true });
  }
);

// ------------------ End of Process the order ---------------------------------------------------------

// -------------------- Create Update account hook ---------------------------------------------

const webhooksHandleConnectedAccountsEventsApp = express();

webhooksHandleConnectedAccountsEventsApp.post(
  "/",
  bodyParser.raw({ type: "application/json" }),
  (request, response) => {
    const sig = request.headers["stripe-signature"];
    let event;

    try {
      event = stripe.webhooks.constructEvent(
        request.rawBody,
        sig,
        endpointConnectedAccountsSecret
      );
    } catch (err) {
      console.log(err);
      return response.status(400).send(`Webhook Error: ${err.message}`);
    }

    if (event.type === "account.updated") {
      if (event.data.object.charges_enabled) {
        admin
          .firestore()
          .collection("users")
          .doc(event.data.object.metadata.userId)
          .update(
            {
              isStripeOnboardingCompleted: true,
            },
            { merge: true }
          );
      }
    }

    return response.json({ received: true });
  }
);

// ------------------ End of Update account hook ---------------------------------------------------------

// --------------------- Create order and session ------------------------------------------------------

const createOrderAndSessionApp = express();
createOrderAndSessionApp.use(cors);

const createOrderAndSession = (req, res) => {
  const {
    body: { data },
  } = req;
  stripe.checkout.sessions
    .create({
      payment_method_types: ["card"],
      mode: "payment",
      line_items: [
        {
          name: data.name,
          description: data.description,
          amount: data.amount,
          currency: data.currency,
          quantity: data.quantity,
        },
      ],
      payment_intent_data: {
        application_fee_amount: data.amount * 0.3,
        transfer_data: {
          destination: data.destination,
        },
      },
      client_reference_id: data.clientId,
      metadata: data.metaData,
      success_url: `${data.url}/bookings`,
      cancel_url: `${data.url}/pro/${data.clientId}`,
    })
    .then((session) => {
      send(res, 200, {
        sessionId: session.id,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

createOrderAndSessionApp.post("/", (req, res) => {
  try {
    createOrderAndSession(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});

// --------------------- End of Create order and session ------------------------------------------------------

// ---------------------- Create stripe account --------------------------------------------------------------

const createStripeAccountApp = express();
createStripeAccountApp.use(cors);

const createStripeAccount = (req, res) => {
  const {
    body: { data },
  } = req;
  stripe.accounts
    .create({
      type: "express",
      country: "US",
      business_type: "individual",
      capabilities: {
        card_payments: { requested: true },
        transfers: { requested: true },
      },
      metadata: {
        userId: data.userId,
      },
      settings: {
        payouts: {
          schedule: {
            delay_days: 7,
          },
        },
      },
    })
    .then((account) => {
      send(res, 200, {
        account,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

createStripeAccountApp.post("/", (req, res) => {
  try {
    createStripeAccount(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});

// ---------------------- End of Create stripe account --------------------------------------------------------------

// ---------------------- Get stripe account info ------------------------------------------------------------------

const retrieveStripeAccountApp = express();
retrieveStripeAccountApp.use(cors);

const retrieveStripeAccount = (req, res) => {
  const {
    body: { data },
  } = req;
  stripe.accounts
    .retrieve(data)
    .then((accountInfo) => {
      send(res, 200, {
        accountInfo,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

retrieveStripeAccountApp.post("/", (req, res) => {
  try {
    retrieveStripeAccount(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});



// ----------------------  Create stripe account --------------------------------------------------------------

// ---------------------- Create Booking Order without charge------------------------------------------------------------------

const createBookingWithCoupon = express();
createBookingWithCoupon.use(cors);

const createBookingAndPayout = (req, res) => {
  const {
    body: { data },
  } = req;

  console.log("booking data",data.metaData)
 
 
  booking.createBooking(data.metaData, 0);




};

createBookingWithCoupon.post("/", (req, res) => {
  try {
    createBookingAndPayout(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});




// ---------------------------------End of Get stripe account info ------------------------------------------

// ---------------------- Get stripe account onboarding link -------------------------------------------------

const getStripeOnboardingLinkApp = express();
getStripeOnboardingLinkApp.use(cors);

const getStripeOnboardingLink = (req, res) => {
  const {
    body: { data },
  } = req;
  stripe.accountLinks
    .create({
      account: data.accountId,
      refresh_url: `${data.url}/payments`,
      return_url: `${data.url}/payments`,
      type: "account_onboarding",
    })
    .then((accountOnboardingLinkInfo) => {
      send(res, 200, {
        accountOnboardingLinkInfo,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

getStripeOnboardingLinkApp.post("/", (req, res) => {
  try {
    getStripeOnboardingLink(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});

// ---------------------------------End of Get stripe account onboarding link --------------------------------------

// ---------------------- Get stripe account login link -------------------------------------------------

const getStripeLoginLinkApp = express();
getStripeLoginLinkApp.use(cors);

const getStripeLoginLink = (req, res) => {
  const {
    body: { data },
  } = req;
  stripe.accounts
    .createLoginLink(data)
    .then((accountLoginLinkInfo) => {
      send(res, 200, {
        accountLoginLinkInfo,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

getStripeLoginLinkApp.post("/", (req, res) => {
  try {
    getStripeLoginLink(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});




//-------------------------- Stripe Get Coupons ---------------------
const retrieveStripeCouponApp = express();
retrieveStripeCouponApp.use(cors);

const retrieveCoupon = (req, res) => {
  const {
    body: { data },
  } = req;

  console.log("data",data)
  stripe.coupons.retrieve(
    data
  ).then((couponInfo) => {
      send(res, 200, {
        couponInfo,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

retrieveStripeCouponApp.get("/", (req, res) => {
  try {
  retrieveCoupon(req, res);
  } catch (e) {
    console.log(e);
    send(res, 500, {
      error: `The server received an unexpected error. Please try again and contact the site admin if the error persists.`,
    });
  }
});



// ---------------------------------End of Get stripe account login link --------------------------------------

// ---------------------- Stripe Exports ----------------------------------------------------------------------------
exports.retrieveStripeCoupon = functions.https.onRequest(retrieveStripeCouponApp);
exports.createStripeAccount = functions.https.onRequest(createStripeAccountApp);


exports.createOrderAndSession = functions.https.onRequest(
  createOrderAndSessionApp
);

exports.webhooksHandleAccountEvents = functions.https.onRequest(
  webhooksHandleAccountEventsApp
);

exports.createBookingWithCoupon = functions.https.onRequest(
  createBookingWithCoupon
);




exports.webhooksHandleConnectedAccountsEvents = functions.https.onRequest(
  webhooksHandleConnectedAccountsEventsApp
);

exports.retrieveStripeAccount = functions.https.onRequest(
  retrieveStripeAccountApp
);

exports.getStripeOnboardingLink = functions.https.onRequest(
  getStripeOnboardingLinkApp
);

exports.getStripeLoginLink = functions.https.onRequest(getStripeLoginLinkApp);










// ---------------------End of Stripe Exports -----------------------------------------------------------------------
