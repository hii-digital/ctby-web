export const specialtiesReducer = (state, { type, profession, payloads }) => {
  switch (type) {
    case "SET_INITIAL":
      return {
        ...state,
        ...payloads,
      };
    case "SET_PROFESSION":
      return {
        ...state,
        [profession]: payloads,
      };

    case "SET_ZIPCODE": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          zipCode: payloads.zipCode,
        },
      };
    }

    case "SET_LICENSE_IMAGE": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          ...payloads,
        },
      };
    }

    case "SET_ADDRESS_LINE_FIRST": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          addressLineFirst: payloads.address,
        },
      };
    }
    case "SET_ADDRESS_LINE_SECOND": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          addressLineSecond: payloads.address,
        },
      };
    }
    case "SET_SPECIALITES": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          specialties: payloads.specialties,
        },
      };
    }

    case "SET_RATES": {
      return {
        ...state,
        [profession]: {
          ...state[profession],
          ...payloads.ratesData,
        },
      };
    }

    case "SET_PRIMARY": {
      const filteredProfs = Object.keys(state).filter(
        (professionValue) => professionValue !== profession
      );

      const updates = filteredProfs.reduce((acc, p) => {
        acc[p] = {
          ...state[p],
          type: "secondary",
        };
        return acc;
      }, {});

      return {
        ...state,
        [profession]: {
          ...state[profession],
          type: "primary",
        },
        ...updates,
      };
    }

    case "RESET": {
      const stateCopy = { ...state };
      delete stateCopy[profession];
      return stateCopy;
    }
    default:
      return state;
  }
};
