import React from "react";
import "./button.scss";

export default function Buttons(props) {
  const { disabled, title } = props;
  return (
    <button type="button" disabled={disabled} className="buttonComponent">
      {title}
    </button>
  );
}
