import React, { useState, useRef } from "react";
import cx from "classnames";

// components
import NavLinks from "../layout/NavLinks";
import AuthButtons from "../layout/AuthButtons";
import ContactSupport from "../layout/ContactSupport";
import CopyRight from "../layout/CopyRight";

// hooks
import { useDisablePageScroll } from "../../hooks/window/scroll";

// icons
import { ReactComponent as BurgerIcon } from "../../assets/icons/burger.svg";
import { ReactComponent as CloseIcon } from "../../assets/icons/cross.svg";

import "./sideBarMenu.scss";

const SideBarMenu = () => {
  const [isOpen, setIsOpen] = useState(false);
  const menuContentRef = useRef(null);

  useDisablePageScroll(isOpen, menuContentRef.current);

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  const toggleOpen = () => {
    if (isOpen) {
      handleClose();
    } else {
      handleOpen();
    }
  };

  return (
    <div className="sideBarMenu">
      <div
        ref={menuContentRef}
        tabIndex={0}
        className={cx(isOpen ? "burger-menu" : "burger-closed")}
      >
        <button
          type="button"
          tabIndex={0}
          onKeyPress={toggleOpen}
          onClick={toggleOpen}
          className={cx(isOpen ? "burger-menu__header" : "burger-menu__lines")}
        >
          {isOpen ? <CloseIcon /> : <BurgerIcon />}
        </button>
        <div className="burger-menu-content">
          <AuthButtons handleNavMenuClose={handleClose} />
          <NavLinks
            type="button"
            className="navlinks"
            handleNavMenuClose={handleClose}
          />
          <ContactSupport />
          <CopyRight handleNavMenuClose={handleClose} />
        </div>
      </div>
    </div>
  );
};

export default SideBarMenu;
