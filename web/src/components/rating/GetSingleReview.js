import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import StarRatingComponent from "react-star-rating-component";

const GetSingleReview = ({ rating }) => {
  return (
    <div className="review-single">
      <div className="review-single__content">
        <StarRatingComponent
          name="rate1"
          starCount={5}
          editing={false}
          starColor="#e5c30a"
          renderStarIcon={() => <FontAwesomeIcon icon={["fa", "star"]} />}
          value={rating.score}
        />
        <p>{rating.text}</p>
      </div>
    </div>
  );
};

export default GetSingleReview;
