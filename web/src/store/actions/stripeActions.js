export const getSessionId = (requestOptions) => {
  return async (dispatch, getState, { getFirebase }) => {
    try {
      dispatch({
        type: "SET_PAYMENT_LOADING",
        paymentLoading: true,
      });
      const firebase = getFirebase();
      const func = firebase.functions().httpsCallable("createOrderAndSession");
      const result = await func(requestOptions);
      dispatch({
        type: "SET_PAYMENT_SESSION",
        sessionId: result.data.sessionId,
      });
    } catch (e) {
      dispatch({ type: "PAYMENT_ERROR", error: e });
      return e;
    } finally {
      dispatch({
        type: "SET_PAYMENT_LOADING",
        paymentLoading: false,
      });
    }
  };
};
export const getCoupon =(requestOptions) => {
  return async (dispatch, getState, { getFirebase }) => {
    try {
      dispatch({
        type: "START_LOADING",
       
      });

      const firebase = getFirebase();
      const func = firebase.functions().httpsCallable("retrieveStripeCoupon");

      const result = await func(requestOptions);
      console.log(result);
      return result;
    } catch (error) {
      return error;
    } finally {
      dispatch({
        type: "STOP_LOADING",
       
      });
    }
  };
};

export const createBookingWithCoupon = (requestOptions) => {
  return async (dispatch, getState, { getFirebase }) => {
    try {
      const firebase = getFirebase();
      const func = firebase
        .functions()
        .httpsCallable("createBookingWithCoupon");

      dispatch({
        type: "START_LOADING",
       
      });

      const result = await func(requestOptions);
      console.log(result);
      return result;
    } catch (error) {
      console.log(error);

      return error;
    } finally {
      dispatch({
        type: "STOP_LOADING",
       
      });
    }
  };
};

export const getAccountLink = (stripeAccountId) => {
  return async (dispatch, getState, { getFirebase }) => {
    try {
      dispatch({
        type: "SET_PAYMENT_LINK_LOADING",
        paymentLinkLoading: true,
      });
      const firebase = getFirebase();
      const getAccountFunc = firebase
        .functions()
        .httpsCallable("retrieveStripeAccount");
      const result = await getAccountFunc(stripeAccountId);
      if (!result.data.accountInfo.charges_enabled) {
        const getOnboardingLink = firebase
          .functions()
          .httpsCallable("getStripeOnboardingLink");
        const data = await getOnboardingLink({
          accountId: stripeAccountId,
          url: window.location.origin,
        });
        if (data.data.accountOnboardingLinkInfo.url) {
          dispatch({
            type: "SET_STRIPE_ONBOARDING_LINK",
            onBoardingStripeAccountLink:
              data.data.accountOnboardingLinkInfo.url,
          });
        }
      } else {
        const getLoginAccountLink = firebase
          .functions()
          .httpsCallable("getStripeLoginLink");
        const data = await getLoginAccountLink(stripeAccountId);
        if (data.data.accountLoginLinkInfo.url) {
          dispatch({
            type: "SET_STRIPE_LOGIN_LINK",
            loginStripeAccountLink: data.data.accountLoginLinkInfo.url,
          });
        }
      }
    } catch (e) {
      dispatch({ type: "GET_PAYMENT_LINK_ERROR", error: e });
      return e;
    } finally {
      dispatch({
        type: "SET_PAYMENT_LINK_LOADING",
        paymentLinkLoading: false,
      });
    }
    return null;
  };
};
