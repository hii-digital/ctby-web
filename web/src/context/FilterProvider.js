import React, {
  useState,
  useEffect,
  createContext,
  useCallback,
  useMemo,
  useRef,
} from "react";
import queryString from "query-string";
import { useSelector, shallowEqual } from "react-redux";
import { useRouteMatch, useHistory, useLocation } from "react-router-dom";
import { useFirestoreConnect, isLoaded } from "react-redux-firebase";

// data
import {
  PROFESSIONS,
  FITNESS_TRAINER,
  MASSAGE_THERAPIST,
  fitnessOptions,
  massageOptions,
  trainingTypeValues,
} from "../data/professions";
import { RADIUS } from "../data/address";

// helpers
import { getRating } from "../helpers/rating";
import { getNearbyZipcodes } from "../helpers/general";

export const FilterContext = createContext({});

const findAProPathname = "/find-a-pro";

const initialState = {
  proType: "",
  price: "",
  trainingType: "",
  interest: "",
  rating: "",
  zipcode: "",
};

const { inperson, online } = trainingTypeValues;

const FilterProvider = ({ children }) => {
  const history = useHistory();
  const location = useLocation();
  const matchExactFindAPro = useRouteMatch(findAProPathname)?.isExact;
  const matchExactProProfile = useRouteMatch("/pro/:uid")?.isExact;
  const matchExactHome = useRouteMatch("/")?.isExact;

  const [state, setState] = useState(initialState);
  const [filteredProsData, setFilteredPros] = useState({
    filteredPros: null,
    byNearbyZipcodes: false,
  });
  const { filteredPros } = filteredProsData;

  function isEmptyObject(obj) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            return false;
        }
    }

    return true;
}

 

  let { approvedUsers:approvedUsers } = useSelector(
    ({ firestore }) => {
      
      return firestore.ordered},
    shallowEqual
  );

  const approvedUsersWithStripeAndFilledCalendar = useMemo(() => {
    return approvedUsers?.filter((item) => {
      if (!item.isStripeOnboardingCompleted) {
        return false;
      }
      const specialDatesFilled =
        item.specialDates &&
        item.specialDates.length &&
        item.specialDates.find(({ state: slotState }) => slotState);

      const hoursFilled =
        item.Hours &&
        Object.values(item.Hours).find(({ state: slotState }) => !!slotState);
      return specialDatesFilled || hoursFilled;
    });
  }, [approvedUsers]);

  const { ordered } = useSelector(({ firestore }) => firestore);

  const approvedUids = useMemo(() => {
    return approvedUsersWithStripeAndFilledCalendar?.map(({ uid }) => uid);
  }, [approvedUsersWithStripeAndFilledCalendar]);

  const isApprovedUsersLoaded = isLoaded(approvedUsers);

  const { proType, price, trainingType, interest, zipcode, rating } = state;

  const isFilterSet =
    !!proType ||
    !!price ||
    !!trainingType ||
    !!interest ||
    !!zipcode ||
    !!rating;

  const setFilteredProsState = (filtered, byNearbyZipcodes = false) => {
    setFilteredPros({ filteredPros: filtered, byNearbyZipcodes });
  };

  const resetFilter = useCallback(() => {
    setState(initialState);
    setFilteredProsState(approvedUids);
    if (matchExactFindAPro) {
      history.push(findAProPathname);
    }
  }, [approvedUids, history, matchExactFindAPro]);

  useEffect(() => {
    if (matchExactHome) {
      resetFilter();
    }
  }, [matchExactHome, resetFilter]);

  useEffect(() => {
    if (
      !matchExactProProfile &&
      !matchExactFindAPro &&
      !matchExactHome &&
      isFilterSet
    ) {
      resetFilter();
    }
  }, [
    matchExactFindAPro,
    matchExactProProfile,
    matchExactHome,
    resetFilter,
    isFilterSet,
  ]);

  const stringifyUrl = useCallback(() => {
    return queryString.stringifyUrl(
      {
        url: findAProPathname,
        query: {
          ...state,
        },
      },
      { skipEmptyString: true }
    );
  }, [state]);

  const checkIsBookingsLoaded = useCallback(() => {
    if (!isApprovedUsersLoaded) {
      return null;
    }
    const bookings = approvedUids.map((uid) => ordered[`${uid}/bookingsAsPro`]);
    return isLoaded(...bookings);
  }, [approvedUids, isApprovedUsersLoaded, ordered]);

  const checkIsProfessionsLoaded = useCallback(() => {
    if (!isApprovedUsersLoaded) {
      return null;
    }
    const professions = approvedUids.map(
      (uid) => ordered[`${uid}/professions`]
    );
    return isLoaded(...professions);
  }, [approvedUids, isApprovedUsersLoaded, ordered]);

  const filteredWithoutZipcode = useMemo(() => {
    if (!checkIsProfessionsLoaded() || !checkIsBookingsLoaded()) {
      return null;
    }
    const getProRating = (uid) => {
      const bookings = ordered?.[`${uid}/bookingsAsPro`];
      return getRating(bookings);
    };

    const checkIsRateAcceptable = (rate) => {
      return rate <= price;
    };

    const checkIsRatingAcceptable = (proRating) => {
      return proRating >= rating;
    };

    return approvedUids?.filter((uid) => {
      const { rating: proRating } = getProRating(uid);
      const found = ordered[`${uid}/professions`]?.find((data) => {
        const { id, onlineRate, inpersonRate, specialties } = data;

        if (proType && proType !== id) {
          return false;
        }
        if (interest && !specialties.includes(interest)) {
          return false;
        }
        if (rating && !checkIsRatingAcceptable(proRating)) {
          return false;
        }
        if (trainingType && trainingType === online.value && !onlineRate) {
          return false;
        }
        if (trainingType && trainingType === inperson.value && !inpersonRate) {
          return false;
        }
        if (
          price &&
          !checkIsRateAcceptable(onlineRate) &&
          !checkIsRateAcceptable(inpersonRate)
        ) {
          return false;
        }

        if (price) {
          if (trainingType === online.value) {
            return checkIsRateAcceptable(onlineRate);
          }
          if (trainingType === inperson.value) {
            return checkIsRateAcceptable(inpersonRate);
          }
          if (
            !checkIsRateAcceptable(onlineRate) &&
            !checkIsRateAcceptable(inpersonRate)
          ) {
            return false;
          }
        }
        return true;
      });
      return found;
    });
  }, [
    checkIsProfessionsLoaded,
    checkIsBookingsLoaded,
    approvedUids,
    ordered,
    price,
    rating,
    proType,
    interest,
    trainingType,
  ]);

  const { filtered, byNearbyZipcodes = false } = useMemo(() => {
    if (!checkIsProfessionsLoaded() || !checkIsBookingsLoaded()) {
      return { filtered: null };
    }
    if (zipcode) {
      const checkIsCityMatch = (searchVal, cityWordsArray) => {
        return cityWordsArray.find((w) =>
          w
            .toLowerCase()
            .startsWith(
              searchVal.toLowerCase().slice(0, Math.max(w.length - 1, 1))
            )
        );
      };

      const checkIsStateMatch = (searchVal, stateWordsArray) => {
        return stateWordsArray.find((w) =>
          w.toLowerCase().startsWith(searchVal.toLowerCase())
        );
      };

      const searchByNearByZipcodes = () => {
        const nearbyZipcodes = getNearbyZipcodes({ zipcode, radius: RADIUS });
        if (nearbyZipcodes?.length) {
          return filteredWithoutZipcode.filter((uid) =>
            ordered[`${uid}/professions`]?.find(({ zipCode }) =>
              nearbyZipcodes.includes(zipCode)
            )
          );
        }
        return [];
      };

      const foundBySpecificZipcode = filteredWithoutZipcode.filter((uid) => {
        return ordered[`${uid}/professions`]?.find(
          ({ zipCode, city, state: stateVal }) => {
            return (
              zipCode === zipcode ||
              checkIsCityMatch(zipcode, city.split(" ")) ||
              checkIsStateMatch(zipcode, stateVal.split(" "))
            );
          }
        );
      });

      if (foundBySpecificZipcode.length) {
        return { filtered: foundBySpecificZipcode };
      }

      return { filtered: searchByNearByZipcodes(), byNearbyZipcodes: true };
    }
    return { filtered: filteredWithoutZipcode };
  }, [
    checkIsProfessionsLoaded,
    checkIsBookingsLoaded,
    zipcode,
    filteredWithoutZipcode,
    ordered,
  ]);

  const searchPros = async (e) => {
    e.preventDefault();
    const url = stringifyUrl();
    history.push(url);
    setFilteredProsState(filtered, byNearbyZipcodes);
  };

  const prosFetch =
    approvedUsers?.map(({ uid }) => ({
      collection: `users/${uid}/professions`,
      storeAs: `${uid}/professions`,
    })) || [];

  const bookingsFetch =
    approvedUsersWithStripeAndFilledCalendar?.map(({ uid }) => ({
      collection: "bookings",
      where: ["proId", "==", uid],
      storeAs: `${uid}/bookingsAsPro`,
    })) || [];

  useFirestoreConnect([
    {
      collection: "users",
      where: ["isApproved", "==", true],
     
      storeAs: "approvedUsers",
    },
    ...prosFetch,
    ...bookingsFetch,
  ]);

  useEffect(() => {
    if (!filteredPros) {
      if (location.search) {
        setFilteredProsState(filtered, byNearbyZipcodes);
      } else if (approvedUids) {
        setFilteredProsState(approvedUids);
      }
    }
  }, [approvedUids, byNearbyZipcodes, filtered, filteredPros, location.search]);

  const prevLocationSearch = useRef(null);

  useEffect(() => {
    if (
      isApprovedUsersLoaded &&
      location.search !== prevLocationSearch.current
    ) {
      const currentSearch = location.search;
      const { query: filteredValues } = queryString.parseUrl(currentSearch);
      setState((prevState) => ({
        ...prevState,
        ...filteredValues,
      }));
      prevLocationSearch.current = currentSearch;
    }
  }, [approvedUids, location.search, isApprovedUsersLoaded]);

  const getProTypeOptions = () => {
    const isFitnessInterestChosen = !!fitnessOptions.find(
      ({ value }) => interest === value
    );
    const isMassageInterestChosen = !!massageOptions.find(
      ({ value }) => interest === value
    );

    if (
      isFitnessInterestChosen &&
      isMassageInterestChosen &&
      trainingType !== online.value
    ) {
      return PROFESSIONS;
    }

    if (trainingType === online.value || isFitnessInterestChosen) {
      return PROFESSIONS.filter(({ value }) => value === FITNESS_TRAINER);
    }
    if (isMassageInterestChosen) {
      return PROFESSIONS.filter(({ value }) => value === MASSAGE_THERAPIST);
    }
    return PROFESSIONS;
  };

  const getInterestsOptions = () => {
    if (proType === FITNESS_TRAINER || trainingType === online.value) {
      return fitnessOptions;
    }
    if (proType === MASSAGE_THERAPIST) {
      return massageOptions;
    }
    const noDublicatesOptions = [...fitnessOptions, ...massageOptions].filter(
      (option, index, options) =>
        options.findIndex((o) => option.value === o.value) === index
    );
    return noDublicatesOptions;
  };

  const getTrainingTypeOptions = () => {
    const isMassageInterestChosen = !!massageOptions.find(
      ({ value }) => interest === value
    );
    const isFitnessInterestChosen = !!fitnessOptions.find(
      ({ value }) => interest === value
    );
    if (
      proType === MASSAGE_THERAPIST ||
      (isMassageInterestChosen && !isFitnessInterestChosen)
    ) {
      return [{ value: inperson.value, text: inperson.text }];
    }
    return [
      { value: online.value, text: online.text },
      { value: inperson.value, text: inperson.text },
    ];
  };

  const updateState = ({ key, value }) => {
    setState((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  };

  const ctx = {
    ...state,
    ...filteredProsData,
    updateState,
    proTypeOptions: getProTypeOptions(),
    interestsOptions: getInterestsOptions(),
    trainingTypeOptions: getTrainingTypeOptions(),
    searchPros,
    resetFilter,
    checkIsProfessionsLoaded,
    checkIsBookingsLoaded,
  };

  return (
    <FilterContext.Provider value={ctx}>{children}</FilterContext.Provider>
  );
};

export default FilterProvider;
