export const STATUSES = {
  awaitingApproval: {
    value: "awaitingApproval",
    text: "Awaiting Approval",
  },
  pending: {
    value: "pending",
    text: "Upcoming",
  },
  active: {
    value: "active",
    text: "Active",
  },
  cancelled: {
    value: "cancelled",
    text: "Cancelled",
  },
  declined: {
    value: "declined",
    text: "Declined",
  },
  completed: {
    value: "completed",
    text: "Completed",
  },
};
