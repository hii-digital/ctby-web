import React, { useState } from "react";
import { Menu, MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import UserIcon from "../../components/userIcon/UserIcon";

import colors from "../colors";

export const useStyles = makeStyles(() => ({
  paper: {
    boxShadow: "0px 8px 16px rgb(0 0 0 / 5%)",
    "& .MuiList-padding": {
      paddingTop: 0,
      paddingBottom: 0,
    },
    "& .MuiListItem-root": {
      "&:not(:last-child)": {
        borderBottom: `1px solid ${colors.GREY_E0D}`,
      },
      color: colors.BLACK_272,
      height: 40,
      "&:hover": {
        background: "rgba(237, 153, 84, 0.1)",
        color: colors.ORANGE_ED9,
      },
    },
  },
}));

const AvatarMenu = ({ menu }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <UserIcon onClick={handleOpen} />
      <Menu
        autoFocus={false}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        keepMounted
        elevation={0}
        getContentAnchorEl={null}
        classes={{
          paper: classes.paper,
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        {menu.map((item) => {
          const handleMenuClick = () => {
            item.action();
            handleClose();
          };

          return (
            <MenuItem key={item.label} onClick={handleMenuClick}>
              {item.label}
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
};

export default AvatarMenu;
