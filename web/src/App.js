import React, { useEffect,Suspense } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Route, Switch, useLocation, Redirect } from "react-router-dom";
import { loadReCaptcha } from "react-recaptcha-google";
import ScrollToTop from "react-router-scroll-top";

// components
import { clearSession } from "./store/actions/conversationsActions";

// hooks
import { useBreakpoints } from "./hooks/window/breakpoints";

import "./vendor/fontawesome";
import "./assets/styles/common/app.scss";




import ProtectedRoute from "./routes";
import Loading from "./components/modules/Loading";


const AdminView= React.lazy(() => import('./components/admin/AdminView'));

const JoinAsPro= React.lazy(() => import('./components/auth/pro/JoinAsPro'));
const ClientToPro= React.lazy(() => import('./components/auth/clientToPro'));
const SignUp= React.lazy(() => import('./components/auth/client/SignUp'));
const SignIn= React.lazy(() => import('./components/auth/SignIn'));

const Calendar = React.lazy(() => import('./components/calendar/Calendar'));
const FileClaim = React.lazy(() => import('./components/claims/FileClaim'));


const Dashboard = React.lazy(() => import('./components/dashboard/Dashboard'));
const Settings = React.lazy(() => import('./components/dashboard/Settings'));
const Report = React.lazy(() => import('./components/dashboard/Report'));
// const ClientReport = React.lazy(() => import('./components/dashboard/ClientReport'));


const Social = React.lazy(() => import('./components/landing/Social'));
const Inbox = React.lazy(() => import('./components/inbox/Inbox'));



const Navbar = React.lazy(() => import('./components/layout/Navbar'));
const Footer = React.lazy(() => import('./components/layout/Footer'));
const GenericNotFound = React.lazy(() => import('./components/layout/GenericNotFound'));


const Bookings = React.lazy(() => import('./components/modules/Bookings'));
const About = React.lazy(() => import('./components/modules/About'));
const NavSidebarWrapper  = React.lazy(() => import('./components/wrappers/NavSidebarWrapper'));
const NavSidebar = React.lazy(() => import('./components/layout/NavSidebar'));

const CancellationPolicy = React.lazy(() => import('./components/modules/CancellationPolicy'));
const ContactUs = React.lazy(() => import('./components/modules/ContactUs'));
const Home = React.lazy(() => import('./components/modules/Home'));
const FindAPro = React.lazy(() => import('./components/modules/FindAPro'));
const ProfileEdit = React.lazy(() => import('./components/modules/ProfileEdit'));
const Terms = React.lazy(() => import('./components/modules/Terms'));

const Privacy = React.lazy(() => import('./components/modules/Privacy'));
const HowItWorks = React.lazy(() => import('./components/modules/HowItWorks'));
const HowProWorks = React.lazy(() => import('./components/modules/HowProWorks'));


const Onboarding= React.lazy(() => import('./components/onboarding/pro'));
const OnboardingClient= React.lazy(() => import('./components/onboarding/client'));
const Payments = React.lazy(() => import('./components/modules/Payments'));

const Profile = React.lazy(() => import('./components/profile/Profile'));
const CreateProject = React.lazy(() => import('./components/projects/CreateProject'));
const Resubmit = React.lazy(() => import('./components/resubmit/Resubmit'));
const SessionPage = React.lazy(() => import('./components/bookings/SessionPage'));
const Conversation = React.lazy(() => import('./components/inbox/Conversation'));
const Faq = React.lazy(() => import('./components/modules/Faq'));
const ThankYouConvertKit = React.lazy(() => import('./components/promo/ThankYouConvertKit'));










// actions



const App = () => {
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { authValid } = useSelector((store) => store.auth, shallowEqual);



  const { api: twilioClient } = useSelector(
    ({ conversationsData }) => conversationsData,
    shallowEqual
  );

  const { isBreakpointL } = useBreakpoints();

  const footerList = [
    "/",
    "/find-a-pro",
    "/how-it-works",
    "/faq",
    "/about",
    "/contact",
    "/privacy-policy",
    "/terms-of-use",
  ];

  useEffect(() => {
    loadReCaptcha();
  }, []);

  useEffect(() => {
    if (authValid === null)
      if (auth.isLoaded && auth?.uid) {
        dispatch({ type: "LOGIN_SUCCESS" });
      }
  }, [auth, authValid, dispatch]);

  // TO_DO ask about this
  useEffect(() => {
    if (twilioClient && !auth.uid) {
    /**
     *   twilioClient.shutdown();
      dispatch(clearSession());
     */
    }
  }, [twilioClient, auth.uid, dispatch]);

  useEffect(() => {
    const script = document.createElement("script");
    script.setAttribute("id", "_agile_min_js");
    script.onload = () => {
      const newScript = document.createElement("script");
      const inlineScript = document.createTextNode(
        "var Agile_API = Agile_API || {}; Agile_API.on_after_load = function(){_agile.set_account('3h6emnq5000gjh577j9gk2ca9n', 'choosetobeyou', false);_agile.track_page_view();_agile_execute_web_rules();}"
      );
      newScript.setAttribute("id", "_agile_js");
      newScript.appendChild(inlineScript);
      document.getElementsByTagName("head")[0].appendChild(newScript);
    };
    script.src = "https://choosetobeyou.agilecrm.com/stats/min/agile-min.js";
    document.getElementsByTagName("head")[0].appendChild(script);
  }, []);

  useEffect(() => {
    const agileMinJS = document.getElementById("_agile_min_js");
    const agileJS = document.getElementById("_agile_js");
    if (agileMinJS && agileJS) {
      agileMinJS.remove();
      agileJS.remove();
    }
  }, []);

  if (!auth.isLoaded) {
    return <Loading />;
  }


  return (
    <Suspense fallback={<Loading />}>
    <ScrollToTop>
      <div className="App">
        <Navbar />
        <NavSidebarWrapper>
          {isBreakpointL && <NavSidebar />}
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/social" component={Social} />
            <Route
              path="/thank-you-for-subscribing"
              component={ThankYouConvertKit}
            />
            <ProtectedRoute
              exact
              path="/admin"
              component={AdminView}
              onlyForAdmin
            />
            <Route exact path="/" component={Home} />
            <ProtectedRoute exact path="/dashboard" component={Dashboard} />
            {/* <Route exact path="/project/:id" component={ProjectDetails} /> */}
            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/join-as-pro" component={JoinAsPro} />
            <Route exact path="/create-project" component={CreateProject} />
            <Route exact path="/find-a-pro" component={FindAPro} />
            <ProtectedRoute exact path="/inbox" component={Inbox} />
            <ProtectedRoute
              exact
              path="/conversations/:id/:uniqueName?"
              component={Conversation}
            />
            <ProtectedRoute exact path="/session/:id" component={SessionPage} />
            <ProtectedRoute exact path="/bookings" component={Bookings} />
            <Route exact path="/pro/:uid" component={Profile} />
            <ProtectedRoute
              exact
              path="/profile-edit"
              component={ProfileEdit}
            />
            <Route exact path="/about" component={About} />
            <Route exact path="/terms-of-use" component={Terms} />
            <Route exact path="/privacy-policy" component={Privacy} />
            <ProtectedRoute exact path="/onboarding" component={Onboarding} />
            <ProtectedRoute exact path="/resubmit" component={Resubmit} />
            <ProtectedRoute
              exact
              path="/onboarding-client"
              component={OnboardingClient}
            />
            <Route exact path="/social" component={Social} />
            <Route exact path="/how-it-works" component={HowItWorks} />
            <Route
              exact
              path="/how-ctby-works-for-pros"
              component={HowProWorks}
            />
            <ProtectedRoute exact path="/settings" component={Settings} />
            <ProtectedRoute exact path="/report" component={Report} />
            {/* <ProtectedRoute exact path="/client-report" component={ClientReport} /> */}
            <ProtectedRoute
              exact
              path="/calendar"
              component={Calendar}
              onlyForPro
            />
            <Route exact path="/faq" component={Faq} />
            <Route exact path="/file-claim" component={FileClaim} />
            <Route exact path="/contact" component={ContactUs} />
            <Route exact path="/upgrade-pro" component={ClientToPro} />
            <Route
              exact
              path="/cancellation-policy"
              component={CancellationPolicy}
            />
            <Route exact path="/payments" component={Payments} />
            <Route exact path="/how-it-works?pro" component={HowItWorks} />
            <Route path="/404" component={GenericNotFound} />
            <Redirect to="/404" />
          </Switch>
        </NavSidebarWrapper>
        {footerList.includes(pathname) && <Footer />}
        {/* <InteractionCron /> */}
      </div>
    </ScrollToTop>
    </Suspense>
  );
};

export default App;
