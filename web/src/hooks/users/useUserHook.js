import { useFirestoreConnect } from "react-redux-firebase";
import { useSelector, shallowEqual } from "react-redux";

export const useUserHook = (uid) => {
  useFirestoreConnect([
    {
      collection: "users",
      doc: uid,
      storeAs: `${uid}/data`,
    },
  ]);

  const { userData } = useSelector(
    ({ firestore }) => ({
      userData: firestore.data[`${uid}/data`],
    }),
    shallowEqual
  );
  return userData;
};
