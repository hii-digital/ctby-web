const initState = {
  sessionId: null,
  paymentError: null,
  paymentLoading: false,
  getLinkLoading: false,
  stripeAccountLink: null,
};

const stripeReducer = (state = initState, action) => {
  switch (action.type) {
    case "PAYMENT_ERROR":
      return {
        ...state,
        sessionId: null,
        paymentError: action.error,
      };
    case "SET_PAYMENT_SESSION":
      return {
        ...state,
        sessionId: action.sessionId,
        paymentError: null,
      };
    case "SET_PAYMENT_LOADING":
      return {
        ...state,
        paymentLoading: action.paymentLoading,
      };
    case "SET_PAYMENT_LINK_LOADING":
      return {
        ...state,
        getLinkLoading: action.paymentLinkLoading,
      };
    case "SET_STRIPE_ONBOARDING_LINK":
      return {
        ...state,
        stripeAccountLink: action.onBoardingStripeAccountLink,
      };
    case "SET_STRIPE_LOGIN_LINK":
      return {
        ...state,
        stripeAccountLink: action.loginStripeAccountLink,
      };
    default:
      return state;
  }
};

export default stripeReducer;
