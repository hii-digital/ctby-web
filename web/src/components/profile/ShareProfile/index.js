import React, { useContext } from "react";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// icons
import { ReactComponent as FacebookIcon } from "../../../assets/icons/facebook-share.svg";
import { ReactComponent as TwitterIcon } from "../../../assets/icons/twitter.svg";
import { ReactComponent as PinterestIcon } from "../../../assets/icons/pinterest.svg";

import "./styles.scss";

const ShareProfile = () => {
  const {
    payloads: { firstName },
  } = useContext(SidebarContext);

  return (
    <div className="sidebar-root share">
      <h1>share profile</h1>
      <div>
        <span>
          Share {firstName}’s profile on your favorite social media platform
        </span>
        <div>
          <a
            href={`http://www.facebook.com/sharer/sharer.php?u=${window.location.href}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span>
              <i>
                <FacebookIcon className="facebook-icon" />
              </i>
              <span>Share on Facebook</span>
            </span>
          </a>
          <a
            href={`https://twitter.com/intent/tweet?text=Choose%20To%20Be%20You&url=${window.location.href}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span>
              <i>
                <TwitterIcon className="twitter-icon" />
              </i>
              <span>Share on Twitter</span>
            </span>
          </a>
          <a
            href={`http://pinterest.com/pin/create/button/?url=${window.location.href}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span>
              <i>
                <PinterestIcon className="pinterest-icon" />
              </i>
              <span>Share on Pinterest</span>
            </span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default ShareProfile;
