const getDetails = (
  text,
  { name, location, date, time, service, hours, rate }
) => {
  return `${text}\nName: ${name} \nLocation: ${location} \nDate & Time: ${date} ${time} \nService: ${service} \nPrice: $${rate} x ${
    hours / 60
  }hours = $${rate * (hours / 60)}`;
};

const notificationsForBoth = {
  sessionStartsIn: {
    text: "Just a friendly reminder that you have an upcoming session.",
    getFinal({ url, id, details }) {
      return `${getDetails(this.text, details)}\n${url}/session/${id}`;
    },
  },
  newMessage: {
    text: "You have a new message.",
    getFinal({ url, sid }) {
      return `${this.text}\n${url}/conversations/${sid}`;
    },
  },
  unreadMessage: {
    text: "You have unread messages.",
    getFinal({ url, sid }) {
      return `${this.text}\n${url}/conversations/${sid}`;
    },
  },
};

exports.notificationsPro = {
  confirmationDetails: {
    text: "You have confirmed the following session:",
    getFinal(details) {
      return getDetails(this.text, details);
    },
  },
  sessionCancelled: {
    text: "Your session has been cancelled.",
    getFinal() {
      return this.text;
    },
  },
  clientRequestToBook: {
    text: "You have a client request to book a session.",
    getFinal({ url, id }) {
      return `${this.text} \n${url}/session/${id}`;
    },
  },
  ...notificationsForBoth,
};

exports.notificationsClient = {
  confirmationDetails: {
    text: "Your pro has confirmed your session. Enjoy your experience!",
    getFinal(details) {
      return getDetails(this.text, details);
    },
  },
  sessionCancelled: {
    text: "Your session has been cancelled. Find another pro near you.",
    getFinal(url) {
      return `${this.text} \n${url}/find-a-pro`;
    },
  },
  sessionDeclined: {
    text: "Your session has been declined. Find another pro near you.",
    getFinal(url) {
      return `${this.text} \n${url}/find-a-pro`;
    },
  },
  sessionCompleted: {
    text:
      "Your session has been completed.  We hope you enjoyed it! Please rate your pro.",
    getFinal({ url, id }) {
      return `${this.text}\n${url}/session/${id}`;
    },
  },
  proOnWay: {
    text: "Your pro is on the way! Enjoy your session.",
    getFinal() {
      return this.text;
    },
  },
  refundInitiated: {
    text: "Your refund has been initiated.",
    getFinal() {
      return this.text;
    },
  },
  ...notificationsForBoth,
};

exports.notificationsForBoth = notificationsForBoth;
