/* eslint-disable */
import React, { useState, useContext } from "react";
import { useSelector, shallowEqual } from "react-redux";
import cx from "classnames";

// context
import { SidebarContext } from "../../context/SidebarProvider";

// config
import firebaseApi from "../../config/fbConfig";

const ContactUs = () => {
  const [state, setState] = useState({
    subject: "",
    message: "",
    formSent: false,
  });

  const { openSidebar } = useContext(SidebarContext);

  const setNewState = (s) => {
    setState((prevState) => ({ ...prevState, ...s }));
  };

  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );

  const { subject, message, formSent } = state;

  const handleChange = (e) => {
    setNewState({ [e.target.id]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!profile.uid) {
      openSidebar("signInClient");
      return;
    }

    const { email } = auth;
    const text = `First name: ${profile.firstName}\nEmail: ${email}\nMessage: ${message}\n`;
    const contactAdmin = firebaseApi.functions().httpsCallable("contactAdmin");

    contactAdmin({
      message: text,
      subject,
      email,
    })
      .then(() => {
        setNewState({
          formSent: true,
          subject: "",
          message: "",
        });
      })
      .catch((err) => console.log("contactAdmin err", err));
  };

  const checkIsValueEmpty = (value) => {
    return !value.trim().length;
  };

  return (
    <div className="settings">
      <div className="container container--top-bottom-padding container--small">
        <div className="row">
          <div className="col">
            <div className="dashboard__head">
              <h1 className="text--lg text--uppercase">Contact Us</h1>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col" style={{ marginBottom: "50px" }}>
            <div>
              <form
                encType="multipart/form-data"
                className="modal__form"
                onSubmit={handleSubmit}
              >
                <div className="field">
                  <label>Enter the subject</label>
                  <input
                    type="text"
                    name="subject"
                    id="subject"
                    value={subject}
                    onChange={handleChange}
                    placeholder="Subject"
                  />
                </div>

                <div className="field">
                  <label>Message</label>
                  <textarea
                    type="textarea"
                    name="message"
                    id="message"
                    value={message}
                    onChange={handleChange}
                    placeholder="Message"
                  />
                </div>

                <button
                  type="submit"
                  className={cx("button button--accent", {
                    inactive:
                      checkIsValueEmpty(subject) || checkIsValueEmpty(message),
                  })}
                >
                  Send Message
                </button>
              </form>
              <div
                className="modal__status modal__status--sent"
                style={formSent ? { display: "block" } : { display: "none" }}
              >
                Thank you! The message has been sent. Please allow up to 2
                business days for our team to review.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactUs;
