const initState = {
  authError: null,
  authValid: null,
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case "LOGIN_ERROR":
      return {
        ...state,
        authError: "Login failed",
      };
    case "LOGIN_SUCCESS":
      return {
        ...state,
        authError: null,
        authValid: true,
      };
    case "SIGNOUT_SUCCESS":
      return state;
    case "SIGNUP_SUCCESS":
      return {
        ...state,
        authError: null,
      };
    case "SIGNUP_ERROR":
      return {
        ...state,
        authError: action.err.message,
      };
    case "CLEAR_ERROR_MESSAGE":
      return {
        ...state,
        authError: null,
      };
    case "RESET_AUTH_VALID":
      return {
        ...state,
        authValid: false,
      };
    default:
      return state;
  }
};

export default authReducer;
