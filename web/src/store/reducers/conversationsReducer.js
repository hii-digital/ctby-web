const initState = {
  api: null,
  conversationsMap: {},
  messages: [],
  changesMap: {},
  initLoaded: null,
  isLoading: false,
};

const conversationsReducer = (state = initState, action) => {
  switch (action.type) {
    case "SET_INIT": {
      return {
        ...state,
        api: action.api,
        conversationsMap: action.conversationsMap,
      };
    }
    case "SET_INIT_LOADED": {
      return {
        ...state,
        initLoaded: action.status,
      };
    }
    case "SET_LOADING": {
      return {
        ...state,
        isLoading: action.isLoading,
      };
    }

    case "SET_CHANGE_ID": {
      const { changesMap } = action;
      return {
        ...state,
        changesMap,
      };
    }

    case "UPDATE_CHANGE_ID": {
      const { sid, newDateCreated, author, body, index } = action;
      return {
        ...state,
        changesMap: {
          ...state.changesMap,
          [sid]: {
            ...state.changesMap[sid],
            dateCreated: newDateCreated,
            author,
            body,
            index,
          },
        },
      };
    }

    case "ADD_CONVERSATION": {
      return {
        ...state,
        conversationsMap: { ...state.conversationsMap, ...action.conversation },
      };
    }

    case "ADD_MESSAGE": {
      return {
        ...state,
        messages: [...state.messages, action.message],
      };
    }
    case "SET_MESSAGES": {
      return {
        ...state,
        messages: action.messages,
      };
    }

    case "CLEAR_SESSION": {
      return initState;
    }

    default:
      return state;
  }
};

export default conversationsReducer;
