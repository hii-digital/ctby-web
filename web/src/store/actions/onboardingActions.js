import { startLoading, stopLoading } from "./loadingActions";

// helpers
import { fileStorage } from "../../config/fbConfig";

export const updateUser = ({ uid, payloads }) => {
  return (dispatch, getState, { getFirestore }) => {
    dispatch(startLoading());
    const firestore = getFirestore();
    return firestore
      .collection("users")
      .doc(uid)
      .set(payloads, { merge: true })
      .then(() => {
        dispatch(stopLoading());
      })
      .catch((error) => {
        console.log("error", error);
        dispatch(stopLoading());
      });
  };
};

export const updateProfessionNode = ({ uid, updates }) => {
  return (dispatch, getState, { getFirestore }) => {
    dispatch(startLoading());

    const firestore = getFirestore();
    const batch = firestore.batch();
    const professionsDocsRefs = Object.keys(updates).map((professionName) =>
      firestore
        .collection("users")
        .doc(uid)
        .collection("professions")
        .doc(professionName)
    );

    professionsDocsRefs.forEach((ref, i) => {
      const updatesValues = Object.values(updates)[i];

      updatesValues.forEach(({ node, value }) => {
        if (!value) {
          batch.update(ref, { [node]: firestore.FieldValue.delete() });
          return;
        }
        batch.set(ref, { [node]: value }, { merge: true });
      });
    });

    return batch
      .commit()
      .then(() => {
        dispatch(stopLoading());
      })
      .catch((e) => {
        console.log("error", e);
        dispatch(stopLoading());
      });
  };
};

export const deleteProfileImage = (uid) => {
  return (dispatch, getState, { getFirestore }) => {
    dispatch(startLoading);
    const firestore = getFirestore();
    return firestore
      .collection("users")
      .doc(uid)
      .update({
        photoURL: firestore.FieldValue.delete(),
      })
      .then(() => {
        dispatch(stopLoading());
      })
      .catch((error) => {
        console.log("error", error);
        dispatch(stopLoading());
      });
  };
};

export const removeLicenseImage = (uid, professionVal) => {
  return async (dispatch, getState, { getFirestore }) => {
    dispatch(startLoading());
    await fileStorage
      .ref(`users/${uid}/license/${professionVal}`)
      .delete()
      .catch((error) => {
        console.log("error", error);
        dispatch(stopLoading());
      });

    const firestore = getFirestore();
    const proRef = firestore
      .collection("users")
      .doc(uid)
      .collection("professions")
      .doc(professionVal);

    return proRef
      .update({ licenseURL: firestore.FieldValue.delete() })
      .then(() => {
        dispatch(stopLoading());
      })
      .catch((error) => {
        dispatch(stopLoading());
        console.log("error", error);
      });
  };
};
