import React, { useContext } from "react";

// context
import { SidebarContext } from "../../context/SidebarProvider";

// styles
import "./signedOutLinks.scss";

const SignedOutLinks = () => {
  const { openSidebar } = useContext(SidebarContext);

  const handleOpenSidebar = () => {
    openSidebar("joinAsPro");
  };

  return (
    <ul>
      <li>
        <button
          type="button"
          onClick={handleOpenSidebar}
          className="header__nav-link-btn pure-button"
        >
          Join as Pro
        </button>
      </li>
    </ul>
  );
};

export default SignedOutLinks;
