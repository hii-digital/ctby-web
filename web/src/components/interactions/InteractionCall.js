/* eslint-disable */
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { connect as twilioConnect } from "twilio-video";
import { Calling } from "../../store/actions/interactionActions";
import Modal from "../modal/Modalreact";
import RoomCall from "./RoomCall";

// config
import firebase from "../../config/fbConfig";

class InteractionCall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCaller: false,
      open: false,
      state: "",
      mute: false,
      // stop: false,
      roomName: "Test",
      room: null,
    };
    this.peerConnections = [];

    this.answerCall = this.answerCall.bind(this);
    this.dropCall = this.dropCall.bind(this);
  }

  createNotification = () => {
    if (this.props.interaction.proUID === this.props.auth.uid) {
      this.props.Call(this.props.iid, "pro");
    } else {
      this.props.Call(this.props.iid, "user");
    }
  };

  clearNotification = () => {
    console.log("clear");
    this.props.Call(this.props.iid, "");
  };

  getOtherUserName = () => {
    if (this.props.interaction.proUID === this.props.auth.uid) {
      return `${this.props.interaction.userFirstName} ${this.props.interaction.userLastName} `;
    }
    return `${this.props.interaction.proFirstName} ${this.props.interaction.proLastName} `;
  };

  answerCall() {
    this.clearNotification();
    this.setState({ state: "callactive" });
  }

  closeModal() {
    this.setState({ open: false });
    this.clearNotification();
  }

  muteToggle = () => {
    const video = document.getElementById("current-user-video");
    const stream = video.srcObject;
    if (stream) {
      stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled;
      this.setState({ mute: !stream.getAudioTracks()[0].enabled });
    }
  };

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  statusContent() {
    switch (this.state.state) {
      case "callpending":
        return <p>Calling...</p>;
      case "callincoming":
        return (
          <button
            type="button"
            className="button button--full button--primary"
            onClick={this.answerCall}
          >
            Answer Video Call
          </button>
        );
      case "callactive":
        return (
          <button
            type="button"
            className="button button--full button--primary"
            onClick={this.dropCall}
          >
            End Video Call
          </button>
        );
      case "callended":
        return (
          <button
            type="button"
            className="button button--full button--primary"
            onClick={this.closeModal}
          >
            Close
          </button>
        );
    }
  }

  createRoom = async () => {
    const { roomName } = this.state;
    const getToken = firebase.functions().httpsCallable("getToken");
    const { data: tokenVal } = await getToken({
      identity: this.props.auth.uid,
      room: roomName,
    });
    const roomVal = await twilioConnect(tokenVal, {
      name: roomName,
      audio: true,
      video: { width: 640 },
    });

    this.setState((prevState) => ({ ...prevState, room: roomVal }));
  };

  createCall = async () => {
    await this.createRoom();
    this.setState({ isCaller: true, state: "callpending", open: true });
  };

  handleLogout = () => {
    this.setState((prevState) => {
      const { room: prevRoom } = prevState;
      if (prevRoom) {
        prevRoom.localParticipant.tracks.forEach((trackPub) => {
          trackPub.track.stop();
        });
        prevRoom.disconnect();
      }
      return {
        ...prevState,
        room: null,
      };
    });
  };

  dropCall() {
    this.clearNotification();
    this.handleLogout();
    this.setState({ state: "callended", open: false });
  }

  render() {
    return (
      <div className="interaction-call--component">
        <div className="text--center">
          <Modal
            buttonStyle="button button--secondary button--full"
            buttonText="Start Video Call"
            call={this.state.state === "callincoming"}
            isOpen={this.state.open}
            content={
              this.state.room ? (
                <RoomCall
                  room={this.state.room}
                  getOtherUserName={this.getOtherUserName}
                  profile={this.props.profile}
                  mute={this.state.mute}
                  muteToggle={this.muteToggle}
                  videoToggle={this.videoToggle}
                  statusContent={this.statusContent}
                  stop={this.state.stop}
                />
              ) : null
            }
            openEvent={this.createCall}
            closeEvent={this.dropCall}
            fullScreenOptions
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Call: (iid, payload) => dispatch(Calling(iid, payload)),
  };
};
const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InteractionCall);
