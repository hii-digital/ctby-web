import React from "react";
import { Link } from "react-router-dom";

// components
import ConversationSummary from "./ConversationSummary";
import TopNotificationMessage from "../../ui/TopNotificationMessage";
import CircularProgress from "../../ui/CircularProgress";

// hooks
import { useConversations } from "../../hooks/conversations";

// helpers
import { parseUniqueName } from "../../helpers/conversations";

const Inbox = () => {
  const { conversationsArr, changesMap, initLoaded } = useConversations();

  const sorted =
    initLoaded &&
    conversationsArr.slice().sort((a, b) => {
      return changesMap[b.sid]?.dateCreated - changesMap[a.sid]?.dateCreated;
    });

  return (
    <div className="inbox">
      <TopNotificationMessage />
      <div className="container container--top-bottom-padding container--small">
        <div className="row">
          <div className="col">
            <div className="inbox__head">
              <h1 className="text--lg text--uppercase">Inbox</h1>
            </div>
          </div>
        </div>
        {initLoaded !== "success" ? (
          <CircularProgress size={20} />
        ) : (
          <div className="interaction-list">
            {sorted.length ? (
              sorted?.map(({ sid, uniqueName, lastReadMessageIndex }, i) => {
                const lastReadMessageIdx =
                  lastReadMessageIndex === null ? -1 : lastReadMessageIndex;
                return (
                  <Link to={`conversations/${sid}`} key={sid}>
                    <ConversationSummary
                      participants={parseUniqueName(uniqueName)}
                      conversation={sorted[i]}
                      lastReadMessageIdx={lastReadMessageIdx}
                      {...changesMap[sid]}
                    />
                  </Link>
                );
              })
            ) : (
              <span>You have not any conversations yet.</span>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Inbox;
