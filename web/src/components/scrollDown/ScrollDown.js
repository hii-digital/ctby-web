import React from 'react';
import "./scrollDown.scss";

export default function ScrollDown () {
    return (
      <div>
        <div className="scrollDown-wrapper">
          <div className="scrollDown">
          <section id="section07">

            <a href="#section08">
              <p className="scrollDown__textBlock">Scroll Down For More</p>
              <div className="scrollDown__arrowBlock">
                <span></span>
                <span></span>
              </div>
            </a>
          </section>
          </div>
        </div>
        </div>
    )
}