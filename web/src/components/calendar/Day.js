import React, { useEffect, useState } from "react";
import cx from "classnames";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../ui/Select";
import Switch from "../../ui/Switch";
import Divider from "../../ui/Divider";

// data
import { HOURS } from "../../data/hours";

// icons
import { ReactComponent as CrossIcon } from "../../assets/icons/cross.svg";

const selectStyles = {
  width: 140,
  height: 224,
  itemHeight: 36,
};

const WorkingTime = ({
  id,
  from,
  to,
  onChange,
  remove,
  item,
  isValid,
  clearValid,
}) => {
  const [fromIndex, setFromIndex] = useState(null);
  const [toIndex, setToIndex] = useState(null);

  useEffect(() => {
    const initFromIdx = HOURS.findIndex(({ value }) => value === from);
    const initToIdx = HOURS.findIndex(({ value }) => value === to);

    setFromIndex(initFromIdx);
    setToIndex(initToIdx);
  }, [from, to]);

  const handleChange = (params) => {
    onChange(id, params);
  };

  const onFromClick = (fromIdx) => {
    setFromIndex(fromIdx);
    if (toIndex <= fromIdx) {
      handleChange({ value: "", name: item, id: "to" });
    }
  };

  const removeHours = () => {
    remove(id);
  };

  return (
    <div>
      <div>
        <div>
          <Select
            renderValue={(value) => (from ? `from ${value}` : "from")}
            value={from}
            displayEmpty
            id="from"
            name={`${item}`}
            onOpen={clearValid}
            onChange={handleChange}
            styles={{ ...selectStyles, isError: !isValid && !from }}
            isIconVisible={false}
          >
            {HOURS.slice(0, -1).map(({ value, text }, i) => (
              <MenuItem
                key={value}
                value={value}
                onClick={() => onFromClick(i)}
              >
                {text}
              </MenuItem>
            ))}
          </Select>
          <Divider />
          <Select
            styles={{ ...selectStyles, isError: !isValid && !to }}
            renderValue={(value) => (to ? `to ${value}` : "to")}
            displayEmpty
            value={to}
            id="to"
            name={`${item}`}
            onChange={handleChange}
            onClick={clearValid}
            isIconVisible={false}
          >
            {HOURS.slice(1).map(({ value, text }, i) => (
              <MenuItem
                key={value}
                value={value}
                disabled={Boolean(i + 1 <= fromIndex)}
              >
                {text}
              </MenuItem>
            ))}
          </Select>
        </div>
      </div>
      <button type="button" className="icon cross" onClick={removeHours}>
        <CrossIcon />
      </button>
    </div>
  );
};

const Day = ({
  day,
  displayedDay,
  initialState = {},
  onChange,
  toggle,
  isSaved,
  addHours,
  removeHours,
  children,
  isSpecialDay,
  removeDate,
  isValid,
  clearValid,
}) => {
  const { state, workingTimes } = initialState;

  const handleToggle = () => {
    toggle(day);
  };

  const handleRemoveHours = (id) => {
    removeHours({ day, timeId: id }, workingTimes.length === 1);
  };

  const handleRemoveDate = () => {
    removeDate(day);
  };

  const handleAddHours = () => {
    addHours(day);
  };

  return (
    <li className={cx({ saved: isSaved, unsaved: !isSaved, available: !!state })}>
      {!isSaved ? (
        <>
          <div>
            <div>
              {children}
              <Switch checked={state} onChange={handleToggle} />
              <span>{state ? "Available" : "Not Available"}</span>
            </div>
          </div>
          <>
            {!!state && (
              <div>
                <div className="time-select">
                  {workingTimes.map((time) => (
                    <WorkingTime
                      key={time.id}
                      item={day}
                      onChange={onChange}
                      remove={handleRemoveHours}
                      isValid={isValid}
                      clearValid={clearValid}
                      {...time}
                    />
                  ))}
                  {state && (
                    <button
                      type="button"
                      className="add"
                      onClick={handleAddHours}
                    >
                      Add Hours
                    </button>
                  )}
                </div>
              </div>
            )}
          </>
          {!state && isSpecialDay && (
            <div className="cross-wrapper">
              <button
                type="button"
                className="icon cross"
                onClick={handleRemoveDate}
              >
                <CrossIcon />
              </button>
            </div>
          )}
        </>
      ) : (
        <div>
          <div className="weekday">{displayedDay}</div>
          <div>
            {state
              ? workingTimes.map(({ from, to, id }) => (
                  <span key={id}>{`Available From ${from} - To ${to}`}</span>
                ))
              : "Not Available"}
          </div>
        </div>
      )}
    </li>
  );
};

export default Day;
