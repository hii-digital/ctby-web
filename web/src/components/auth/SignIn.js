import React, { useState, useContext, useEffect } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { useHistory, useRouteMatch } from "react-router-dom";

// components
import SignInClient from "./client/SignIn";
import SignInPro from "./pro/SignIn";
import Toggler from "../../ui/Toggler";

// context
import { SidebarContext } from "../../context/SidebarProvider";

const SignIn = () => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { openedId: openedIdCtx, closeSidebar } = useContext(SidebarContext);
  const [openedId, setOpenedId] = useState(openedIdCtx || "signInClient");
  const { authError: authErrorMsg, authValid } = useSelector(
    (store) => store.auth,
    shallowEqual
  );

  const history = useHistory();
  const match = useRouteMatch();

  useEffect(() => {
    if (auth.uid && match.path === "/signin" && !authErrorMsg && authValid) {
      closeSidebar();
      history.push("/dashboard");
    }
  }, [auth.uid, closeSidebar, history, match.path, authErrorMsg, authValid]);

  const toggleActive = (value) => {
    setOpenedId(value);
  };

  const items = [
    { title: "client", action: toggleActive, value: "signInClient" },
    { title: "pro", action: toggleActive, value: "signInPro" },
  ];

  return (
    <>
      {openedId === "signInClient" && (
        <SignInClient>
          <Toggler items={items} className="auth-toggler" active={openedId} />
        </SignInClient>
      )}
      {openedId === "signInPro" && (
        <SignInPro>
          <Toggler items={items} className="auth-toggler" active={openedId} />
        </SignInPro>
      )}
    </>
  );
};

export default SignIn;
