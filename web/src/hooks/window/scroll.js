import { useEffect } from "react";
import {
  disablePageScroll,
  enablePageScroll,
  addScrollableTarget
} from "scroll-lock";

export const useDisablePageScroll = (isDisabled, el) => {
  useEffect(() => {
    if (isDisabled) {
      disablePageScroll();
      addScrollableTarget(el)
    } else {
      enablePageScroll();
    }
  }, [isDisabled]);

  return null;
};
