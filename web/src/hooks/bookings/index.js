import { useSelector, shallowEqual } from "react-redux";
import { useFirestoreConnect } from "react-redux-firebase";

export const useBookingsHook = (uid) => {
  const { approvedUsers } = useSelector(
    ({ firestore }) => firestore.data,
    shallowEqual
  );

  const isPro = approvedUsers?.[uid];

  const bookingsConnect = [];

  bookingsConnect.push({
    collection: "bookings",
    where: [["clientId", "==", uid]],
    storeAs: `${uid}/bookingsAsClient`,
  });

  if (isPro) {
    bookingsConnect.push({
      collection: "bookings",
      where: [["proId", "==", uid]],
      storeAs: `${uid}/bookingsAsPro`,
    });
  }
  useFirestoreConnect(bookingsConnect);

  const bookingsAsClientOrdered = useSelector(
    ({ firestore }) => firestore.ordered?.[`${uid}/bookingsAsClient`],
    shallowEqual
  );
  const bookingsAsProOrdered = useSelector(
    ({ firestore }) => firestore.ordered?.[`${uid}/bookingsAsPro`],
    shallowEqual
  );
  const bookingsAsClient = useSelector(
    ({ firestore }) => firestore.data?.[`${uid}/bookingsAsClient`],
    shallowEqual
  );
  const bookingsAsPro = useSelector(
    ({ firestore }) => firestore.data?.[`${uid}/bookingsAsPro`],
    shallowEqual
  );

  return {
    bookingsAsClientOrdered,
    bookingsAsProOrdered,
    bookingsAsClient,
    bookingsAsPro,
  };
};
