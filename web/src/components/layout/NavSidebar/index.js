import React from "react";
import cx from "classnames";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useLocation, useHistory, Link, matchPath } from "react-router-dom";
import { Divider } from "@material-ui/core";
import { useFirestoreConnect } from "react-redux-firebase";

// actions
import { signOut } from "../../../store/actions/authActions";

// components
import Avatar from "../../../ui/Avatar";

// hooks
import { useBreakpoints } from "../../../hooks/window/breakpoints";

// icons
import { ReactComponent as DashboardIcon } from "../../../assets/icons/dashboard.svg";
import { ReactComponent as AdminIcon } from "../../../assets/icons/admin.svg";
import { ReactComponent as ProfileIcon } from "../../../assets/icons/profile.svg";
import { ReactComponent as InboxIcon } from "../../../assets/icons/inbox.svg";
import { ReactComponent as CalendarIcon } from "../../../assets/icons/calendar.svg";
import { ReactComponent as TimeIcon } from "../../../assets/icons/time.svg";
import { ReactComponent as PaymentIcon } from "../../../assets/icons/payment.svg";
import { ReactComponent as SettingsIcon } from "../../../assets/icons/settings.svg";
import { ReactComponent as ExitIcon } from "../../../assets/icons/exit.svg";
import { ReactComponent as MarkerIcon } from "../../../assets/icons/marker.svg";
import { ReactComponent as ReportIcon } from "../../../assets/icons/quote.svg";

import "./styles.scss";

const proMenu = [
  {
    title: "Dashboard",
    link: "/dashboard",
    icon: DashboardIcon,
  },
  {
    title: "Profile",
    link: "/profile-edit",
    icon: ProfileIcon,
  },
  { title: "Inbox", link: "/inbox", icon: InboxIcon },
  { title: "Calendar", link: "/calendar", icon: CalendarIcon },
  { title: "Bookings", link: "/bookings", icon: TimeIcon },
  { title: "Payments", link: "/payments", icon: PaymentIcon },
  { title: "Settings", link: "/settings", icon: SettingsIcon },
  { title: "Report", link: "/report", icon: ReportIcon },
];

const adminMenu = [...proMenu];
adminMenu.splice(1, 0, { title: "Admin", link: "/admin", icon: AdminIcon });

const clientMenu = [
  {
    title: "Dashboard",
    link: "/dashboard",
    icon: DashboardIcon,
  },
  {
    title: "Profile",
    link: "/profile-edit",
    icon: ProfileIcon,
  },
  { title: "Inbox", link: "/inbox", icon: InboxIcon },
  { title: "Bookings", link: "/bookings", icon: TimeIcon },
  { title: "Settings", link: "/settings", icon: SettingsIcon },
  // { title: "Report", link: "/client-report", icon: ReportIcon },
];

const Sidebar = () => {
  const { pathname } = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();

  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);

  const {
    firstName,
    lastName,
    photoURL,
    isApproved,
    isPro,
    personalGoal,
    uid,
    isAdmin,
  } = profile;

  const { isBreakpointL } = useBreakpoints();

  useFirestoreConnect([
    {
      collection: `users/${uid}/professions`,
      where: [["type", "==", "primary"]],
      storeAs: "profession",
    },
  ]);

  const { city, state } =
    useSelector(
      ({ firestore }) => firestore.ordered?.profession?.[0],
      shallowEqual
    ) || {};

  const logout = () => {
    dispatch(signOut());
    history.push("/");
  };

  const getMenuItems = () => {
    if (isAdmin) {
      return adminMenu;
    }
    if (isApproved) {
      return proMenu;
    }
    return clientMenu;
  };

  const sessionPage = "/session/:id";

  const menuItems = getMenuItems();
  const links = menuItems.map(({ link }) => link);
  const linksWithSessionPage = [...links, sessionPage];

  const matchRoute = linksWithSessionPage.find((path) =>
    matchPath(pathname, { path, exact: true })
  );

  if (!matchRoute && isBreakpointL) {
    return null;
  }

  return (
    <div className="user-sidebar-root">
      <div>
        <div>
          <Avatar alt="user" src={photoURL} />
          <h3>
            Hi, {firstName} {lastName}!
          </h3>
          {isApproved && (
            <div>
              <span className="icon">
                <MarkerIcon />
              </span>
              <h5>
                {city}, {state}
              </h5>
            </div>
          )}
          {isPro === false && (
            <div className="goal">
              <h4>Personal Goal</h4>
              <span>{personalGoal}</span>
            </div>
          )}
        </div>
        <Divider />
        <div>
          <ul>
            {menuItems.map(({ title, link, icon: Icon }) => (
              <li key={title} className={cx({ active: pathname === link })}>
                <span>
                  <Icon />
                </span>
                <Link to={link}>{title}</Link>
              </li>
            ))}
          </ul>
          <button onClick={logout} type="button">
            <span>
              <ExitIcon />
            </span>
            <span>log out</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
