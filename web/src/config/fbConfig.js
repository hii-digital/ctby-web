import firebase from "firebase/app";
import firebaseRoot from "firebase";
import "firebase/firestore";
import "firebase/auth";
import "firebase/analytics";

const firebaseConfig = require("./firebase.json");

const settings = {};

firebase.initializeApp(firebaseConfig);
firebase.firestore().settings(settings);

export default firebase;
export const db = firebase.firestore();
export const { auth } = firebase;
export const rtdb = firebaseRoot.database();
export const fileStorage = firebase.storage();
export const analytics = firebase.analytics();
