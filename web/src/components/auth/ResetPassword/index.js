import React, { useState, useContext, useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";

// components
import Input from "../../../ui/Input";
import { FilledButton, Button } from "../../../ui/Button";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// actions
import {
  passwordReset,
  clearErrorMessage,
} from "../../../store/actions/authActions";

import "../styles.scss";
import "./styles.scss";

const ResetPassword = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [state, setState] = useState({
    isSent: null,
    msg: null,
  });

  const { authError } = useSelector((store) => store.auth, shallowEqual);

  useEffect(() => {
    if (authError) {
      dispatch(clearErrorMessage());
    }
  }, [dispatch]);

  const { openSidebar, payloads } = useContext(SidebarContext);

  const onSubmit = async (e) => {
    e.preventDefault();
    const { isSent, msg } = await dispatch(passwordReset(email));
    setState((prevState) => ({ ...prevState, isSent, msg }));
  };

  const onChange = (e) => {
    setEmail(e.target.value);
  };

  const isNextBtnActive = Boolean(email);

  const handleBack = () => {
    openSidebar(payloads.prevOpenedId);
  };

  return (
    <div className="auth-form-root reset-password">
      <h1>forgot password</h1>
      <div>
        {!state.msg ? (
          <form onSubmit={onSubmit}>
            <label htmlFor="email" className="input-label">
              your email
            </label>
            <Input
              type="email"
              className="text-input"
              value={email}
              id="email"
              onChange={onChange}
              placeholder="abc@mail.com"
            />
            {authError && <p>{authError}</p>}
            <div className="reset-password-btn-group">
              <Button fullWidth onClick={handleBack}>
                back
              </Button>
              <FilledButton fullWidth type="submit" disabled={!isNextBtnActive}>
                next
              </FilledButton>
            </div>
          </form>
        ) : (
          <div className="email-sent-msg">
            <p>{state.msg}</p>
            <FilledButton fullWidth onClick={handleBack}>
              back to sign in
            </FilledButton>
          </div>
        )}
      </div>
    </div>
  );
};

export default ResetPassword;
