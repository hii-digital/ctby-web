import React from "react";

// components
import { Button, FilledButton, LoadingButton } from "../../../ui/Button";

import "./styles.scss";

const NavButtons = ({ goBack, isLoading, isLastStep = false, hideNext = false }) => {
  const buttonText = isLastStep ? "Finish" : "Next";

  return (
    <div className="nav-buttons-root">
      <Button onClick={goBack}>
        Previous
      </Button>
      {!isLoading && !hideNext && (
        <FilledButton type="submit">
          {buttonText}
        </FilledButton>
      )}
      {isLoading && (
        <LoadingButton
          component={FilledButton}
          type="submit"
        >
          {buttonText}
        </LoadingButton>
      )}
    </div>
  );
};

export default NavButtons;
