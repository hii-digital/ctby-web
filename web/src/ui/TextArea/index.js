import React from "react";

// styles
import "./styles.scss";

const TextArea = ({
  placeholder,
  id,
  label,
  onChange,
  value,
  required = false,
  ...rest
}) => {
  return (
    <div className="textarea-root">
      <label htmlFor={id}>{label}</label>
      <textarea
        placeholder={placeholder}
        id={id}
        value={value}
        onChange={onChange}
        required={required}
        {...rest}
      />
    </div>
  );
};

export default TextArea;
