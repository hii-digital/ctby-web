const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");
const twilio = require("./twilio");
const mailgun = require("./mailgun");

const mainURL = "https://choosetobeyou.com";
const stripeURL = "https://dashboard.stripe.com";

const {
  notificationsPro,
  notificationsClient,
  notificationsForBoth,
} = require("./notificationsTexts");

const { getUserData, getBookingDetails } = require("./helpers");

const handleSessionConfirmed = async (bookingData) => {
  const { proId, clientId } = bookingData;
  const details = await getBookingDetails(bookingData);
  const proData = await getUserData(proId);
  const clientData = await getUserData(clientId);
  try {
    details.name = proData.firstName;
    const text = notificationsPro.confirmationDetails.getFinal(details);
    twilio.sendSMS({ to: proData.phoneNumber, body: text });
  } catch (err) {
    console.log("send confirmationDetails sms error", err);
  }
  try {
    details.name = clientData.firstName;
    const text = notificationsClient.confirmationDetails.getFinal(details);
    twilio.sendSMS({ to: clientData.phoneNumber, body: text });
  } catch (err) {
    console.log("send confirmationDetails sms error", err);
  }
};

const handleSessionDeclined = async ({ clientId }) => {
  try {
    const clientData = await getUserData(clientId);
    const text = notificationsClient.sessionDeclined.getFinal(mainURL);
    twilio.sendSMS({ to: clientData.phoneNumber, body: text });
  } catch (err) {
    console.log("send confirmationDetails sms error", err);
  }
};
const handleSessionCompleted = async ({ clientId, id }) => {
  try {
    const clientData = await getUserData(clientId);
    const text = notificationsClient.sessionCompleted.getFinal({
      url: mainURL,
      id,
    });
    twilio.sendSMS({ to: clientData.phoneNumber, body: text });
  } catch (err) {
    console.log("send confirmationDetails sms error", err);
  }
};

const handleSessionCancelled = async ({
  clientId,
  proId,
  cancellationInitiator,
}) => {
  try {
    const [docId, notificationVar] =
      cancellationInitiator === proId
        ? [clientId, notificationsClient]
        : [proId, notificationsPro];

    const userData = await getUserData(docId);
    const { phoneNumber } = userData;
    const text = notificationVar.sessionCancelled.getFinal(mainURL);
    twilio.sendSMS({ to: phoneNumber, body: text });
  } catch (err) {
    console.log("send confirmationDetails sms error", err);
  }
};

const handleProOnWay = async ({ clientId }) => {
  try {
    const { phoneNumber } = await getUserData(clientId);
    const text = notificationsClient.proOnWay.getFinal();
    twilio.sendSMS({ to: phoneNumber, body: text });
  } catch (err) {
    console.log("send proOnWay sms error", err);
  }
};

const handleRefundInquired = async (bookingData) => {
  const { clientId, refundInquiry, transactionId } = bookingData;

  try {
    const { firstName, lastName, phoneNumber } = await getUserData(clientId);
    const { rate, hours } = await getBookingDetails(bookingData);
    const amount = `$${rate * (hours / 60)}`;
    const userRecord = await admin.auth().getUser(clientId);
    const { email } = userRecord.providerData[0];
    const transactionLink = `${stripeURL}/payments/${transactionId}`;
    const emailText = `First Name: ${firstName}\nLast Name: ${lastName}\nEmail: ${email}\nAmount: ${amount}\nRefund Reason: ${refundInquiry}\nTransaction Link: ${transactionLink}`;
    const subject = "Refund Inquired";
    mailgun.contactAdminFunc({ message: emailText, subject, email });

    const smsText = notificationsClient.refundInitiated.getFinal();
    twilio.sendSMS({ to: phoneNumber, body: smsText });
  } catch (err) {
    console.log("handle Refund Inquired err", err);
  }
};

exports.sendBookingNotification = functions.firestore
  .document("bookings/{bookingId}")
  .onUpdate(async (change) => {
    const before = change.before.data();
    const after = change.after.data();

    const isSessionConfirmed =
      before.status !== "pending" && after.status === "pending";

    const isSessionCancelled =
      before.status !== "cancelled" && after.status === "cancelled";

    const isSessionDeclined =
      before.status !== "declined" && after.status === "declined";

    const isSessionCompleted =
      before.status !== "completed" && after.status === "completed";

    const isProOnWay = !before.proOnWay && !!after.proOnWay;

    const refundInquired = !before.refundInquiry && !!after.refundInquiry;

    if (isSessionConfirmed) {
      handleSessionConfirmed(after);
    } else if (isSessionDeclined) {
      handleSessionDeclined(after);
    } else if (isSessionCancelled) {
      handleSessionCancelled(after);
    } else if (isSessionCompleted) {
      handleSessionCompleted(after);
    } else if (isProOnWay) {
      handleProOnWay(after);
    } else if (refundInquired) {
      handleRefundInquired(after);
    }
  });

exports.sendApproveDecline = functions.firestore
  .document("users/{userId}")
  .onUpdate(async (change) => {
    const before = change.before.data();
    const after = change.after.data();

    const isApproved = !before.isApproved && after.isApproved;
    const firstDecline = !isApproved && !before.isDeclined && after.isDeclined;
    /* if resubmit after onboardingAgain equals to true; and then new decline */
    const decline = !isApproved && before.resubmit && !after.resubmit;

    if (isApproved || firstDecline || decline) {
      const dashboardLink = `${mainURL}/dashboard`;
      const resubmitLink = `${mainURL}/resubmit`;
      const approvedText = `Congratulations! Your application has been approved. In order to receive bookings and payments, please update your calendar and complete the Stripe onboarding form in the Payment section of your Dashboard.  Your profile will then be visbile to clients for booking sessions. ${dashboardLink}`;
      try {
        const { body, subject } = after.isApproved
          ? { body: approvedText, subject: "Application has been approved" }
          : {
              body: `${after.declineMessage}\nFollow link to update your profile: ${resubmitLink}`,
              subject: "Application has been declined",
            };
        mailgun.sendEmail({
          uid: after.uid,
          message: body,
          subject,
        });
        twilio.sendSMS({ to: after.phoneNumber, body: `${subject}: ${body}` });
      } catch (err) {
        console.log("send approve/decline sms error", err);
      }
    }
  });

const getSessionsStartsInTime = async (time) => {
  const fiveMinsAfter = new Date(time);
  fiveMinsAfter.setMinutes(time.getMinutes() + 5);

  const fiveMinsBefore = new Date(time);
  fiveMinsBefore.setMinutes(time.getMinutes() - 5);

  const snap = await admin
    .firestore()
    .collection("bookings")
    .where("from", ">", fiveMinsBefore)
    .where("from", "<", fiveMinsAfter)
    .where("status", "==", "pending")
    .get();
  return snap.docs;
};

const sendReminder = async (doc) => {
  const { proId, clientId, id } = doc.data();
  const details = await getBookingDetails(doc.data());

  const proData = await getUserData(proId);
  const clientData = await getUserData(clientId);

  const sendToUser = (notifications, firstName, phoneNumber) => {
    details.name = firstName;
    const text = notifications.sessionStartsIn.getFinal({
      url: mainURL,
      id,
      details,
    });
    try {
      twilio.sendSMS({ to: phoneNumber, body: text });
    } catch (err) {
      console.log("sendReminderBeforeSession sendSMS err", err);
    }
  };

  sendToUser(notificationsPro, proData.firstName, clientData.phoneNumber);
  sendToUser(notificationsClient, clientData.firstName, proData.phoneNumber);
};

exports.sendReminderBeforeSession = functions.pubsub
  .schedule("0,30 0-23 * * *") // once in 30min
  .onRun(async () => {
    const today = new Date();

    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);

    const inTwoHours = new Date(today);
    inTwoHours.setHours(today.getHours() + 2);
    const sessionsStartsIn24H = await getSessionsStartsInTime(tomorrow);
    const sessionsStartsIn2H = await getSessionsStartsInTime(inTwoHours);

    sessionsStartsIn24H.forEach(async (doc) => {
      sendReminder(doc);
    });

    sessionsStartsIn2H.forEach(async (doc) => {
      sendReminder(doc);
    });
  });

// ---------------------- Start Send Notification About Unread -------------------------------------------------
exports.sendNotificationAboutUnread = functions.pubsub
  .schedule("every 1 mins")
  .onRun(async () => {
    const snap = await admin.firestore().collection("conversations").get();
    snap.forEach(async (doc) => {
      const convId = doc.id;
      const {
        lastUnreadMessage,
        unreadMessage,
        userSid,
        lastNotifiedAt,
      } = doc.data();

      const now = moment();
      const nextNotifyAtMoment = moment(
        lastUnreadMessage.nextNotifyAt.toDate()
      );

      const lastNotifiedAtMoment =
        lastNotifiedAt && moment(lastNotifiedAt.toDate());

      const createdAtDate = lastUnreadMessage.createdAt.toDate();
      const createdAtMoment = moment(createdAtDate);

      const participant = await twilio.client.conversations
        .conversations(convId)
        .participants(userSid)
        .fetch();

      const { lastReadMessageIndex, identity } = participant;
      const lastReadMsgIdx =
        lastReadMessageIndex === null ? -1 : lastReadMessageIndex;
      const isAbleToNotify =
        !lastNotifiedAtMoment || now.diff(lastNotifiedAtMoment, "hours") >= 1;

      const updates = {};

      const textNewMessage = notificationsForBoth.newMessage.getFinal({
        url: mainURL,
        sid: convId,
      });

      const textUnreadMessage = notificationsForBoth.unreadMessage.getFinal({
        url: mainURL,
        sid: convId,
      });

      if (unreadMessage) {
        const { phoneNumber } = await getUserData(identity);
        const firstUnreadNotifyAt = moment(unreadMessage.nextNotifyAt.toDate());
        if (firstUnreadNotifyAt.diff(now, "seconds") < 0) {
          if (isAbleToNotify && unreadMessage.index > lastReadMsgIdx) {
            twilio.sendSMS({ to: phoneNumber, body: textNewMessage });
            mailgun.sendEmail({
              uid: identity,
              message: textNewMessage,
              subject: "You have a new message.",
            });
            updates.lastNotifiedAt = now.toDate();
          }
        }
        updates.unreadMessage = admin.firestore.FieldValue.delete();
      }

      const recentMessage =
        nextNotifyAtMoment.diff(createdAtMoment, "hours") < 1;
      if (lastUnreadMessage.index <= lastReadMsgIdx) {
        await doc.ref.delete();
      } else if (nextNotifyAtMoment.diff(now, "seconds") < 0) {
        const { phoneNumber } = await getUserData(identity);
        if (isAbleToNotify && !updates.lastNotifiedAt) {
          if (recentMessage) {
            twilio.sendSMS({ to: phoneNumber, body: textNewMessage });
            mailgun.sendEmail({
              uid: identity,
              message: textNewMessage,
              subject: "You have a new message.",
            });
          } else {
            twilio.sendSMS({ to: phoneNumber, body: textUnreadMessage });
          }
          updates.lastNotifiedAt = now.toDate();
        }

        if (recentMessage) {
          const newNextNotify = new Date(createdAtDate);
          newNextNotify.setHours(newNextNotify.getHours() + 12);

          const newLastUnread = {
            ...lastUnreadMessage,
            nextNotifyAt: newNextNotify,
          };

          admin
            .firestore()
            .collection("conversations")
            .doc(convId)
            .update({
              lastUnreadMessage: newLastUnread,
              ...updates,
            });
        } else {
          await doc.ref.delete();
        }
      }
    });
  });

// ---------------------- End Send Notification About Unread -------------------------------------------------

exports.handleMessageAdded = async (body, res) => {
  const {
    DateCreated: timestamp,
    Index: index,
    ConversationSid: conversationSid,
    ParticipantSid: authorSid,
  } = body;
  const sendNotificationAt = new Date(timestamp);
  const docRef = admin
    .firestore()
    .collection("conversations")
    .doc(conversationSid);
  const updates = {
    nextNotifyAt: new Date(
      sendNotificationAt.setMinutes(sendNotificationAt.getMinutes() + 2)
    ),
    createdAt: new Date(timestamp),
    index: +index,
  };
  const doc = await docRef.get();
  const getUpdates = () => {
    if (doc.exists) {
      const { lastUnreadMessage, unreadMessage } = doc.data();
      if (lastUnreadMessage && !unreadMessage) {
        return {
          lastUnreadMessage: updates,
          unreadMessage: lastUnreadMessage,
        };
      }
    }
    return { lastUnreadMessage: updates };
  };

  const participantsList = await twilio.client.conversations
    .conversations(conversationSid)
    .participants.list({ limit: 2 });

  const otherParticipant = participantsList.find((p) => p.sid !== authorSid);

  await docRef.set(
    {
      userSid: otherParticipant.sid,
      ...getUpdates(),
    },
    { merge: true }
  );
  res.sendStatus(200);
};
