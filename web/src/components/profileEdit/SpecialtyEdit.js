import React from "react";
import { Form, Input } from "semantic-ui-react";

// components
import BookingType from "./BookingType";
import ImageUpload from "./imageUpload/ImageUpload";
import RenderImage from "./imageUpload/RenderImage";

// data
import {
  FITNESS_TRAINER,
  MASSAGE_THERAPIST,
  fitnessOptions,
  massageOptions,
  trainingTypeValues,
} from "../../data/professions";

// helpers
import { checkValidNumbers } from "../../helpers/general";

const { online, inperson } = trainingTypeValues;

const fitnessBookingTypeOptions = [
  { value: "both", text: "Both" },
  { value: online.value, text: online.text },
  { value: inperson.value, text: inperson.text },
];

const massageBookingTypeOptions = [
  { value: inperson.value, text: inperson.text },
];

const SpecialtyEdit = ({ id: profValue, onChange, specialty }) => {
  const {
    inpersonRate,
    specialties,
    name,
    onlineRate,
    zipCode,
    addressLineFirst,
    addressLineSecond,
    licenseURL,
    preview,
    progress = 0,
    image,
  } = specialty;

  const handleSpecialitiesChange = (e) => {
    const { name: speciality, checked } = e.target;
    const newSpecialites = checked
      ? [...specialties, speciality]
      : specialties.filter((s) => s !== speciality);

    onChange({
      type: "SET_SPECIALITES",
      profession: profValue,
      payloads: { specialties: newSpecialites },
    });
  };

  const handleZipCodeChange = (e, data) => {
    const { value } = data;
    const isValidNumbers = checkValidNumbers(value);
    if (value && !isValidNumbers) {
      e.preventDefault();
    } else {
      onChange({
        type: "SET_ZIPCODE",
        profession: profValue,
        payloads: { zipCode: value },
      });
    }
  };

  const handleAddressLineChange = (e, data) => {
    const { value, id } = data;
    const type =
      id === `${profValue}-addressLineFirst`
        ? "SET_ADDRESS_LINE_FIRST"
        : "SET_ADDRESS_LINE_SECOND";
    onChange({
      type,
      profession: profValue,
      payloads: { address: value },
    });
  };

  const handleImageChange = (img) => {
    onChange({
      type: "SET_LICENSE_IMAGE",
      profession: profValue,
      payloads: {
        preview: URL.createObjectURL(img),
        image: img,
        progress: 0,
        licenseURL: licenseURL || "",
      },
    });
  };

  return (
    <div className="form__inner">
      {profValue === FITNESS_TRAINER && (
        <>
          <BookingType
            professionName={name}
            id={FITNESS_TRAINER}
            options={fitnessBookingTypeOptions}
            onlineRate={onlineRate}
            inpersonRate={inpersonRate}
            handleRateChange={onChange}
          />
          <div className="divider" style={{ margin: "50px 0" }} />
          <h2 className="field">Set Address For {name}</h2>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-addressLineFirst`}
              type="text"
              label="Address Line 1"
              value={addressLineFirst}
              onChange={handleAddressLineChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-addressLineSecond`}
              type="text"
              label="Address Line 2"
              value={addressLineSecond}
              onChange={handleAddressLineChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-zipCode`}
              type="text"
              label="Zip Code"
              value={zipCode}
              onChange={handleZipCodeChange}
            />
          </Form.Field>
          <div className="divider" style={{ margin: "50px 0" }} />
          <legend className="text--uppercase text--bold">
            Select Your Fitness Specialties
          </legend>
          <Form.Field className="field--cols" style={{ padding: "20px 0" }}>
            {fitnessOptions.map(({ value, text }) => (
              <div className="ui checkbox" key={value}>
                <input
                  id={`${profValue}-${value}`}
                  name={value}
                  tabIndex="0"
                  type="checkbox"
                  checked={specialties.includes(value)}
                  onChange={handleSpecialitiesChange}
                />
                <label htmlFor={`${profValue}-${value}`}>{text}</label>
              </div>
            ))}
          </Form.Field>
          <div className="divider" style={{ margin: "50px 0" }} />
          <h2 className="field">Set License For {name}</h2>
          <div className="profile-edit__image">
            <RenderImage urlPath={preview || licenseURL}>
              <RenderImage.RemoveButton
                profValue={profValue}
                deleteImage={onChange}
              />
            </RenderImage>
            <ImageUpload
              image={image}
              handleChange={handleImageChange}
              progress={progress}
            />
          </div>
        </>
      )}
      {profValue === MASSAGE_THERAPIST && (
        <>
          <BookingType
            professionName={name}
            id={MASSAGE_THERAPIST}
            options={massageBookingTypeOptions}
            inpersonRate={inpersonRate}
            handleRateChange={onChange}
          />
          <div className="divider" style={{ margin: "50px 0" }} />
          <h2 className="field">Set Address For {name}</h2>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-addressLineFirst`}
              type="text"
              label="Address Line 1"
              value={addressLineFirst}
              onChange={handleAddressLineChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-addressLineSecond`}
              type="text"
              label="Address Line 2"
              value={addressLineSecond}
              onChange={handleAddressLineChange}
            />
          </Form.Field>
          <Form.Field className="field--half">
            <Input
              id={`${profValue}-zipCode`}
              type="text"
              label="Zip Code"
              value={zipCode}
              onChange={handleZipCodeChange}
            />
          </Form.Field>
          <div className="divider" style={{ margin: "50px 0" }} />
          <legend className="text--uppercase text--bold">
            Select Your Massage Therapist Specialties
          </legend>
          <Form.Field className="field--cols" style={{ padding: "20px 0" }}>
            {massageOptions.map(({ value, text }) => (
              <div className="ui checkbox" key={value}>
                <input
                  id={`${profValue}-${value}`}
                  name={value}
                  tabIndex="0"
                  type="checkbox"
                  checked={specialties.includes(value)}
                  onChange={handleSpecialitiesChange}
                />
                <label htmlFor={`${profValue}-${value}`}>{text}</label>
              </div>
            ))}
          </Form.Field>
          <h2 className="field">Set License For {name}</h2>
          <div className="profile-edit__image">
            <RenderImage urlPath={preview || licenseURL}>
              <RenderImage.RemoveButton
                profValue={profValue}
                deleteImage={onChange}
              />
            </RenderImage>
            <ImageUpload
              image={image}
              handleChange={handleImageChange}
              progress={progress}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default SpecialtyEdit;
