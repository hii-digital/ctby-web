import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, shallowEqual } from "react-redux";
import { isLoaded, isEmpty } from "react-redux-firebase";

// hooks
import { useProfessionsHook } from "../professions";

export const useProfileHook = (uid) => {
  const history = useHistory();
  const [activeProfession, setActiveProfession] = useState(null);

  const approvedUsers = useSelector(
    ({ firestore }) => firestore.data?.approvedUsers,
    shallowEqual
  );
  const userData = approvedUsers?.[uid];





  if (isLoaded(approvedUsers) && isEmpty(userData)) {
    history.replace("/404");
  }

  const { professionsOrderedData, professionsData } = useProfessionsHook(uid);
  
  useEffect(() => {
    if (!activeProfession) {
      setActiveProfession(professionsOrderedData?.[0]?.id);
    }
  }, [activeProfession, professionsOrderedData]);

  const professions = professionsOrderedData?.map(({ id, name }) => ({
    title: name,
    value: id,
    action: setActiveProfession,
  }));

  const professionData = professionsData?.[activeProfession];
  const { inpersonRate, onlineRate } = professionData || {};

  const getFromRate = () => {
    if (!inpersonRate) {
      return onlineRate;
    }
    if (!onlineRate) {
      return inpersonRate;
    }
    return onlineRate < inpersonRate ? onlineRate : inpersonRate;
  };

  return {
    professions,
    activeProfession,
    fromRate: getFromRate(),
    inpersonRate, onlineRate,
    ...userData,
    ...professionData,
  };
};
