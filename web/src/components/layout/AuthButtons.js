import React, { useContext } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { useHistory } from "react-router-dom";

// actions
import { signOut } from "../../store/actions/authActions";

// context
import { SidebarContext } from "../../context/SidebarProvider";

// components
import AvatarMenu from "../../ui/AvatarMenu";

const AuthButtons = ({ handleNavMenuClose }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { openSidebar } = useContext(SidebarContext);

  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const menu = [
    {
      label: "Dashboard",
      action: () => {
        history.push("/dashboard");
      },
    },
    {
      label: "Sign out",
      action: () => {
        dispatch(signOut());
        history.push("/");
      },
    },
  ];

  const handleNavMenuCloseOnMedia = () => {
    if (handleNavMenuClose) {
      handleNavMenuClose();
    }
  };

  const openJoinAsProSidebar = () => {
    handleNavMenuCloseOnMedia();
    openSidebar("joinAsPro");
  };

  const openSignUpSidebar = () => {
    handleNavMenuCloseOnMedia();
    openSidebar("signUpClient");
  };

  const openSignInSidebar = () => {
    handleNavMenuCloseOnMedia();
    openSidebar("signInClient");
  };

  return (
    <div className="auth-buttons">
      {!auth.uid && (
        <>
          <button
            type="button"
            className="header__nav-link-btn pure-button"
            onClick={openSignInSidebar}
          >
            Sign in
          </button>

          <button
            type="button"
            onClick={openSignUpSidebar}
            className="header__nav-link-btn pure-button"
          >
            Sign Up
          </button>
          <button
            type="button"
            onClick={openJoinAsProSidebar}
            className="header__nav-link-btn pure-button"
          >
            Join as Pro
          </button>
        </>
      )}
      {auth.uid && <AvatarMenu menu={menu} />}
    </div>
  );
};

export default AuthButtons;
