const functions = require("firebase-functions");
const admin = require("firebase-admin");

const twilio = require("./twilio");
const booking = require("./booking");
const mailgun = require("./mailgun");
const stripe = require("./stripe");
const notifications = require("./notifications");

if (!admin.apps.length) {
  admin.initializeApp(functions.config().firebase);
}

exports.contactAdmin = mailgun.contactAdmin;
exports.sendBookingNotification = notifications.sendBookingNotification;
exports.sendApproveDecline = notifications.sendApproveDecline;
exports.sendReminderBeforeSession = notifications.sendReminderBeforeSession;
exports.webhookTwilioConversationUpdate = twilio.webhookTwilioConversationUpdate;
exports.sendNotificationAboutUnread = notifications.sendNotificationAboutUnread;
exports.getVideoToken = twilio.getVideoToken;
exports.getChatToken = twilio.getChatToken;
exports.updateSessionsToCompleted = booking.updateSessionsToCompleted;
exports.updateSessionsToDeclined = booking.updateSessionsToDeclined;
exports.createStripeAccount = stripe.createStripeAccount;
exports.createBookingWithCoupon = stripe.createBookingWithCoupon;
exports.retrieveStripeCoupon = stripe.retrieveStripeCoupon;
exports.createOrderAndSession = stripe.createOrderAndSession;
exports.retrieveStripeAccount = stripe.retrieveStripeAccount;
exports.retrieveStripeCoupon = stripe.retrieveStripeCoupon;
exports.getStripeOnboardingLink = stripe.getStripeOnboardingLink;
exports.getStripeLoginLink = stripe.getStripeLoginLink;
exports.webhooksHandleAccountEvents = stripe.webhooksHandleAccountEvents;
exports.webhooksHandleConnectedAccountsEvents =
  stripe.webhooksHandleConnectedAccountsEvents;

const createNotification = (notification) => {
  return admin
    .firestore()
    .collection("notifications")
    .add(notification)
    .then((doc) => {
      console.log("notification added", doc);
    });
};

exports.projectCreated = functions.firestore
  .document("projects/{projectId}")
  .onCreate((doc) => {
    const project = doc.data();
    const notification = {
      content: "Added a new project",
      user: `${project.authorFirstName} ${project.authorLastName}`,
      time: admin.firestore.FieldValue.serverTimestamp(),
    };
    return createNotification(notification);
  });

exports.userJoined = functions.auth.user().onCreate((user) => {
  return admin
    .firestore()
    .collection("users")
    .doc(user.uid)
    .get()
    .then((doc) => {
      const newUser = doc.data();
      const notification = {
        content: `Joined CTBY as ${newUser.isPro ? "a Pro" : "a Client"}`,
        user: `${newUser.firstName} ${newUser.lastName}`,
        time: admin.firestore.FieldValue.serverTimestamp(),
      };
      return createNotification(notification);
    });
});
