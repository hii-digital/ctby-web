import React, { useState } from "react";
import PropTypes from "prop-types";
import "./InputField.scss";

const InputField = ({ value, label, placeholder, type, onChange }) => {
  const [error, setError] = useState(false);

  const handleChange = (event) => {
    const { value } = event.target;
    onChange(value);
  };

  return (
    <div className="form-group inputField">
      {label && <label htmlFor="app-input-field">{label}</label>}

      {type === "textarea" ? (
        <input
          value={value}
          className="form-icon-control"
          placeholder={placeholder}
          onChange={handleChange}
        />
      ) : (
        <input
          type={type}
          value={value}
          className="form-control"
          placeholder={placeholder}
          onChange={handleChange}
        />
      )}
      {error && <span className="text-danger">{error.message}</span>}
    </div>
  );
};

InputField.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

InputField.defaultProps = {
  value: "",
  label: "",
  placeholder: "",
  type: "text",
};

export default InputField;
