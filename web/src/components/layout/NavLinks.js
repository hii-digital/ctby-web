import React from "react";
import { NavLink, useLocation } from "react-router-dom";

const SignedOutLinks = ({ handleNavMenuClose }) => {
  const navClick = (e) => {
    if (handleNavMenuClose) {
      handleNavMenuClose();
    }
  };

  const location = useLocation();

  return (
    <>
      <ul>
        <li>
          <NavLink
            onClick={navClick}
            to="/"
            className={
              location.pathname === "/"
                ? "header__nav-link__active"
                : "header__nav-link"
            }
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            onClick={navClick}
            to="/find-a-pro"
            className={
              location.pathname === "/find-a-pro"
                ? "header__nav-link__active"
                : "header__nav-link"
            }
          >
            Find a Pro
          </NavLink>
        </li>
        <li>
          <NavLink
            onClick={navClick}
            to="/how-it-works"
            className={
              location.pathname === "/how-it-works"
                ? "header__nav-link__active"
                : "header__nav-link"
            }
          >
            How It Works
          </NavLink>
        </li>
      </ul>
    </>
  );
};

export default SignedOutLinks;
