import React, { Component } from "react";
import { connect } from "react-redux";
import Fade from "react-reveal/Fade";
import { Redirect, withRouter } from "react-router-dom";
import Loading from "../../modules/Loading";

// components
import FitnessInterests from "./FitnessInterests";
import MassageInterest from "./MassageInterests";
import PersonalGoal from "./PersonalGoal";
import OnboardingHeader from "../Header";
import Welcome from "../Welcome";
import Snackbar from "../../../ui/Snackbar";
import BackgroundStrip from "../../../ui/BackgroundStrip";

// actions
import * as actions from "../../../store/actions/onboardingActions";

// styles
import "../styles.scss";

const headers = {
  1: {
    title: "Select fitness related interests",
    subtitle: "This helps us match your interests with our Pros",
  },
  2: {
    title: "Select massage therapist related interests",
    subtitle: "This helps us match your interests with our Pros",
  },
  3: {
    title: "type your personal goal",
    subtitle:
      "What is your personal goal? This will be displayed on your dashboard to motivate and remind you of where you want to be",
  },
};

class OnboardingClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      right: true,
      left: false,
      warningMessage: "",
    };
  }

  componentDidUpdate(prevProps){
    if (prevProps.profile.clientStep !== this.props.profile.clientStep) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  }

  goNext = () => {
    this.setState({
      left: false,
      right: true,
    });
  };

  goBack = () => {
    const { profile, auth, updateUser } = this.props;
    const clientStep = profile.clientStep || 1;
    updateUser({
      uid: auth.uid,
      payloads: { clientStep: clientStep - 1 },
    });
    this.setState({
      left: true,
      right: false,
    });
  };

  startOnboarding = () => {
    const { profile, auth, updateUser } = this.props;
    const clientStep = profile.clientStep || 0;
    updateUser({
      uid: auth.uid,
      payloads: { clientStep: clientStep + 1 },
    });
    this.goNext();
  };

  showWarning = (message) => {
    this.setState({ warningMessage: message });
  };

  closeWarning = () => {
    this.setState({ warningMessage: "" });
  };

  render() {
    const { profile } = this.props;
    const { right, left, warningMessage } = this.state;
    if (profile.isOnboardingClientCompleted || profile.isPro) {
      return <Redirect to="/dashboard" />;
    }
    let { clientStep: step } = profile;

    if (!step) {
      step = 0;
    }

    if (profile.isEmpty !== true) {
      return (
        <div className="onboarding-root">
          <BackgroundStrip />
          {step > 0 && (
            <OnboardingHeader
              title={headers[step].title}
              subtitle={headers[step].subtitle}
              step={step}
              stepsAmount={Object.keys(headers).length}
            />
          )}
          <div>
            {step === 0 && (
              <Fade right={right} left={left}>
                <Welcome
                  goNext={this.startOnboarding}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 1 && (
              <Fade right={right} left={left}>
                <FitnessInterests
                  goNext={this.goNext}
                  goBack={this.goBack}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 2 && (
              <Fade right={right} left={left}>
                <MassageInterest
                  goNext={this.goNext}
                  goBack={this.goBack}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            {step === 3 && (
              <Fade right={right} left={left}>
                <PersonalGoal
                  step={step}
                  goNext={this.goNext}
                  goBack={this.goBack}
                  showMessage={this.showWarning}
                />
              </Fade>
            )}

            <Snackbar message={warningMessage} onClose={this.closeWarning} />
          </div>
        </div>
      );
    }
    return <Loading />;
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: ({ uid, payloads }) =>
      dispatch(actions.updateUser({ uid, payloads })),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(OnboardingClient));
