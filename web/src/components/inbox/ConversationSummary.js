import React, { useEffect, useState } from "react";
import { useSelector, shallowEqual } from "react-redux";
import moment from "moment";

// helpers
import { renderProfileImage } from "../helpers/HelpersProfile";

// hooks
import { useUserHook } from "../../hooks/users/useUserHook";

const Summary = (props) => {
  const {
    isNew,
    photoURL,
    initials,
    firstName,
    lastName,
    messageBody,
    messageAuthor,
    messageTimestamp,
    uid,
  } = props;
  const isCurrentUserMsg = messageAuthor !== uid;
  return (
    <div
      className={`interaction interaction--conversation ${
        isNew && `interaction--unread`
      }`}
    >
      <div className="interaction__user">
        {isNew && <div className="new">New Updates</div>}
        <div className="interaction__user-img">
          {photoURL ? (
            renderProfileImage(photoURL)
          ) : (
            <div className="initials">{initials}</div>
          )}
        </div>
        <div className="interaction__user-name text--capitalize">
          {`${firstName} ${lastName[0]}.`}
        </div>
      </div>
      <div className="interaction__snippet">
        <div className="interaction__snippet-content">
          <p>
            {isCurrentUserMsg ? "You" : firstName}: {messageBody}
          </p>
        </div>
      </div>
      <div className="interaction__type">
        <div className="text--right">
          <p className="text--no-margin">
            {moment(messageTimestamp).format("MM/DD/YYYY h:mm A")}
          </p>
        </div>
      </div>
    </div>
  );
};

const ConversationSummary = ({
  participants,
  conversation,
  dateCreated: timestamp,
  index,
  lastReadMessageIdx,
  author,
  body,
}) => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const otherUser = participants.find((id) => id !== auth.uid);
  const userData = useUserHook(otherUser);
  const [hasUnread, setHasUnread] = useState(false);

  useEffect(() => {
    const setUnread = async () => {
      if (author === auth.uid && index !== lastReadMessageIdx) {
        await conversation.advanceLastReadMessageIndex(lastReadMessageIdx);
        setHasUnread(false);
      } else if (index !== lastReadMessageIdx) {
        setHasUnread(true);
      }
    };
    setUnread();
  }, [auth.uid, author, conversation, index, lastReadMessageIdx]);

  if (userData) {
    return (
      <Summary
        isNew={hasUnread}
        messageBody={body}
        messageAuthor={author}
        messageTimestamp={timestamp}
        {...userData}
      />
    );
  }
  return null;
};

export default ConversationSummary;
