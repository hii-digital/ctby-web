import React, { useEffect, useReducer, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Button, Form } from "semantic-ui-react";

// actions
import {
  updateProfessionNode,
  removeLicenseImage,
} from "../../store/actions/onboardingActions";

// reducers
import { specialtiesReducer } from "./reducer/specialities";

// hooks
import { useProfessionsHook } from "../../hooks/professions";

// components
import SpecialtyEdit from "./SpecialtyEdit";
import Snackbar from "../../ui/Snackbar";

// helpers
import {
  FITNESS_TRAINER,
  DEFAULT_VALUE,
  PROFESSIONS,
  MASSAGE_THERAPIST,
  warningMessages,
  MIN_RATE,
  MAX_RATE,
} from "../../data/professions";
import { getZipCodeData } from "../../helpers/general";

import { db, fileStorage } from "../../config/fbConfig";

const SpecialtiesEdit = () => {
  const dispatch = useDispatch();

  const [warningMessage, setWarningMessage] = useState("");

  const { auth, profile } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );

  const { professionsData: professionsFS } = useProfessionsHook(auth.uid);

  const [specialties, dispatchSpecialties] = useReducer(
    specialtiesReducer,
    null
  );

  useEffect(() => {
    dispatchSpecialties({
      type: "SET_INITIAL",
      payloads: professionsFS,
    });
  }, [professionsFS]);

  const specialtiesArr = specialties && Object.keys(specialties);

  const closeWarning = () => {
    setWarningMessage("");
  };

  const getDataFromZipcode = async (zipCode) => {
    const result = await getZipCodeData(zipCode);
    if (!result) {
      return null;
    }
    const city = result.address_components[1].long_name;
    const state =
      // eslint-disable-next-line no-nested-ternary
      result.address_components[2].short_name.length === 2
        ? result.address_components[2].short_name
        : result.address_components[3].short_name.length === 2
        ? result.address_components[3].short_name
        : result.address_components[4].short_name;
    return { city, state };
  };

  const uploadLicenseImage = async (resolve, profValue) => {
    const { image, licenseURL } = specialties[profValue];
    if (!image) {
      resolve({ profValue, licenseURL });
      return;
    }
    const imageOwner = auth.uid;
    const uploadTask = fileStorage
      .ref(`users/${imageOwner}/license`)
      .child(profValue)
      .put(image);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // Progress
        const p = Math.ceil(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        dispatchSpecialties({
          type: "SET_LICENSE_IMAGE",
          profession: profValue,
          payloads: { progress: p },
        });
      },
      (error) => {
        // Errors
        console.log("Error:", error);
      },
      async () => {
        // Complete
        try {
          const url = await fileStorage
            .ref(`users/${imageOwner}/license`)
            .child(profValue)
            .getDownloadURL();
          dispatchSpecialties({
            type: "SET_LICENSE_IMAGE",
            profession: profValue,
            payloads: {
              licenseURL: url,
            },
          });
          resolve({ profValue, licenseURL: url });
        } catch (error) {
          console.log("inside error", error);
          resolve({ profValue });
        }
      }
    );
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!specialtiesArr.length) {
      setWarningMessage(warningMessages.atLeastOneProfession);
      return;
    }

    const cityState = {};

    const promises = [];

    // eslint-disable-next-line no-restricted-syntax
    for (const specialty of specialtiesArr) {
      const s = specialties[specialty];
      if (!s.specialties.length) {
        setWarningMessage(warningMessages.atLeastOneSpecialty);
        return;
      }

      if (!s.zipCode || !s.addressLineFirst) {
        setWarningMessage(warningMessages.fillAllFields);
        return;
      }

      const checkIsOutOfRange = (value) => {
        return value ? value > MAX_RATE || value < MIN_RATE : false;
      };

      if (
        checkIsOutOfRange(s.onlineRate) ||
        checkIsOutOfRange(s.inpersonRate)
      ) {
        setWarningMessage(warningMessages.validRate);
        return;
      }

      // eslint-disable-next-line no-await-in-loop
      const zipCodeData = await getDataFromZipcode(s.zipCode);
      if (!zipCodeData) {
        setWarningMessage(warningMessages.validZipCode);
        return;
      }
      cityState[specialty] = zipCodeData;
      if (professionsFS[specialty] && professionsFS[specialty].licenseURL && !s.licenseURL) {
        dispatch(removeLicenseImage(auth.uid, specialty));
      }

      promises.push(
        new Promise((resolve) => {
          uploadLicenseImage(resolve, specialty);
        })
      );
    }

    const professionsToDelete = await db
      .collection("users")
      .doc(auth.uid)
      .collection("professions")
      .get();

    professionsToDelete.forEach((doc) => {
      if (!specialtiesArr.includes(doc.id)) {
        doc.ref
          .delete()
          .then(() => dispatch(removeLicenseImage(auth.uid, doc.id)));
      }
    });

    Promise.all(promises).then((licensesURLArr) => {
      const updates = licensesURLArr.reduce(
        (acc, { licenseURL, profValue: profession }) => {
          const getProData = ({
            preview,
            progress,
            image,
            ...professionData
          }) => ({
            ...professionData,
            ...cityState[profession],
            licenseURL,
          });
          const dataToUpload = getProData(specialties[profession]);
          const updatedProf = dataToUpload
            ? Object.keys(dataToUpload).map((key) => ({
                node: key,
                value: dataToUpload[key],
              }))
            : [];
          acc[profession] = updatedProf;
          return acc;
        },
        {}
      );
      dispatch(updateProfessionNode({ uid: auth.uid, updates }));
    });
  };

  const toggleProfessionChecked = (e) => {
    const { value: profession, checked } = e.target;

    const rates = {
      inpersonRate: DEFAULT_VALUE,
    };
    if (profession === FITNESS_TRAINER) {
      rates.onlineRate = DEFAULT_VALUE;
    }

    const payloads = professionsFS[profession] || {
      specialties: [],
      name: PROFESSIONS.find(({ value }) => value === profession).text,
      zipCode: "",
      licenseURL: "",
      ...rates,
    };

    if (checked) {
      dispatchSpecialties({
        type: "SET_PROFESSION",
        profession,
        payloads,
      });
      dispatchSpecialties({
        type: "SET_PRIMARY",
        profession,
      });
    } else {
      dispatchSpecialties({
        type: "RESET",
        profession,
      });
      const lastProfession = specialtiesArr.find((sp) => sp !== profession);
      if (lastProfession) {
        dispatchSpecialties({
          type: "SET_PRIMARY",
          profession: lastProfession,
        });
      }
    }
  };

  const togglePrimary = (e) => {
    const { value: profession } = e.target;
    dispatchSpecialties({
      type: "SET_PRIMARY",
      profession,
    });
  };

  if (!profile.isEmpty && specialties) {
    const { fitnessTrainer, massageTherapist } = specialties;
    return (
      <div className="profile-edit__specialties">
        <Form onSubmit={handleSubmit}>
          <h2>Select Service(s)</h2>
          <Form.Field className="field">
            {PROFESSIONS.map(({ value, text }) => (
              <div className="ui checkbox" key={value}>
                <input
                  id={`${value}-checkbox`}
                  value={value}
                  tabIndex="0"
                  type="checkbox"
                  checked={Boolean(specialties[value])}
                  onChange={toggleProfessionChecked}
                />
                <label htmlFor={`${value}-checkbox`}>{text}</label>
              </div>
            ))}
          </Form.Field>
          <div className="divider" style={{ margin: "50px 0" }} />
          {!!specialtiesArr.length && (
            <>
              <h2>Set Primary Service</h2>
              <Form.Field className="field">
                {PROFESSIONS.map(({ value, text }) =>
                  specialties[value] ? (
                    <div className="ui checkbox" key={value}>
                      <input
                        id={`${value}-radio`}
                        value={value}
                        tabIndex="0"
                        type="radio"
                        checked={specialties[value].type === "primary"}
                        onChange={togglePrimary}
                      />
                      <label htmlFor={`${value}-radio`}>{text}</label>
                    </div>
                  ) : null
                )}
              </Form.Field>
              <div className="divider" style={{ margin: "50px 0" }} />
            </>
          )}
          {specialties[FITNESS_TRAINER] && (
            <SpecialtyEdit
              key={FITNESS_TRAINER}
              specialty={fitnessTrainer}
              id={FITNESS_TRAINER}
              onChange={dispatchSpecialties}
            />
          )}
          {specialties[MASSAGE_THERAPIST] && (
            <SpecialtyEdit
              key={MASSAGE_THERAPIST}
              specialty={massageTherapist}
              id={MASSAGE_THERAPIST}
              onChange={dispatchSpecialties}
            />
          )}
          <Form.Field>
            <Button className="button button--secondary text--uppercase text--font-secondary text--sm">
              Update Profile
            </Button>
          </Form.Field>
        </Form>
        <Snackbar message={warningMessage} onClose={closeWarning} />
      </div>
    );
  }
  return null;
};

export default SpecialtiesEdit;
