import React from "react";
import { Button } from "semantic-ui-react";

// components
import { CancelBooking } from "../actions";

const AwaitingApproval = ({
  isEdited,
  handleEdit,
  handleSave,
  startTime,
  setCheckValid,
}) => {
  const toggleEdit = () => {
    if (!isEdited) {
      handleEdit(!isEdited);
      return;
    }
    if (!startTime) {
      setCheckValid(true);
      return;
    }
    handleSave();
  };

  const cancelEdit = () => {
    handleEdit(false);
  };

  return (
    <>
      <Button className="button--secondary button--full" onClick={toggleEdit}>
        {isEdited ? "Save" : "Edit"}
      </Button>
      {isEdited && (
        <Button className="button--primary button--full" onClick={cancelEdit}>
          Cancel Editing
        </Button>
      )}
      <CancelBooking />
    </>
  );
};

export default AwaitingApproval;
