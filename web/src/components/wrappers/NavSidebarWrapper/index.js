import React from "react";
import cx from "classnames";
import { matchPath, useLocation } from "react-router-dom";

// data
import { blackHeaderRoutes } from "../../../data/header";

import "./styles.scss";

const NavSidebarWrapper = ({ children }) => {
  const { pathname } = useLocation();
  const matchRoute = blackHeaderRoutes.find((path) =>
    matchPath(pathname, { path, exact: true })
  );

  return (
    <div
      className={cx("navsidebar-wrapper", {
        "navsidebar-wrapper__margin": matchRoute,
      })}
    >
      {children}
    </div>
  );
};

export default NavSidebarWrapper;
