import React from "react";
import { Link } from "react-router-dom";

// components
import BookingSummary from "./BookingSummary";

const BookingsList = ({ status, bookings }) => {
  return (
    <div className="interaction-list">
      {bookings &&
        bookings.map((booking) => {
          return booking.status === status ? (
            <Link
              to={`/session/${booking.id}`}
              iid={booking.id}
              key={booking.id}
              style={{ marginBottom: 25 }}
            >
              <BookingSummary {...booking} />
            </Link>
          ) : null;
        })}
    </div>
  );
};

export default BookingsList;
