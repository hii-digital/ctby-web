import { v4 as uuid } from "uuid";

export const datesReducer = (state, { type, payloads }) => {
  switch (type) {
    case "SET_INITIAL": {
      return payloads.dates;
    }
    case "ADD_DATE": {
      return [...state, payloads.date];
    }
    case "ADD_HOURS": {
      const newState = [...state];
      const { day } = payloads;
      const foundIdx = state.findIndex(({ date }) => date === day);
      const workingTimes = [
        ...state[foundIdx].workingTimes,
        { from: "", to: "", id: uuid() },
      ];

      newState[foundIdx] = { ...state[foundIdx], workingTimes };
      return newState;
    }
    case "REMOVE_HOURS": {
      const newState = [...state];
      const { day, timeId } = payloads;
      const foundIdx = state.findIndex(({ date }) => date === day);
      const workingTimes = state[foundIdx].workingTimes.filter(
        ({ id }) => timeId !== id
      );

      newState[foundIdx] = { ...state[foundIdx], workingTimes };
      return newState;
    }
    case "SET_TIME": {
      const { date, timeId, value, id } = payloads;
      const newState = [...state];
      const foundDayIdx = state.findIndex((d) => d.date === date);
      const { workingTimes } = state[foundDayIdx];
      const foundTimesIdx = workingTimes.findIndex(
        (time) => time.id === timeId
      );
      workingTimes[foundTimesIdx] = {
        ...workingTimes[foundTimesIdx],
        [id]: value,
      };

      newState[foundDayIdx] = { ...state[foundDayIdx], workingTimes };
      return newState;
    }
    case "TOGGLE_STATE": {
      const { item } = payloads;
      const newState = [...state];
      const foundDayIdx = state.findIndex(({ date }) => date === item);
      const { state: dateState } = newState[foundDayIdx];
      newState[foundDayIdx] = {
        ...newState[foundDayIdx],
        state: !dateState,
        workingTimes: [{ from: "", to: "", id: uuid() }],
      };
      return newState;
    }
    case "DATE_CHANGE": {
      const { id, newDate } = payloads;
      const newState = [...state];
      const foundDayIdx = state.findIndex(({ date }) => date === id);
      newState[foundDayIdx] = { ...newState[foundDayIdx], date: newDate };
      return newState;
    }

    case "DELETE_DATE": {
      return state.filter(({ date }) => date !== payloads.date);
    }
    default:
      return state;
  }
};
