import React, { useState, useReducer, useEffect } from "react";
import cx from "classnames";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import "react-datepicker/dist/react-datepicker.css";

// actions
import { updateHours } from "../../../store/actions/profileActions";

// reducer
import { weekdaysReducer } from "./reducer";

// components
import { FilledButton, Button } from "../../../ui/Button";
import Day from "../Day";

// data
import { weekdays } from "../../../data/hours";

// helpers
import {
  parseWeekdaysToDB,
  unparseWeekdaysFromDB,
} from "../../../helpers/time";

// icons
import { ReactComponent as ClockIcon } from "../../../assets/icons/clock.svg";

const WorkingHours = () => {
  const [weekdaysState, weekdaysDispatch] = useReducer(weekdaysReducer, {});
  const [isSaved, setSaved] = useState(true);
  const [isValid, setValid] = useState(true);

  const dispatch = useDispatch();

  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);
  const hours = profile.Hours || {};

  useEffect(() => {
    const weekdaysKeys = Object.keys(weekdaysState);
    const hoursKeys = Object.keys(hours);
    if (!weekdaysKeys.length && hoursKeys.length) {
      const data = unparseWeekdaysFromDB(hours);
      weekdaysDispatch({ type: "SET", payloads: { data } });
    }
  }, [hours, weekdaysState, weekdaysDispatch]);

  const clearValid = () => {
    if (!isValid) {
      setValid(true);
    }
  };

  const save = () => {
    clearValid();
    const empties = weekdays.reduce((acc, day) => {
      const { workingTimes, state } = weekdaysState[day];
      return acc.concat(
        workingTimes.filter(({ from, to }) => state && (!from || !to))
      );
    }, []);

    if (empties.length) {
      setValid(false);
      return;
    }
    const data = parseWeekdaysToDB(weekdaysState);
    dispatch(updateHours(data));
    setSaved(true);
  };

  const edit = () => {
    const data = unparseWeekdaysFromDB(hours);
    weekdaysDispatch({ type: "SET", payloads: { data } });
    setSaved(false);
  };

  const cancel = () => {
    const data = unparseWeekdaysFromDB(hours);
    weekdaysDispatch({ type: "SET", payloads: { data } });
    setSaved(true);
  };

  const addHours = (day) => {
    clearValid();
    weekdaysDispatch({ type: "ADD_HOURS", payloads: { day } });
  };

  const removeHours = ({ day, timeId }, isLast) => {
    clearValid();
    if (isLast) {
      weekdaysDispatch({
        type: "TOGGLE_STATE",
        payloads: { item: day },
      });
    } else {
      weekdaysDispatch({ type: "REMOVE_HOURS", payloads: { day, timeId } });
    }
  };

  const handleChange = (timeId, { name, value, id }) => {
    clearValid();
    weekdaysDispatch({
      type: "SET_TIME",
      payloads: { timeId, name, value, id },
    });
  };

  const toggleAvailable = (item) => {
    clearValid();
    weekdaysDispatch({
      type: "TOGGLE_STATE",
      payloads: { item },
    });
  };

  if (profile) {
    return (
      <div>
        <div className={cx({ "saved-mode": isSaved, "edit-mode": !isSaved })}>
          <div className="title">
            <span className="icon">
              <ClockIcon />
            </span>
            <span>working hours</span>
          </div>
          <div className="btn-group">
            {isSaved ? (
              <Button className="edit" onClick={edit}>
                Edit
              </Button>
            ) : (
              <>
                <FilledButton onClick={save}>Save</FilledButton>
                <Button onClick={cancel}>Cancel</Button>
              </>
            )}
          </div>
        </div>
        <div>
          <ul>
            {weekdays.map((day) => (
              <Day
                key={day}
                day={day}
                displayedDay={day}
                isSaved={isSaved}
                initialState={weekdaysState[day]}
                toggle={toggleAvailable}
                onChange={handleChange}
                addHours={addHours}
                removeHours={removeHours}
                isValid={isValid}
                clearValid={clearValid}
              >
                <div className="weekday">{day}</div>
              </Day>
            ))}
          </ul>
        </div>
      </div>
    );
  }
  return null;
};

export default WorkingHours;
