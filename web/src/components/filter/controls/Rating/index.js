import React, { useContext } from "react";
import { MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import Select from "../../../../ui/Select";
import colors from "../../../../ui/colors";

// context
import { FilterContext } from "../../../../context/FilterProvider";

// icons
import { ReactComponent as StarIcon } from "../../../../assets/icons/star.svg";

const length = 5;
const options = Array.from({ length }, (_, index) => index + 1);

const useStyles = makeStyles(() => ({
  menuItemRoot: {
    "& svg.starIcon": {
      width: 16,
      marginRight: 7,
      "& path": {
        width: 16,
        height: "100%",
        strokeWidth: 1,
        stroke: colors.BLACK_000,
      },
    },
  },
}));

const Rating = ({ styles }) => {
  const classes = useStyles();
  const { rating, updateState } = useContext(FilterContext);

  const handleChange = ({ id, value }) => {
    updateState({ key: id, value });
  };

  const renderValue = (value) => {
    if (rating) {
      return (
        <>
          <StarIcon className="starIcon" />
          <span>{value} &amp; Up</span>
        </>
      );
    }
    return "All";
  };

  return (
    <div className="ratingContainer">
      <label htmlFor="rating">Min Rating</label>
      <Select
        styles={styles}
        id="rating"
        value={rating || ""}
        renderValue={renderValue}
        displayEmpty
        onChange={handleChange}
      >
        <MenuItem value="">All</MenuItem>
        {options.map((value) => (
          <MenuItem
            classes={{ root: classes.menuItemRoot }}
            key={value}
            value={value}
          >
            <StarIcon className="starIcon" />
            {value} &amp; Up
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
export default Rating;
