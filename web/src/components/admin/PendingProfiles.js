import React from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useFirestoreConnect } from "react-redux-firebase";

// components
import Modal from "../modal/Modal";
import Pro from "./Pro";

// actions
import { declineProfile } from "../../store/actions/authActions";

const PendingProfiles = () => {
  const [message, setMessage] = React.useState("");
  const dispatch = useDispatch();

  useFirestoreConnect([
    {
      collection: "users",
      where: [
        ["isPro", "==", true],
        ["onboardingCompleted", "==", true],
        ["isApproved", "==", false],
      ],
      storeAs: "unApprovedUsers",
    },
  ]);

  const { unApprovedUsers } = useSelector(
    ({ firestore }) => firestore.ordered,
    shallowEqual
  );

  const handleDecline = (userId) => {
    dispatch(declineProfile(userId, message));
  };

  return (
    <div className="pending-profiles">
      <h2>Pending Profiles</h2>
      <ul>
        {Boolean(unApprovedUsers?.length) &&
          unApprovedUsers.map((user) => {
            if (!user.isDeclined && !user.reSubmit) {
              return (
                <li key={user.id}>
                  <Modal
                    buttonStyle="button"
                    declineButton
                    declineButtonAction={() => handleDecline(user.id)}
                    buttonText={`Review ${
                      user.declineMessage ? "Resubmitted Profile" : ""
                    } : ${user.firstName} ${user.lastName}`}
                    content={
                      <Pro
                        message={message}
                        setMessage={setMessage}
                        user={user}
                      />
                    }
                    message={message}
                    setMessage={setMessage}
                  />
                </li>
              );
            }
            return null;
          })}
      </ul>
      <h2 style={{ marginTop: "50px" }}>Resubmitted Profiles</h2>
      <ul>
        {Boolean(unApprovedUsers?.length) &&
          unApprovedUsers.map((user) => {
            if (user.reSubmit) {
              return (
                <li key={user.id}>
                  <Modal
                    buttonStyle="button"
                    declineButton
                    declineButtonAction={() => handleDecline(user.id)}
                    buttonText={`Review ${user.declineMessage && "Profile"} : ${
                      user.firstName
                    } ${user.lastName}`}
                    content={
                      <Pro
                        message={message}
                        setMessage={setMessage}
                        user={user}
                      />
                    }
                    message={message}
                    setMessage={setMessage}
                  />
                </li>
              );
            }
            return null;
          })}
      </ul>
    </div>
  );
};

export default PendingProfiles;
