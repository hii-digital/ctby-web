import React, { useState, useEffect } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Divider } from "@material-ui/core";

// components
import ImageUpload from "./ImageUpload";
import NavButtons from "../../NavButtons";
import ProfessionForm from "./ProfessionForm";
import Checkbox from "../../../../ui/Checkbox";

// hooks
import { useLicenseProfileHook } from "../../../../hooks/onboarding/licenseProfile";

// helpers
import { getZipCodeData } from "../../../../helpers/general";

// data
import { warningMessages } from "../../../../data/professions";

// styles
import "./styles.scss";

const LicenseProfileImage = ({ goBack, goNext, showMessage }) => {
  const { isLoading } = useSelector((store) => store, shallowEqual);
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);
  const [isAddProfessionActive, setAddProfessionActive] = useState(false);

  const {
    setInitial,
    setAddressData,
    options,
    profileImagePreview,
    setProfileImage,
    removeProfileImage,
    submit,
    resetProfession,
    professions,
    ...rest
  } = useLicenseProfileHook();

  const { firstProfession, secondProfession } = professions;

  useEffect(() => {
    if (secondProfession) {
      setAddProfessionActive(true);
    }
  }, [secondProfession]);

  const selectedProfessions = Object.values(professions)
    .filter((pro) => pro !== null)
    .map(({ value }) => value);

  const checkIsValidToMoveNext = () => {
    const { value, zipCode, addressLineFirst } = firstProfession;
    let isValid = Boolean(
      profileImagePreview && value && zipCode && addressLineFirst
    );

    if (!isValid) {
      return false;
    }

    if (secondProfession) {
      const {
        value: secondValue,
        zipCode: secondZipcode,
        addressLineFirst: secProfAddressLineFirst,
      } = secondProfession;
      isValid = Boolean(
        secondValue && secondZipcode && secProfAddressLineFirst
      );
    }
    return isValid;
  };

  const getDataFromZipcode = async (zipCode) => {
    const result = await getZipCodeData(zipCode);
    if (!result) {
      return null;
    }
    const city = result.postcode_localities
      ? result.postcode_localities[result.postcode_localities.length - 1]
      : result.address_components[1].long_name;

    const state =
      // eslint-disable-next-line no-nested-ternary
      result.address_components[2].short_name.length === 2
        ? result.address_components[2].short_name
        : result.address_components[3].short_name.length === 2
        ? result.address_components[3].short_name
        : result.address_components[4].short_name;
    return { city, state };
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const submitData = { step: profile.proStep };

    const isValid = checkIsValidToMoveNext();
    if (!isValid) {
      showMessage(warningMessages.fillAllFields);
      return;
    }

    const firstProfessionZipCodeData = await getDataFromZipcode(
      firstProfession.zipCode
    );
    if (!firstProfessionZipCodeData) {
      showMessage(warningMessages.validZipCode);
      return;
    }

    submitData.firstProfessionZipCodeData = {
      state: firstProfessionZipCodeData.state,
      city: firstProfessionZipCodeData.city,
    };

    if (secondProfession) {
      const secondProfessionZipCodeData = await getDataFromZipcode(
        secondProfession.zipCode
      );
      if (!secondProfessionZipCodeData) {
        showMessage(warningMessages.validZipCode);
        return;
      }

      submitData.secondProfessionZipCodeData = {
        state: secondProfessionZipCodeData.state,
        city: secondProfessionZipCodeData.city,
      };
    }

    submit(submitData);
    goNext(e);
  };

  const toggleCheckbox = () => {
    setAddProfessionActive((prevState) => {
      if (prevState) {
        resetProfession("secondProfession");
      } else {
        const { value } = options.filter(
          (profession) => !selectedProfessions.includes(profession.value)
        )[0];
        setInitial("secondProfession", { value });
      }
      return !prevState;
    });
  };

  const handleProfileImageDrop = ({ file, preview }) => {
    setProfileImage({ file, preview });
  };

  return (
    <form className="license-root" onSubmit={handleSubmit}>
      <div>
        {Boolean(firstProfession) && (
          <ProfessionForm
            profession="firstProfession"
            professions={professions}
            options={options}
            unAvailable={selectedProfessions}
            {...rest}
          >
            <ImageUpload
              label="profile image"
              handleDrop={handleProfileImageDrop}
              handleDelete={removeProfileImage}
              imageSrc={profileImagePreview}
            />
          </ProfessionForm>
        )}
        <div className="checkbox-zone">
          <Checkbox
            id="addProfession"
            checked={isAddProfessionActive}
            onChange={toggleCheckbox}
            label="add profession"
          />
          <Divider />
        </div>
        {isAddProfessionActive && (
          <ProfessionForm
            profession="secondProfession"
            professions={professions}
            unAvailable={selectedProfessions}
            options={options}
            {...rest}
          />
        )}
      </div>
      <NavButtons goBack={goBack} isLoading={isLoading} />
    </form>
  );
};

export default LicenseProfileImage;
