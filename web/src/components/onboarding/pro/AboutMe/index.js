import React, { useState } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { isPossiblePhoneNumber } from "react-phone-number-input";

// components
import NavButtons from "../../NavButtons";
import TextArea from "../../../../ui/TextArea";
import Input from "../../../../ui/Input";
import PhoneNumberInput from "../../../../ui/PhoneInput";

// actions
import { updateUser } from "../../../../store/actions/onboardingActions";

// data
import { warningMessages } from "../../../../data/professions";

// styles
import "./styles.scss";
import colors from "../../../../ui/colors";

const inputStyles = {
  background: colors.WHITE_FFF,
};

const AboutMe = ({ goBack, showMessage }) => {
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { isLoading } = useSelector((store) => store, shallowEqual);

  const dispatch = useDispatch();
  const history = useHistory();

  const [state, setState] = useState({
    about: profile.about || "",
    funFact: profile.funFact || "",
    favQuote: profile.favQuote || "",
    socialInstagram: profile.socialInstagram || "",
    phoneNumber: profile.phoneNumber || "",
  });

  const { about, funFact, favQuote, socialInstagram, phoneNumber } = state;

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleNumberChange = (v = "") => {
    setState((prevState) => ({ ...prevState, phoneNumber: v }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const isBlank = (value) => {
      return !value.trim().length;
    };
    const mandatoryFields = Object.keys(state).filter(
      (key) => key !== "socialInstagram"
    );
    const isValidToMoveNext = !mandatoryFields.filter((key) =>
      isBlank(state[key])
    ).length;

    const regex = /(https?:\/\/(?:www\.)?instagram\.com\/([^/?#&]+)).*/g;

    const addMissedProtocol = (url) => {
      const newUrl = url.replace(/^http:\/\//i, "https://");

      if (!/^(?:f|ht)tps?\:\/\//.test(newUrl)) {
        return `https://${url}`;
      }
      return newUrl;
    };

    if (!isValidToMoveNext) {
      showMessage(warningMessages.fillAllFields);
      return;
    }

    if (!isPossiblePhoneNumber(phoneNumber)) {
      showMessage("Please fill valid phone number.");
      return;
    }

    const payloads = {
      onboardingCompleted: true,
    };

    if (socialInstagram) {
      const formattedInstagramUrl = addMissedProtocol(
        socialInstagram.toLowerCase()
      );
      const isInstagramValid = new RegExp(regex).test(formattedInstagramUrl);

      if (!isInstagramValid) {
        showMessage("Please fill valid instagram link.");
        return;
      }

      payloads.socialInstagram = formattedInstagramUrl;
    }

    if (profile.isDeclined) {
      payloads.declineMessage = "";
      payloads.reSubmit = true;
      payloads.isDeclined = false;
    }

    dispatch(
      updateUser({
        uid: auth.uid,
        payloads: { ...state, ...payloads },
      })
    );
    if (!isLoading) {
      history.push("/dashboard");
    }
  };

  return (
    <form onSubmit={handleSubmit} className="root-about">
      <div>
        <div className="textarea-zone">
          <TextArea
            label="About Me*"
            placeholder="I'm..."
            name="about"
            value={about}
            onChange={onInputChange}
          />
          <TextArea
            label="What’s a fun fact about you?*"
            placeholder="Lol..."
            name="funFact"
            value={funFact}
            onChange={onInputChange}
          />
          <TextArea
            label="What’s your favorite quote?*"
            placeholder="e.g. Never give up!"
            name="favQuote"
            value={favQuote}
            onChange={onInputChange}
          />
        </div>
        <div className="contactsZone">
          <div>
            <label htmlFor="phoneNumber" className="phoneNumber">
              Phone Number*
            </label>
            <PhoneNumberInput
              id="phoneNumber"
              value={phoneNumber}
              onChange={handleNumberChange}
            />
          </div>
          <div>
            <label htmlFor="socialInstagram">Instagram Profile URL</label>
            <Input
              id="socialInstagram"
              placeholder="https://www.instagram.com/"
              value={socialInstagram}
              name="socialInstagram"
              onChange={onInputChange}
              styles={inputStyles}
            />
          </div>
        </div>
      </div>
      <NavButtons goBack={goBack} isLoading={isLoading} isLastStep />
    </form>
  );
};

export default AboutMe;
