import React, { useState, useRef, useEffect } from "react";
import moment from "moment";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import { Button } from "semantic-ui-react";

// components
import ModalView from "./ModalView";
import CancelBooking from "../CancelBooking";

// actions
import { updateBooking } from "../../../../../store/actions/bookingsActions";

// hooks
import { useBookingHook } from "../../../../../hooks/bookings/booking";

// data
import { STATUSES } from "../../../../../data/bookings";
import {
  trainingTypeValues,
  inpersonTypeValues,
} from "../../../../../data/professions";

const { active, pending, completed } = STATUSES;

const { online, inperson } = trainingTypeValues;
const StartVideoCall = () => {
  const { params } = useRouteMatch();
  const dispatch = useDispatch();
  const booking = useBookingHook(params.id);
  const { from, status, proId, type, inpersonType, proOnWay } = booking;
  const { comeToMe } = inpersonTypeValues;
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const [isStartBtnActive, setActive] = useState(false);

  const timer = useRef(null);

  useEffect(() => {
    const timeDifference = moment(from.toDate()).diff(moment(), "milliseconds");
    const minute = 1000 * 60;
    const delay = timeDifference - minute * 5;
    const maxDelay = 21600000; // 6 hours

    if (delay <= maxDelay) {
      if (timeDifference >= 0) {
        timer.current = setTimeout(() => {
          setActive(true);
        }, delay);
      } else {
        setActive(true);
      }
    }

    return () => {
      if (timer.current) {
        clearTimeout(timer.current);
      }
    };
  }, [booking.from, from]);

  const handleCompleteSession = () => {
    const payloads = {
      status: completed.value,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  const handleStartSession = () => {
    const payloads = {
      status: active.value,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  const handleSetOnWay = () => {
    const payloads = {
      proOnWay: true,
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  const renderFinishButton = () => {
    if (status === active.value && proId === auth.uid) {
      return (
        <Button
          className="button--primary button--full"
          onClick={handleCompleteSession}
        >
          Finish
        </Button>
      );
    }
    return null;
  };

  const isStartBtnPresent = status !== active.value && proId === auth.uid;
  const isProIsOnHisWayPresent =
    isStartBtnPresent && inpersonType === comeToMe.value;

  return (
    <>
      {type === online.value && (
        <ModalView isStartActive={isStartBtnActive}>
          {status === pending.value && <CancelBooking />}
        </ModalView>
      )}

      {status !== active.value && type === inperson.value && (
        <>
          {isProIsOnHisWayPresent &&
            (proOnWay ? (
              <Button disabled className="button--secondary button--full">
                Notification sent
              </Button>
            ) : (
              <Button
                className="button--secondary button--full"
                onClick={handleSetOnWay}
              >
                I&apos;m on my way to client
              </Button>
            ))}
          {isStartBtnPresent && (
            <Button
              disabled={!isStartBtnActive}
              className="button--secondary button--full"
              onClick={handleStartSession}
            >
              Start Session
            </Button>
          )}
          <CancelBooking />
        </>
      )}
      {renderFinishButton()}
    </>
  );
};

export default StartVideoCall;
