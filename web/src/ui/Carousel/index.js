import React from "react";
import Slider from "react-slick";

import "./styles.scss";

export const NextArrow = (props) => {
  const { onClick } = props;
  return (
    <div className="slick-slider__next-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow"
        onClick={onClick}
      />
    </div>
  );
};

export const PrevArrow = (props) => {
  const { onClick } = props;
  return (
    <div className="slick-slider__prev-arrow-wrapper">
      <button
        type="button"
        className="slick-slider__next-arrow prev-arrow"
        onClick={onClick}
      />
    </div>
  );
};

const CarouselComponent = (props) => {
  const { children, isInfinite = true, settings: customSettings } = props;

  const settings = {
    infinite: isInfinite,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3,
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: "20%",
        },
      },
    ],
  };

  const settingsToApply = customSettings || settings;
  return (
    <div className="slider-wrapper">
      <Slider {...settingsToApply}>{children}</Slider>
    </div>
  );
};

export default CarouselComponent;
