import React, { useContext, useEffect, useState } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Link, useLocation, matchPath } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import cx from "classnames";
import { FilterContext } from "../../context/FilterProvider";
import { SidebarContext } from "../../context/SidebarProvider";

// pictures
import imageLogo from "../../assets/images/logo-emblem.png";

// components
import NavLinks from "./NavLinks";
import AuthButtons from "./AuthButtons";
import UserIcon from "../../components/userIcon/UserIcon";
import { Button } from "../../ui/Button";
import SideBarMenu from "../sideBarMenu/sideBarMenu";

// hooks
import { useBreakpoints } from "../../hooks/window/breakpoints";

// data
import { blackHeaderRoutes } from "../../data/header";

import "./navbar.scss";

const useStyles = makeStyles(() => ({
  root: {
    color: "#FFFFFF",
    padding: "5px 20px",
    "&:hover": {
      background: "none",
    },
  },
}));

const Navbar = () => {
  const classes = useStyles();
  const [navbar, setNavbar] = useState(false);
  const [menuActive, setMenuState] = useState(false);
  const { zipcode, updateState, searchPros } = useContext(FilterContext);
  const { openedId, openSidebar } = useContext(SidebarContext);
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { isBreakpointL } = useBreakpoints();

  const { pathname } = useLocation();

  const matchRoute = blackHeaderRoutes.find((path) =>
    matchPath(pathname, { path, exact: true })
  );

  const toggleMenu = () => {
    setMenuState(!menuActive);
  };

  const onZipcodeChange = (e) => {
    updateState({ key: "zipcode", value: e.target.value }, e);
  };

  const zipCodeSearch = (e) => {
    e.preventDefault();
    searchPros(e);
  };

  useEffect(() => {
    const scrollYPoint = isBreakpointL ? 380 : 190;
    const changeBackGround = () => {
      // if (window.scrollY >= scrollYPoint) {
        setNavbar(true);
      // } else {
        // setNavbar(false);
      // }
    };

    window.addEventListener("scroll", changeBackGround);

    return () => {
      window.removeEventListener("scroll", changeBackGround);
    };
  });

  useEffect(() => {
    // if (!isBreakpointL) {
      // if (openedId) {
        setNavbar(true);
      // } else {
        // setNavbar(false);
      // }
    // }
  }, [openedId, isBreakpointL]);

  const handleOpenRightMediaMenu = () => {
    openSidebar("mediaRightMenu");
  };

  return (
    <div className={cx(matchRoute ? "header__dashboard" : "")}>
      <div className={cx(navbar || matchRoute ? "header__black" : "header")}>
        <div className="header__userIcon-media">
          {!isBreakpointL && auth.uid && (
            <UserIcon onClick={handleOpenRightMediaMenu} />
          )}
        </div>
        <div className="header__logo">
          <Link to="/" className="header__brand">
            <img src={imageLogo} alt="Choose To Be You Logo" />
          </Link>
        </div>
        {navbar && pathname === "/" ? (
          <>
            <div className="header__nav-items">
              <form onSubmit={zipCodeSearch} className="header__navbar-center">
                <input
                  type="search"
                  className={
                    navbar ? "navbar navbar-input" : "navbar navbar-active"
                  }
                  placeholder="Search by zip code"
                  value={zipcode}
                  onChange={onZipcodeChange}
                />
                <Button
                  type="submit"
                  className="header__zip-search-button"
                  classes={{ root: classes.root }}
                >
                  Search
                </Button>
              </form>
            </div>
          </>
        ) : (
          <div className="header__nav-items">
            <NavLinks menuActive={toggleMenu} />
          </div>
        )}
        <div className="header__login">
          <div className="header__login-wrapper">
            <AuthButtons />
          </div>
          <div className="header__burger_menu">
            <SideBarMenu />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
