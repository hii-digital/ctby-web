const FITNESS_TRAINER = "fitnessTrainer";
const MASSAGE_THERAPIST = "massageTherapist";
const DEFAULT_VALUE = 80;
const MIN_RATE = 50;
const MAX_RATE = 300;
const STEP = 10;

export {
  FITNESS_TRAINER,
  MASSAGE_THERAPIST,
  DEFAULT_VALUE,
  MIN_RATE,
  MAX_RATE,
  STEP,
};

export const PROFESSIONS = [
  { value: FITNESS_TRAINER, text: "Fitness Trainer" },
  { value: MASSAGE_THERAPIST, text: "Massage Therapist" },
];

export const fitnessOptions = [
  { value: "competitionPrep", text: "Competition Prep" },
  { value: "powerLifting", text: "Powerlifting" },
  { value: "weightLoss", text: "Weight Loss" },
  { value: "bodyFatLoss", text: "Body Fat Loss" },
  { value: "sizeGaining", text: "Size Gaining" },
  { value: "enduranceTraining", text: "Endurance Training" },
  { value: "formingAndToning", text: "Forming and Toning" },
  { value: "flexibility", text: "Flexibility" },
  { value: "aerobicFitness", text: "Aerobic Fitness" },
  { value: "pregnancy", text: "Pregnancy" },
  { value: "rehabilitation", text: "Rehabilitation" },
  { value: "pilates", text: "Pilates" },
  { value: "yoga", text: "Yoga" },
  { value: "athletic", text: "Athletic" },
];

export const massageOptions = [
  { value: "deepTissue", text: "Deep Tissue" },
  { value: "swedish", text: "Swedish" },
  { value: "stone", text: "Stone" },
  { value: "pregnancy", text: "Pregnancy" },
  { value: "sports", text: "Sports" },
  { value: "reflexology", text: "Reflexology" },
];

export const trainingTypeValues = {
  online: {
    value: "online",
    text: "Online",
    db: "onlineRate",
  },
  inperson: {
    value: "inperson",
    text: "In Person",
    db: "inpersonRate",
  },
};

export const inpersonTypeValues = {
  comeToMe: {
    value: "comeToMe",
    text: "Come to Me",
  },
  goToPro: {
    value: "goToPro",
    text: "Go To Pro",
  },
};

export const warningMessages = {
  atLeastOneProfession: "Please Select at least 1 profession.",
  atLeastOneInterest: "Please Select at least 1 interest.",
  atLeastOneSpecialty: "Please Select at least 1 specialty.",
  fillAllFields: "Please fill all fields.",
  validZipCode: "Enter a valid zip code.",
  validRate: `Rate should be between $${MIN_RATE} and $${MAX_RATE}`,
};
