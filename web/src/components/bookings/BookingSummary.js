import React from "react";
import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";

// helpers
import { renderProfileImage } from "../helpers/HelpersProfile";

// hooks
import { useBookingData } from "../../hooks/bookings/bookingData";

// data
import { STATUSES } from "../../data/bookings";

const Summary = (props) => {
  const {
    photoURL,
    initials,
    firstName,
    lastName,
    serviceName,
    status,
    from,
    to,
  } = props;
  return (
    <div
      className={`interaction interaction--booking ${
        status === STATUSES.awaitingApproval.value && `interaction--unread`
      }`}
    >
      <div className="interaction__user">
        {status === STATUSES.awaitingApproval.value && (
          <div className="new">New Updates</div>
        )}
        <div className="interaction__user-img">
          {photoURL ? (
            renderProfileImage(photoURL)
          ) : (
            <div className="initials">{initials}</div>
          )}
        </div>
        <div className="interaction__user-name text--capitalize">
          {`${firstName} ${lastName[0]}.`}
        </div>
      </div>
      <div className="interaction__snippet">
        <div className="interaction__snippet-content">
          <p>
            <span className="text--capitalize text--bold">
              {serviceName}
            </span>
            <br />
            <span>
              {moment(from.toDate()).format("MMMM Do YYYY")},
              {moment(from.toDate()).format("h:mm a")}&nbsp;-&nbsp;
              {moment(to.toDate()).format("h:mm a")}
            </span>
          </p>
        </div>
      </div>
      <div className="interaction__type">
        <div className="text--right">
          <p className="text--no-margin">{STATUSES[status].text}</p>
        </div>
      </div>
    </div>
  );
};

const BookingSummary = ({ proId, clientId, ...rest }) => {
  const { clientData, proData } = useBookingData({ proId, clientId });

  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const summaryProps = proId === auth.uid ? clientData : proData;

  if (summaryProps) {
    return (
      <Summary {...rest} {...summaryProps} />
    );
  }
  return null;
};

export default BookingSummary;
