import React from "react";
import { Route, Redirect, useLocation } from "react-router-dom";
import { useSelector, shallowEqual } from "react-redux";

// components
import Loading from "./components/modules/Loading";

// config
import { auth as authMethod } from "./config/fbConfig";

const ProtectedRoute = ({
  component: Component,
  onlyForPro,
  onlyForAdmin,
  ...rest
}) => {
  const { auth, profile } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { pathname } = useLocation();

  const {
    isLoaded,
    isApproved,
    isPro,
    isOnboardingClientCompleted,
    onboardingCompleted,
    isAdmin,
  } = profile || {};

  if (!isLoaded) return <Loading />;

  const { uid } = auth;
  const { emailVerified } = authMethod().currentUser || {};

  const getRedirectDestination = () => {
    const client = uid && !isPro;
    if (!uid) {
      return "/signin";
    }
    if (client && (!isOnboardingClientCompleted || !emailVerified)) {
      return "/onboarding-client";
    }
    if (
      isPro &&
      !isOnboardingClientCompleted &&
      (!emailVerified || !onboardingCompleted)
    ) {
      return "/onboarding";
    }
    if (onlyForAdmin && !isAdmin) {
      return "/dashboard";
    }
    if (onlyForPro && ((isPro && !isApproved) || !isPro)) {
      return "/dashboard";
    }
    return null;
  };

  const redirectDestination = getRedirectDestination();

  const shouldRedirect =
    redirectDestination && redirectDestination !== pathname;

  return (
    <Route
      {...rest}
      render={(routeProps) => {
        return !shouldRedirect ? (
          <Component {...routeProps} />
        ) : (
          <Redirect to={{ pathname: redirectDestination }} />
        );
      }}
    />
  );
};

export default ProtectedRoute;
