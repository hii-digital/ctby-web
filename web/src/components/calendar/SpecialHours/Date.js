import React, { useState, useEffect } from "react";
import moment from "moment";

// components
import Day from "../Day";
import DatePicker from "../../../ui/DatePicker";

// data
import { FORMAT } from "../../../data/hours";

const Date = (props) => {
  const {
    initialState,
    timeChange,
    dateChange,
    removeHours,
    blockedDates,
    addDateButton,
    isLast,
    clearValid,
    ...rest
  } = props;

  const { date: initialDate } = initialState;
  const momentInitDate = moment(initialDate, FORMAT);

  const [selectedDate, setSelectedDate] = useState(null);

  useEffect(() => {
    setSelectedDate(moment(initialDate, FORMAT).toDate());
  }, [initialDate]);

  const onDateChange = (date) => {
    dateChange(initialDate, moment(date).format(FORMAT));
    setSelectedDate(date);
  };

  const filterDates = (date) => {
    return !blockedDates.includes(moment(date).format(FORMAT));
  };

  return (
    <>
      <Day
        displayedDay={momentInitDate.format("DD MMM YYYY")}
        day={initialDate}
        initialState={initialState}
        onChange={timeChange}
        removeHours={removeHours}
        isSpecialDay
        clearValid={clearValid}
        {...rest}
      >
        <DatePicker
          selected={selectedDate}
          onChange={onDateChange}
          formatWeekDay={(dayName) => dayName.substr(0, 1)}
          dateFormat="d MMM"
          minDate={moment().toDate()}
          filterDate={filterDates}
          onFocus={clearValid}
        />
        {isLast && addDateButton}
      </Day>
    </>
  );
};

export default Date;
