import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import { useSelector, shallowEqual } from "react-redux";

// hooks
import { useBookingData } from "../../hooks/bookings/bookingData";

// data
import { STATUSES } from "../../data/bookings";

const { awaitingApproval } = STATUSES;

const Booking = ({ id, from, proId, clientId, status, serviceName }) => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { clientData, proData } = useBookingData({ proId, clientId });

  const data = auth.uid === proId ? clientData : proData;
  const { photoURL, firstName, initials } = data || {};

  return (
    <Link to={`/session/${id}`}>
      <div key={id} className="slider-wrapper__item-block">
        <div className="slider-wrapper__icon-wrapper">
          <div className="slider-wrapper__icon">
            {photoURL ? (
              <img src={photoURL} alt="userSecond" />
            ) : (
              <div className="slider-wrapper__initials">{initials}</div>
            )}
          </div>
          <div className="slider-wrapper__name">{firstName}</div>
        </div>

        <div className="slider-wrapper__text">
          <div className="slider-wrapper__news">
            <div className="slider-wrapper__date">
              {moment(from.toDate()).format("D MMM YYYY")}
            </div>
            {status === awaitingApproval.value ? (
              <div className="slider-wrapper__new">new</div>
            ) : null}
          </div>
          <div className="slider-wrapper__text-description">{serviceName}</div>
        </div>
      </div>
    </Link>
  );
};

export default Booking;
