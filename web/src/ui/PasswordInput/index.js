import React from "react";
import { InputAdornment } from "@material-ui/core";

// components
import Input from "../Input";
import IconButton from "../IconButton";

// icons
import { ReactComponent as EyeIcon } from "../../assets/icons/eye.svg";
import { ReactComponent as EyeCrossedIcon } from "../../assets/icons/eye-crossed.svg";

const PasswordInput = ({ isVisible, id, toggleVisible, ...rest }) => {
  const handleClick = () => {
    toggleVisible(id);
  };

  return (
    <Input
      type={isVisible ? "text" : "password"}
      className="text-input"
      id={id}
      {...rest}
      endAdornment={
        <InputAdornment position="end">
          <IconButton onClick={handleClick}>
            {isVisible ? <EyeCrossedIcon /> : <EyeIcon />}
          </IconButton>
        </InputAdornment>
      }
    />
  );
};

export default PasswordInput;
