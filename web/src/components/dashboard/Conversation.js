import React, { useEffect, useState } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { Link } from "react-router-dom";
import moment from "moment";
import LazyLoad from 'react-lazyload';
// hooks
import { useUserHook } from "../../hooks/users/useUserHook";

// helpers
import { parseUniqueName } from "../../helpers/conversations";

const Conversation = ({
  sid,
  uniqueName,
  body,
  author,
  conversation,
  dateCreated,
  lastMessageIndex,
  lastReadMessageIndex,
}) => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const participants = parseUniqueName(uniqueName);
  const otherUser = participants.find((id) => id !== auth.uid);
  const userData = useUserHook(otherUser);
  const { photoURL, firstName, initials } = userData || {};
  const [unreadAmount, setUnreadAmount] = useState(0);

  const lastReadMessageIdx =
    lastReadMessageIndex === null ? -1 : lastReadMessageIndex;

  useEffect(() => {
    const setUnread = async () => {
      if (author === auth.uid && lastMessageIndex !== lastReadMessageIndex) {
        await conversation.advanceLastReadMessageIndex(lastMessageIndex);
        setUnreadAmount(0);
      } else {
        setUnreadAmount(lastMessageIndex - lastReadMessageIdx);
      }
    };
    setUnread();
  }, [
    author,
    auth.uid,
    lastMessageIndex,
    conversation,
    lastReadMessageIndex,
    setUnreadAmount,
    lastReadMessageIdx,
  ]);

  return (
    <Link to={`/conversations/${sid}`}>
      <div key={sid} className="slider-wrapper__item-block">
        <div className="slider-wrapper__icon-wrapper">
          <div className="slider-wrapper__icon">
            {photoURL ? (
              
              <img src={photoURL} alt="userSecond" />
            ) : (
              <div className="slider-wrapper__initials">{initials}</div>
            )}
          </div>
          <div className="slider-wrapper__name">{firstName}</div>
        </div>

        <div className="slider-wrapper__text">
          <div className="slider-wrapper__numbers">
            <div className="slider-wrapper__date">
              {moment(dateCreated).format("D MMM YYYY")}
            </div>
            {!!unreadAmount && (
              <div className="slider-wrapper__number">{unreadAmount}</div>
            )}
          </div>
          <div className="slider-wrapper__text-description">{body}</div>
        </div>
      </div>
    </Link>
  );
};

export default Conversation;
