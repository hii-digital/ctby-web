import { useSelector, shallowEqual } from "react-redux";

// hooks
import { useBookingsHook } from "./index";

export const useBookingHook = (id) => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { bookingsAsClient, bookingsAsPro } = useBookingsHook(auth.uid);
  const bookings = { ...bookingsAsClient, ...bookingsAsPro };

  return bookings[id];
};
