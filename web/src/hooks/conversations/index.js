import { useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import Conversations from "@twilio/conversations";

// actions
import {
  setInit,
  addConversation,
  addMessage,
  setInitLoaded,
  updateChangeId,
} from "../../store/actions/conversationsActions";

// config
import firebaseApi from "../../config/fbConfig";

const getToken = firebaseApi.functions().httpsCallable("getChatToken");

let currentMatch = {};

export const useConversations = () => {
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);
  const dispatch = useDispatch();

  const {
    api,
    conversationsMap,
    messages,
    changesMap,
    initLoaded,
    isLoading,
  } = useSelector(({ conversationsData }) => conversationsData, shallowEqual);

  const apiConversations = api?.conversations.conversations.values() || [];
  const conversationsArr = [...apiConversations];

  const match = useRouteMatch();

  useEffect(() => {
    currentMatch = match;
  }, [match]);

  useEffect(() => {
    const init = async () => {
      try {
        const { data: token } = await getToken({
          identity: auth.uid,
        });
        const conversationsClient = await Conversations.create(token);
        dispatch(setInitLoaded("in process"));
        const convs = await conversationsClient.getSubscribedConversations();
        dispatch(setInit({ api: conversationsClient, chats: convs.items }));
        conversationsClient.on("conversationJoined", async (conv) => {
          dispatch(addConversation(conv));
          const { items: msgsItems } = await conv.getMessages();
          const lastM = msgsItems[msgsItems.length - 1];
          const { author, body, timestamp, index } = lastM.state;
          dispatch(
            updateChangeId({
              sid: lastM.conversation.sid,
              newDateCreated: timestamp.getTime(),
              author,
              body,
              index,
            })
          );
        });
        conversationsClient.on("messageAdded", (message) => {
          const { author, body, timestamp, index } = message.state;
          dispatch(
            updateChangeId({
              sid: message.conversation.sid,
              newDateCreated: timestamp.getTime(),
              author,
              body,
              index,
            })
          );
          const { path, params } = currentMatch;
          const isConversationRoute =
            path === "/conversations/:id/:uniqueName?";
          if (isConversationRoute && params.id === message.conversation.sid) {
            dispatch(addMessage(message));
          }
        });
        conversationsClient.on("tokenAboutToExpire", async () => {
          const { data: newToken } = await getToken({
            identity: auth.uid,
          });
          conversationsClient.updateToken(newToken);
        });
        conversationsClient.on("tokenExpired", async () => {
          const { data: newToken } = await getToken({
            identity: auth.uid,
          });
          conversationsClient.updateToken(newToken);
        });
      } catch (err) {
        console.log("Unable to connect err:", err);
      }
    };
    if (!api && !initLoaded && auth.uid) {
      init();
    }
  }, [api, auth.uid, conversationsMap, dispatch, initLoaded]);

  const getUnreadAmount = async () => {
    let unreadAmount = 0;
    // eslint-disable-next-line
    for (const conv of conversationsArr) {
      // eslint-disable-next-line
      const unread = await conv.getUnreadMessagesCount();
      unreadAmount += unread;
    }
    return unreadAmount;
  };

  return {
    api,
    conversationsArr,
    messages,
    changesMap,
    initLoaded,
    conversationsMap,
    isLoading,
    getUnreadAmount,
  };
};
