import { withStyles } from "@material-ui/core/styles";
import MuiSwitch from "@material-ui/core/Switch";

import colors from "../colors";

const Switch = withStyles({
  root: {
    margin: "-12px 0px",
  },
  switchBase: {
    color: colors.WHITE_FFF,
    margin: 9,
    padding: 0,
    "&$checked": {
      color: colors.ORANGE_E99,
      boxShadow:
        "0px 0px 2px rgba(0, 0, 0, 0.14), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 1px 3px rgba(0, 0, 0, 0.2)",
    },
    "&$checked + $track": {
      backgroundColor: colors.ORANGE_ED9,
      opacity: 0.38,
    },
  },
  checked: {},
  track: {
    backgroundColor: "#BDBDBD",
  },
})(MuiSwitch);

export default Switch;