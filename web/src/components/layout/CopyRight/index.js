import React from "react";

// icons
import { ReactComponent as FacebookIcon } from "../../../assets/icons/fb.svg";
import { ReactComponent as YoutubeIcon } from "../../../assets/icons/youtube.svg";
import { ReactComponent as InstaIcon } from "../../../assets/icons/insta.svg";

import "./styles.scss";

const CopyRight = ({ handleNavMenuClose }) => {
  const handleNavMenuCloseOnMedia = () => {
    if (handleNavMenuClose) {
      handleNavMenuClose();
    }
  };

  return (
    <div className="container bottom-wrapper">
      <p className="mb--0">
        Copyright &copy; {new Date().getFullYear()} Choose To Be You
      </p>
      <div>
        <ul className="list list--inline">
          <li onClick={handleNavMenuCloseOnMedia}>
            <a
              href="https://www.facebook.com/c2byou"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FacebookIcon />
            </a>
          </li>
          <li onClick={handleNavMenuCloseOnMedia}>
            <a
              href="https://www.instagram.com/choosetobeyou_/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <InstaIcon />
            </a>
          </li>
          <li onClick={handleNavMenuCloseOnMedia}>
            <a
              href="https://www.youtube.com/channel/UCgy3W5Jq5mSp2Tn9_4tUd2Q"
              target="_blank"
              rel="noopener noreferrer"
            >
              <YoutubeIcon />
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default CopyRight;
