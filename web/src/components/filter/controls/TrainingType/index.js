import React, { useContext } from "react";
import { MenuItem } from "@material-ui/core";

// components
import Select from "../../../../ui/Select";

// context
import { FilterContext } from "../../../../context/FilterProvider";

const TrainingType = ({ styles }) => {
  const { trainingTypeOptions, updateState, trainingType } = useContext(
    FilterContext
  );

  const handleChange = ({ id, value }) => {
    updateState({ key: id, value });
  };

  return (
    <div className="placeLocationContainer">
      <label htmlFor="trainingType">Online/In Person</label>
      <Select
        styles={styles}
        id="trainingType"
        value={trainingType || ""}
        displayEmpty
        onChange={handleChange}
      >
        <MenuItem value="">All</MenuItem>
        {trainingTypeOptions.map(({ value, text }) => (
          <MenuItem key={value} value={value}>
            {text}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};
export default TrainingType;
