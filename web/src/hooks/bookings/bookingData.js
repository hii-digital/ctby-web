import { useSelector, shallowEqual } from "react-redux";

// hooks
import { useUserHook } from "../users/useUserHook";

export const useBookingData = ({ proId, clientId }) => {
  const clientData = useUserHook(clientId);
  const { professionData, approvedUsers: approvedPros } = useSelector(
    ({ firestore }) => {
      return {
        professionData: firestore.data[`${proId}/professions`],
        approvedUsers: firestore.data.approvedUsers || {},
      };
    },
    shallowEqual
  );

  return {
    clientData,
    proData: { ...approvedPros[proId], professions: professionData },
  };
};
