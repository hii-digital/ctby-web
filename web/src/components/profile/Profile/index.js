import React, { useContext } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Divider } from "@material-ui/core";
import { isLoaded } from "react-redux-firebase";
import LazyImage from "../../ImageRenderer/ImageRenderer";
// components
import { FilledButton, Button } from "../../../ui/Button";
import Carousel from "../../../ui/Carousel";
import Toggler from "../../../ui/Toggler";
import RequestForm from "../RequestForm";
import Review from "../Review";
import Loading from "../../modules/Loading";
import SessionProgression from "../SessionProgression";

// hooks
import { useProfileHook } from "../../../hooks/profile";
import { useBookingsHook } from "../../../hooks/bookings";
import { useConversations } from "../../../hooks/conversations";
import { useBreakpoints } from "../../../hooks/window/breakpoints";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// helpers
import { checkIsVerified } from "../../../helpers/general";
import { getRating } from "../../../helpers/rating";
import { generateUniqueName } from "../../../helpers/conversations";

// icons
import { ReactComponent as TicketIcon } from "../../../assets/icons/ticket.svg";
import { ReactComponent as StarIcon } from "../../../assets/icons/star.svg";
import { ReactComponent as MarkerIcon } from "../../../assets/icons/marker.svg";
import { ReactComponent as FaceIcon } from "../../../assets/icons/face.svg";
import { ReactComponent as QuoteIcon } from "../../../assets/icons/quote.svg";

import "./styles.scss";

const Profile = () => {
  const { openSidebar } = useContext(SidebarContext);
  const { params } = useRouteMatch();
  const {
    firstName,
    lastName,
    photoURL,
    about,
    funFact,
    favQuote,
    specialties,
    professions,
    activeProfession,
    fromRate,
    onlineRate,
    inpersonRate,
    licenseURL,
    city,
    state,
  } = useProfileHook(params.uid);

  const history = useHistory();
  const { isBreakpointL } = useBreakpoints();
  const {
    auth: { uid },
    profile: { isPro },
  } = useSelector(({ firebase }) => firebase, shallowEqual);

  const { initLoaded, conversationsMap } = useConversations();

  const { bookingsAsProOrdered } = useBookingsHook(params.uid);

  const shareSidebarOpen = () => {
    openSidebar("shareProfile", { firstName });
  };

  const isAbleToClick = uid && checkIsVerified();

  const handleUnverfiedClick = () => {
    if (!uid) {
      openSidebar("signInClient");
      return;
    }
    if (!checkIsVerified()) {
      if (isPro) {
        history.push("/onboarding");
      } else {
        history.push("/onboarding-client");
      }
    }
  };

  const requestBookingSidebarOpen = () => {
    openSidebar("requestBooking", {
      onUnverfiedClick: handleUnverfiedClick,
      isAbleToClick,
      uid: params.uid,
      anchor: "bottom",
    });
  };

  const handleSendMessageClick = async () => {
    if (!isAbleToClick) {
      handleUnverfiedClick();
      return;
    }
    const uniqueName = generateUniqueName(uid, params.uid);
    const pushRoute = conversationsMap[uniqueName]
      ? conversationsMap[uniqueName]
      : `new/${uniqueName}`;
    history.push(`/conversations/${pushRoute}`);
  };

  const checkIsLoaded = () => {
    return isLoaded(bookingsAsProOrdered);
  };

  const renderLicense = () => (
    <div className="licenses">
      <h5 className="title">Licenses</h5>
      <div>
        <a href={licenseURL} target="_blank" rel="noopener noreferrer">
          <img alt="license" src={licenseURL} />
         
        </a>
      </div>
    </div>
  );

  if (professions && checkIsLoaded()) {
    const bookingsWithReview = bookingsAsProOrdered.filter(
      ({ rating }) => !!rating
    );

    const descSettings = {
      infinite: bookingsWithReview > 5,
      dots: false,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            infinite: bookingsWithReview > 3,
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 768,
          settings: {
            infinite: bookingsWithReview > 2,
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 512,
          settings: {
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            centerPadding: "20%",
          },
        },
      ],
    };

    const { rating, length } = getRating(bookingsAsProOrdered);
console.log("city",city)
    return (
      <div className="profile-root">
        <div className="main">
          <div className="image-section">
            <div className="image-container">
              <img alt="profile" src={photoURL} />
            </div>
            <div className="btn-group">
              <FilledButton
                type="button"
                onClick={handleSendMessageClick}
                disabled={
                  uid === params.uid || (uid && initLoaded !== "success")
                }
              >
                Send Message
              </FilledButton>

              <Button type="button" onClick={shareSidebarOpen}>
                Share
              </Button>
            </div>
            {isBreakpointL && licenseURL && renderLicense()}
          </div>
          <div className="profile-data">
            <h1>
              {firstName} {lastName}
            </h1>
            <Toggler items={professions} active={activeProfession} />
            <div className="main-data">
              <Divider />
              <div>
                <span className="price">
                  {onlineRate && (
                    <span>
                      <TicketIcon />
                      <h5>Online: ${onlineRate}</h5>
                    </span>
                  )}
                  {inpersonRate && (
                    <span className="inperson">
                      <TicketIcon />
                      <h5>In Person: ${inpersonRate}</h5>
                    </span>
                  )}
                </span>
                <span className="reviews">
                  <StarIcon />
                  <h5>{length ? `${rating} (${length})` : "No Reviews Yet"}</h5>
                </span>
                <span className="city">
                  <MarkerIcon />
                  <h5>
                    {city}, {state}
                  </h5>
                </span>
              </div>
              <Divider />
            </div>
            <div className="bio">
              <h5 className="title">Bio</h5>
              <span>{about}</span>
            </div>
            {!isBreakpointL && (
              <FilledButton type="button" onClick={requestBookingSidebarOpen}>
                Request
              </FilledButton>
            )}
            <div className="specializes">
              <h5 className="title">Specializing in</h5>
              <div className="tags">
                {specialties?.length &&
                  specialties.map((value) => <div key={value}>#{value}</div>)}
              </div>
            </div>
            <div className="facts-quotes">
              <h5 className="title">Facts & Quotes</h5>
              <div>
                <div>
                  <FaceIcon />
                  <span>{funFact}</span>
                </div>
                <div>
                  <QuoteIcon />
                  <span>{favQuote}</span>
                </div>
              </div>
            </div>
            {!isBreakpointL && licenseURL && renderLicense()}
          </div>
          {isBreakpointL && (
            <RequestForm
            uid={params.uid}
            onUnverfiedClick={handleUnverfiedClick}
            isAbleToClick={isAbleToClick}
            />
          )}
        </div>
        <div className="reviews">
          {Boolean(bookingsWithReview.length) && (
            <>
              <h5 className="title">Last Reviews</h5>
              <Carousel settings={descSettings}>
                {bookingsWithReview.map((booking) => (
                  <Review key={booking.id} {...booking} />
                ))}
              </Carousel>
            </>
          )}
        </div>
            <div>
            <SessionProgression />
            </div>
      </div>
    );
  }
  return <Loading />;
};

export default Profile;
