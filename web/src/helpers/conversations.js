export const generateUniqueName = (uid1, uid2) => {
  return [uid1, uid2].sort().join("-");
};

export const parseUniqueName = (uniqueName) => {
  if (!uniqueName) {
    return [];
  }
  return uniqueName.split("-");
};
