import React, { useState } from "react";
import { useDispatch } from "react-redux";
import cx from "classnames";
import { useRouteMatch } from "react-router-dom";
import { Button, TextArea, Form } from "semantic-ui-react";

// actions;
import { updateBooking } from "../../../../store/actions/bookingsActions";

// hooks
import { useRefundHook } from "../../../../hooks/bookings/refundHook";
import { useBookingHook } from "../../../../hooks/bookings/booking";

// components
import Modal from "../../../modal/Modal";

const AskForRefund = () => {
  const { params } = useRouteMatch();
  const isAvailableToRefund = useRefundHook(params.id);
  const { refundInquiry } = useBookingHook(params.id);
  const dispatch = useDispatch();
  const [value, setValue] = useState("");

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    document.body.style.overflow = "unset"; // old code style
    const payloads = {
      refundInquiry: value,
      refundTimestamp: new Date(),
    };
    dispatch(updateBooking({ id: params.id, payloads }));
  };

  if (refundInquiry) {
    return <p>Your refund has been initiated.</p>;
  }

  if (isAvailableToRefund) {
    return (
      <Modal
        buttonText="Ask for refund"
        buttonStyle="button button--primary button--full"
        content={
          <>
            <h2>Refund Reason</h2>
            <Form onSubmit={handleSubmit}>
              <div className="form__inner">
                <Form.Field>
                  <TextArea
                    id="refund-reason"
                    value={value}
                    onChange={onChange}
                  />
                </Form.Field>
              </div>
              <Button
                className={cx("button--secondary button--full", {
                  inactive: !value.trim().length,
                })}
                type="submit"
              >
                Send Refund Inquiry
              </Button>
            </Form>
          </>
        }
      />
    );
  }
  return null;
};

export default AskForRefund;
