import React, { useState } from "react";
import cx from "classnames";
import { isLoaded } from "react-redux-firebase";

// hooks
import { useBookingData } from "../../../hooks/bookings/bookingData";

// icons
import { ReactComponent as StarIcon } from "../../../assets/icons/filledStar.svg";
import LinkButton from "../../../ui/LinkButton";

import "./styles.scss";

const Review = ({ clientId, proId, rating }) => {
  const [isWholeVisible, setWholeVisible] = useState(false);
  const { score, text } = rating;
  const { clientData } = useBookingData({ proId, clientId });
  
  const toggleWholeVisible = () => {
    setWholeVisible((prevState) => !prevState);
  };
  
  if (isLoaded(clientData)) {
    const { firstName, lastName, photoURL, initials } = clientData;
    return (
      <div className="review-root">
        <div>
          {photoURL ? (
            <img alt="reviewer" src={photoURL} />
          ) : (
            <div className="initials">{initials}</div>
          )}
          <div>
            <h5 className="title">
              {firstName} {lastName}
            </h5>
            <p>
              <span>
                <StarIcon />
              </span>
              <span>{score}/5</span>
            </p>
          </div>
        </div>
        <span className={cx({ "whole-visible": isWholeVisible })}>{text}</span>
        <LinkButton onClick={toggleWholeVisible}>
          Read {isWholeVisible ? "less" : "more"}
        </LinkButton>
      </div>
    );
  }
  return null;
};

export default Review;
