import React, { useState, useEffect } from "react";

import TabsComponent from "./tabs.component";
import Tab from "./tab.component";

const Hola = () => {
  useEffect(() => {
    console.log("Petición a API 1");

    return function () {
      console.log("Unmount 1");
    };
  }, []);

  return <h1>Hola</h1>;
};

const Hola2 = () => {


  return <h1>Cómo te vai?</h1>;
};

export default function Tabs() {
  const [key, setKey] = useState(0);

  return (
    <div className="App">
      <TabsComponent
        activeKey={key}
        onSelect={(k) => {
          setKey(k);
        }}
      >
        <Tab eventKey={0} title="Hola">
          <Hola />
        </Tab>
        <Tab eventKey={1} title="Mundo">
          <Hola2 />
        </Tab>
      </TabsComponent>
    </div>
  );
}

