import React from "react";
import cx from "classnames";
import MuiButton from "@material-ui/core/Button";
import MuiCircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import colors from "../assets/theme/colors";
import analytics from '../config/fbConfig';
// import { getAnalytics, logEvent } from "firebase/";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    border: `2px solid ${colors.ORANGE_ED9}`,
    borderRadius: 8,
    filter: "dropShadow(0px 8px 16px rgba(102, 173, 132, 0.4))",
    textTransform: "capitalize",
    textAlign: "center",
    fontSize: 20,
    lineHeight: "24px",
    fontFamily: "RubikBold",
    display: "block",
    background: "transparent",
    "&.Mui-disabled": {
      color: colors.WHITE_FFF,
      boxShadow: "none",
      background: colors.GREY_D8D,
    },
  },
  hover: {
    "&:hover": {
      border: `4px solid ${colors.ORANGE_ED9}`,
      filter: "drop-shadow(0px 8px 16px rgba(102, 173, 132, 0.4))",
      borderRadius: 8,
      background: "inherit",
    },
  },
  filled: ({ hover: hoverEffect, ...rest }) => {
    const hover = hoverEffect || {
      background: `linear-gradient(93.5deg, ${colors.ORANGE_EC9} 0%, ${colors.ORANGE_E17} 100%)`,
      "&::after": {
        display: "block",
        content: "''",
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        margin: 2,
        transition: "border .2s",
        border: "3px solid rgb(255 255 255 / 80%)",
        borderRadius: 8,
      },
    };
    return {
      background: `linear-gradient(93.5deg, ${colors.ORANGE_EC9} 0%, ${colors.ORANGE_E17} 100%)`,
      boxShadow: "0px 8px 16px rgba(235, 151, 85, 0.4)",
      color: colors.WHITE_FFF,
      border: "none",
      ...rest,
      "&:hover": {
        ...hover,
      },
    };
  },
  loading: {
    "& .MuiButton-label": {
      display: "flex",
      alignItems: "center",
      "& .MuiCircularProgress-root": {
        color: colors.WHITE_FFF,
      },
    },
  },
}));

export const Button = ({ className, children, ...restProps }) => {
  const classes = useStyles();
  return (
    <MuiButton
      disableRipple
      className={cx(classes.root, classes.hover, className)}
      {...restProps}
    >
      {children}
    </MuiButton>
  );
};

export const FilledButton = ({ className, children, styles, ...restProps }) => {
  const classes = useStyles(styles);
  // const analytics = getAnalytics();

  const logButtonClickEvent = () => {
    console.log('clicked', analytics, analytics.getAnalytics())
    // getAnalytics
    // logEvent(analytics, 'select_content', {
    //   content_type: 'image',
    //   content_id: 'P12453',
    //   items: [{ name: 'Kittens' }]
    // });
  }

  return (
    <MuiButton
      disableRipple
      className={className}
      classes={{
        root: cx(classes.root, classes.filled),
      }}
      onClick={logButtonClickEvent}
      {...restProps}
    >
      {children}
    </MuiButton>
  );
};

export const LoadingButton = ({
  component: Component,
  className,
  children,
  ...restProps
}) => {
  const classes = useStyles();
  return (
    <Component
      {...restProps}
      className={cx(classes.root, classes.loading, className)}
      endIcon={<MuiCircularProgress size={20} />}
    >
      {children}
    </Component>
  );
};

export default Button;
