import React, { useContext, useState } from "react";
import moment from "moment";
import cx from "classnames";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Divider, MenuItem } from "@material-ui/core";
import { getCoupon } from "../../..//store/actions/stripeActions";
// components
import Toggler from "../../../ui/Toggler";
import CustomSelect from "../../../ui/Select";
import { FilledButton, LoadingButton } from "../../../ui/Button";
import Input from "../../../ui/Input";
import LinkButton from "../../../ui/LinkButton";
import DatePicker from "../../../ui/DatePicker";
import AddressInput from "../AddressInput";
import { useHistory } from "react-router-dom";
// hooks
import { useBookRequestHook } from "../../../hooks/bookRequest";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// data
import { HOURS, durations, FORMAT } from "../../../data/hours";
import {
  trainingTypeValues,
  inpersonTypeValues,
} from "../../../data/professions";

// helpers
import { getTomorrow } from "../../../helpers/time";

import "./styles.scss";

const selectStyles = {
  height: 215,
};

const withParams = (params) => (Component) => (props) => (
  <Component {...params} {...props} />
);

const RequestForm = ({ onUnverfiedClick, isAbleToClick, uid }) => {
  const dispatch = useDispatch();
  let history = useHistory();
  const { openSidebar } = useContext(SidebarContext);
  const { inperson } = trainingTypeValues;
  const { comeToMe, goToPro } = inpersonTypeValues;
  const {
    hasErrorBorder,
    professions,
    service,
    date,
    startTime,
    duration,
    inpersonType,
    from,
    to,
    addressLineSecond,
    address,
    errorMsg,
    setErrorMsg,
    clearError,
    setIsCloseToPro,
    checkIsCloseToPro,
    checkIsSelectorDisabled,
    checkIsDurationAvailable,
    checkIsStartTimeAvailable,
    filterDates,
    renderTotal,
    renderPrice,
    renderDurationValue,
    renderServiceValue,
    renderInpersonTypeValue,
    items,
    activeType,
    onDateChange,
    onSelectChange,
    onInputChange,
    onSubmit,
  } = useBookRequestHook(uid);
  const [isLoading, setLoading] = useState(false);
  const [showCoupon, setShowCoupon] = useState(false);
  const [couponError, setCouponError] = useState(false);
  const [couponSuccess, setCouponSuccess] = useState(false);
  const [coupon, setCoupon] = useState("");
  const [discount, setDiscount] = useState(0);
  const { auth } = useSelector(({ firebase }) => firebase, shallowEqual);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    if (!isAbleToClick) {
      onUnverfiedClick();
      return;
    }
    if (!duration || checkIsSelectorDisabled("duration")) {
      setErrorMsg("Required fields should be filled");
      return;
    }
    if (inpersonType === comeToMe.value) {
      setLoading(true);
      const { isClose, message } = await checkIsCloseToPro();
      setLoading(false);
      if (!isClose) {
        setIsCloseToPro({ isClose, message });
        setErrorMsg(message);
        return;
      }
    }

    openSidebar("finishBooking", {
      duration,
      totalPrice: renderTotal(discount),
      date: moment(from).format(FORMAT),
      from: moment(from).format("hh:mm A"),
      to: moment(to).format("hh:mm A"),
      proId: uid,
      onSubmit: () => onSubmit(discount,history),
    });
  };

  const onAddressSelect = (value) => {
    onSelectChange({ id: "address", value });
  };

  const handleAddressChange = (e) => {
    const { id, value } = e.target;
    onInputChange({ id, value });
  };

  const Select = isAbleToClick
    ? CustomSelect
    : withParams({ onOpen: onUnverfiedClick })(CustomSelect);

  const sendCoupon = async () => {
    try {
      setLoading(true);
      let res = await dispatch(getCoupon(coupon));
console.log("coupon",res.data)
      let cp = res.data?.couponInfo;
      let discount = cp.amount_off ? cp.amount_off : cp;
      setDiscount(discount ? discount : 0);
      setCouponError(false);
      setCouponSuccess(true)
    } catch (error) {
      console.log("error", error);
      setCouponError(true);
      setCouponSuccess(false)
      setDiscount(0);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="request-form">
      <div>
        <form onSubmit={handleFormSubmit}>
          <Toggler InPersonDısabled={false} items={items} active={activeType} />
          <Divider />
          <div className="controls">
            {activeType === inperson.value && (
              <div>
                <label htmlFor="inpersonType" className="title">
                  In Person Type*
                </label>
                <Select
                  id="inpersonType"
                  renderValue={renderInpersonTypeValue}
                  value={inpersonType}
                  disabled={checkIsSelectorDisabled("inpersonType")}
                  displayEmpty
                  onChange={onSelectChange}
                  styles={{ isError: hasErrorBorder("inpersonType") }}
                >
                  {[goToPro, comeToMe].map(({ value, text }) => (
                    <MenuItem key={value} value={value}>
                      {text}
                    </MenuItem>
                  ))}
                </Select>
              </div>
            )}
            {inpersonType === comeToMe.value && (
              <div>
                <label htmlFor="address" className="title">
                  Address Line 1*
                </label>
                <AddressInput
                  state={address}
                  onSelect={onAddressSelect}
                  isError={hasErrorBorder("address")}
                />
              </div>
            )}
            {inpersonType === comeToMe.value && (
              <div>
                <label htmlFor="addressLineSecond" className="title">
                  Address Line 2
                </label>
                <Input
                  value={addressLineSecond}
                  id="addressLineSecond"
                  placeholder="Apartment, suite, etc."
                  onChange={handleAddressChange}
                />
              </div>
            )}
            <div>
              <label htmlFor="service" className="title">
                Service*
              </label>
              <Select
                id="service"
                renderValue={renderServiceValue}
                value={service}
                disabled={checkIsSelectorDisabled("service")}
                displayEmpty
                onChange={onSelectChange}
                styles={{ isError: hasErrorBorder("service") }}
              >
                {professions.map(({ id: value, name }) => (
                  <MenuItem key={value} value={value}>
                    {name}
                  </MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <label htmlFor="date" className="title">
                Date*
              </label>
              <DatePicker
                className={cx({ error: hasErrorBorder("date") })}
                disabled={checkIsSelectorDisabled("date")}
                selected={date}
                onChange={onDateChange}
                formatWeekDay={(dayName) => dayName.substr(0, 1)}
                dateFormat="d MMM"
                minDate={getTomorrow()}
                popperPlacement="bottom-center"
                popperModifiers={{
                  flip: { behavior: ["bottom"] },
                  preventOverflow: { enabled: false },
                  hide: {
                    enabled: false,
                  },
                }}
                filterDate={filterDates}
                placeholderText="Date"
                onFocus={clearError}
              />
            </div>
            <div className="time-section">
              <div>
                <label htmlFor="time" className="title">
                  Start Time*
                </label>
                <Select
                  id="startTime"
                  renderValue={(value) => value || "Start Time"}
                  value={startTime}
                  displayEmpty
                  onChange={onSelectChange}
                  disabled={checkIsSelectorDisabled("startTime")}
                  styles={{
                    ...selectStyles,
                    isError: hasErrorBorder("startTime"),
                  }}
                >
                  {date &&
                    HOURS.map(({ value, text }) => (
                      <MenuItem
                        key={value}
                        value={value}
                        disabled={!checkIsStartTimeAvailable(value, date)}
                      >
                        {text}
                      </MenuItem>
                    ))}
                </Select>
              </div>
              <div>
                <label htmlFor="duration" className="title">
                  Duration*
                </label>
                <Select
                  id="duration"
                  renderValue={renderDurationValue}
                  value={duration}
                  displayEmpty
                  onChange={onSelectChange}
                  disabled={checkIsSelectorDisabled("duration")}
                  styles={{ isError: hasErrorBorder("duration") }}
                >
                  {startTime &&
                    durations.map(({ value, text }) => (
                      <MenuItem
                        key={value}
                        value={value}
                        disabled={
                          !checkIsDurationAvailable({
                            duration: value,
                            startTime,
                            date,
                          })
                        }
                      >
                        {text}
                      </MenuItem>
                    ))}
                </Select>
              </div>
            </div>
            <div>
              {!showCoupon ? (
                <a onClick={() => setShowCoupon(true)}>
                  Do you have a promo code ?
                </a>
              ) : (
                <div>
                  <label htmlFor="date" className="title">
                    Coupon Code :
                    {couponError && (
                      <div style={{ color: "red" }}>Wrong Coupon code !</div>
                    )}
                    {couponSuccess && (
                      <div style={{ color: "green" }}>Coupon code has been applied!</div>
                    )}
                  </label>
                  <Input
                    type="text"
                    className="text-input"
                    value={coupon}
                    id="coupon"
                    onChange={(e) => setCoupon(e.target.value)}
                    placeholder="Enter Coupon Code"
                  />

                  {!isLoading && (
                    <FilledButton
                      onClick={() => sendCoupon()}
                      disabled={auth.uid === uid}
                    >
                      Apply
                    </FilledButton>
                  )}

                  {isLoading && (
                    <LoadingButton component={FilledButton} type="submit">
                      Apply
                    </LoadingButton>
                  )}
                </div>
              )}
            </div>
            <h5 className="title">Total Price</h5>
            <div className="total">
              <span>
                ${renderPrice(discount)}*{`${duration / 60 || 0}h`}
              </span>
              <h5>${renderTotal(discount)}</h5>
            </div>
          </div>
          <Divider />
          {errorMsg && <p className="error-msg">{errorMsg}</p>}
          {!isLoading && (
            <FilledButton type="submit" disabled={auth.uid === uid}>
              Request
            </FilledButton>
          )}
          {isLoading && (
            <LoadingButton component={FilledButton} type="submit">
              Request
            </LoadingButton>
          )}
        </form>
        <LinkButton to="/cancellation-policy">Cancellation Policy</LinkButton>
      </div>
    </div>
  );
};

export default RequestForm;
