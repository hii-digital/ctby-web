import React from "react";
import { MenuItem } from "@material-ui/core";

// components
import ImageUpload from "./ImageUpload";
import Select from "../../../../ui/Select";
import Input from "../../../../ui/Input";

// helpers
import { checkValidNumbers } from "../../../../helpers/general";

// assets
import wrong from '../../../../assets/images/image-guidelines-wrong.png';
import wrong2 from '../../../../assets/images/image-guidelines-wrong2.jpg';
import right from '../../../../assets/images/image-guidelines-right.png';

const ProfessionForm = (props) => {
	const {
		profession,
		children,
		unAvailable,
		professions,
		setProfession,
		setLicenseImg,
		setZipcode,
		setAddressLineFirst,
		setAddressLineSecond,
		deleteLicenseImg,
		options,
	} = props;

	const {
		zipCode,
		value: professionValue,
		licensePreview,
		addressLineFirst,
		addressLineSecond,
	} = professions[profession];

	const onProfessionChange = ({ value }) => {
		setProfession(profession, { value });
	};

	const onZipcodeChange = (e) => {
		const { value } = e.target;
		const isValidNumbers = checkValidNumbers(value);
		if (value && !isValidNumbers) {
			e.preventDefault();
		} else {
			setZipcode(profession, e.target.value);
		}
	};

	const onLicenseImageDrop = (data) => {
		setLicenseImg(profession, data);
	};

	const onLicenseImageDelete = () => {
		deleteLicenseImg(profession);
	};

	const onAddressLineChange = (e) => {
		const { value, id } = e.target;
		if (id === "address-line-first") {
			setAddressLineFirst(profession, value);
		} else setAddressLineSecond(profession, value);
	};

	return (
		<div className="profession-form">
			<div className="profession-form__form">
				<div>
					<span>your primary profession</span>
					<Select value={professionValue} onChange={onProfessionChange}>
						{options.map(({ value, text }) => (
							<MenuItem
								key={value}
								value={value}
								disabled={
									unAvailable?.includes(value) && value !== professionValue
								}
							>
								{text}
							</MenuItem>
						))}
					</Select>
				</div>
				<div>
					<div>
						<span>address line 1</span>
						<Input
							id="address-line-first"
							disableUnderline
							placeholder="Street Address"
							value={addressLineFirst}
							onChange={onAddressLineChange}
						/>
					</div>
				</div>
				<div>
					<div>
						<span>address line 2</span>
						<Input
							id="address-line-second"
							disableUnderline
							placeholder="Apartment, suite, unit, building, floor, etc."
							value={addressLineSecond}
							onChange={onAddressLineChange}
						/>
					</div>
				</div>
				<div>
					<span>enter your zipcode</span>
					<Input
						disableUnderline
						placeholder="e.g.11111"
						value={zipCode}
						onChange={onZipcodeChange}
					/>
				</div>
			</div>
			<div className="profession-form__uploads">
				{children}
				<ImageUpload
					label="license image"
					imageSrc={licensePreview}
					handleDrop={onLicenseImageDrop}
					handleDelete={onLicenseImageDelete}
				/>
			</div>
			<div className="profession-form__example">
				<div className="profession-form__example-inner">
					<span className="heading heading--md">Example</span>
					<div className="profession-form__example-images">
						<div className="profession-form__example-image">
							<img src={right} />
						</div>
						<div className="profession-form__example-image">
							<img src={wrong} />
						</div>
						<div className="profession-form__example-image">
							<img src={wrong2} />
						</div>
					</div>

					<span className="heading heading--sm">Image Guidelines</span>
					<p>For a quick image approval, follow these guidelines:</p>
					<ul>
						<li>Keep images under 7mb</li>
						<li>Image must include your face clearly</li>
						<li>Keep images professional</li>
					</ul>
				</div>	
			</div>
		</div>
	);
};

export default ProfessionForm;
