import React from "react";
import Slider from "react-slick";
import "./Carousel.scss";
import rightArrow from "../../assets/images/arrow_right.svg";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      role="button"
      tabIndex={0}
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
      onKeyDown={onClick}
    >
      <img src={rightArrow} alt="rightArrow" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      role="button"
      tabIndex={0}
      className={className}
      style={{ ...style, display: "none", background: "green" }}
      onClick={onClick}
      onKeyDown={onClick}
    />
  );
}

export default function CarouselComponent() {
  const settings = {
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <div className="slider-wrapper-main-block">
      <Slider {...settings}>
        <div className="slider-wrapper__item-block">
          <div className="how-it-works__main-slider">
            <div className="how-it-works__main-slider__img-wrapper">
              <div className="img"></div>
            </div>
            <div className="slider-wrapper-main-block__text-wrapper">
              <div className="slider-wrapper-main-block__title">
                work on being the best version of you
              </div>
              <div className="slider-wrapper-main-block__text">
                <p>
                To be the best version of you means to envision what success as a person would look like to you and strive to be that person.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="how-it-works__main-slider">
            <div className="how-it-works__main-slider__img-wrapper">
              <div className="img"></div>
            </div>
            <div className="slider-wrapper-main-block__text-wrapper">
              <div className="slider-wrapper-main-block__title">
                Find a pro in person or online
              </div>
              <div className="slider-wrapper-main-block__text">
                <p>
                  C2byou is a platform where you can book massage therapists and
                  trainers instantly online, based on your convenience, and get
                  the resources you need to live a healthy lifestyle.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="how-it-works__main-slider">
            <div className="how-it-works__main-slider__img-wrapper">
              <div className="img"></div>
            </div>
            <div className="slider-wrapper-main-block__text-wrapper">
              <div className="slider-wrapper-main-block__title">
                Resources to support your fitness journey
              </div>
              <div className="slider-wrapper-main-block__text">
                <p>
                C2B is here to help you in achieving your health and wellness goals through our platform, newsletter, and social media pages.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="slider-wrapper__item-block">
          <div className="how-it-works__main-slider">
            <div className="how-it-works__main-slider__img-wrapper">
              <div className="img"></div>
            </div>
            <div className="slider-wrapper-main-block__text-wrapper">
              <div className="slider-wrapper-main-block__title">
                Your safety is important to us
              </div>
              <div className="slider-wrapper-main-block__text">
                <p>
                We know you’re feeling the effects of stress now more than ever, which means it’s even more important to make sure you feel safe and comfortable when you’re booking in-person sessions with our massage therapists and trainers.
                </p>
              </div>
            </div>
          </div>
        </div>
      </Slider>
    </div>
  );
}
