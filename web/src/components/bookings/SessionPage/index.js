import React, { useState,useContext } from "react";
import moment from "moment";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useRouteMatch, Redirect,useHistory } from "react-router-dom";
import { isLoaded } from "react-redux-firebase";

import { FilledButton } from "../../../ui/Button";
import { generateUniqueName } from "../../../helpers/conversations";

// context
import { SidebarContext } from "../../../context/SidebarProvider";

// components
import BookingDetails from "../BookingsDetails";
import Loading from "../../modules/Loading";
import { AwaitingApprovalPro } from "./pro";
import { AwaitingApprovalClient, CompletedClient } from "./client";
import { AskForRefund, StartVideoCall } from "./actions";
import { checkIsVerified } from "../../../helpers/general";
// actions
import { updateBooking } from "../../../store/actions/bookingsActions";
import { useConversations } from "../../../hooks/conversations";
// hooks
import { useBookingsHook } from "../../../hooks/bookings";

// data
import { STATUSES } from "../../../data/bookings";
import { FORMAT } from "../../../data/hours";

const {
  active,
  pending,
  awaitingApproval,
  completed,
  cancelled,
  declined,
} = STATUSES;

const SessionPage = () => {
  const dispatch = useDispatch();
  const { openSidebar } = useContext(SidebarContext);
  const {
    auth: { uid },
    profile: { isPro },
  } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { auth, profile } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { initLoaded, conversationsMap } = useConversations();
  const { bookingsAsClient, bookingsAsPro } = useBookingsHook(auth.uid);
  const { params } = useRouteMatch();

  const [isEdit, setIsEdit] = useState(false);
  const [date, setDate] = useState(null);
  const [startTime, setStartTime] = useState("");
  const [checkValid, setCheckValid] = useState(false);

  const { isApproved } = profile;
  const bookings = { ...bookingsAsClient, ...bookingsAsPro };
  const booking = bookings[params.id];


  const isAbleToClick = uid && checkIsVerified();

  const history = useHistory();




  const handleUnverfiedClick = () => {
    if (!uid) {
      openSidebar("signInClient");
      return;
    }
    if (!checkIsVerified()) {
      if (isPro) {
        history.push("/onboarding");
      } else {
        history.push("/onboarding-client");
      }
    }
  };


  const checkIsLoaded = () => {
    if (!isApproved) {
      return isLoaded(bookingsAsClient);
    }
    return isLoaded(bookingsAsClient) && isLoaded(bookingsAsPro);
  };

  const saveUpdatedDateTime = () => {
    const initFrom = moment(booking.from.toDate());
    const initTo = moment(booking.to.toDate());

    const stringDate = moment(date).format(FORMAT);
    const from = moment(
      `${stringDate} ${startTime}`,
      `${FORMAT} HH:mm A`
    ).toDate();

    const duration = initTo.diff(initFrom, "minutes");
    const to = moment(from).add(duration, "m").toDate();
    const nextDayAfterSessionCompleted = new Date(to);
    nextDayAfterSessionCompleted.setDate(
      nextDayAfterSessionCompleted.getDate() + 1
    );

    const payloads = {
      from,
      to,
      refundTimestamp: nextDayAfterSessionCompleted,
    };
    if (booking.from.toDate() !== from) {
      // TODO: set loading
      dispatch(updateBooking({ id: params.id, payloads }));
    }
    setIsEdit(false);
  };

  const handleEditClick = (state) => {
    setIsEdit(state);
    setStartTime("");
    setDate(null);
  };

  if (checkIsLoaded()) {
    if (!booking) {
      return <Redirect to="/404" />;
    }

    const renderClient = (status) => {
      switch (status) {
        case active.value:
        case pending.value:
          return <StartVideoCall />;
        case awaitingApproval.value:
          return (
            <AwaitingApprovalClient
              startTime={startTime}
              isEdited={isEdit}
              handleEdit={handleEditClick}
              handleSave={saveUpdatedDateTime}
              setCheckValid={setCheckValid}
            />
          );
        case completed.value:
          return <CompletedClient />;
        case cancelled.value:
        case declined.value:
          return <AskForRefund />;
        default:
          return null;
      }
    };

    const renderPro = (status) => {
      switch (status) {
        case active.value:
        case pending.value:
          return <StartVideoCall />;
        case awaitingApproval.value:
          return <AwaitingApprovalPro />;
        default:
          return null;
      }
    };
    const handleSendMessageClick = async () => {
      if (!isAbleToClick) {
        handleUnverfiedClick();
        return;
      }
      const uniqueName = generateUniqueName(uid, params.uid);
      const pushRoute = conversationsMap[uniqueName]
        ? conversationsMap[uniqueName]
        : `new/${uniqueName}`;
      history.push(`/conversations/${pushRoute}`);
    };

    const { status, clientId, proId } = booking;
    return (
      <div className="interaction-details">
        <div className="container container--top-bottom-padding">
          <BookingDetails
            {...booking}
            isEdited={isEdit}
            date={date}
            startTime={startTime}
            handleChangeDate={setDate}
            handleChangeTime={setStartTime}
            checkValid={checkValid}
            setCheckValid={setCheckValid}
          />
          <div className="interaction-details__buttons text--center">
            {clientId === auth.uid && renderClient(status)}
            {proId === auth.uid && renderPro(status)}

             <FilledButton
                type="button"
                onClick={handleSendMessageClick}
                disabled={
                  uid === params.uid || (uid && initLoaded !== "success")
                }
              >
                Send Message
              </FilledButton>
          </div>
        </div>
      </div>
    );
  }
  return <Loading />;
};

export default SessionPage;
