import React, { useState } from "react";
import { Select as MuiSelect } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// icons
import { ReactComponent as ArrowIcon } from "../assets/icons/arrow.svg";

import colors from "./colors";

export const useStyles = makeStyles((theme) => ({
  root: ({ isOpen, isError, inputWidth, root }) => {
    const errorBorder = `2px solid ${colors.RED_ED5}`;
    const border = isOpen
      ? `2px solid ${colors.ORANGE_ED9}`
      : `1px solid ${colors.GREY_E0D}`;

    return {
      width: inputWidth || "100%",
      border: isError ? errorBorder : border,
      boxSizing: "border-box",
      borderRadius: 8,
      height: 44,
      textTransform: "capitalize",
      color: colors.BLACK_484,
      display: "inline-flex",
      position: "relative",
      alignItems: "center",
      fontWeight: 400,
      padding: "11px 20px 11px 16px",
      fontSize: 16,
      lineHeight: "16.42px",
      fontFamily: "QuestrialRegular",
      [theme.breakpoints.up("l")]: {
        fontSize: 14,
        lineHeight: "14.42px",
      },
      "&:focus": {
        backgroundColor: "transparent",
        borderRadius: 8,
      },
      "&.Mui-disabled": {
        opacity: 0.6,
        backgroundColor: colors.GREY_FBF,
      },
      ...root,
    };
  },
  nativeInput: {
    boxSizing: "border-box",
    height: "100%",
  },
  icon: ({ isIconVisible }) => {
    if (!isIconVisible) {
      return {
        display: "none",
      };
    }
    return {
      top: "unset",
      right: 16,
      "&.Mui-disabled": {
        opacity: 0.6,
      },
      "& path": {
        stroke: colors.BLACK_272,
      },
    };
  },
  paper: ({ width, height, itemHeight, listItem }) => {
    return {
      width,
      maxHeight: height,
      boxShadow: "0px 8px 16px rgb(0 0 0 / 5%)",
      "& .MuiList-padding": {
        paddingTop: 0,
        paddingBottom: 0,
      },
      "& .MuiListItem-root": {
        fontFamily: "QuestrialRegular",
        "&:not(:last-child)": {
          borderBottom: `1px solid ${colors.GREY_E0D}`,
        },
        height: itemHeight || 40,
        color: colors.BLACK_272,
        fontSize: 16,
        lineHeight: 16,
        [theme.breakpoints.up("l")]: {
          fontSize: 14,
          lineHeight: 14,
        },
        "&.Mui-selected": {
          background: "transparent",
        },
        "&.Mui-selected::after": {
          display: "block",
          position: "absolute",
          content: "''",
          backgroundImage: `url("data:image/svg+xml,%3Csvg width='22' height='15' viewBox='0 0 22 15' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1 7.67568L7.54545 14L21 1' stroke='%23ED9954' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E%0A")`,
          height: 15,
          width: 33,
          right: 0,
          zIndex: 4,
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          top: "50%",
          transform: "translateY(-50%)",
        },
        "&:hover": {
          background: "rgba(237, 153, 84, 0.1)",
          color: colors.ORANGE_ED9,
          "& path": {
            stroke: colors.ORANGE_ED9,
          },
        },
        ...listItem,
      },
    };
  },
}));

const Select = ({
  children,
  id,
  onChange,
  onOpen,
  styles,
  isIconVisible = true,
  ...rest
}) => {
  const [isOpen, setOpen] = useState(false);
  const classes = useStyles({ ...styles, isIconVisible, isOpen });
  const handleChange = (e) => {
    const { value, name } = e.target;
    onChange({ value, name, id }, e);
  };

  const handleOpen = () => {
    if (onOpen) {
      onOpen();
    }
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <MuiSelect
      fullWidth
      open={isOpen}
      disableUnderline
      MenuProps={{
        classes: { paper: classes.paper },
        elevation: 0,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },
        transformOrigin: {
          vertical: "top",
          horizontal: "center",
        },
        getContentAnchorEl: null,
      }}
      onChange={handleChange}
      IconComponent={ArrowIcon}
      onOpen={handleOpen}
      onClose={handleClose}
      classes={{
        root: classes.root,
        nativeInput: classes.nativeInput,
        icon: classes.icon,
      }}
      {...rest}
    >
      {children}
    </MuiSelect>
  );
};

export default Select;
