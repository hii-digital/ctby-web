import React, { useContext } from "react";
import { useLocation } from "react-router-dom";

// components
import ProType from "./controls/ProType";
import Price from "./controls/Price";
import TrainingType from "./controls/TrainingType";
import Interests from "./controls/Interests";
import Rating from "./controls/Rating";
import Zipcode from "./controls/Zipcode";
import { FilledButton } from "../../ui/Button";
import LinkButton from "../../ui/LinkButton";

// context
import { FilterContext } from "../../context/FilterProvider";

// icons
import { ReactComponent as ResetIcon } from "../../assets/icons/reset.svg";

import "./styles.scss";

const selectStyles = {
  height: "200px",
};

const findAProRoute = "/find-a-pro";

const Filter = () => {
  const { searchPros, resetFilter } = useContext(FilterContext);
  const { pathname } = useLocation();

  const onSearchSubmit = (e) => {
    e.preventDefault();
    searchPros(e);
  };

  return (
    <div className="filter-root">
      {pathname === findAProRoute ? (
        <>
          <form onSubmit={searchPros}>
            <ProType styles={selectStyles} />
            <Price styles={selectStyles} />
            <TrainingType styles={selectStyles} />
            <Interests styles={selectStyles} />
            <Rating styles={selectStyles} />
            <Zipcode />
            <FilledButton type="submit">Search</FilledButton>
          </form>
          <LinkButton className="reset-filters" onClick={resetFilter}>
            <span>
              <ResetIcon />
            </span>
            Reset filters
          </LinkButton>
        </>
      ) : (
        <form onSubmit={onSearchSubmit}>
          <ProType styles={selectStyles} />
          <Price styles={selectStyles} />
          <TrainingType styles={selectStyles} />
          <Zipcode />
          <FilledButton type="submit">Search</FilledButton>
        </form>
      )}
    </div>
  );
};

export default Filter;
