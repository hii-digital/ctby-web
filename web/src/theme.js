import { red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";
import createBreakpoints from "@material-ui/core/styles/createBreakpoints";

const BREAKPOINTS = {
  xs: 480,
  s: 512,
  ml: 991,
  l: 1024,
  xl: 1280,
  xxl: 1440,
};

const breakpointsFull = createBreakpoints({
  values: { ...BREAKPOINTS },
  keys: Object.keys(BREAKPOINTS),
});

// A custom theme for this app
const theme = createMuiTheme({
  breakpoints: breakpointsFull,
});

export default theme;
