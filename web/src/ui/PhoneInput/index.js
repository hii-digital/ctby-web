import React, { forwardRef, useState } from "react";
import PhoneInput, { getCountryCallingCode } from "react-phone-number-input";
import { MenuItem, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import MuiInput from "../Input";
import Select from "../Select";

import colors from "../colors";
import "./styles.scss";

const iconStyles = {
  height: "100%",
  width: 24,
  display: "flex",
  marginRight: 4,
};

const useStyles = makeStyles(() => ({
  root: {
    "& .PhoneInputCountryIcon": iconStyles,
  },
}));

const selectStyles = {
  height: 200,
  root: {
    background: colors.WHITE_FFF,
    "& .PhoneInputCountryIcon": iconStyles,
    padding: "12px 10px",
    "&": {
      borderRadius: "8px 0px 0px 8px",
      "&:focus": {
        borderRadius: "8px 0px 0px 8px",
      },
    },
  },
  listItem: {
    paddingLeft: 7,
  },
};

const inputStyles = {
  borderRadius: "0px 8px 8px 0px",
  borderLeft: 0,
  marginLeft: -1,
};

const SelectComponent = ({
  iconComponent: Icon,
  onChange,
  value,
  options,
  ...rest
}) => {
  const classes = useStyles();
  const renderValue = (v) => {
    return (
      <>
        <Icon country={v} label={v} />+{getCountryCallingCode(v)}
      </>
    );
  };

  const handleChange = ({ value: countryCode }) => {
    onChange(countryCode);
  };

  return (
    <Select
      {...rest}
      styles={selectStyles}
      renderValue={renderValue}
      onChange={handleChange}
      value={value}
    >
      {options.slice(1).map(({ value: c }) => (
        <MenuItem key={c} value={c} classes={{ root: classes.root }}>
          <Icon country={c} label={c} />
          {c}
        </MenuItem>
      ))}
    </Select>
  );
};

const InputWithStyles = forwardRef((props, ref) => (
  <MuiInput styles={inputStyles} inputRef={ref} {...props} />
));

const PhoneNumberInput = ({ value, onChange }) => {
  const [country, setCountry] = useState("US");

  const handleCountryChange = (countryCode) => {
    setCountry(countryCode);
  };

  return (
    <PhoneInput
      international={false}
      initialValueFormat="national"
      countrySelectComponent={SelectComponent}
      inputComponent={InputWithStyles}
      value={value}
      onChange={onChange}
      country={country}
      onCountryChange={handleCountryChange}
      defaultCountry={country}
      className="PhoneInput"
    />
  );
};

export default PhoneNumberInput;
