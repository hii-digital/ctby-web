import React, { useContext, useEffect, useMemo } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { isLoaded } from "react-redux-firebase";

// actions
import { getAccountLink } from "../../store/actions/stripeActions";

// context
import { FilterContext } from "../../context/FilterProvider";

// components
import ProCard from "../search/ProCard";
import Booking from "./Booking";
import Conversation from "./Conversation";
import Carousel, { NextArrow, PrevArrow } from "../../ui/Carousel";
import { FilledButton } from "../../ui/Button";
import CircularProgress from "../../ui/CircularProgress";
import TopNotificationMessage from "../../ui/TopNotificationMessage";

// hooks
import { useBookingsHook } from "../../hooks/bookings";
import { useConversations } from "../../hooks/conversations";
import { useBreakpoints } from "../../hooks/window/breakpoints";

// data
import { STATUSES } from "../../data/bookings";
import { generateUniqueRandom } from "../../helpers/general";

import "./styles.scss";

const { pending, awaitingApproval, completed, declined, cancelled } = STATUSES;

const getRandomPros = (pros) => {
  const uniqueNumbers = [];
  return {
    title: "Pros you may like",
    items: pros.slice(0, 4).map(() => {
      const i = generateUniqueRandom(pros.length, uniqueNumbers);
      uniqueNumbers.push(i);
      return pros[i];
    }),
  };
};

const getProsBasedOnInterests = (interests, pros, ordered) => {
  const found = pros.filter((uid) => {
    const proProfessions = ordered[`${uid}/professions`];
    if (!proProfessions) {
      return false;
    }
    return !!interests.find(
      (interest) =>
        !!proProfessions.find(
          ({ specialties }) => !!specialties.includes(interest)
        )
    );
  });

  if (found.length) {
    return {
      title: "Pros based on your interests",
      items: found.length > 4 ? getRandomPros(found).items : found,
    };
  }
  return getRandomPros(pros);
};

const Client = ({ bookings, isBookingsLoaded }) => {
  const onlyPendingOrAwaiting =
    isBookingsLoaded &&
    bookings.filter(
      ({ status }) =>
        status === pending.value || status === awaitingApproval.value
    );

  return (
    <Component
      bookings={onlyPendingOrAwaiting}
      isBookingsLoaded={isBookingsLoaded}
    />
  );
};

const Pro = ({ bookings, isBookingsLoaded }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { getLinkLoading, stripeAccountLink } = useSelector(
    ({ stripe }) => stripe,
    shallowEqual
  );
  const statusesArr = [
    completed,
    pending,
    awaitingApproval,
    cancelled,
    declined,
  ];
  useEffect(() => {
    if (profile.isStripeOnboardingCompleted) {
      dispatch(getAccountLink(profile.stripeId));
    }
  }, [dispatch, profile.isStripeOnboardingCompleted, profile.stripeId]);

  const handleWatchFinancialInfoClick = () => {
    if (profile.isStripeOnboardingCompleted) {
      window.open(stripeAccountLink, "_blanc");
    } else {
      history.push("/payments");
    }
  };
  const renderBookings = () =>
    isBookingsLoaded && (
      <div className="dashboard-container__bookings-data-wrapper dashboard-container__booking-wrapper">
        {statusesArr.map(({ value, text }) => (
          <div key={value} className="dashboard-container__booking-item">
            <div className="dashboard-container__booking-title">{text}</div>
            <div className="dashboard-container__booking-amount">
              <div className="dashboard-container__booking-transactions">
                {bookings.filter(({ status }) => status === value).length}
              </div>
            </div>
          </div>
        ))}
      </div>
    );

  return (
    <Component
      bookings={bookings}
      renderBookings={renderBookings}
      isBookingsLoaded={isBookingsLoaded}
    >
      <div className="dashboard-container__main__title-wrapper">
        <h2 className="dashboard-container__main__title">Finances</h2>
      </div>
      <div className="dashboard-container__finance-wrapper">
        <FilledButton
          onClick={handleWatchFinancialInfoClick}
          disabled={getLinkLoading}
        >
          Watch your financial information
        </FilledButton>
      </div>
    </Component>
  );
};

const Component = ({
  children,
  bookings,
  renderBookings,
  isBookingsLoaded,
}) => {
  const { filteredPros, checkIsProfessionsLoaded } = useContext(FilterContext);
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);
  const { ordered } = useSelector(({ firestore }) => firestore, shallowEqual);
  const { isBreakpointL } = useBreakpoints();

  const checkIsInfinite = (value) => {
    return value.length > 3;
  };
  const { conversationsArr, changesMap, initLoaded } = useConversations();

  const { massageInterests = [], fitnessInterests = [] } = profile;
  const interests = [...massageInterests, ...fitnessInterests];

  const isProfessionsLoaded = checkIsProfessionsLoaded();

  const proBasedOnInterests = useMemo(
    () => {
      return interests.length && isProfessionsLoaded
        ? getProsBasedOnInterests(interests, filteredPros, ordered)
        : getRandomPros(filteredPros);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [interests.length, filteredPros.length, isProfessionsLoaded]
  );

  const convLoaded = initLoaded === "success";

  const sortedConvs = conversationsArr?.slice().sort((a, b) => {
    return changesMap[b.sid]?.dateCreated - changesMap[a.sid]?.dateCreated;
  });

  const getSlidesToShow = (defaultValue) => {
    return proBasedOnInterests.items.length < defaultValue
      ? proBasedOnInterests.items.length
      : defaultValue;
  };

  const prosSettings = {
    isInfinite: checkIsInfinite(proBasedOnInterests.items),
    slidesToShow: getSlidesToShow(4),
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: getSlidesToShow(3),
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
        },
      },
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: getSlidesToShow(2),
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
        },
      },
    ],
  };

  const renderPros = () => {
    return proBasedOnInterests.items?.map((uid) => (
      <Link className="pro-list__card col--3" to={`/pro/${uid}`} key={uid}>
        <ProCard pro={uid} />
      </Link>
    ));
  };

  return (
    <div className="dashboard-container__wrapper">
      <div className="dashboard-container">
        <div className="dashboard-container__main">
          {children}
          <>
            <div className="dashboard-container__main__title-wrapper">
              <h2 className="dashboard-container__main__title">
                Conversations
              </h2>
              <div className="dashboard-container__main__item-score">
                <Link to="/inbox">
                  <div>
                    {convLoaded ? (
                      sortedConvs.length
                    ) : (
                      <CircularProgress size={20} />
                    )}
                  </div>
                  <div className="dashboard-container__arrow" />
                </Link>
              </div>
            </div>
            {convLoaded && !!sortedConvs.length && (
              <Carousel isInfinite={checkIsInfinite(sortedConvs.length)}>
                {sortedConvs.map(
                  ({ sid, uniqueName, lastReadMessageIndex }, i) => {
                    const { dateCreated, body, index, author } =
                      changesMap[sid] || {};
                    return (
                      <Conversation
                        conversation={sortedConvs[i]}
                        key={sid}
                        sid={sid}
                        uniqueName={uniqueName}
                        dateCreated={dateCreated}
                        body={body}
                        author={author}
                        lastMessageIndex={index}
                        lastReadMessageIndex={lastReadMessageIndex}
                      />
                    );
                  }
                )}
              </Carousel>
            )}
          </>
          <div className="dashboard-container__main__title-wrapper">
            <h2 className="dashboard-container__main__title">Bookings</h2>
            <div className="dashboard-container__main__item-score">
              <Link to="/bookings">
                <div>
                  {isBookingsLoaded ? (
                    bookings.length
                  ) : (
                    <CircularProgress size={20} />
                  )}
                </div>
                <div className="dashboard-container__arrow" />
              </Link>
            </div>
          </div>
          {isBookingsLoaded && (
            <>
              {renderBookings && renderBookings()}
              {!!bookings.length && (
                <Carousel isInfinite={checkIsInfinite(bookings)}>
                  {bookings.map((booking) => {
                    return <Booking key={booking.id} {...booking} />;
                  })}
                </Carousel>
              )}
            </>
          )}
          <div className="dashboard-container__main__title-wrapper">
            <h2 className="dashboard-container__main__title">
              {isProfessionsLoaded && proBasedOnInterests.title}
            </h2>
          </div>
          {isBreakpointL ? (
            <div className="dashboard-container__carousel-pro">
              <Carousel settings={prosSettings}>{renderPros()}</Carousel>
            </div>
          ) : (
            <div className="row">{renderPros()}</div>
          )}
        </div>
      </div>
    </div>
  );
};

const Dashboard = () => {
  const { profile, auth } = useSelector(
    ({ firebase }) => firebase,
    shallowEqual
  );
  const { bookingsAsClientOrdered, bookingsAsProOrdered } = useBookingsHook(
    auth.uid
  );

  const isApproved = profile.isApproved && profile.isPro;

  const isProLoaded =
    isLoaded(bookingsAsClientOrdered) && isLoaded(bookingsAsProOrdered);

  return (
    <div className="dashboard">
      <TopNotificationMessage />
      {isApproved ? (
        <Pro bookings={bookingsAsProOrdered} isBookingsLoaded={isProLoaded} />
      ) : (
        <Client
          bookings={bookingsAsClientOrdered}
          isBookingsLoaded={isLoaded(bookingsAsClientOrdered)}
        />
      )}
    </div>
  );
};

export default Dashboard;
