/* eslint-disable */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import ProfileUpdate from "../profileEdit/ProfileUpdate";
import ProfileImageUpdate from "../profileEdit/ProfileImageUpdate";
import SpecialtiesEdit from "../profileEdit/SpecialtiesEdit";
import TopNotificationMessage from "../../ui/TopNotificationMessage";

// import SocialUpdate from '../profileEdit/SocialUpdate'; // PREMIUM FEATURE
// import InterestsUpdate from "../profileEdit/InterestsUpdate"; // OUT-OF-SCOPE
// import PhotosVideos from "./PhotosVideos"; // PREMIUM FEATURE
// import PaypalModal from "../paypal/paypalModal";
// import FAQUpdate from '../profileEdit/FAQUpdate'; // PREMIUM FEATURE
class ProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.getUrlTabIndex = this.getUrlTabIndex.bind(this);
  }

  getUrlTabIndex = (hash) => {
    const initHash = hash;
    if (initHash.startsWith("#")) {
      const removeHash = initHash.replace("#", "");
      return parseInt(removeHash);
    }
    return parseInt(initHash);
  };

  render() {
    const { profile, auth, history } = this.props;
    if (!auth.uid) return <Redirect to="/signin" />;

    return (
      <div className="profile-edit">
        <TopNotificationMessage />
        <div className="container container--top-bottom-padding container--small">
          <div className="row">
            <div className="col">
              <div className="bookings__head">
                <h1 className="text--lg text--uppercase">Profile</h1>
              </div>
            </div>
          </div>

          {/* {profile.isPro && !profile.isProPremium && (
            <div className="row">
              <div className="col">
                <div
                  className="status status--secondary status--success"
                  style={{ width: "100%", marginBottom: "20px" }}
                >
                  <PaypalModal
                    buttonText="Want to become a Pro Premium? Click here to upgrade your account today."
                    buttonClass="link link--light"
                  />
                </div>
              </div>
            </div>
          )} */}

          {/* {profile.isPro && profile.isProPremium && (
            <div className="row">
              <div className="col">
                <div
                  className=""
                  style={{ width: "100%", marginBottom: "20px" }}
                >
                  <p>
                    You're a Pro Premium! Enjoy the benefits of Premium with
                    Video, Galleries, and adding your social accounts!
                  </p>
                </div>
              </div>
            </div>
          )} */}

          <div className="row">
            <div className="col">
              <Tabs defaultIndex={this.getUrlTabIndex(history.location.hash)}>
                <TabList>
                  <Tab>Profile</Tab>
                  <Tab>Image</Tab>
                  {/* <Tab>Interests</Tab> */}
                  {profile.isApproved && <Tab>Specialties</Tab>}
                  {/* {profile.isPro && <Tab>Social</Tab>}
                  {profile.isPro && <Tab>FAQ</Tab>}
                  {profile.isPro && <Tab>Photos/Videos</Tab>} */}
                </TabList>
                <TabPanel>
                  <ProfileUpdate />
                </TabPanel>
                <TabPanel>
                  <ProfileImageUpdate />
                </TabPanel>
                {/* <TabPanel>
                  <InterestsUpdate />
                </TabPanel> */}
                {profile.isApproved && (
                  <TabPanel>
                    <SpecialtiesEdit />
                  </TabPanel>
                )}
                {/* {profile.isPro && (
									<TabPanel>
										<SocialUpdate />
									</TabPanel>
								)}
								{profile.isPro && (
									<TabPanel>
										<FAQUpdate />
									</TabPanel>
								)}
								{profile.isPro && (
									<TabPanel>
										<PhotosVideos />
									</TabPanel>
								)} */}
              </Tabs>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  };
};

export default connect(mapStateToProps)(ProfileEdit);
