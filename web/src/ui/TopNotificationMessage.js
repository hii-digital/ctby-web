import React from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { onboardingAgain } from "../store/actions/authActions";
import Modal from "../components/modal/Modal";

const TopMessage = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { profile } = useSelector(({ firebase }) => firebase, shallowEqual);

  const reSubmit = () => {
    dispatch(onboardingAgain());
    history.push("/onboarding");
  };

  return (
    <>
      {!profile.isPro && (
        <div className="dashboard__callout">
          <div className="container">
            <p>
              <em>
                <Link to="upgrade-pro" className="link link--light">
                  Want to become a Pro? Upgrade your account today.
                </Link>
              </em>
            </p>
          </div>
        </div>
      )}

      {profile.isPro && (
        <>
          {profile.isDeclined && (
            <div className="status status--danger">
              <div className="container ">
                <p>
                  Your account has been declined. Review the Admin notes and
                  resubmit when completed.
                </p>
              </div>

              <div className="buttons buttons--inline">
                {profile.declineMessage && (
                  <Modal
                    buttonStyle="button"
                    buttonText="Review Notes"
                    content={
                      <>
                        <h2>Review Notes</h2>
                        {profile.declineMessage}
                      </>
                    }
                  />
                )}
                <button type="button" className="button" onClick={reSubmit}>
                  Resubmit
                </button>
              </div>
            </div>
          )}
          {profile.onboardingCompleted &&
            !profile.isApproved &&
            !profile.isDeclined && (
              <div className="status status--warning">
                <div className="container">
                  <p>
                    Your profile is currently being reviewed by one our admins.{" "}
                    <Link to="/contact">Contact us</Link> if you have any
                    questions.
                  </p>
                </div>
              </div>
            )}
        </>
      )}
    </>
  );
};

export default TopMessage;
