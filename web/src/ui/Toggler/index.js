import React from "react";
import cx from "classnames";

import "./styles.scss";

const Toggler = ({ items, className, active }) => {
  const activeItem = active || items?.[0]?.value;

  return (
    <div className={cx("toggler-root", className)}>
      {Boolean(items.length) && items.map(({ title, action, value, ...rest }) => {
        const handleClick = (e) => {
          e.stopPropagation();
          e.preventDefault();
          action(e.target.value, e);
        };
        // console.log(items)
        return (
          <button
            type="button"
            className={cx({ active: activeItem === value })}
            onClick={handleClick}
            key={title}
            value={value}
            {...rest}
          >
            {title}
          </button>
        );
      })}
    </div>
  );
};

export default Toggler;
