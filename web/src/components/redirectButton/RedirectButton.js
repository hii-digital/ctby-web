import React from "react";
import { useHistory } from "react-router-dom";
import Button from "../../components/button/Buttons";

export default function RedirectButton() {
  const history = useHistory();

  const routeChange = () => {
    let path = `how-ctby-works-for-pros`;
    history.push("/how-ctby-works-for-pros");
  };

  return (
    <Button title="See all" onClick={routeChange} />
    // <button onClick={routeChange}/>
  );
}
